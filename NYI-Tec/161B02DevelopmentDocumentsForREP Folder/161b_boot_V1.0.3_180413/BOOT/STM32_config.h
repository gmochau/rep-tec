/******************************************************************************
* @ File name --> 
* @ Author    --> 
* @ Version   --> 
* @ Date      -->
* @ Brief     -->

*******************************************************************************                            
* @ Version   -->
* @ Author    --> 
* @ Date      --> 
* @ Revise    -->
*
******************************************************************************/

#ifndef		_STM32_config_h_
#define		_STM32_config_h_

//#define __CFG_FLASH_W25N01__        // 1Gbit
#define __CFG_FLASH_W25Q16__        //16Mbit
//#define __CFG_FLASH_W25Q64__        //64Mbit

#ifdef __CFG_FLASH_W25N01__
    #define FLASH_SEL_NANDFLASH
    
    #define FALSH_PAGESIZE 				0x20000			// 128K
    #define FALSH_ERASE_PAGESIZE 		0x20000			// 128K
    #define FALSH_ADD_END 				0x8000000		// 1Gbit=128MB
    #define FALSH_MAX_SECTORS			FALSH_ADD_END/FALSH_PAGESIZE //最大扇区数
    //-----------------------
    //扇区 0/1/2 软件运行产生的参数
    #define SOFTPARA_ADDROFFSET     	( ( (sizeof(SysRunParameter) - 1) / 128) * 128 + 128 ) //512         								
    #define SOFTWAREPARA_ADDR   		0x0000000    		     				
    #define SOFTWAREPARA_ADDR_END		0x0060000       //0x00000-0x1ffff,0x20000-0x3ffff,0x40000-5ffff

    //扇区 3-4 远程升级
    #define ADDR_UPDATE       			0x0060000       // 128K*2	系统升级
    #define ADDR_UPDATE_END				0x00a0000		// 0xa0000-0x7ffff,0x80000-0x9ffff

    //扇区 5-6 远程升级
    #define ADDR_UPDATE_BACK       		0x00a0000	    // 128K*2	系统升级
    #define ADDR_UPDATE_BACK_END	    0x00e0000		// 0xa0000-0xbffff,0xc0000-0xdffff

    //扇区 7/8 系统设置参数
    #define SAVE_MAINPARA1   			0x00e0000								
    #define SAVE_MAINPARA1_END			0x0100000		// 0xe0000-0xfffff

    #define SAVE_MAINPARA2  		 	0x0100000   	// 备份2048
    #define SAVE_MAINPARA2_END			0x0120000		// 0x100000-0x11ffff

    //扇区 9 DEBUG数据地址
    #define LOG_ADDR  					0x0120000	    // 128k
    #define LOG_ADDR_END				0x0140000		// 0x120000 - 13ffff

    //扇区10 -存储GPS数据
    #define GpsData_ADDR      			0x0140000   	// 0x140000 - 0x800,0000 FALSH_ADD_END GPS数据存储开始的位置
    #define GpsData_ADDR_END			0x8000000	
#endif

#ifdef __CFG_FLASH_W25Q16__
    #define FLASH_SEL_NORFLASH
    //-----------------------
    //扇区 0/1/2 软件运行产生的参数
    #define SOFTPARA_ADDROFFSET     	( ( (sizeof(SysRunParameter) - 1) / 128) * 128 + 128 ) //256        								
    #define SOFTWAREPARA_ADDR   		0x000000    		     				
    #define SOFTWAREPARA_ADDR_END		0x003000       //0x000000-0x000fff,0x001000-0x001fff,0x002000-0x002fff

    //扇区 3-4 远程升级
    #define ADDR_UPDATE       			0x003000       // 128K*2	系统升级
    #define ADDR_UPDATE_END				0x043000	

    //扇区 5-6 远程升级
    #define ADDR_UPDATE_BACK       		0x043000	    // 128K*2	系统升级
    #define ADDR_UPDATE_BACK_END	    0x083000		

    //扇区 7/8 系统设置参数
    #define SAVE_MAINPARA1   			0x083000								
    #define SAVE_MAINPARA1_END			0x084000		// 0x083000-0x083fff

    #define SAVE_MAINPARA2  		 	0x084000   	
    #define SAVE_MAINPARA2_END			0x085000		// 0x084000-0x084fff

    //扇区 9 DEBUG数据地址
    #define LOG_ADDR  					0x085000	    // 
    #define LOG_ADDR_END				0x086000		// 0x085000 - 085fff

    //扇区10 -存储GPS数据
    #define GpsData_ADDR      			0x086000   	    // 0x086000 - 0x200000 FALSH_ADD_END GPS数据存储开始的位置
    #define GpsData_ADDR_END			0x200000	
#endif

#ifdef __CFG_FLASH_W25Q64__
    #define FLASH_SEL_NORFLASH
    //-----------------------
    //扇区 0/1/2 软件运行产生的参数
    #define SOFTPARA_ADDROFFSET     	( ( (sizeof(SysRunParameter) - 1) / 128) * 128 + 128 ) //256        								
    #define SOFTWAREPARA_ADDR   		0x000000    		     				
    #define SOFTWAREPARA_ADDR_END		0x003000       //0x000000-0x000fff,0x001000-0x001fff,0x002000-0x002fff

    //扇区 3-4 远程升级
    #define ADDR_UPDATE       			0x003000       // 128K*2	系统升级
    #define ADDR_UPDATE_END				0x043000	

    //扇区 5-6 远程升级
    #define ADDR_UPDATE_BACK       		0x043000	    // 128K*2	系统升级
    #define ADDR_UPDATE_BACK_END	    0x083000		

    //扇区 7/8 系统设置参数
    #define SAVE_MAINPARA1   			0x083000								
    #define SAVE_MAINPARA1_END			0x084000		// 0x083000-0x083fff

    #define SAVE_MAINPARA2  		 	0x084000   	
    #define SAVE_MAINPARA2_END			0x085000		// 0x084000-0x084fff

    //扇区 9 DEBUG数据地址
    #define LOG_ADDR  					0x085000	    // 
    #define LOG_ADDR_END				0x086000		// 0x085000 - 085fff

    //扇区10 -存储GPS数据
    #define GpsData_ADDR      			0x086000   	    // 0x086000 - 0x800000 FALSH_ADD_END GPS数据存储开始的位置
    #define GpsData_ADDR_END			0x800000	
#endif

#define IAP_PAGE_SIZE			2048								// GD32F105XX
#define IAP_BOOT_ADDR			0x08000000
#define IAP_BOOT_END            0x08002000
#define IAP_DATA_ADDR			0x08002000
#define IAP_APP_ADDR			0x08002800     
#define IAP_VER_ADDR			0x08003000
#define IAP_APP_END             0x08040000

#define FLASH_PAGE_SIZE		    0x00000800							// 2048
#define FLASH_BLOCK_SIZE        0x20000
#define FLASH_APP_ADDR     		ADDR_UPDATE   						// 256K*2	系统升级
#define FLASH_APP_ADDR_END		(FLASH_APP_ADDR + 0x40000)   						// 256K*2	系统升级
#define FLASH_TMP_ADDR     		(FLASH_APP_ADDR_END)   						// 256K*2	系统升级
#define FLASH_TMP_ADDR_END		(FLASH_TMP_ADDR + 0x40000)   						// 256K*2	系统升级

/******************************************************************************/

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef union
{
	u32 r;
	struct st_16w
	{
		u16 l;
		u16 h;
	}w;
	struct st_08b
	{
		u8 l;
		u8 m;
		u8 h;
		u8 u;
	}b;
}un32;


#ifndef c_ret_ok
#define c_ret_ok				0
#endif
#ifndef c_ret_nk
#define c_ret_nk				1
#endif
#ifndef c_ret_next
#define c_ret_next				2
#endif
#ifndef c_ret_exit
#define c_ret_exit				3
#endif
#ifndef c_ret_nk_tmo
#define c_ret_nk_tmo			4
#endif

#ifndef c_systick
#define c_systick				10
#endif

/******************************************************************************
                               外部函数头文件
                   应用到不同的外设头文件请在这里修改即可                        
******************************************************************************/

#include "sys.h"
#include "stm32f10x.h"

#include <string.h>	//内存操作相关函数库
#include <math.h>	//数学运算相关函数库

#include "boot.h"
#include "spiDrive.h"
#include "w25xx.h"

#endif

