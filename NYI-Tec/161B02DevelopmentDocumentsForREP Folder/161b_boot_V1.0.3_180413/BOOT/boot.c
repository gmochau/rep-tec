/*
	boot.c
*/

#include "stm32_config.h"

u32 systick;

u8  firstPacket;
u8  checksumRcc;
u16 receHead;
u16 receTail;
u8  receBuf[132 + 1];
u8  receType;
u8  update_run = 0;
u8  update_printf_cnt = 0;

void LED_Init(void){

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//GPIOB时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	//GPIOB时钟

	GPIO_InitStructure.GPIO_Pin = LED_GPS_RED;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LED_GPS_GOIOX, &GPIO_InitStructure);
	GPS_LED_OFF;


	GPIO_InitStructure.GPIO_Pin = LED_OBD_RED;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LED_OBD_GOIOX, &GPIO_InitStructure);
	OBD_LED_OFF;
	
}

void USART1_Init(uint32_t bound)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	MY_NVIC_Init(3, 3, USART1_IRQn, NVIC_PriorityGroup_0);	//中断分组0，最高优先级
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);	//使能USART1，GPIOA时钟

	USART_DeInit(USART1);  //复位串口1
	
	//USART1_TX   PA.9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure); //初始化PA9
   
	//USART1_RX	  PA.10
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);  //初始化PA10
  
	//USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;	//设置波特率，一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;	//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;	//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;	//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	
	USART_Init(USART1, &USART_InitStructure); //初始化串口
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启中断
	USART_Cmd(USART1, ENABLE);                    //使能串口 

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

void USART1_IRQHandler(void)
{
	//if(USART_GetFlagStatus(USART1, USART_FLAG_ORE) != RESET)	// 进行空读操作，目的是清除ORE位
	//	USART_ReceiveData(USART1);	
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)	// 将接收到的数据写入缓冲 
	{	
		if(receHead >= sizeof(receBuf)) receHead = sizeof(receBuf) - 1;
		receBuf[receHead++] = USART_ReceiveData(USART1);		
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	}
}

void SYSTICK_init(void)
{

	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);   //选择外部时钟 HCLK / 8	

	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
	SysTick->LOAD = ((SystemCoreClock / 8) / 1000);			//1000 * 1毫秒需要的计数值		
	SysTick->VAL = 0x00;           							//清空计数器
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;      			//开始倒数  
}

void SysTick_Handler(void)
{
    static u8 cnt;
	systick++;
    
    cnt++;
    if(cnt >= 100)
    {
        cnt = 0;
        if(update_run == 1)
        {
            if(update_printf_cnt == 0)
                USART1->DR = '\r';
            else if(update_printf_cnt == 1)
                USART1->DR = '\n';
            else
                USART1->DR = '>';
            update_printf_cnt++;
            if(update_printf_cnt >= 60)
                update_printf_cnt = 0;
        }           
    }    
} 

u32 tick_get(void)
{
	return systick;
}

u32 tick_cmp(u32 tmr,u32 tmo)
{
	return (systick - tmr) >= tmo ? c_ret_ok : c_ret_nk;
}


void update_st32(u32 x,u8 *buf)
{
	un32 ux;
	
	ux.r = x;
	buf[0] = ux.b.l;
	buf[1] = ux.b.m;
	buf[2] = ux.b.h;
	buf[3] = ux.b.u;
}

u32 update_ld32(u8 *buf)
{
	un32 ux = {0};
	
	ux.b.l = buf[0];
	ux.b.m = buf[1];
	ux.b.h = buf[2];
	ux.b.u = buf[3];
	
	return ux.r;
}

void writeFlash(u8 *src ,u32 addr,  u16 len)
{
	u16 x;
	
	FLASH_Unlock();
		
	for(x = 0; x < len; x += 4)
	{
        if(addr % IAP_PAGE_SIZE == 0)  
            FLASH_ErasePage(addr);
        
		FLASH_ProgramWord(addr, update_ld32(&src[x]));
		addr += 4;
	}		
	
	FLASH_Lock();
}

void readFlash(u8 *src ,u32 addr, u16 len)
{ 
	u16 x;
	u32 dat;
	
	for(x = 0;x < len; x += 4)
	{
		dat = *(u32 *)addr;
		update_st32(dat, &src[x]);
		addr += 4;
	}	
}

void update_printf(u8 *str)
{
	while(*str)
	{	
		while((USART1->SR & 0x40) == 0) { };			
			USART1->DR = *str;			    		
		str++;
	}
}

void update_printfSum(u32 sum)
{
    u8 buf[9],c;
    un32 ux;
    
    ux.r = sum;
    
    c = ux.b.u >> 4;
    buf[0] = (c > 9) ? 'A' + c - 10 : '0' + c;
    c = ux.b.u & 0x0f;
    buf[1] = (c > 9) ? 'A' + c - 10 : '0' + c;

    c = ux.b.h >> 4;
    buf[2] = (c > 9) ? 'A' + c - 10 : '0' + c;
    c = ux.b.h & 0x0f;
    buf[3] = (c > 9) ? 'A' + c - 10 : '0' + c;

    c = ux.b.m >> 4;
    buf[4] = (c > 9) ? 'A' + c - 10 : '0' + c;
    c = ux.b.m & 0x0f;
    buf[5] = (c > 9) ? 'A' + c - 10 : '0' + c;

    c = ux.b.l >> 4;
    buf[6] = (c > 9) ? 'A' + c - 10 : '0' + c;
    c = ux.b.l & 0x0f;
    buf[7] = (c > 9) ? 'A' + c - 10 : '0' + c;
    
    buf[8] = 0;
    
    update_printf(buf);
}

void update_getFileLenSumVer(u32 *fileLen,u32 *fileSum,u32 addr)
{
    un32 ux = {0};
    
    spiFlash_readAddrSet(addr);
    spiFlash_read(NULL,0);

    ux.b.l = w25n.readBuf[ 3];
    ux.b.m = w25n.readBuf[ 2];
    ux.b.h = w25n.readBuf[ 1];
    ux.b.u = w25n.readBuf[ 0];
    *fileLen = ux.r + 8;

    ux.b.l = w25n.readBuf[ 7];
    ux.b.m = w25n.readBuf[ 6];
    ux.b.h = w25n.readBuf[ 5];
    ux.b.u = w25n.readBuf[ 4];
    *fileSum = ux.r;

    spiFlash_read(NULL,0);      // 版本
}

u32 update_getCheckSum(u32 startAddr,u32 fileLen)
{
	u32 readLen, len, sum, x;
	
	spiFlash_readAddrSet(startAddr);
	for(sum = len = 0; len < fileLen; )
	{		
        spiFlash_read(NULL,0);
		readLen = ((fileLen - len) >= sizeof(w25n.readBuf))  ? sizeof(w25n.readBuf) : (fileLen - len);
        if(len == 0)
        {            
            for(x = 8; x < readLen; x ++)
                sum += w25n.readBuf[x];		
        }
        else
        {
            for(x = 0; x < readLen; x ++)
                sum += w25n.readBuf[x];		
        }
		len      += readLen;		
	}
	return sum;
}

void update_sendCmd(u8 cmd)	
{
	receTail = receHead = 0;	
	while((USART1->SR & 0x40) == 0) { };			
		USART1->DR = cmd;			    		
}

u8  update_receNewData(u16 *len)
{
	u8 ret;
	
	ret = c_ret_nk;
	if(receTail != receHead)  
	{
		ret = c_ret_ok;
		receTail = receHead;
	}
	*len = receHead;
	
	return ret;
}

//	soh sn ~sn d0...d127 sum
u8  update_xmodemComm(u8 sn,u8 cmd)
{
    u32 tmr, cnt;
	u16 len, sum, x, i;

	update_sendCmd(cmd);
	
	for(x = cnt = 0; cnt < 1000; cnt++)
	{    
		x++;
		tmr = tick_get();
		while(tick_cmp(tmr,10) == c_ret_nk)
		{	
			if(update_receNewData(&len) == c_ret_ok) x = 0;    
			if(len >= 132) break;
		}	
		if((receType == 0)&&(len == 0)&&(x >= 110)) 		// 试探包
			return 	c_xmodemRet_null;						// 无应答
		
        if(firstPacket == 1)
        {
            if((len != 0)&&(x >= 110)) break;
        }            
		else if(    ((len != 0)&&(x >= 110))				// 字节间隔超时1秒 
			||	    ((checksumRcc==1)&&(len >= 132))
			||	    ((checksumRcc==0)&&(len >= 133)) )
			break;
	}

	if(receBuf[0] == c_xmodem_eot)     						// 全部传输完成
		return c_xmodemRet_done;
    
	if(cnt >= 1000) 										// 包超时间隔为10秒
		return c_xmodemRet_tmo;	

	if(len >= 132)
	{	
		if(		(receBuf[0] == c_xmodem_soh) 				// 信息包头
			&&	(receBuf[1] == (0xff - receBuf[2])))		// 信息包序号
		{
            if(firstPacket == 1)
            {                
                if(len >= 133)
                {
                    for(sum = 0, x = 3; x < 131; x++) 
                    {
                        sum = sum ^ (receBuf[x] << 8);
                        for( i = 0; i < 8; i++)
                        {
                            if(sum & 0x8000) sum = (sum << 1) ^ 0x1021;
                            else sum <<= 1;
                        }                        
                    }
                    if(sum == (receBuf[131] << 8) + receBuf[132])
                    {
                        firstPacket = 0;
                        checksumRcc = 0;
                        if(receBuf[1] == sn)
                            return c_xmodemRet_next;				    // 包接收正常    
                        else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                            return c_xmodemRet_last;			
                        else
                            return c_xmodemRet_err;					    // 包序号错误退出				
                    }
                }

                for(sum = 0, x = 3; x < 131; x++) 
                    sum += receBuf[x];
                if((sum&0xff) == receBuf[x])            
                {	
                    firstPacket = 0;
                    checksumRcc = 1;
                    if(receBuf[1] == sn)
                        return c_xmodemRet_next;				    // 包接收正常    
                    else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                        return c_xmodemRet_last;			
                    else
                        return c_xmodemRet_err;					    // 包序号错误退出				
                }                
            }
            else if(checksumRcc == 1)
            {    
                for(sum = 0, x = 3; x < 131; x++) 
                    sum += receBuf[x];
                if((sum&0xff) == receBuf[x])            
                {	
                    if(receBuf[1] == sn)
                        return c_xmodemRet_next;				    // 包接收正常    
                    else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                        return c_xmodemRet_last;			
                    else
                        return c_xmodemRet_err;					    // 包序号错误退出				
                }
            }
            else
            {
                for(sum = 0, x = 3; x < 131; x++) 
                {
                    sum = sum ^ (receBuf[x] << 8);
                    for( i = 0; i < 8; i++)
                    {
                        if(sum & 0x8000) sum = (sum << 1) ^ 0x1021;
                        else sum <<= 1;
                    }                        
                }
                if(sum == (receBuf[131] << 8) + receBuf[132])
                {
                    if(receBuf[1] == sn)
                        return c_xmodemRet_next;				    // 包接收正常    
                    else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                        return c_xmodemRet_last;			
                    else
                        return c_xmodemRet_err;					    // 包序号错误退出				
                }
            }
		}
	}	
	return c_xmodemRet_last;
}

u8 update_ftpXmodem(void)
{
    u32 sum, fileSum, len, fileLen;
  	u8 ret, sn, errCnt, lastCnt, x, y;
    
    firstPacket = 1;
    receType = 0;
    sn = 1;													    // 第一个信息包的序号为 1		
	for(errCnt =0; errCnt < 3; errCnt++)
	{
		//ret = update_xmodemComm(sn, c_xmodem_nak);		        // 启动传输
        ret = update_xmodemComm(sn, 'C');
		if(ret == c_xmodemRet_next) break;
	}	
    if(errCnt >= 3) return c_ret_nk;
	
    receType = 1;
    spiFlash_writeAddrSet(FLASH_TMP_ADDR);	

	len = errCnt = lastCnt = 0;
    while(1)
    {               
		switch(ret)
		{		
			case c_xmodemRet_next:
                spiFlash_write(&receBuf[3],128,0);
				if(w25n.writeAddr >= FLASH_TMP_ADDR + FLASH_BLOCK_SIZE * 2)
				{
					update_sendCmd(c_xmodem_can);
					return c_ret_nk;
				}					

				errCnt = lastCnt = 0;
				sn++;    
				len++;
				ret = update_xmodemComm(sn,c_xmodem_ack);       // 请求下一包    
			break;
			
			case c_xmodemRet_last:
                sum = tick_get();
                while(tick_cmp(sum,50) == c_ret_nk) {}                
              
				errCnt = 0;
                lastCnt++;
                if(lastCnt < 5)
                    ret = update_xmodemComm(sn,c_xmodem_nak);   // 请求重传                    
                else
                {
                    update_sendCmd(c_xmodem_can);
                    return c_ret_nk;
                }
			break;

			case c_xmodemRet_err:      			
			case c_xmodemRet_tmo:      
				errCnt++;
				if(errCnt >= 10) 
				{	
					update_sendCmd(c_xmodem_can);
					return c_ret_nk;
				}	
				ret = update_xmodemComm(sn,c_xmodem_nak);       // 请求重传
			break;
				
			case c_xmodemRet_done:   
				update_sendCmd(c_xmodem_ack);				
                for(y = x = 0; x < 128; x++)
				{
					if(receBuf[130 - x] != c_xmodem_eof) break;
					y++;
				}
                len = len * 128 - y;
                spiFlash_write(NULL, 0, 1);
                
                update_getFileLenSumVer(&fileLen, &fileSum, FLASH_TMP_ADDR);
                if(fileLen != len) 
                {
                    update_printf("len error\r\n");
                    return c_ret_nk;
                }
                
                for(x = 0; x < sizeof(versionApp); x++)	
                { 
                    if(versionApp[x] != w25n.readBuf[8 + x]) 
                    {
                        update_printf("version error\r\n");
                        return c_ret_nk;	
                    }
                    if(versionApp[x] == '.') break;
                }				
                
                sum = update_getCheckSum(FLASH_TMP_ADDR, fileLen);
                if(fileSum != sum)
                {                    
                    update_printfSum(sum);
                    return c_ret_nk;
                }               
                                
                spiFlash_readAddrSet(FLASH_TMP_ADDR);
                spiFlash_writeAddrSet(FLASH_APP_ADDR); 
                for(len = 0; len < fileLen; )
                {
                    spiFlash_read(NULL,0);
                    spiFlash_write(w25n.readBuf, sizeof(w25n.readBuf), 0);
                    len       += sizeof(w25n.readBuf);				
                }

                sum = update_getCheckSum(FLASH_APP_ADDR, fileLen);
                if(fileSum != sum)
                {                    
                    update_printfSum(sum);
                    return c_ret_nk;
                }                         

                readFlash(w25n.readBuf, IAP_DATA_ADDR, sizeof(w25n.readBuf));
                update_st32(0x88888888,&w25n.readBuf[0]);
                writeFlash(w25n.readBuf,IAP_DATA_ADDR, sizeof(w25n.readBuf));    // 存入升级标志
                
			return c_ret_ok;
			
			default:return c_ret_nk;
		}	
    } 
}

u8 update_programApp(void)
{
	u32 writeAddr, readAddr, len, fileLen, sum, fileSum, x;
	u8  fapp_ok, cnt, *px;

    if(*(u32*)IAP_DATA_ADDR != 0x88888888)                      // 无升级标志
    {
        px = (u8*)IAP_VER_ADDR;
        for(x = 0; x < sizeof(versionApp); x++)
        {
            if(versionApp[x] != px[x]) break;
            if(versionApp[x] == '.')    					    // 只比较硬件的主版本和项目名称
                return c_ret_ok;                                // 不需要升级,运行app
        }
        return c_ret_nk;                                        // 版本不对
    }
    
	fapp_ok = 0;
    update_getFileLenSumVer(&fileLen, &fileSum, FLASH_APP_ADDR);
    if(fileLen != 0xffffffff)
    {
        for(x = 0; x < sizeof(versionApp); x++)	
        { 
            if(versionApp[x] != w25n.readBuf[8 + x]) break;
            if(versionApp[x] == '.') 
            {
                sum = update_getCheckSum(FLASH_APP_ADDR, fileLen);
                if(fileSum == sum)
                    fapp_ok = 1;
                break;
            }
        }				
    }

    if(fapp_ok == 0)
        return c_ret_nk;                                        // 长度、版本、校验不对
    
    update_printf_cnt = 0;
    update_run = 1;
    
    for(cnt = 0; cnt < 3; cnt++)
    {
        writeAddr = IAP_APP_ADDR;
        spiFlash_readAddrSet(FLASH_APP_ADDR);
        
        for(len = 0; len < fileLen ; )							
        {
            spiFlash_read(NULL,0);
            if(len == 0)
            {
                writeFlash(w25n.readBuf + 8, writeAddr, sizeof(w25n.readBuf) - 8);
                len       += sizeof(w25n.readBuf);
                writeAddr += sizeof(w25n.readBuf) - 8;
            }
            else
            {
                writeFlash(w25n.readBuf, writeAddr, sizeof(w25n.readBuf));
                len       += sizeof(w25n.readBuf);
                writeAddr += sizeof(w25n.readBuf);
            }
        }	

		readAddr     =   IAP_APP_ADDR;
		spiFlash_readAddrSet(FLASH_APP_ADDR);
		for(len = 0; len < fileLen; )
		{
			spiFlash_read(NULL,0);            			
            if(len == 0)
            {
                readFlash(w25n.writeBuf, readAddr, sizeof(w25n.readBuf) - 8);	
                for(x = 8; x < sizeof(w25n.readBuf); x++)
                {
                    if(w25n.writeBuf[x - 8] != w25n.readBuf[x])  break;
                }            
                if(x < sizeof(w25n.readBuf)) break;
                readAddr       += sizeof(w25n.readBuf) - 8;
                len            += sizeof(w25n.readBuf);
            }
            else
            {
                readFlash(w25n.writeBuf, readAddr, sizeof(w25n.readBuf));	
                for(x = 0; x < sizeof(w25n.readBuf); x++)
                {
                    if(w25n.writeBuf[x] != w25n.readBuf[x])  break;
                }            
                if(x < sizeof(w25n.readBuf)) break;
                readAddr       += sizeof(w25n.readBuf);
                len            += sizeof(w25n.readBuf);
            }
		}         
        if(len >= fileLen) 
        {
            break;
        }
    }
    
    readFlash(w25n.readBuf, IAP_DATA_ADDR, sizeof(w25n.readBuf));
    update_st32(0x00000000,&w25n.readBuf[0]);
    writeFlash(w25n.readBuf,IAP_DATA_ADDR, sizeof(w25n.readBuf));    // 存入升过级标志
    
    update_run = 0;
    if(len >= fileLen) 
    {
        update_printf((u8*)"\r\n\r\n**** update succ ****\r\n\r\n");
        return c_ret_ok;
    }    	
	return c_ret_nk;							    
}

void update_gotoApp(void)
{
	u32 sp, addr;
	void (*gotoFunc)(void);
	NVIC_InitTypeDef NVIC_InitStructure;

	sp   = *(u32*)IAP_APP_ADDR;
	addr = *(u32*)(IAP_APP_ADDR + 4);

	if(		/*((sp & 0x2fff0000) == 0x20000000)	// 64K ram		
		&&*/	(addr >= IAP_APP_ADDR)  			// 32K flash	0x08008000
		&&	(addr <= IAP_APP_END) )				//256K flash
	{		
		USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);		
		USART_Cmd(USART1, DISABLE);
		
		SPI_Cmd(sFLASH_SPI, DISABLE);
		
		SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
		SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;				
		SysTick->VAL = 0x00;									

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, DISABLE);	
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, DISABLE);	
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, DISABLE);	
		
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, DISABLE);
		
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,DISABLE);		

		NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
		NVIC_Init(&NVIC_InitStructure);
		
        //__ASM("CPSID I");
		__set_MSP(sp);
		gotoFunc = (void (*)(void))addr;
		(*gotoFunc)();
	}
}

//const u8 versionApp[IAP_PAGE_SIZE] __attribute__((at(IAP_DATA_ADDR))) = 
//        "\xff\xff\xff\xff"                                          // app长度                  4字节
//        "\x30\x30\x30\x30"                                          // app校验和                 4字节
//        "\xff\xff\xff\xff"                                          // app升级标志               4字节    0xffffffff:不需升级,0x88888888:执行升级，0x00000000：执行过升级
//        "161B H1.0V1.0.0"__DATE__" "__TIME__" Nyitech Co.,Ltd.";     // app版本信息               64字节

const u8 versionApp[32] = "161B H1.0";

int main(void)
{
	SYSTICK_init();
	LED_Init();
	USART1_Init(115200);
	SPI_FLASH_Init();
	flash_init();
    
	update_ftpXmodem();
	
	update_printf((u8*)"\r\n************************************************************\r\n");
	update_printf((u8*)"* 161B_BOOT H1.0 V1.0.3 "__DATE__" "__TIME__" Nyitech co.,Ltd.*\r\n");
	update_printf((u8*)"************************************************************\r\n\r\n");
	
    while(1)
	{
        if(update_programApp() == c_ret_ok)
        {            
            update_gotoApp();
            update_printf((u8*)"**** bootJump fail ****\r\n");
        }
		while(1)					
		{				
			GPS_LED_ON;
			OBD_LED_ON;
			STM_RESET_DOG();
		}		
	}
}
