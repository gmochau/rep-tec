/*
	boot.h
*/

#ifndef __boot_h__
#define __boot_h__

#include "stm32_config.h"

#define LED_GPS_GOIOX 			GPIOA
#define LED_GPS_RED				GPIO_Pin_8		//
#define GPS_LED_ON				GPIO_WriteBit(LED_GPS_GOIOX, LED_GPS_RED, Bit_SET)
#define GPS_LED_OFF				GPIO_WriteBit(LED_GPS_GOIOX, LED_GPS_RED, Bit_RESET)

#define LED_OBD_GOIOX 			GPIOB
#define LED_OBD_RED				GPIO_Pin_7		//
#define OBD_LED_ON				GPIO_WriteBit(LED_OBD_GOIOX, LED_OBD_RED, Bit_SET)
#define OBD_LED_OFF				GPIO_WriteBit(LED_OBD_GOIOX, LED_OBD_RED, Bit_RESET)


#define c_xmodem_soh    0x01        //数据头
#define c_xmodem_stx    0x02        //1k_xmodem协议数据头
#define c_xmodem_eot    0x04        //发送结束
#define c_xmodem_ack    0x06        //认可响应
#define c_xmodem_nak    0x15        //不认可响应
#define c_xmodem_can    0x18        //撤销传送
#define c_xmodem_eof    0x1a        //最后一包数据填充

enum
{
	c_xmodemRet_null,				// 无应答
	c_xmodemRet_done,				// 传输完成 
	c_xmodemRet_next,				// 传输下一包
	c_xmodemRet_last,				// 收到上一包	
	c_xmodemRet_tmo,				// 超时
	c_xmodemRet_err					// 包解析错误
};	

extern const u8 versionApp[32];
extern void flash_init(void);
extern void setReadAddrRange(u32 addr);

extern void setWriteAddrRange(u32 addr);

extern u32 tick_get(void);

extern u32 tick_cmp(u32 tmr,u32 tmo);

#endif
