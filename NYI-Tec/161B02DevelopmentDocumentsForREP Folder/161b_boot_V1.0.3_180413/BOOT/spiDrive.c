
#include "stm32_config.h"			//

#ifdef FLASH_SEL_NANDFLASH
struct st_w25n w25n;

/*******************************************************************************
* Function Name  : SPI_FLASH_Init
* Description    : Initializes the peripherals used by the SPI FLASH driver.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;

	/* Configure SPI2 pins: SCK, MISO and MOSI -*/
	GPIO_InitStructure.GPIO_Pin = FLASH_SPI_SCK  | FLASH_SPI_SDI;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin =  FLASH_SPI_SDO ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

	/* Configure PA.4 as Output push-pull, used as Flash Chip select */
	GPIO_InitStructure.GPIO_Pin = SPI_CS;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

	SPI_FLASH_CS_HIGH();	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);

    // SPI2 时钟源72M/2=36M(不能超过54M),SPI硬件支持上至18M
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master; //设置为主SPI
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;//SPI_BaudRatePrescaler_128;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(sFLASH_SPI, &SPI_InitStructure);
	SPI_Cmd(sFLASH_SPI, ENABLE);

		//GPIO_PinRemapConfig(GPIO_Remap_SPI1,ENABLE);
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte 
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
u8 SPI_FLASH_SendByte(u8 byte)  //  通过SPI硬件发送/接收一个字节
{		
	// Loop while DR register in not emplty 
	while (SPI_I2S_GetFlagStatus(sFLASH_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(sFLASH_SPI, byte);																				
	while (SPI_I2S_GetFlagStatus(sFLASH_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	return SPI_I2S_ReceiveData(sFLASH_SPI);
}

/*******************************************************************************
* Function Name  : SPI_Flash_Get_Byte
* Description    : Get one byte from spi flash
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
u8 SPI_FLASH_Get_Byte(void)
{
	return (SPI_FLASH_SendByte(Dummy_Byte));
}


/*******************************************************************************
* Function Name  : SPI_FLASH_Read_Status
* Description    : Read Spi flash tatus register
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
u8 SPI_FLASH_Read_Status(u8 RegisterAdd)
{
	u8 byte = 0;
	
	SPI_FLASH_CS_LOW();				//enable device
	SPI_FLASH_SendByte(0x05);		//send Read StatusRegister command
	SPI_FLASH_SendByte(RegisterAdd);		//send Read StatusRegister command
	byte = SPI_FLASH_Get_Byte();	//receive byte
	SPI_FLASH_CS_HIGH();			//disable device
	
	return byte;
}

void spi_waitBusyClr(void)
{
    u8 status;
    u32 x;

    for(x = 0; x < 0x8000; x++)
    {
        STM_RESET_DOG();
        status = SPI_FLASH_Read_Status(c_statusAddr_status);
        if((status & c_statusBit_busy) == 0)
            break;
    }
}

void spi_waitWelSet(void)
{
    u8 status;
    u32 x;

    for(x = 0; x < 0x8000; x++)
    {
        STM_RESET_DOG();
        status = SPI_FLASH_Read_Status(c_statusAddr_status);
        if(     ((status & c_statusBit_wel ) != 0) 
            &&  ((status & c_statusBit_busy) == 0) )
            break;
    }
}

void spi_waitWelClr(void)
{
    u8 status;
    u32 x;

    for(x = 0; x < 0x8000; x++)
    {
        STM_RESET_DOG();
        status = SPI_FLASH_Read_Status(c_statusAddr_status);
        if(     ((status & c_statusBit_wel ) == 0) 
            &&  ((status & c_statusBit_busy) == 0) )
            break;
    }
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteDisable
* Description    : Disable the write access to the FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WriteDisable(void)
{    
	SPI_FLASH_CS_LOW();				//enable device
	SPI_FLASH_SendByte(WRDISA);		//send WriteDisable command
	SPI_FLASH_CS_HIGH();			//disable device
    spi_waitWelClr();    
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteEnable
* Description    : Enables the write access to the FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WriteEnable(void)
{
  	SPI_FLASH_CS_LOW();           //Select the FLASH: Chip Select low   
  	SPI_FLASH_SendByte(WRENA);    //Send "Write Enable" instruction     	
  	SPI_FLASH_CS_HIGH();          //Deselect the FLASH: Chip Select high 
    spi_waitWelSet();    
}

/*******************************************************************************
* Function Name  : SPI_FLASH_Read_Status
* Description    : Read Spi flash tatus register
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
u8 SPI_FLASH_Write_Status(u8 RegisterAdd, u8 data)
{
	u8 byte = 0;
	
	SPI_FLASH_CS_LOW();				//enable device
	SPI_FLASH_SendByte(0x01);	    //send Write StatusRegister command 
	SPI_FLASH_SendByte(RegisterAdd);	    //send Write StatusRegister command 
	SPI_FLASH_SendByte(data);
	SPI_FLASH_CS_HIGH();			//disable device
	
	return byte;
}

/*******************************************************************************
* Function Name  : SPI_Flash_WaitForWriteEnd
* Description    : Polls the status of the Write In Progress (WIP) flag in the  
*                  FLASH's status  register  and  loop  until write  opertaion
*                  has completed.  
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WaitForWriteEnd(void)
{
    spi_waitWelClr();
}

/*******************************************************************************
* Function Name  : SPI_FLASh_SectorErase
* Description    : Erases the specified FLASH sector.
* Input          : SectorAddr: address of the sector to erase.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_SectorErase(u32 SectorAddr)
{

	u16 BlockAdd = 0;

	BlockAdd =  SectorAddr / sFLASH_SPI_PAGESIZE;      // 页地址
	#if FLASH_DEBUG
	printf("SectorErase =% d\r\n", SectorAddr);
	#endif

	spi_waitBusyClr();    
	SPI_FLASH_WriteEnable();                        //Send write enable instruction
	
    SPI_FLASH_CS_LOW();                             //Select the FLASH: Chip Select low
	SPI_FLASH_SendByte(SE128K);                       //Send Sector Erase instruction
	SPI_FLASH_SendByte(0x00);		//8个空时钟																									//Send SectorAddr medium nibble address byte
	SPI_FLASH_SendByte((BlockAdd & 0xFF00) >> 8);
	SPI_FLASH_SendByte(BlockAdd & 0xFF);										
	SPI_FLASH_CS_HIGH();	                        //Deselect the FLASH: Chip Select high

	SPI_FLASH_WaitForWriteEnd();                    //Wait the end of Flash writing

    SPI_FLASH_WriteDisable();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BlockErase
* Description    : Erases the specified FLASH block.
* Input          : BlockAddr: address of the block to erase.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BlockErase(u32 BlockAddr)
{
    spi_waitBusyClr();
    
  	SPI_FLASH_WriteEnable();                       //Send write enable instruction 

  	/******************** Block Erase **********************/ 
  	SPI_FLASH_CS_LOW();                            //Select the FLASH: Chip Select low
  	
  	SPI_FLASH_SendByte(SE4K);                     //Send Block Erase instruction
 	                                               //Send BlockAddr high nibble address byte
	SPI_FLASH_SendByte(0x00);		
  	                                               //Send BlockAddr medium nibble address byte
  	SPI_FLASH_SendByte((BlockAddr & 0xFF00) >> 8);
  	                                               //Send BlockAddr low nibble address byte
  	SPI_FLASH_SendByte(BlockAddr & 0xFF);

	SPI_FLASH_SendByte(0x00);		
  	                                                                 
  	SPI_FLASH_CS_HIGH();                           //Deselect the FLASH: Chip Select high
						    
  	SPI_FLASH_WaitForWriteEnd();                   //Wait the end of Flash writing/    

    SPI_FLASH_WriteDisable();

}

/*******************************************************************************
* Function Name  : SPI_FLASh_BulkErase
* Description    : Erases the entire FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BulkErase(void)
{	
    spi_waitBusyClr();
    
  	SPI_FLASH_WriteEnable();                   //Send write enable instruction

  	/************************** Bulk Erase ***************************/   	
  	SPI_FLASH_CS_LOW();                        //Select the FLASH: Chip Select low
  	
  	SPI_FLASH_SendByte(CHIP_ERASE);            //Send Bulk Erase instruction 
  	
  	SPI_FLASH_CS_HIGH();                       //Deselect the FLASH: Chip Select high 
        
  	SPI_FLASH_WaitForWriteEnd();               //Wait the end of Flash writing 

    SPI_FLASH_WriteDisable();
    
}


/*******************************************************************************
* Function Name  : SPI_FLASH_Read
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read 
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_Read_Page(u32 PageAdd)
{
    spi_waitBusyClr();
    
	SPI_FLASH_CS_LOW();         						//Select the FLASH: Chip Select low
	SPI_FLASH_SendByte(READ_PAGE); 				 //Send "Read from Memory " instruction
	SPI_FLASH_SendByte(0x00);		
	SPI_FLASH_SendByte((PageAdd& 0xFF00) >> 8);
	SPI_FLASH_SendByte(PageAdd & 0xFF);																											  												
    SPI_FLASH_CS_HIGH();    							 //Deselect the FLASH: Chip Select high
    
}

/*******************************************************************************
* Function Name  : SPI_FLASH_Read_data
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read 
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_Read_Data(u8* pBuffer, u32 ReadcolumnAdd, u16 NumByteToRead)
{
    spi_waitBusyClr();
    
	SPI_FLASH_CS_LOW();        							 //Select the FLASH: Chip Select low
	SPI_FLASH_SendByte(READ);  							//Send "Read from Memory " instruction
	SPI_FLASH_SendByte((ReadcolumnAdd& 0xFF00)>>8);
	SPI_FLASH_SendByte(ReadcolumnAdd & 0xFF);
	SPI_FLASH_SendByte(0x00);			
	while(NumByteToRead--)      							//while there is data to be read
	{                                   
		*pBuffer = SPI_FLASH_Get_Byte();  				//Read a byte from the FLASH
		pBuffer++;           								//Point to the next location where the byte read will be saved
	}
	SPI_FLASH_CS_HIGH();     							//Deselect the FLASH: Chip Select high
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteData
* Description    : Writes more than one byte to the FLASH with a single WRITES
*                  cycle(Page WRITE sequence). The number of byte hane no limit
* Input          : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/

void SPI_FLASH_WriteData(u8 * pBuffer, u32  WriteAddr, u16 NumByteToWrite)
{
    spi_waitBusyClr();    
  	SPI_FLASH_WriteEnable();               //Enable the write access to the FLASH
    
  	SPI_FLASH_CS_LOW();                    //Select the FLASH: Chip Select low
  	SPI_FLASH_SendByte(WRITES);            //Send "Write to Memory " instruction
	SPI_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
  	SPI_FLASH_SendByte(WriteAddr & 0xFF);
  	while (NumByteToWrite--)              //while there is data to be written on the FLASH 
  	{  
   		SPI_FLASH_SendByte(*pBuffer);    //Send the current byte  
   		pBuffer++;                       //Point on the next byte to be written
  	}    
  	SPI_FLASH_CS_HIGH();                 //Deselect the FLASH: Chip Select high

	SPI_FLASH_WaitForWriteEnd();         //Wait the end of Flash writing
    SPI_FLASH_WriteDisable();
}

void SPI_FLASH_WritePage(u32 PageAddr)//u32 WritePageAddr)
{    
    spi_waitBusyClr();    
  	SPI_FLASH_WriteEnable();               //Enable the write access to the FLASH

	SPI_FLASH_CS_LOW();                    //Select the FLASH: Chip Select low
	SPI_FLASH_SendByte(WRD_PAGE); 	
	SPI_FLASH_SendByte(0x00);		
	SPI_FLASH_SendByte((PageAddr& 0xFF00) >> 8);
	SPI_FLASH_SendByte(PageAddr & 0xFF);											
	SPI_FLASH_CS_HIGH();                 //Deselect the FLASH: Chip Select high
    
	SPI_FLASH_WaitForWriteEnd();         //Wait the end of Flash writing
    SPI_FLASH_WriteDisable();
    
}



void SPI_FLASH_WriteEraseData(u32 WriteAddr, u16 NumByteToWrite)
{
    spi_waitBusyClr();

  	SPI_FLASH_WriteEnable();               //Enable the write access to the FLASH

  	SPI_FLASH_CS_LOW();                    //Select the FLASH: Chip Select low
  	
  	SPI_FLASH_SendByte(WRITES);            //Send "Write to Memory " instruction
                                                        //Send WriteAddr medium nibble address byte to write to
	SPI_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
                                                       //Send WriteAddr low nibble address byte to write to
  	SPI_FLASH_SendByte(WriteAddr & 0xFF);

  	while (NumByteToWrite--)              //while there is data to be written on the FLASH 
  	{  
    		SPI_FLASH_SendByte(0xFF);    //Send the current byte  
  	}
  	SPI_FLASH_CS_HIGH();                 //Deselect the FLASH: Chip Select high
  
  	SPI_FLASH_WaitForWriteEnd();         //Wait the end of Flash writing

    SPI_FLASH_WriteDisable();

}

/*******************************************************************************
* Function Name  : SPI_FLASH_BytesWrite
* Description    : Writes more than one byte to the FLASH with a single WRITES
*                  cycle(Page WRITE sequence). The number of byte hane no limit
* Input          : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Output         : None
* Return         : None
* 写入数据不能超过一页
*******************************************************************************/

void SPI_FLASH_SetBadMark(u32  WriteAddr)
{
    spi_waitBusyClr();    
  	SPI_FLASH_WriteEnable();               //Enable the write access to the FLASH
    
  	SPI_FLASH_CS_LOW();                    //Select the FLASH: Chip Select low
  	SPI_FLASH_SendByte(0x84);                // 内部缓冲区未用部分保持不变
	SPI_FLASH_SendByte((2050 & 0xFF00) >> 8);
  	SPI_FLASH_SendByte(2050 & 0xFF);
	SPI_FLASH_SendByte(0x55);    //Send the current byte  
	SPI_FLASH_SendByte(0x55);    //Send the current byte  
  	SPI_FLASH_CS_HIGH();                 //Deselect the FLASH: Chip Select high

	SPI_FLASH_WaitForWriteEnd();         //Wait the end of Flash writing
    SPI_FLASH_WriteDisable();
    
    SPI_FLASH_WritePage(WriteAddr);

}

void SPI_FLASH_ClrBadMark(u32  WriteAddr)
{
    spi_waitBusyClr();    
  	SPI_FLASH_WriteEnable();               //Enable the write access to the FLASH
    
  	SPI_FLASH_CS_LOW();                    //Select the FLASH: Chip Select low
  	SPI_FLASH_SendByte(0x84);                // 内部缓冲区未用部分保持不变
	SPI_FLASH_SendByte((2050 & 0xFF00) >> 8);
  	SPI_FLASH_SendByte(2050 & 0xFF);
	SPI_FLASH_SendByte(0xff);    //Send the current byte  
	SPI_FLASH_SendByte(0xff);    //Send the current byte  
  	SPI_FLASH_CS_HIGH();                 //Deselect the FLASH: Chip Select high

	SPI_FLASH_WaitForWriteEnd();         //Wait the end of Flash writing
    SPI_FLASH_WriteDisable();
    
    SPI_FLASH_WritePage(WriteAddr);
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BytesWrite
* Description    : Writes more than one byte to the FLASH with a single WRITES
*                  cycle(Page WRITE sequence). The number of byte hane no limit
* Input          : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BytesWrite(u8 *pBuffer, u32 WriteAddr,u16 NumByteToWrite)
{

	u16 NumOfPage = 0, NumOfSingle = 0, columnAdd = 0,StartOfPage = 0;

	StartOfPage =  WriteAddr / sFLASH_SPI_PAGESIZE;      		//起始页地址	
	columnAdd = WriteAddr % sFLASH_SPI_PAGESIZE;      		//列地址

	NumOfPage =  NumByteToWrite / sFLASH_SPI_PAGESIZE;      //所需页数
	NumOfSingle = NumByteToWrite % sFLASH_SPI_PAGESIZE;   //写入所需最后一页的字节数

	if(NumOfPage == 0)
	{									//
		SPI_FLASH_WriteData(pBuffer, columnAdd, NumByteToWrite);
		SPI_FLASH_WritePage(StartOfPage);
	}
	else
	{
		while (NumOfPage--)
		{
			SPI_FLASH_WriteData(pBuffer, 0x0000, sFLASH_SPI_PAGESIZE);
			SPI_FLASH_WritePage(StartOfPage);
			pBuffer += sFLASH_SPI_PAGESIZE;		
			StartOfPage++;
			STM_RESET_DOG();
		}
		if(NumOfSingle != 0)
		{
			SPI_FLASH_WriteData(pBuffer, 0x0000, NumOfSingle);
			SPI_FLASH_WritePage(StartOfPage);
			STM_RESET_DOG();
		}
	}

}

/*******************************************************************************
* Function Name  : SPI_FLASH_Read
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read 
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_Read(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead)
{

	u16 NumOfPage = 0, NumOfSingle = 0, columnAdd = 0,StartOfPage = 0;

	STM_RESET_DOG();

	StartOfPage =  ReadAddr / sFLASH_SPI_PAGESIZE;      		//起始页地址	0x800
	columnAdd = ReadAddr % sFLASH_SPI_PAGESIZE;      		//列地址

	NumOfPage =  NumByteToRead / sFLASH_SPI_PAGESIZE;      //所需页数
	NumOfSingle = NumByteToRead % sFLASH_SPI_PAGESIZE;   //写入所需最后一页的字节数

	if(NumOfPage == 0)
	{
		SPI_FLASH_Read_Page(StartOfPage);
		SPI_FLASH_Read_Data(pBuffer, columnAdd, NumByteToRead);

	}
	else
	{
		while (NumOfPage--)
		{
			SPI_FLASH_Read_Page(StartOfPage);
			SPI_FLASH_Read_Data(pBuffer, 0x0000, NumByteToRead);
			pBuffer += sFLASH_SPI_PAGESIZE;		
			StartOfPage++;
			STM_RESET_DOG();
		}
		if(NumOfSingle != 0)
		{
			SPI_FLASH_Read_Page(StartOfPage);
			SPI_FLASH_Read_Data(pBuffer, 0x0000, NumOfSingle);
			STM_RESET_DOG();
		}
	}
}

/*******************************************************************************
* Function Name  : SPI_FLASH_Read
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read 
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_FastRead(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead)
{
    spi_waitBusyClr();
    
	SPI_FLASH_CS_LOW();     //Select the FLASH: Chip Select low

	SPI_FLASH_SendByte(H_READ);        //Send "Read from Memory " instruction
																	//Send ReadAddr high nibble address byte to read from 
	SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
																 //Send ReadAddr medium nibble address byte to read from 
	SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
															 //Send ReadAddr low nibble address byte to read from 
	SPI_FLASH_SendByte(ReadAddr & 0xFF);


	while(NumByteToRead--)                  //while there is data to be read
	{
		*pBuffer = SPI_FLASH_SendByte(Dummy_Byte);  //Read a byte from the FLASH 

		pBuffer++;         //Point to the next location where the byte read will be saved
	}
	SPI_FLASH_CS_HIGH();        //Deselect the FLASH: Chip Select high
}

/************************************************************************/
/* PROCEDURE: Poll_SO													*/
/* This procedure polls for the SO line during AAI programming  		*/
/* waiting for SO to transition to 1 which will indicate AAI programming*/
/* is completed															*/
/* Input:																*/
/*		SO																*/
/* Output:																*/
/*		None															*/
/************************************************************************/
void SPI_FLASH_Poll_SO(void)
{
	u8 temp = 0;

	SPI_FLASH_CS_LOW();
	while (temp == 0x00)	                 //waste time until not busy
	{
	//	temp = GPIO_ReadInputDataBit(GPIO_SPI, SPI_SDI);	//save input
	}
	SPI_FLASH_CS_HIGH();
}

/************************************************************************/
/* PROCEDURE: EBSY														*/
/* This procedure enable SO as output RY/BY# status signal during AAI	*/
/* programming.															*/
/* Input:None															*/
/* Returns:	Nothing														*/
/************************************************************************/
void SPI_FLASH_SoEnable(void)
{
  	/* Select the FLASH: Chip Select low */
  	SPI_FLASH_CS_LOW();
  
  	/* Send "Write Enable" instruction */
  	SPI_FLASH_SendByte(0x70);
  
  	/* Deselect the FLASH: Chip Select high */
  	SPI_FLASH_CS_HIGH();
}

/************************************************************************/
/* PROCEDURE: DBSY														*/
/* This procedure disable SO as output RY/BY# status signal during AAI	*/
/* programming.															*/
/* Input:None															*/
/* Returns:	Nothing														*/
/************************************************************************/
void SPI_FLASH_SoDisable(void){
  	/* Select the FLASH: Chip Select low */
  	SPI_FLASH_CS_LOW();
  
  	/* Send "Write Enable" instruction */
  	SPI_FLASH_SendByte(0x80);
  
  	/* Deselect the FLASH: Chip Select high */
  	SPI_FLASH_CS_HIGH();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadID
* Description    : Reads FLASH identification.
* Input          : None
* Output         : None
* Return         : FLASH identification
*******************************************************************************/

void flash_init(void)
{    
    u8 c;
    //  配置ECC允许,缓冲读模式,使用每页附加区的第3/4字节(不受ECC保护的用户数据)做坏块标记
	while (1)
	{
        SPI_FLASH_Write_Status(c_statusAddr_protect,0x00);  // 取消写保护
        c = SPI_FLASH_Read_Status(c_statusAddr_config);
        c |= (1<<4 | 1<<3) ;                  
        SPI_FLASH_Write_Status(c_statusAddr_config, c);

		if(     (SPI_FLASH_Read_Status(c_statusAddr_protect) == 0)
            &&  (SPI_FLASH_Read_Status(c_statusAddr_config) == c))
            break;
	}  
}	

void setReadAddrRange(u32 addr)
{
    if((addr >= FLASH_APP_ADDR)&&(addr < FLASH_APP_ADDR_END))
    {
        w25n.readStartAddr = FLASH_APP_ADDR;
        w25n.readEndAddr   = FLASH_APP_ADDR_END;
    }
    else //if((addr >= FLASH_TMP_ADDR)&&(addr < FLASH_TMP_ADDR_END))
    {
        w25n.readStartAddr = FLASH_TMP_ADDR;
        w25n.readEndAddr   = FLASH_TMP_ADDR_END;
    }
    
}

void setWriteAddrRange(u32 addr)
{
    if((addr >= FLASH_APP_ADDR)&&(addr < FLASH_APP_ADDR_END))
    {
        w25n.writeStartAddr = FLASH_APP_ADDR;
        w25n.writeEndAddr   = FLASH_APP_ADDR_END;
    }
    else //if((addr >= FLASH_TMP_ADDR)&&(addr < FLASH_TMP_ADDR_END))
    {
        w25n.writeStartAddr = FLASH_TMP_ADDR;
        w25n.writeEndAddr   = FLASH_TMP_ADDR_END;
    }    
}

u32 SPI_FLASH_ReadID(void)
{
  	u32 Temp = 0;

    spi_waitBusyClr();
    
  	SPI_FLASH_CS_LOW();         //Select the FLASH: Chip Select low
  	SPI_FLASH_SendByte(READID);    //Send "READID " instruction

  	Temp = (Temp | SPI_FLASH_Get_Byte() );
  	Temp = (Temp | SPI_FLASH_Get_Byte() << 16);
  	Temp = (Temp | SPI_FLASH_Get_Byte() << 8);
  	Temp = (Temp | SPI_FLASH_Get_Byte() );
	
  	SPI_FLASH_CS_HIGH();        //Deselect the FLASH: Chip Select high
  	return Temp;
}

void spiFlash_readAddrSet(u32 addr)
{
    w25n.readAddr = addr;
    setReadAddrRange(addr);
}

void spiFlash_writeAddrSet(u32 addr)
{
    w25n.writeAddr = addr;
    setWriteAddrRange(addr);
}

//  不能跨页读
//  buf:NULL--整页读出到w25n.readBuf
void spiFlash_read(u8* buf, u16 len)
{
    u8 badBuf[2];
    u32 page, startPage, endPage, idx;
    
    idx       = w25n.readAddr  % FLASH_PAGE_SIZE;
    page      = w25n.readAddr  / FLASH_PAGE_SIZE;
    endPage   = w25n.readEndAddr   / FLASH_PAGE_SIZE;
    startPage = w25n.readStartAddr / FLASH_PAGE_SIZE;
    
    while(1)
    {    
        SPI_FLASH_Read_Page(page);
        SPI_FLASH_Read_Data(badBuf,2050,2);
        if((badBuf[0] == 0xff)&&(badBuf[1] == 0xff))
        {
            if(buf == NULL)    
            {    
                SPI_FLASH_Read_Data(w25n.readBuf, 0, sizeof(w25n.readBuf));
                idx = sizeof(w25n.readBuf);
            }
            else if(len != 0)
            {                
                if((idx + len) > sizeof(w25n.readBuf)) len = sizeof(w25n.readBuf) - idx;
                SPI_FLASH_Read_Data(buf, idx, len);
                idx += len;    
            } 
            break;    
        }
        page++;                
        if(page >= endPage) page = startPage;
    }     
    w25n.readAddr = page * FLASH_PAGE_SIZE + idx;
}

//  不能跨页写
//  type:   1--直接写入,0--缓冲到满页再写入
void spiFlash_write(u8 *buf,u16 len,u8 type)
{
    static u32 lastErase = 0xffffffff;
    static u32 lastPage  = 0xffffffff;    
    u32 x, end, idx, page, startPage, endPage;
 
    page = w25n.writeAddr / FLASH_PAGE_SIZE;
    idx  = w25n.writeAddr % FLASH_PAGE_SIZE;
    
    if( (w25n.writeAddr % FLASH_BLOCK_SIZE) == 0)
    {    
        if(lastErase != page)
        {
            lastErase = page;            
            SPI_FLASH_SectorErase(page * FLASH_PAGE_SIZE);
        }         
    }
    
    if(page != lastPage)
    {
        lastPage = page;
        SPI_FLASH_Read_Page(page);
        SPI_FLASH_Read_Data(w25n.writeBuf, 0, sizeof(w25n.writeBuf));
    }
    
    if((buf != NULL) && (len != 0))
    {    
        end = idx + len;
        if(end > sizeof(w25n.writeBuf)) end = sizeof(w25n.writeBuf);    
        for(x = 0; idx < end; idx++)
            w25n.writeBuf[idx] = buf[x++];
    }
    
    if( (type == 1) || (idx >= sizeof(w25n.writeBuf)) )
    {    
        startPage = w25n.writeStartAddr / FLASH_PAGE_SIZE;
        endPage   = w25n.writeEndAddr   / FLASH_PAGE_SIZE;
        
        while(1)
        {               
            STM_RESET_DOG();

            SPI_FLASH_WriteData(w25n.writeBuf, 0, sizeof(w25n.writeBuf));                
            SPI_FLASH_WritePage(page);
            SPI_FLASH_Read_Page(page);
            SPI_FLASH_Read_Data(w25n.readBuf, 0, sizeof(w25n.readBuf));
            
            for(x = 0; x < sizeof(w25n.readBuf); x++)
            {
                if(w25n.readBuf[x] != w25n.writeBuf[x])
                {    
                    SPI_FLASH_SetBadMark(page);
                    page++;    
                    if(page >= endPage) page = startPage;   

                    if( (page % (FLASH_BLOCK_SIZE / FLASH_PAGE_SIZE)) == 0)
                    {    
                        if(lastErase != page)
                        {
                            lastErase = page;            
                            SPI_FLASH_SectorErase(page * FLASH_PAGE_SIZE);
                        }         
                    }
                    break;
                }
            }
            if(x >= sizeof(w25n.readBuf)) break;            
        }   
    }
    w25n.writeAddr = page * FLASH_PAGE_SIZE + idx;    
}
#endif
