
/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/

#ifndef _spidrive_h_
#define _spidrive_h_

#include "stm32_config.h"

#ifdef FLASH_SEL_NANDFLASH
struct st_w25n
{
    u8 readBuf[FLASH_PAGE_SIZE];
    u8 writeBuf[FLASH_PAGE_SIZE];
    u32 readAddr;
    u32 readAddrBk;
    u32 readStartAddr;
    u32 readEndAddr;
    u32 writeAddr;
    u32 writeStartAddr;
    u32 writeEndAddr;
};
extern struct st_w25n w25n;

//============================FLASH======================================
#define sFLASH_SPI_SECTIONSIZE 		512     //扇区   512    
#define sFLASH_SPI_PAGESIZE 		0x800   //页     2048
#define sFLASH_SPI_Erase_PAGESIZE 	0x20000 //块     64*2048

//spi FLASH命令
#define WRSTA       	0x01  /* Write Status Register instruction */ 
#define RDSTA       	0x05  /* Read Status Register instruction  */
#define WRENA       	0x06  /* Write enable instruction */
#define WRDISA          0x04  /* Write diable instruction */

#define SE128K      	0xD8  /* Sector Erase instruction(128K) */
#define WRITES      	0x02  /* Write to Memory instruction */
#define WRD_PAGE        0x10  /* Write diable instruction */

#define READ_PAGE       0x13  /* Page Data Read (13h)  */
#define READ       	    0x03  /* Read from Memory instruction */

#define AAIS       	    0xAD  /* AAI word program instruction */
#define LAST_PAGE       0xa9  /* Last ECC failure Page Address   */
#define H_READ 	        0x0B  /* Read from Memory instruction */
#define READID          0x9F  /* Read identification */
#define SE4K       	    0x20  /* Sector Erase instruction(4K) */
#define SE32K      	    0x20  /* Sector Erase instruction(32K) */
#define CHIP_ERASE      0xC7  /* Chip Erase instruction */	 

#define WIP_Flag   	    0x01  /* Write In Progress (WIP) flag */
#define Dummy_Byte      0xA5

#define c_statusBit_busy        (1<<0)
#define c_statusBit_wel         (1<<1)
#define c_statusBit_efail       (1<<2)
#define c_statusBit_pfail       (1<<3)
#define c_statusAddr_protect    0xa0
#define c_statusAddr_config     0xb0
#define c_statusAddr_status     0xc0

/******************************************************************************/
//FLASH 通讯SPI口
#define 	sFLASH_SPI			SPI2
#define	FLASH_GPIO_SPI		GPIOB
#define	FLASH_SPI_SCK		GPIO_Pin_13
#define	FLASH_SPI_SDO		GPIO_Pin_14
#define	FLASH_SPI_SDI		GPIO_Pin_15

//#define	GPIO_SPI			GPIOA
//#define	SPI_SCK				GPIO_Pin_5
//#define	SPI_SDO			GPIO_Pin_6
//#define	SPI_SDI				GPIO_Pin_7	    
#define	SPI_CS				GPIO_Pin_12

#define SPI_FLASH_CS_LOW()     GPIO_ResetBits(FLASH_GPIO_SPI, SPI_CS)
#define SPI_FLASH_CS_HIGH()    GPIO_SetBits(FLASH_GPIO_SPI, SPI_CS)

/******************************************************************************/

u8 SPI_FLASH_SendByte(u8 byte);
void SPI_FLASH_WriteDisable(void);
void SPI_FLASH_WriteEnable(void);
void SPI_FLASH_Poll_SO(void);
void SPI_FLASH_SoEnable(void);
void SPI_FLASH_SoDisable(void);
void SPI_FLASH_SectorErase(u32 SectorAddr);//擦除4K
void SPI_FLASH_WaitForWriteEnd(void);
u8 SPI_FLASH_Read_Status(u8 RegisterAdd);
u8 SPI_FLASH_Get_Byte(void);
void SPI_FLASH_BulkErase(void);//擦除整个芯片
void SPI_FLASH_BlockErase(u32 BlockAddr);//擦除64K
void SPI_FLASH_FastRead(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead);
u8 SPI_FLASH_Write_Status(u8 RegisterAdd, u8 data);

void SPI_FLASH_Init(void);
u32 SPI_FLASH_ReadID(void);

//指定的地址开始读一串数据
void SPI_FLASH_Read(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead);
//指定的地址开始写一串数据
void SPI_FLASH_BytesWrite(u8 *pBuffer, u32 WriteAddr,u16 NumByteToWrite);

extern void spi_setBadMark(u32 addr);

extern u8   spi_getBadMark(u32 addr);

extern void spiFlash_read(u8* buf,u16 len);

extern void spiFlash_write(u8 *buf,u16 len,u8 type);

extern void spiFlash_readAddrSet(u32 addr);

extern void spiFlash_writeAddrSet(u32 addr);

#endif

#endif

