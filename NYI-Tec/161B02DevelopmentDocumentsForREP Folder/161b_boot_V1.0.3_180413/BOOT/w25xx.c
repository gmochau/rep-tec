/*
    w25xx.c
*/

//  仅需修改各块边界定义
//  段擦除处理、连续写边界回环处理、连续读边界回环处理都在底层驱动中实现

#include "stm32_config.h"			

#ifdef FLASH_SEL_NORFLASH

struct st_w25n w25n;

void SPI_FLASH_Init(void)
{  
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;

    /* Configure SPI2 pins: SCK, MISO and MOSI -*/
    GPIO_InitStructure.GPIO_Pin = FLASH_SPI_SCK  | FLASH_SPI_SDI;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin =  FLASH_SPI_SDO ;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

    /* Configure PA.4 as Output push-pull, used as Flash Chip select */
    GPIO_InitStructure.GPIO_Pin = SPI_CS;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

    SPI_FLASH_CS_HIGH();
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

    // SPI2 时钟源72M/2=36M(不能超过54M),SPI硬件支持上至18M
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master; //设置为主SPI
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;//SPI_BaudRatePrescaler_128;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(sFLASH_SPI, &SPI_InitStructure);
    SPI_Cmd(sFLASH_SPI, ENABLE);

    //GPIO_PinRemapConfig(GPIO_Remap_SPI1,ENABLE);
}

u8 SPI_FLASH_SendByte(u8 byte)  //  通过SPI硬件发送/接收一个字节
{
    u8 cnt;

    for(cnt = 0; cnt < 20; cnt++)
    {
        if(SPI_I2S_GetFlagStatus(sFLASH_SPI, SPI_I2S_FLAG_TXE) != RESET) break;
    }
    SPI_I2S_SendData(sFLASH_SPI, byte);
    for(cnt = 0; cnt < 20; cnt++)
    {
        if(SPI_I2S_GetFlagStatus(sFLASH_SPI, SPI_I2S_FLAG_RXNE) != RESET) break;
    }
    return SPI_I2S_ReceiveData(sFLASH_SPI);         
}

u8 SPI_FLASH_Get_Byte(void)
{ 
    return (SPI_FLASH_SendByte(0xff));
}


static u8 w25xx_readStatus(void)
{
    u8 byte = 0;

    SPI_FLASH_CS_LOW();				
    SPI_FLASH_SendByte(0x05);		
    byte = SPI_FLASH_Get_Byte();	
    SPI_FLASH_CS_HIGH();			

    return byte;
}

static void w25xx_waitTmo(u8* str) 
{ 
    //printf("flash_tmo:%s\r\n",str);
}

static void w25xx_waitBusyClr(void)
{ 
    u8 status;
    u32 tmr;

    tmr = tick_get();
    while( tick_cmp(tmr, 400) == c_ret_nk )            
    {        
        STM_RESET_DOG();
        status = w25xx_readStatus();
        if((status & c_statusBit_busy) == 0)
        {
            return;
        }
    }
    w25xx_waitTmo("busyClr");
}

static void w25xx_waitWelSet(void)
{ 
    u8 status;
    u32 tmr;
    
    tmr = tick_get();
    while( tick_cmp(tmr, 50) == c_ret_nk )            
    {
        STM_RESET_DOG();
        status = w25xx_readStatus();
        if(     ((status & c_statusBit_wel ) != 0)
            &&  ((status & c_statusBit_busy) == 0) )
        {
            return;
        }
    }
    w25xx_waitTmo("welSet");
}

static void w25xx_waitWelClr(void)
{ 
    u8 status;
    u32 tmr;
    
    tmr = tick_get();
    while( tick_cmp(tmr, 400) == c_ret_nk )            
    {
        STM_RESET_DOG();
        status = w25xx_readStatus();
        if(     ((status & c_statusBit_wel ) == 0)
            &&  ((status & c_statusBit_busy) == 0) )
        {
            return;
        }
    }
    w25xx_waitTmo("welClr");
}

static void w25xx_writeEnable(void)
{
    SPI_FLASH_CS_LOW(); 
    SPI_FLASH_SendByte(0x06);
    SPI_FLASH_CS_HIGH();
    w25xx_waitWelSet();
}

static void w25xx_writeDisable(void)
{ 
    SPI_FLASH_CS_LOW();				
    SPI_FLASH_SendByte(0x04);		
    SPI_FLASH_CS_HIGH();		
    w25xx_waitWelClr();
}

void w25xx_erase(u32 addr)
{    
    un32 ux;

    w25xx_waitBusyClr();
    w25xx_writeEnable();              
    
    ux.r = addr;
    SPI_FLASH_CS_LOW();                    
    SPI_FLASH_SendByte(0x20);                   //  块擦除时间太长300ms~2sec，使用段擦除60ms~400ms   
    SPI_FLASH_SendByte(ux.b.h);
    SPI_FLASH_SendByte(ux.b.m);
    SPI_FLASH_SendByte(ux.b.l);
    SPI_FLASH_CS_HIGH();        

    w25xx_waitWelClr();
    w25xx_writeDisable();    
}

void w25xx_read(u32 addr,u8* buf,u16 len)
{ 
    un32 ux;
    u16 x;

    ux.r = addr;
    if(ux.r >= w25n.readEndAddr)     
        ux.r = w25n.readStartAddr;
        
    w25xx_waitBusyClr();    
    SPI_FLASH_CS_LOW();         	
    SPI_FLASH_SendByte(0x03); 		
    SPI_FLASH_SendByte(ux.b.h);
    SPI_FLASH_SendByte(ux.b.m);
    SPI_FLASH_SendByte(ux.b.l);
    for(x = 0; x < len; )                    //  可以连续读完整个芯片
    {
        buf[x] = SPI_FLASH_Get_Byte();
        x++;
        ux.r++;
        if((ux.r & (4096 - 1)) == 0)            //  段边界    
        {
            if(ux.r >= w25n.readEndAddr)     
            {
                ux.r = w25n.readStartAddr;
                SPI_FLASH_CS_HIGH();
                
                w25xx_waitBusyClr();    
                SPI_FLASH_CS_LOW();         	
                SPI_FLASH_SendByte(0x03); 		
                SPI_FLASH_SendByte(ux.b.h);
                SPI_FLASH_SendByte(ux.b.m);
                SPI_FLASH_SendByte(ux.b.l);
            }
        }                   
    }
    SPI_FLASH_CS_HIGH();   
}

void w25xx_write(u32 addr, u8* buf, u16 len)
{   
    un32 ux;
    u16 x;
    u8 step = 0;
    
    ux.r = addr;    
    x = 0;
    while(1)
    {
        switch(step)
        {
            case 0:
                if( (ux.r & (4096 - 1)) == 0 )
                {
                    if(ux.r >= w25n.writeEndAddr)
                        ux.r = w25n.writeStartAddr;
                    w25xx_erase(ux.r);                    
                }
                step = 1;
            
            case 1:
                w25xx_waitBusyClr();
                w25xx_writeEnable();              
                
                SPI_FLASH_CS_LOW();                    
                SPI_FLASH_SendByte(0x02);            
                SPI_FLASH_SendByte(ux.b.h);
                SPI_FLASH_SendByte(ux.b.m);
                SPI_FLASH_SendByte(ux.b.l);
                for( ; x < len; )
                {
                    SPI_FLASH_SendByte(buf[x]); 
                    x++;
                    ux.r++;
                    if( (ux.r & (256 - 1)) == 0 )
                    {
                       if( (ux.r & (4096 - 1)) == 0 ) step = 0;
                       break;
                    }
                }
                SPI_FLASH_CS_HIGH();        
                w25xx_waitWelClr();
                w25xx_writeDisable();
            break;
            
            default:step = 0;
            break;    
        }
        if(x >= len) break;
    }                                                         
}

//  SysParameter    561
//  SysRunParameter 193
//  日志            512
//  程序升级        2048
//  内部管理写地址w25n.writeAddr
void spiFlash_write(u8 *buf, u16 len, u8 type)
{
    if(buf == NULL) return;
    w25xx_write(w25n.writeAddr, buf, len);     
    
    w25n.writeAddr += len;
    //w25n.writeAddr += ((len - 1) / 256) * 256 + 256;
}

//  buf:NULL--整页读出到w25n.readBuf
//  type:   0--先检测有效性，再读出
//          1--直接读出
//  内部管理读地址w25n.readAddr
void spiFlash_read(u8* buf, u16 len)
{ 
    if(buf == NULL)
    {
        w25xx_read(w25n.readAddr, w25n.readBuf, sizeof(w25n.readBuf));
        len = sizeof(w25n.readBuf);
    }
    else
    {
        w25xx_read(w25n.readAddr, buf, len);
    }
    
    w25n.readAddr += len;
    //w25n.readAddr += ((len - 1) / 256) * 256 + 256;
}

void SPI_FLASH_SectorErase(u32 SectorAddr)
{
    //w25xx_erase(SectorAddr);
}

u32 SPI_FLASH_CheckID(void)
{
    u8 x, y;
    
    w25xx_waitBusyClr();
    w25xx_writeEnable();                  
    x = w25xx_readStatus();     //0x02
    
    w25xx_writeDisable();    
    y = w25xx_readStatus();     //0x00
    
    if( ((x & c_statusBit_wel) != 0) && ((y & c_statusBit_wel) == 0) )
        return c_ret_ok;
    else
        return c_ret_nk;
}

void flash_init(void)
{    
    u32 x;
    
	while (1)
	{
        if( SPI_FLASH_CheckID() == c_ret_ok) break;
	}  
    
    for(x = 0; x < sizeof(w25n.writeBuf); x++)
        w25n.writeBuf[x] = x;
    
    spiFlash_writeAddrSet(FLASH_TMP_ADDR);
    spiFlash_write(w25n.writeBuf, sizeof(w25n.writeBuf),0);
    
    spiFlash_readAddrSet(FLASH_TMP_ADDR);
    spiFlash_read(w25n.readBuf, sizeof(w25n.readBuf));
    
    for(x = 0; x < sizeof(w25n.writeBuf); x++)
    {
        if(w25n.writeBuf[x]  != w25n.readBuf[x]) break;
    }
    if(x < sizeof(w25n.writeBuf))
    {
        while(1){}
    }    
}	

void setReadAddrRange(u32 addr)
{
    if((addr >= FLASH_APP_ADDR)&&(addr < FLASH_APP_ADDR_END))
    {
        w25n.readStartAddr = FLASH_APP_ADDR;
        w25n.readEndAddr   = FLASH_APP_ADDR_END;
    }
    else //if((addr >= FLASH_TMP_ADDR)&&(addr < FLASH_TMP_ADDR_END))
    {
        w25n.readStartAddr = FLASH_TMP_ADDR;
        w25n.readEndAddr   = FLASH_TMP_ADDR_END;
    }
    
}

void setWriteAddrRange(u32 addr)
{
    if((addr >= FLASH_APP_ADDR)&&(addr < FLASH_APP_ADDR_END))
    {
        w25n.writeStartAddr = FLASH_APP_ADDR;
        w25n.writeEndAddr   = FLASH_APP_ADDR_END;
    }
    else //if((addr >= FLASH_TMP_ADDR)&&(addr < FLASH_TMP_ADDR_END))
    {
        w25n.writeStartAddr = FLASH_TMP_ADDR;
        w25n.writeEndAddr   = FLASH_TMP_ADDR_END;
    }    
}

void spiFlash_readAddrSet(u32 addr)
{ 
    w25n.readAddrBk = w25n.readAddr = addr;
    setReadAddrRange(addr);
}

void spiFlash_writeAddrSet(u32 addr)
{ 
    w25n.writeAddr = addr;
    setWriteAddrRange(addr);
}

#endif
