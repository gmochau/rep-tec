/*
    w25xx.h    
*/
    
#ifndef __w25xx_h__
#define __w25xx_h__

#include "stm32_config.h"

#ifdef FLASH_SEL_NORFLASH
struct st_w25n
{
    u8 readBuf[FLASH_PAGE_SIZE];
    u8 writeBuf[FLASH_PAGE_SIZE];
    u32 readAddr;
    u32 readAddrBk;
    u32 readStartAddr;
    u32 readEndAddr;
    u32 writeAddr;
    u32 writeStartAddr;
    u32 writeEndAddr;
};
extern struct st_w25n w25n;

//============================FLASH======================================

#define c_statusBit_busy        (1<<0)
#define c_statusBit_wel         (1<<1)

/******************************************************************************/
//FLASH ͨѶSPI��
#define 	sFLASH_SPI			SPI2
#define	FLASH_GPIO_SPI		GPIOB
#define	FLASH_SPI_SCK		GPIO_Pin_13
#define	FLASH_SPI_SDO		GPIO_Pin_14
#define	FLASH_SPI_SDI		GPIO_Pin_15

//#define	GPIO_SPI			GPIOA
//#define	SPI_SCK				GPIO_Pin_5
//#define	SPI_SDO			GPIO_Pin_6
//#define	SPI_SDI				GPIO_Pin_7	    
#define	SPI_CS				GPIO_Pin_12

#define SPI_FLASH_CS_LOW()     GPIO_ResetBits(FLASH_GPIO_SPI, SPI_CS)
#define SPI_FLASH_CS_HIGH()    GPIO_SetBits(FLASH_GPIO_SPI, SPI_CS)

/******************************************************************************/

extern void w25xx_erase(u32 addr);

extern void w25xx_read(u32 addr,u8* buf,u16 len);

extern void w25xx_write(u32 addr, u8* buf, u16 len);



extern void SPI_FLASH_Init(void);

extern void spiFlash_write(u8 *buf, u16 len, u8 type);

extern void spiFlash_read(u8* buf, u16 len);

extern void SPI_FLASH_SectorErase(u32 SectorAddr);

extern u32 SPI_FLASH_CheckID(void);

extern void spiFlash_readAddrSet(u32 addr);

extern void spiFlash_writeAddrSet(u32 addr);

extern void flash_init(void);
#endif

#endif

