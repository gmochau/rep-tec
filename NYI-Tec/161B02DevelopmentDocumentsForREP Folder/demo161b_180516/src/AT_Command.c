
#include "stm32_config.h"			//


typedef void (*at_funcp)(void);

struct at_cmd 
{
	char const * at_cmd;
	at_funcp at_func;
};

AT_BUF at_buf;

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void at_CmdConfiguration(void)
{

		printf("\r\nat_CmdSetting\r\n");


}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void at_CmdSetting(void)
{

		printf("\r\nat_CmdSetting\r\n");


}

void at_CmdUart(void)
{
}
/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
static struct at_cmd const at_CmdTab[] = 
{
	{"AT+CONFIG", at_CmdConfiguration},
	{"AT+SETTING", at_CmdSetting},
    {"AT$",at_CmdUart},
	{NULL, NULL}
};


/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void at_CommandAnalysis(void)
{
	u8 i = 0;

	for (i = 0; at_CmdTab[i].at_cmd != NULL; i++) 
	{
		if (0 == strncmp((char *)at_buf.rbuf , at_CmdTab[i].at_cmd, strlen((char *)at_CmdTab[i].at_cmd) ))
		{
			(*at_CmdTab[i].at_func)();
			break;
		}
	}
	if(at_CmdTab[i].at_cmd == NULL )
	{

		dbg(GPS_DBG_EN,"gps data err ......\r\n" );

	}

}



