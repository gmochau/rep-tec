
#ifndef _at_command_h_
#define _at_command_h_

#include "stm32_config.h"

#define AT_COMMAND_DEBUG	1
#define AT_COMMAND_LEN	256

typedef struct  
{
	u16 len;
	u8 rbuf[AT_COMMAND_LEN];
}AT_BUF;

extern AT_BUF at_buf;


extern void at_CommandAnalysis(void);

void App_Command_Task(void * pdata);

#endif

