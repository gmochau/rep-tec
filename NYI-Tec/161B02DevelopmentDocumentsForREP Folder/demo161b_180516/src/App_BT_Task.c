
/*******************************************************************************                            
* @ Version   -->
* @ Author    --> 
* @ Date      --> 
* @ Revise    -->
*
******************************************************************************/

#include "stm32_config.h"			//

OS_STK BT_poll_STK[BT_poll_Size];

enum
{
	c_btSendStep_idle,
	c_btSendStep_set,
	c_btSendStep_run,
	c_btSendStep_tmo,
	c_btSendStep_err,
	c_btSendStep_done,
};	
u8 btSendStep = c_btSendStep_idle;
/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void get_Flash_Start_End_Time(rtc_time *StartTm, rtc_time *EndTm)
{
	//rtc_time_to_tm(SysRunParameter.FLASH_Parameter.StartTime, StartTm);
	//rtc_time_to_tm(SysRunParameter.FLASH_Parameter.EndTime, EndTm);
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

Response:{
	results: [{start date,start time},{end date,end time}] //Format is YYYYMMDD HHMMSS
}

******************************************************************************/
u8 bt_Package_0x01_Ack(u8 *src)
{
	u8 n = 0;
	rtc_time StartTm;
	rtc_time EndTm;
	
	get_Flash_Start_End_Time(&StartTm, &EndTm);
	n = sprintf((char*)src, "Response:{results: [{%04d%d%02d%02d%02d%02d},{%04d%02d%02d%02d%02d%02d}]}",
							StartTm.tm_year,StartTm.tm_mon,StartTm.tm_yday,StartTm.tm_hour,StartTm.tm_min,StartTm.tm_sec,
							EndTm.tm_year,EndTm.tm_mon,EndTm.tm_yday,EndTm.tm_hour,EndTm.tm_min,EndTm.tm_sec
							);

	return n;
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

request: {
	source_id:bt1dcd468888666,
	Querylog:[0]
}
******************************************************************************/
void bt_Response_0x00_Ack(BT_MESSAGE *bt_Message)
{
	switch(bt_Message->bt_LinkState[bt_Message->currentId].btMode)
	{
		case BT_SSP_MODE:

			break;
		case BT_LES_MODE:

			break;

		case BT_LEC_MODE:

			break;

	}
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

request: {
	source_id:bt1dcd468888666,
	Querylog:[1]
}


******************************************************************************/
u8 bt_Response_0x01_Ack(BT_MESSAGE *bt_Message)
{

	u8 err = OS_ERR_NONE;
	u8 buf[100];
	u8 len = 0;
	
	//len = bt_Package_0x01_Ack(buf);

	memcpy(&bt_Message->bt_LinkState[bt_Message->currentTab].btTxBuf, buf, len);
	bt_Message->bt_LinkState[bt_Message->currentTab].IdTxLen = len;


	dbg(BT_DBG_EN,"BT SEND Tab:[%d] Len:[%d]\r\n", bt_Message->currentTab, len);

	
	for(;;)
	{
		if(OS_ERR_BUS == bt_Message->btUartBus)
		{
		}
		else
		{
			err = app_bt_SendData(bt_Message);
			break;
		}
	}
	return err;
	
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

request: 
{
	source_id:bt1dcd468888666,
	Querylog:[{start date,start time},{end date,end time}]		 //[{20170808,010558},{20170908,230558}]Format is YYYYMMDD HHMMSS
}

******************************************************************************/
void bt_Response_0x02_Ack(BT_MESSAGE *bt_Message)
{
	switch(bt_Message->bt_LinkState[bt_Message->currentId].btMode)
	{
		case BT_SSP_MODE:

			break;
		case BT_LES_MODE:

			break;

		case BT_LEC_MODE:

			break;

	}
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

******************************************************************************/
void bt_CommandAnalysis(BT_MESSAGE *bt_Message)
{
	u8 *ptr = NULL;

	ptr = (u8 *)strstr((char *)bt_Message->bt_LinkState[bt_Message->currentTab].btRxBuf, "Querylog:[");
	
	if(NULL != ptr++)
	{
		switch(*ptr)
		{
			case '0':
				bt_Response_0x00_Ack(bt_Message);
				break;
				
			case '1':
				bt_Response_0x01_Ack(bt_Message);
				break;
				
			default:
				bt_Response_0x02_Ack(bt_Message);
				break;
		}
	}
	else
	{

		dbg(ELD_DBG_EN,"BT ERORR DATA\r\n");

	}

}

void app_sendFtpSet(struct ftpPara *pFtpPara)
{
	u8 err;
	
	ftpSendPara = *pFtpPara;
	ftpSendPara.step = c_sendStep_set;		
	OSFlagPost(ELD_send_status,ELD_send_done_ftp,OS_FLAG_CLR,&err);	
}

u8 app_sendFtpStatus(void)
{
	return (ftpSendPara.step == c_sendStep_idle) ? OS_ERR_IDLE : ((ftpSendPara.step == c_sendStep_done) ? OS_ERR_NONE : OS_ERR_RUN);	
}


void bt_poll_task_proc(void)
{
	u8 err,i;
	u16 len = 0;
	static BT_MESSAGE *boxMsg;

    if(powerCtl.btFunDisable == 1)
    {
        if( btSendStep != c_btSendStep_run )
            btSendStep = c_btSendStep_done;
        if(     ((ftpSendPara.step == c_sendStep_set)||((ftpSendPara.step == c_sendStep_run)))
            &&	(ftpSendPara.src == ELD_PACKET_bt) )
            ftpSendPara.step = c_sendStep_done; 
        return;
    }
    
    len = 0;
    if( (boxMsg = (BT_MESSAGE *)OSMboxAccept(bt_Command_Mbox)) != NULL)
    {	
        len = boxMsg->currentLen;
    }		
    
    if(len > 0)
    {

        for(i=0; i<sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); ++i)
        {
            if(boxMsg->bt_LinkState[i].IdRxLen != 0)	// 接收数据
            {	
                //dbg(BT_DBG_EN,"\r\n**** Id:[%d] mode:[%d] len:[%d] btRxBuf:[%s] ****\r\n",	boxMsg->bt_LinkState[i].btID, 
                //                                            boxMsg->bt_LinkState[i].btMode, 
                //                                            boxMsg->bt_LinkState[i].IdRxLen, 
                //                                            //boxMsg->bt_LinkState[i].btRxBuf
                //                                            &boxMsg->bt_LinkState[i].btRxBuf[boxMsg->bt_LinkState[i].rxTail]
                //                                            );
                dbg(BT_DBG_EN,"\r\n**** Id:[%d] mode:[%d] len:[%d] ****\r\n",	boxMsg->bt_LinkState[i].btID, 
                                                            boxMsg->bt_LinkState[i].btMode, 
                                                            boxMsg->bt_LinkState[i].IdRxLen
                                                            );
                
            }
        }

        
        //bt_Response_0x01_Ack(boxMsg);
        //if(OS_ERR_NONE == err)
        {
            //bt_CommandAnalysis(boxMsg);
                        
            OSFlagPost(ELD_rece_status,(OS_FLAGS)ELD_rece_done_bt,OS_FLAG_CLR,&err);
            if(OSMboxPost(ELD_rece_bt,(void *)boxMsg) == OS_ERR_NONE)										
            {
                OSFlagPend(ELD_rece_status,(OS_FLAGS)ELD_rece_done_bt,OS_FLAG_WAIT_SET_ANY, 15*1000/c_systick, &err);	
                //bt_Response_0x01_Ack(boxMsg);
            }							
        }
    }

    if(OS_ERR_BUS != bt_Message.btUartBus)
    {    
        if(btSendStep == c_btSendStep_set)
        {	
            btSendStep = c_btSendStep_run;
            if( app_bt_SendData(&bt_Message) == OS_ERR_NONE)
                btSendStep = c_btSendStep_done;
            else
                btSendStep = c_btSendStep_err;
        }    
        
        if(		(ftpSendPara.step == c_sendStep_set)
            &&	(ftpSendPara.src == ELD_PACKET_bt) )
        {
            bt_Message.btUartBus = OS_ERR_BUS;
            
            ftpSendPara.step = c_sendStep_run;		
            update_ftpProc();			
            ftpSendPara.step = c_sendStep_done;
            
            dbg(BT_DBG_EN,"**** BT xmodem %s\r\n ****", (ftpSendPara.result == c_ret_ok) ? "ok" : "fail");
            
            OSFlagPost(ELD_send_status,ELD_send_done_ftp,OS_FLAG_SET,&err);			
            bt_Message.btUartBus = OS_ERR_NONE;
        }        
        
    }	

    if(dbgBtRespRun == 1)
    {
        dbg_btRespProc();
    }    
}

void BT_poll_Task(void * pdata)
{
	printf("\r\nBT poll task................OK.\r\n");
    
	while(1)
	{	

		dbg(TASK_DBG_EN,"[bt_poll_Task]\r\n");
		taskCntInc();

		bt_poll_task_proc();
	}
}


/******************************************************************************
* Function Name --> 
* Description   --> 轮询式调用
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void app_sendBtSet(u8 *src , u16 len , u8 type)
{
	u8 x;


	dbg(BT_DBG_EN,"\r\n**** BT SEND REQ Len:[%d] ****\r\n",len);

	
	if(type == 0)
	{	
        if(strncmp((char*)src,"busying", len) == 0)
        {
            for(x = 0; x < sizeof(bt_Message.bt_LinkState) / sizeof(bt_Message.bt_LinkState[0]); x++)
            {
                if(bt_Message.bt_LinkState[ x ].btConnect == BT_CONNECT_OK)
                {
                    if(x != ELD_data.bt_currentTab)
                    {
                        memcpy(bt_Message.bt_LinkState[ x ].btTxBuf, src, len);
                        bt_Message.bt_LinkState[ x ].IdTxLen = len;
                    }
                }
            }            
        }        
        else
        {        
            memcpy(bt_Message.bt_LinkState[ELD_data.bt_currentTab].btTxBuf, src, len);
            bt_Message.bt_LinkState[ELD_data.bt_currentTab].IdTxLen = len;
        }
	}
	else
	{
		for(x = 0; x < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); x++)
		{
			if(bt_Message.bt_LinkState[ x ].btConnect == BT_CONNECT_OK)
			{
				memcpy(bt_Message.bt_LinkState[ x ].btTxBuf, src, len);
				bt_Message.bt_LinkState[ x ].IdTxLen = len;				
			}
		}
	}		
	btSendStep = c_btSendStep_set;
	OSFlagPost(ELD_send_status,(OS_FLAGS)ELD_send_done_bt,OS_FLAG_CLR,&x); 
	
}

/******************************************************************************
* Function Name --> 
* Description   --> 轮询式调用
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
INT8U app_sendBtStatus(void)
{
	u8 err;	
	
	if(		(btSendStep != c_btSendStep_idle)
		&&	(btSendStep != c_btSendStep_set)
		&&	(btSendStep != c_btSendStep_run) )
	{
        dbg(BT_DBG_EN,"\r\n**** BT SEND done ****\r\n");
		btSendStep = c_btSendStep_idle;
		OSFlagPost(ELD_send_status,(OS_FLAGS)ELD_send_done_bt,OS_FLAG_SET,&err);
		return OS_ERR_NONE;
	}		
	else 
		return (btSendStep == c_btSendStep_idle) ? OS_ERR_IDLE : OS_ERR_RUN;	
}

