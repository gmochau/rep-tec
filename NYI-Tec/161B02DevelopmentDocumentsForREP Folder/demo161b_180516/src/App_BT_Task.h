

#ifndef _App_BT_Task_h_
#define _App_BT_Task_h_


extern void app_sendBtSet(u8 *src , u16 len , u8 type);

extern INT8U app_sendBtStatus(void);

extern void bt_poll_task_proc(void);

#endif


