

#include "stm32_config.h"			//

#define SEND_REPORT_EN		1

#define	REPORT_TEST		1

u8 ProtocolSendBuf[800];

SEND_PEPORT SendPeport;

PROTOCOL_ASCII_FORMAT protocol_ASCII_Format = {0};
PROTOCOL_HEX_FORMAT	protocol_HEX_Format = {0};


/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

0001,8,1,1,1,1,20170603,001247,"",43234,6774,"343j438204930984393000","34.56","-22.97",2,0,0,"","",38203,04,E3

******************************************************************************/
void app_Package_HexToAscii_Format(void)
{
	rtc_time tm;
	
	if(protocol_HEX_Format.SequenceID >= 0x10000)
	{	
		SysRunParameter.ReportId++;
		protocol_HEX_Format.SequenceID = SysRunParameter.ReportId;
	}
	sprintf((char*)protocol_ASCII_Format.SequenceID,"%04x", protocol_HEX_Format.SequenceID);
	sprintf((char*)protocol_ASCII_Format.EventType,"%d", protocol_HEX_Format.EventType);
	sprintf((char*)protocol_ASCII_Format.EventCode,"%d", protocol_HEX_Format.EventCode);

    //if((protocol_ASCII_Format.Date[0] == 0)&&(protocol_ASCII_Format.Time[0] == 0))
    {
        protocol_HEX_Format.RtcTime = get_RTCCounter();	// �������
        rtc_time_to_tm(protocol_HEX_Format.RtcTime, &tm);	// Format: MMDDYY	Format: HHMMSS
        sprintf((char*)protocol_ASCII_Format.Date,"%02d%02d%02d", tm.tm_mon,tm.tm_mday,tm.tm_year % 2000);
        sprintf((char*)protocol_ASCII_Format.Time,"%02d%02d%02d", tm.tm_hour,tm.tm_min,tm.tm_sec);
    }
    
    ELD_strncpy(protocol_ASCII_Format.EventData, protocol_HEX_Format.EventData, sizeof(protocol_HEX_Format.EventData)-1);
	
}


/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 

0001,8,1,1,1,1,20170603,001247,"",43234,6774,"343j438204930984393000","34.56","-22.97",2,0,0,"","",38203,04,E3
******************************************************************************/
void app_PackageToFlash(void)
{
	static u8 packageBuf[GPS_FLASH_SIZE] = {0};	//

	app_Package_HexToAscii_Format();
	sprintf((char*)packageBuf,"%s,%s,%s,%s,%s,%s,%s", 
                    SysParameter.UnitData.SerialNum, 
                    protocol_ASCII_Format.SequenceID,
					protocol_ASCII_Format.EventType,
					protocol_ASCII_Format.EventCode,
                    protocol_ASCII_Format.Date,
                    protocol_ASCII_Format.Time,
                    protocol_ASCII_Format.EventData);	
	ELD_sum_set(packageBuf);

	WriteDataToFlash(packageBuf, strlen((char *)packageBuf));
}
