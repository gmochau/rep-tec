
#ifndef _App_SendReport_h_
#define _App_SendReport_h_

#define	GPRS_PORT	1
#define	BT_PORT	2
#define	UART_PORT	3

#define	PRECISION_HIGH		0
#define	PRECISION_LOW		1

/*
####,#,#,#,#,#,YYYYMMDD,HHMM,XXXXXXX,## , ###, ###, ##, ##.#, ##.#, ##, #, #, ###, ##, ##, ##
（ 十六进制序列ID，
	事件类型，
	事件代码，
	起源，源，
	状态，
	日期，
	时间，
	事件数据，
	里程表，
	引擎小时数，
	VIN号码，
	经度，
	经度，
	自上次以来的距离，
	故障指示灯状态，
	诊断事件指示灯状态，
	注释 ，
	位置说明，
	驱动程序ID，
	十六进制事件校验和值，
	十六进制事件数据校验和值
）

Example: 例子：
0001,8,1,1,1,1,20170603,001247,"",43234,6774,"343j438204930984393000","34.56","-22.97",2,0,0,"","",38203,04,E3
*/

/******************************************************************************
******************************************************************************/
//#pragma pack(1)
typedef struct 
{
    u8  deviceID[19];
	u8	SequenceID[5];
	u8 	EventType[3];
	u8 	EventCode[5];
	u8	Origin[2];
	u8	Status[2];
	u8	Source[2];
	u8	Date[9];
	u8	Time[7];
	u8	EventData[501];
	u8 	Odometer[11];
	u8	EngineHours[11];
	u8	VINNumber[20];
	u8	Latitude[11];
	u8	Longitude[11];
	u8	DistancesinceLast[11];
	u8	MalfunctionIndicatorStatus[2];
	u8	DiagnosticEventIndicatorStatus[2];
	u8	Comment[60];
	u8	LocationDescription[60];
	u8	DriverID[13];
	u8  Version[2];
}PROTOCOL_ASCII_FORMAT;

typedef struct 
{
    u8  deviceID[19];
	u32	SequenceID;
	u8 	EventType;
	u16	EventCode;
	u8	Origin;
	u8	Status;
	u8	Source;
	u32	RtcTime;
	u8	EventData[501];
	u32 	Odometer;
	u32	EngineHours;
	u8	VINNumber[20];
	double	Latitude;
	double	Longitude;
	u16	DistancesinceLast;
	u8	MalfunctionIndicatorStatus;
	u8	DiagnosticEventIndicatorStatus;
	u8	Comment[60];
	u8	LocationDescription[60];
	u8	DriverID[13];
	u8  Version;
}PROTOCOL_HEX_FORMAT;

extern PROTOCOL_HEX_FORMAT	protocol_HEX_Format;
extern PROTOCOL_ASCII_FORMAT protocol_ASCII_Format;

typedef struct 
{
	u32 TempReadDataAddr;
	u32	ReadDataAddr[7];
	u8	Ack[7];
	u8	PackCnt;
	u8	Answer;
}SEND_PEPORT;

//#pragma pack()

extern u8 ProtocolSendBuf[800];
extern SEND_PEPORT SendPeport;
/******************************************************************************
******************************************************************************/
extern void app_SendReport(u8 PortType,u8 *done);
extern void app_SendResponse(u8 PortType,u8 *done);
extern void app_PackageToFlash(void);

extern void app_Package_HexToAscii_Format(void);

#endif

