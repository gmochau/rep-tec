

#include "stm32_config.h"			//

#define PROTOCOL_EN		1

static u8 ProtocolBuf[2048];
static u16 ProtocolLen = 0;

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
* 在字符串prt中查找第n个字符sing的下一字符位置 
******************************************************************************/
u8 * app_SearchNChar(u8* prt, u8 sing, u8 n)
{
	u8 *px = prt;
	if(n == 0) return prt;
	
	while (px != prt+2048) 
	{
		if (*px == sing)	n--;
		px++;
		if (n == 0)	break;
	}	
	return px;
}
