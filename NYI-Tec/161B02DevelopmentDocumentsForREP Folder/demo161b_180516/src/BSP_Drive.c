
#include "stm32_config.h"			//

/******************************************************************************
* Function Name -->
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void MCU_HoldInit(void)     // PC4--ATSHA204_SDA_CA
{

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	//GPIOBê±?ó
	
	GPIO_InitStructure.GPIO_Pin = MCU_HOLD_PIN;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(MCU_GOIOX, &GPIO_InitStructure);
	MCU_HOLD_ON;
	
    //  PC2  高点平进入测试模式
	GPIO_InitStructure.GPIO_Pin = MCU_TEST_PIN;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(MCU_TEST_GPIOX, &GPIO_InitStructure);
    
}

/******************************************************************************
* Function Name -->
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void MCU_HOLD_ON_CONTROL(void)
{
	MCU_HOLD_ON;
}

/******************************************************************************
* Function Name -->
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void MCU_HOLD_OFF_CONTROL(void)
{
	MCU_HOLD_OFF;
}


/******************************************************************************
* Function Name -->
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
*	返回电压mv
******************************************************************************/
u16 BSP_ADcPower12v(void)
{
    //ext12v---150k---adin13---10k---gnd
	return (ADC_GetConversionValue(ADC1) * (3.3*16*1000/4096)) + 500;
}

u16 BSP_ADCConfig()
{
	ADC_InitTypeDef ADC_InitStructure;
       u8 i;

	GPIO_InitTypeDef GPIO_InitStructure;
	// 将 PC3(ADC12_IN13) 设置为模拟输入脚
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;//GPIO_Mode_AIN;
	GPIO_Init(GPIOC , &GPIO_InitStructure);

    RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC1,DISABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC , &GPIO_InitStructure);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    
	ADC_Cmd(ADC1, DISABLE);for(i=0;i<100;i++);//20us
    
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;//ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;//ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;//4;//3;
	ADC_Init(ADC1, &ADC_InitStructure);

	/* 设置 ADC1 使用11,12,13转换通道，转换顺序1-3，采样时间为 71.5 周期 */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_13,      1, ADC_SampleTime_71Cycles5);//PC3 12V

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);for(i=0;i<100;i++);//20us

	return 0;
}

void BSP_adcStart(void)
{
    u32 cnt;    
    BSP_ADCConfig();
    
	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);

	/* Check the end of ADC1 reset calibration register */
    while(ADC_GetResetCalibrationStatus(ADC1))
    {
    }

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	/* Check the end of ADC1 calibration */
    while(ADC_GetCalibrationStatus(ADC1))
    {
    }
    for(cnt = 0; cnt < 100; cnt++); //20us

	/* Start ADC1 Software Conversion */
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

    for(cnt = 0; cnt < 100; cnt++)
    {    
        if(ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC) != RESET) break;
    }
    ADC_ClearFlag(ADC1, ADC_FLAG_EOC);    
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void BSP_ADCInit(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	// 将 PC3(ADC12_IN13) 设置为模拟输入脚

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC , &GPIO_InitStructure);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	BSP_ADCConfig();
	
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void STM_GetSysTick(u32* n)
{
	*n = STM_SysTickCnt;
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
u8 STM_CheckSysTick(u32* n,u32 time)
{
	u8 ret;

	ret = FALSE;
	if(STM_SysTickCnt >= *n + time)
	{
		ret = TRUE;
	}
	return ret;
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void STM_DelayMs(u32 ms)
{
	u32 jiffies;

	STM_GetSysTick((u32*)&jiffies);
	ms/=10;
	
	while(1)
	{
		if(STM_CheckSysTick((u32*)&jiffies,ms))
		{
			break;
		}
		STM_RESET_DOG();
	}
}



