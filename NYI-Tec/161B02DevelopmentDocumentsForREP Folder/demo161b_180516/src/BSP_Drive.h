
/******************************************************************************
* Function Name -->
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/

#ifndef _bsp_drive_h_
#define _bsp_drive_h_

#define MCU_GOIOX 				GPIOC
#define MCU_HOLD_PIN 			GPIO_Pin_4		//
#define MCU_HOLD_ON			    GPIO_WriteBit(MCU_GOIOX, MCU_HOLD_PIN, Bit_SET)
#define MCU_HOLD_OFF			GPIO_WriteBit(MCU_GOIOX, MCU_HOLD_PIN, Bit_RESET)

#define MCU_BUZZ_GPIOX			GPIOC
#define MCU_BUZZ_PIN 			GPIO_Pin_8
#define MCU_BUZZ_ON			    GPIO_WriteBit(MCU_BUZZ_GPIOX, MCU_BUZZ_PIN , Bit_SET)
#define MCU_BUZZ_OFF			GPIO_WriteBit(MCU_BUZZ_GPIOX, MCU_BUZZ_PIN , Bit_RESET)

#define MCU_TEST_GPIOX			GPIOC
#define MCU_TEST_PIN 			GPIO_Pin_2
#define MCU_TEST_PIN_HIGH		do{GPIO_ReadInputDataBit(MCU_TEST_GPIOX, MCU_TEST_PIN)==Bit_SET}while(0)
#define MCU_TEST_PIN_LOW		do{GPIO_ReadInputDataBit(MCU_TEST_GPIOX, MCU_TEST_PIN)==Bit_RESET}while(0)


void MCU_HOLD_ON_CONTROL(void);
void MCU_HOLD_OFF_CONTROL(void);
void MCU_HoldInit(void);

extern u16 BSP_ADcPower12v(void);
extern u16 BSP_ADcPower5v(void);
extern u16 BSP_ADcPowerBattery(void);

extern u16 BSP_ADcVrefint(void);

extern void BSP_ADCInit(void);

extern void BSP_adcStart(void);

void STM_GetSysTick(u32* n);
u8 STM_CheckSysTick(u32* n,u32 time);
void STM_DelayMs(u32 ms);
#endif




