
#ifndef _bt_drive_h_
#define _bt_drive_h_

#define 	BT_UART 			5
#define BT_TX_LEN			(512+128)
#define BT_RX_LEN			(512+128)
#define BT_UART_RX_LEN		1024

#define BT_GPIOX			GPIOA
#define BT_PIN				GPIO_Pin_1
#define BT_POWER_ON			do{GPIO_WriteBit(BT_GPIOX, BT_PIN, Bit_SET);}while(0)
#define BT_POWER_OFF		do{GPIO_WriteBit(BT_GPIOX, BT_PIN, Bit_RESET);}while(0)

#define BT_WAKEUP_GPIO		GPIOA
#define BT_WAKEUP_PIN		GPIO_Pin_15
#define BT_WAKEUP_HIGH		GPIO_WriteBit(BT_WAKEUP_GPIO, BT_WAKEUP_PIN, Bit_SET);
#define BT_WAKEUP_LOW		GPIO_WriteBit(BT_WAKEUP_GPIO, BT_WAKEUP_PIN, Bit_RESET);


typedef enum
{
	BT_DISCONNECT	= 0,
	BT_CONNECT_OK   = 1,
}BT_CONNECT;


struct st_bt_sleep
{
    u32 tmr;
    u8  run;
};
extern struct st_bt_sleep bt_sleep;

extern u8 btDeviceName[19 + 8];//[17];

extern u8 btRenameDone;

typedef struct 
{
	u8 btID;
	u8 btMode;
	u8 btConnect;
	u8 btMAC[6];
	u16 Handle;
	u8 write_type;
	u16 IdRxLen;
	u16 IdTxLen;
	u8 btTxBuf[BT_TX_LEN];
	u8 btRxBuf[BT_RX_LEN];
    u16 rxHead;
    u16 rxTail;
} BT_LINK_STATE;

typedef struct 
{
	u8 currentTab;
	u8 currentId;
	u8 currentMode;
	u16 currentLen;
	BT_LINK_STATE  bt_LinkState[4] ;
} BT_MESSAGE;

extern BT_MESSAGE bt_Message;

extern u32 BtuartTimeDelay;
	
typedef struct 
{
	u16 iHead;
	u16 iTail;
	u8 UartbtRxBuf[BT_UART_RX_LEN];
} Uart_bt;
extern Uart_bt btBuf ;

extern void bt_wakeup_dly(void);

extern void bt_sleep_run(void);

extern void bt_sleep_set(void);

extern void bt_PowerOn(void);

extern void bt_PowerOff(void);

extern void bt_PowerCtrIOInit(void) ;

extern u8   bt_GetLinkState(void);

extern void bt_SendData(BT_MESSAGE *bt_Message);

extern void bt_sendDataSet(u8 *src , u16 len ,u8 ch);

extern void bt_poll(void);

#endif



