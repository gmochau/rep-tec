
#include "STM32_config.h"			//

#define PACK_LEN	255

#pragma pack(1)
typedef struct 
{
	u8 ack;
	u16 PacketLength;
	u8 mac[6];
	u16 Handle;
	u8 write_type;

}D_HAED;
D_HAED d_head;

static u8 MacBuf[6];

u8 btDeviceName[19 + 8] = {0};

u8 btRenameDone = 0;

#pragma pack()

BT_MESSAGE bt_Message = {0};

u32 BtuartTimeDelay = 0;

Uart_bt btBuf = {0} ;

u8 BtInitFlag = 0;

static const u8 btzyCommandHead[] = {"itcz"};
//static const u8 btzyAckHaed[] = {"itaz"};

static u8 stateCnt[sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0])] = {0};

typedef enum {
	CMD_TEST = 0,				 	/*（0x00）设备测试*/
	CMD_VERSION = 1, 				/*（0x01）获取软件版本*/
	CMD_RESET = 2,				 	/*（0x02）模块复位*/
	CMD_ORGL = 3, 					/*（0x03）恢复出厂状态(清除各种设置)*/
	CMD_STATE = 11, 				/*（0x0b）获取蓝牙工作状态*/
	CMD_DISCOVERABLE = 13, 		/*（0x0d）可发现和广播状态控制*/
	CMD_READADDR = 20, 			/*（0x14）读取蓝牙模组的地址*/
	CMD_REENDISCOVERABLE = 22, 	/*（0x16）控制断开后是否自动进入可发现状态*/
	CMD_LEADVPARAMS = 23, 			/*（0x17）设置LE广播间隔时间*/ //(GATT协议专用)
	CMD_RSSI = 24, 					/*（0x18）获取连接信号强度*/
	CMD_LECONPARAMS = 25, 			/*（0x19）设置LE连接间隔时间*///(GATT协议专用)
	CMD_UARTBAUD = 26, 				/*（0x1a）设置UART波特率*/
	CMD_RENAME = 27, 				/*（0x1b）修改设备名*/
	CMD_MODBTADDR = 28, 			/*（0x1c）修改模组蓝牙地址*/
	CMD_POLL_TIME = 33, 				/*（0x21）报告发送间隔时间(ms)*/
	CMD_MOD_IF_CHECKSUM = 38, 		/*（0x26）控制是否使用checksum*/
	CMD_SETUP_PAIRPARAM = 39, 		/*设置配对方式（只在uart上有效）*/
	CMD_IAP2_START = 42, 			//（0x2a）用于IAP2
	CMD_IAP2_FILE = 44, 				//（0x2c）用于IAP2
	CMD_IAP2_FINISH = 46, 			//（0x2e）用于IAP2
	CMD_SETUP_BEACON = 51, 			/*设置Beacon参数（只在uart上有效）*/
	CMD_SET_GPIO = 52, 				/*设置gpio电平*/
	CMD_ADV_DUAL = 54, 				/*设置是否让android识别为双模模组*/
	CMD_TESTMODE = 62, 				/*进入测试模式*/
	CMD_OTAMODE = 65, 				/*进入OTA模式*/
									//
	CMD_LE_SET_SCAN = 66, 			
									/*LE使能扫描周围设备*///(GATT协议主机专用)
	CMD_LE_CONNECT = 67,			/*LE连接特定地址设备*///(GATT协议主机专用)
	CMD_LE_EXIT_CONNECT = 68, 		/*LE退出连接特定地址设备*///(GATT协议主机专用)
	CMD_LE_DISCONNECT = 69, 		/*LE断开特定设备连接*///(GATT协议主机专用)
	CMD_LE_FIND_SERVICES = 70, 		/*LE查找服务*///(GATT协议主机专用)
	CMD_LE_FIND_CHARA = 71, 		/*LE查找属性*///(GATT协议主机专用)
	CMD_LE_FIND_DESC = 72, 			/*LE查找描述*///(GATT协议主机专用)

	CMD_LE_SET_NOTIFY = 73, 			/*LE设置使能从机的通知功能*///(GATT协议主机专用)
	CMD_LE_XFER2S = 74, 				/*LE作为主机角色收发数据,接收从机通知数据*///(GATT协议主机专用)
	CMD_LE_READ = 75, 				/*LE作为主机角色读属性*///(GATT协议主机专用)
	CMD_LE_REMOTEN = 76, 			/*LE查询当前连接的设备*///(GATT协议专用)
	CMD_DM_XFER2H = 77, 			/*LE&Classic作为从机角色收发数据,接收主机写数据*/
	CMD_DM_CONNSTATE = 78, 		/*LE&Classic查询多连接状态*/
	CMD_IAPMODE = 79, 				/*进入IAP模式*/
	CMD_USE_WECHAT = 80, 			/*是否使用wechat协议*/
	CMD_CLASSIC_REMOTEN = 81, 			
	CMD_BEACON_ONOFF = 82, 			/*BEACON开关*/
	CMD_ADV_SWITCHTIME = 83, 		/*BEACON和ADV切换的间隔时间*/
	//
	CMD_READ_NAME = 0x801B, 		/*读设备名*/
	CMD_MODIFY_COMPUTE_ID =0x8080, /*修改广播数据里的厂商识别代码*/
    
    CMD_RE_SEND = 0xfffe,
	CMD_UNKNOW = 0xFFFF, 				/*未知命令*/
} Command_t;


struct st_bt_sleep bt_sleep = {0};

void bt_wakeup_dly(void)
{    
    #if 0
    bt_sleep.run = 0;
    BT_WAKEUP_LOW;
    if(		(ftpSendPara.step == c_sendStep_run)
            &&	(ftpSendPara.src == ELD_PACKET_bt) )
    {
        return;
    }
    
    delay_us(500);
    #endif
}

void bt_sleep_run(void)
{
    #if 0
    if(		(ftpSendPara.step == c_sendStep_run)
            &&	(ftpSendPara.src == ELD_PACKET_bt) )
    {
        return;
    }
    
    if( (bt_sleep.run == 1) && ((dbg_tmr - bt_sleep.tmr) > 100) )
    {
        bt_sleep.run = 0;
        BT_WAKEUP_HIGH;
    }
    #endif
}

void bt_sleep_set(void)
{
    #if 0
    if(		(ftpSendPara.step == c_sendStep_run)
            &&	(ftpSendPara.src == ELD_PACKET_bt) )
    {
        return;
    }
    
    bt_sleep.tmr = dbg_tmr;
    bt_sleep.run = 1;
    #endif
}

void bt_PowerCtrIOInit(void)  //
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//GPIOB时钟
	
	//BT 电源控制
	GPIO_InitStructure.GPIO_Pin = BT_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(BT_GPIOX, &GPIO_InitStructure);


	GPIO_InitStructure.GPIO_Pin = BT_WAKEUP_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(BT_WAKEUP_GPIO, &GPIO_InitStructure);    
    //BT_WAKEUP_HIGH;
    BT_WAKEUP_LOW;
    
}


void bt_PowerOn(void)
{
	BT_POWER_ON;

	dbg(BT_DBG_EN,"BT POWER ON...................OK.\r\n");


}

void bt_PowerOff(void)
{	
	BT_POWER_OFF;

	dbg(BT_DBG_EN,"BT POWER OFF...................OK.\r\n");

}

u8 get_PayloadDataSum(u8 *str, u16 len)
{
	u8 sum = 0;
	u16 i = 0;
	for(i=0; i<len; ++i)
	{
		sum += *str++;
	}
	sum = 0xFF - sum + 0x01;
	return sum;
}

u8 bt_GetLinkState(void)
{
	u8 i = 0;
	u8 ret = BT_DISCONNECT;

	for(i = 0; i<sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]) ;++i)
	{
		if(BT_CONNECT_OK == bt_Message.bt_LinkState[i].btConnect) break;
	}
	if(i < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]))
	{
		ret = BT_CONNECT_OK;
	}
	return ret;
}

u8 bt_macAddrCmp(u8 *src, u8 *ptr)
{
	u8 i;
	
	for(i = 0; i<6; ++i)
	{
		if(src[i] != ptr[i])
		{
			return c_ret_nk;
		}
	}
	return c_ret_ok;
	
}

u8 bt_lookatNullMACAddr(D_HAED *head, u8 *tab)
{
	u8 i;
	
	for(i = 0; i<sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); ++i)
	{
		if(     bt_Message.bt_LinkState[i].btMAC[0] == 0x00 
            &&  bt_Message.bt_LinkState[i].btMAC[1] == 0x00 
            &&  bt_Message.bt_LinkState[i].btMAC[2] == 0x00
            &&  bt_Message.bt_LinkState[i].btMAC[3] == 0x00 
            &&  bt_Message.bt_LinkState[i].btMAC[4] == 0x00 
            &&  bt_Message.bt_LinkState[i].btMAC[5] == 0x00 )
        {
            *tab = i;
            bt_Message.bt_LinkState[i].btConnect = BT_CONNECT_OK;
            memcpy(bt_Message.bt_LinkState[i].btMAC, head->mac, 6);
            bt_Message.bt_LinkState[i].Handle = head->Handle;
            bt_Message.bt_LinkState[i].write_type= head->write_type;
            
            return c_ret_ok;
        }
	}
	return c_ret_full;
	
}

u8 bt_lookatConnect(D_HAED *head, u8 *tab)
{
	u8 i, connectStaus;
	
		
	for(i = 0; i<sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); ++i)
	{
        connectStaus = bt_macAddrCmp(bt_Message.bt_LinkState[i].btMAC, head->mac);
        if(c_ret_ok == connectStaus)
        {
            *tab = i;
            return c_ret_ok;
        }
	}

	connectStaus = bt_lookatNullMACAddr(head, (u8 *)&i);
	*tab = i;
	return connectStaus;
	
}

u8 bt_CommandPack(u16 command, u8 *str, u16 len)
{
	u8 buf[32];
	u16 n = 0;
	u8 checksum = 0;
	u16 temp = 0;

	memset(buf, 0x00, sizeof(buf));
	
	memcpy(buf, btzyCommandHead, strlen((char *)btzyCommandHead));
	n += strlen((char *)btzyCommandHead);

	command = SYSTOBE_S(command);
	memcpy(&buf[n], (char *)&command, sizeof(u16));
	n += sizeof(u16);

	temp = len;
	temp = SYSTOBE_S(temp);
	//memcpy(&bt_buf[n], (char *)&len, sizeof(u16));
	memcpy(&buf[n], (char *)&temp, sizeof(u16));
	n += sizeof(u16);

	memcpy(&buf[n], str, len);
	n += len;
	checksum = get_PayloadDataSum(str,len);
	buf[n] = checksum;
	n += 1;
	
    UartSendByte(BT_UART , buf, n);
	
	return 0;
}

void bt_enableCheckSum(u16 command)
{
	u8 str = 1; // 0:禁止累加和校验，1:允许累加和校验
	bt_CommandPack(command, &str, sizeof(u8));
}

void bt_onlyCommand(u16 command)
{
	bt_CommandPack(command, NULL, 0);
}

u8 bt_SendCommand(u16 command)
{

	switch(command)
	{
		case CMD_MOD_IF_CHECKSUM:
			bt_enableCheckSum(command);
			break;

		case CMD_LE_REMOTEN:
			bt_onlyCommand(command);
			break;
		
		case CMD_CLASSIC_REMOTEN:
			bt_onlyCommand(command);
			break;

        case CMD_VERSION:
            bt_onlyCommand(command);
        break;
        
        case CMD_RENAME:
            bt_CommandPack(command, btDeviceName, strlen((char*)btDeviceName));
        break;
        
        case CMD_READ_NAME:
			bt_onlyCommand(command);
		break;            
            
	}
	
	return 0;
}

u16 bt_ProtocolFormatPack(u8 * ptr, u16 command, u8 *scr, BT_MESSAGE * msg, u16 len,u8 idx)
{

	u16 n = 0;
	u16 temp = 0;
	u8 checksum = 0;
	
	memcpy(ptr, btzyCommandHead, strlen((char *)btzyCommandHead));
	n += strlen((char *)btzyCommandHead);
	
	command = SYSTOBE_S(command);
	memcpy(ptr+n, (char *)&command, sizeof(u16));
	n += sizeof(u16);

	temp = len + 9;
	temp = SYSTOBE_S(temp);
	memcpy(ptr+n, (char *)&temp, sizeof(u16));
	n += sizeof(u16);
	
	memcpy(ptr+n, msg->bt_LinkState[idx].btMAC, 6);
	n += 6;

	temp = msg->bt_LinkState[idx].Handle;
	temp = SYSTOBE_S(temp);
	memcpy(ptr+n, &temp, 2);
	n += 2;
	
	ptr[n++] = msg->bt_LinkState[idx].write_type;
	
	memcpy(ptr+n, scr, len);
	n += len;
	
	checksum = get_PayloadDataSum(ptr+8, n-8);
	ptr[n++] = checksum;

	return n;

}

void bt_sendDataSet(u8 *src , u16 len, u8 ch)
{
	u8 x;


	dbg(BT_DBG_EN,"\r\n**** BT SEND REQ Len:[%d] ****\r\n",len);

    if(ch >= sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]))
    {
        for(x = 0; x < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); x++)
        {
            if(bt_Message.bt_LinkState[ x ].btConnect == BT_CONNECT_OK)
            {
                memcpy(bt_Message.bt_LinkState[ x ].btTxBuf, src, len);
                bt_Message.bt_LinkState[ x ].IdTxLen = len;				
            }
        }
    }
    else
    {
        if(bt_Message.bt_LinkState[ ch ].btConnect == BT_CONNECT_OK)
        {
            memcpy(bt_Message.bt_LinkState[ ch ].btTxBuf, src, len);
            bt_Message.bt_LinkState[ ch ].IdTxLen = len;				
        }
    }
}

void bt_SendData(BT_MESSAGE *bt_Message)
{
	u32 x, n, len, cnt;
	u8 TxBuf[PACK_LEN+50] = {0};

		
    for(x = 0; x < sizeof(bt_Message->bt_LinkState)/sizeof(bt_Message->bt_LinkState[0]); x++)
    {
        if(bt_Message->bt_LinkState[ x ].btConnect == BT_CONNECT_OK)
        {	
            if(bt_Message->bt_LinkState[ x ].IdTxLen != 0)
            {
                dbg(BT_DBG_EN,"BT SEND to %02X:%02X:%02X:%02X:%02X:%02X\r\n",
                    bt_Message->bt_LinkState[ x ].btMAC[0],
                    bt_Message->bt_LinkState[ x ].btMAC[1],
                    bt_Message->bt_LinkState[ x ].btMAC[2],
                    bt_Message->bt_LinkState[ x ].btMAC[3],
                    bt_Message->bt_LinkState[ x ].btMAC[4],
                    bt_Message->bt_LinkState[ x ].btMAC[5]);
            }
            
            for(len = 0; len < bt_Message->bt_LinkState[ x ].IdTxLen; )
            {

                cnt = bt_Message->bt_LinkState[ x ].IdTxLen - len;
                
                if(cnt >= PACK_LEN) cnt = PACK_LEN;
                
                memset(TxBuf, 0x00, sizeof(TxBuf));
                
                n = bt_ProtocolFormatPack(TxBuf, 
                                            CMD_DM_XFER2H, 
                                            &bt_Message->bt_LinkState[ x ].btTxBuf[len], 
                                            bt_Message, 
                                            cnt,x
                                            );
                                            
                UartSendByte(BT_UART , TxBuf, n);
                len += PACK_LEN;
                delay_ms(10);
                
            }
            bt_Message->bt_LinkState[ x ].IdTxLen = 0x00;
        }
        else
            bt_Message->bt_LinkState[ x ].IdTxLen = 0x00;
    }
}


void bt_recv_CheckSumAck(u8 *pata)
{
	if(*pata == 1)
	{	
		BtInitFlag = 1;

		dbg(BT_DBG_EN,"BT INTIL...................[OK].\r\n");

	}
}

//                    ack  len    
//69 74 61 7A 00 51 //01   00 01  01   FF 
//69 74 61 7A 00 51 //01   00 09  01   01  01  0C D6 BD F7 A9 6A  54 
u16 bt_recv_queryLinkState(u8 *pata, u8 mode)   // 每循环结束(6秒超时),计数没变化表示断开连接
{
	u16 len;
	u8 i;
	    
    if(*pata != 1) return 0;
    
    pata++;
	len = *(u16*)pata;		// Packet_length
	len = SYSTOBE_S(len);	
	pata += 2;

	if(len >= 9)                // 应答2
	{
		pata += 3;

		if(memcmp(&MacBuf, pata, 6) != 0)
		{	
			dbg(BT_DBG_EN,"BT CONNECT MAC MODE [%s].[%02X:%02X:%02X:%02X:%02X:%02X].\r\n", (mode == CMD_LE_REMOTEN) ? "LE" : "CL",
																			  	pata[0],pata[1],pata[2],
																	  			pata[3],pata[4],pata[5] 
															 			   );	
		}

		memcpy(&MacBuf, pata, 6);

		for(i = 0; i < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); i++)
		{
			if(	memcmp(bt_Message.bt_LinkState[i].btMAC, pata, 6) == 0x00) 
            {
                stateCnt[i]++;
				break;
            }
		}
		if(	i >= sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]) )
		{	// 新加入连接
			for(i = 0; i < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); i++)
			{
				if(     (bt_Message.bt_LinkState[i].btMAC[0] == 0x00)
					&& 	(bt_Message.bt_LinkState[i].btMAC[1] == 0x00) 
					&& 	(bt_Message.bt_LinkState[i].btMAC[2] == 0x00)
					&&	(bt_Message.bt_LinkState[i].btMAC[3] == 0x00) 
					&& 	(bt_Message.bt_LinkState[i].btMAC[4] == 0x00) 
					&& 	(bt_Message.bt_LinkState[i].btMAC[5] == 0x00) )
				{   // 8个连接满了，丢弃新加入的连接
					bt_Message.bt_LinkState[i].btConnect = BT_CONNECT_OK;
					memcpy(bt_Message.bt_LinkState[i].btMAC, pata, 6);	// addr[6]
					bt_Message.bt_LinkState[i].Handle = 0xff01;
					bt_Message.bt_LinkState[i].write_type = 0x05;
                    
                    stateCnt[i]++;
                    
                    bt_sendDataSet("Hello demo161B!\r\n", sizeof("Hello demo161B!\r\n") - 1, i);
                    
					break;
				}
			}
		}			
	}
	return len;

}

u16 bt_recv_DataAnalysis(u8 *pata)
{
	u16 len;
	u8 status;
	u8 tab;
	
	d_head = *(D_HAED*)pata;
	
    pata += sizeof(d_head);
	d_head.PacketLength = SYSTOBE_S(d_head.PacketLength);
	d_head.Handle = SYSTOBE_S(d_head.Handle);
	
	status = bt_lookatConnect(&d_head, &tab);
	if(c_ret_ok == status)
	{
		bt_Message.currentTab = tab;
		bt_Message.bt_LinkState[tab].IdRxLen = d_head.PacketLength - sizeof(D_HAED) + 3;
		bt_Message.currentLen += bt_Message.bt_LinkState[tab].IdRxLen;
        
        for(len = 0; len < bt_Message.bt_LinkState[tab].IdRxLen; len++)
        {
            bt_Message.bt_LinkState[tab].btRxBuf[bt_Message.bt_LinkState[tab].rxHead++] = pata[len];
            if(bt_Message.bt_LinkState[tab].rxHead >= sizeof(bt_Message.bt_LinkState[tab].btRxBuf))
                bt_Message.bt_LinkState[tab].rxHead = 0;
            if(bt_Message.bt_LinkState[tab].rxHead == bt_Message.bt_LinkState[tab].rxTail)
            {
                bt_Message.bt_LinkState[tab].rxTail++;
                if(bt_Message.bt_LinkState[tab].rxTail >= sizeof(bt_Message.bt_LinkState[tab].btRxBuf))
                {
                    bt_Message.bt_LinkState[tab].rxTail = 0;
                }
            }
        }  
        bt_Message.bt_LinkState[tab].btRxBuf[bt_Message.bt_LinkState[tab].rxHead] = 0;
	}
	else if(c_ret_full == status)
	{

		dbg(BT_DBG_EN,"BT CONNECT_FULL....................\r\n");

	}
	return d_head.PacketLength;
}

u16 bt_rece_version(u8* src)
{
    u16 len = 0;
    
    if(*src == 1)
    {
        src++;
        len = *(u16*)src;		// Packet_length
        len = SYSTOBE_S(len);	
        src += 2;	
        if(len == 3)
        {
            BtInitFlag = 2;
            dbg(BT_DBG_EN,"BT VERSION:%d.%d%d\r\n",src[0],src[1],src[2]);
        }        
    }
    return len;
}

u16 bt_recv_readName(u8* src)   // 最大长度16字节
{    
    u8 c;
    u16 x, y, i, len = 0;
    
    if(*src == 1)
    {
        src++;
        len = *(u16*)src;		// Packet_length
        len = SYSTOBE_S(len);	
        src += 2;	
        
        src[len] = 0;
        if( strstr((char*)src,"161B_") != NULL)
        {
            BtInitFlag = 4;        
            dbg(BT_DBG_EN,"BT RENAME:%s\r\n", src);            
        }
        else
        {
            strcpy((char*)btDeviceName,"161B_");
            y = strlen((char*)src) - 1;
            for(x = 0; x < 8; x++)
            {
                c = src[y - x];
                if(     /*(c == '_')
                    ||*/  ((c >= '0')&&(c <= '9'))
                    ||  ((c >= 'a')&&(c <= 'f'))
                    ||  ((c >= 'A')&&(c <= 'F')) )
                {
                    for(i = 0; i < 8; i++)
                    {    
                        btDeviceName[13 - i] = btDeviceName[13 - i - 1];        
                    }
                    btDeviceName[5] = c;
                }
                else break;
            }

            BtInitFlag = 3;
            
            dbg(BT_DBG_EN,"BT NAME:%s\r\n",btDeviceName + 5);
        }
    }
    return len;

}

//  command_t[h-l] response_t[1] packetLen[h-l] data[9~n-1] checksum[n]
u16 bt_recv_CommandAnalysis(u8 *src)
{
	u16 command;
	u16 len;
	
	command =* (u16*)src;
    src += 2;
	command = SYSTOBE_S(command);
        
    len = 0;
	switch(command)
	{
		case	CMD_MOD_IF_CHECKSUM:    //resp:[0/1]+len(0)+chksum(x)
			bt_recv_CheckSumAck(src);
			break;
		case	CMD_LE_REMOTEN:         //resp:[0/1]+len:0x0001+remoten(x)+chksum(x)    resp:[0/1]+len:0x0009+remoten+connid+role+addr[0~5]+chksum[x]
			len = bt_recv_queryLinkState(src, CMD_LE_REMOTEN);
			break;		
		case	CMD_CLASSIC_REMOTEN:
			len = bt_recv_queryLinkState(src, CMD_CLASSIC_REMOTEN);
			break;
		case CMD_DM_XFER2H:
			len = bt_recv_DataAnalysis(src);
			break;
		case	CMD_VERSION:
            len = bt_rece_version(src);
		break;

		case	CMD_READ_NAME:
            len = bt_recv_readName(src);
		break;

        /*    
		case	CMD_TEST:
		case	CMD_RESET:
		case	CMD_ORGL:
		case	CMD_STATE:
		case	CMD_DISCOVERABLE:
		case	CMD_READADDR:
		case	CMD_REENDISCOVERABLE:
		case	CMD_LEADVPARAMS:
		case	CMD_RSSI:
		case	CMD_LECONPARAMS:
		case	CMD_UARTBAUD:
		case	CMD_MODBTADDR:
		case	CMD_POLL_TIME:
		case	CMD_SETUP_PAIRPARAM:
		case	CMD_IAP2_START:
		case	CMD_IAP2_FILE:
		case	CMD_IAP2_FINISH:
		case	CMD_SETUP_BEACON:
		case	CMD_SET_GPIO:
		case	CMD_ADV_DUAL:
		case	CMD_TESTMODE:
		case	CMD_OTAMODE:
		case	CMD_LE_SET_SCAN:
		case	CMD_LE_CONNECT:
		case	CMD_LE_EXIT_CONNECT:
		case	CMD_LE_DISCONNECT:
		case	CMD_LE_FIND_SERVICES:
		case	CMD_LE_FIND_CHARA:
		case	CMD_LE_SET_NOTIFY:
		case	CMD_LE_XFER2S:
		case	CMD_LE_READ:
		case	CMD_DM_CONNSTATE:
		case	CMD_IAPMODE:
		case	CMD_USE_WECHAT:
		case	CMD_BEACON_ONOFF:
		case	CMD_ADV_SWITCHTIME:
		case	CMD_MODIFY_COMPUTE_ID:
		case	CMD_UNKNOW:
        case    CMD_LE_FIND_DESC:
        */
        default:
        break;

	}

	return len + 2 + 2;//Response_t(1),Packet Length(2) ,CHECKSUM(1)
}

void bt_rece_parse(void)
{
    static enum
    {
        c_step_head,
        c_step_cmdh,
        c_step_cmdl,
        c_step_resp,
        c_step_lenh,
        c_step_lenl,
        c_step_dat,
        c_step_chksum,
    }step = c_step_head;
    static u8 buf[4] = {0};
    static u8 datBuf[BT_RX_LEN];
    static u16 cmd = 0xffff;
    static u16 len = 0xffff;
    static u16 idx = 0;
    u8 dat, sum;
    
    while(btBuf.iTail != btBuf.iHead)
    {
        dat = btBuf.UartbtRxBuf[btBuf.iTail];
        btBuf.iTail++;
        if(btBuf.iTail >= sizeof(btBuf.UartbtRxBuf)) 
            btBuf.iTail = 0; 
        
        buf[0] = buf[1];
        buf[1] = buf[2];
        buf[2] = buf[3];
        buf[3] = dat;
                
        switch(step)
        {
            case c_step_head:
                if((buf[0] == 'i')&&(buf[1] == 't')&&(buf[2] == 'a')&&(buf[3] == 'z')) 
                    step = c_step_cmdh;
            break;

            case c_step_cmdh:cmd = dat << 8;step = c_step_cmdl;datBuf[0] = dat;break;

            case c_step_cmdl:cmd += dat;step = c_step_resp;datBuf[1] = dat;break;
            
            case c_step_resp:
                if(dat == 1)
                {
                    datBuf[2] = dat;
                    step = c_step_lenh;
                }
                else
                {
                    buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                    step = c_step_head;
                }
            break;

            case c_step_lenh:len = dat << 8;step = c_step_lenl;datBuf[3] = dat;break;

            case c_step_lenl:
                datBuf[4] = dat;
                len += dat;
                if(len >= sizeof(datBuf))
                {
                    buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                    step = c_step_head;
                }  
                else if(len == 0)
                {   //command_t[h-l] response_t[1] packetLen[h-l] data[9~n-1] checksum[n]
                    bt_recv_CommandAnalysis(datBuf);
                    buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                    step = c_step_head;                    
                }                    
                else 
                {
                    idx = 0;
                    step = c_step_dat;
                }
            break;
            
            case c_step_dat:
                datBuf[5 + idx++] = dat;
                if(idx == len)
                    step = c_step_chksum;
            break;

            case c_step_chksum:
                sum = 0;
                for(idx = 0; idx < len; idx++)
                    sum += datBuf[5 + idx];
                sum = 0xff - sum + 1;
                if(sum != dat) 
                {
                    buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                    step = c_step_head;
                }                    
                else 
                {
                    bt_recv_CommandAnalysis(datBuf);
                    buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                    step = c_step_head;
                }
            break;
                
            default:
                buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                step = c_step_head;
            break;            
        }        
    }
}

void bt_FormatAnalysis(void)
{  
	u16 n = 0;
    
	for(n = 0; n<sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); ++n)
	{
		bt_Message.bt_LinkState[n].IdRxLen = 0;
	}
	bt_Message.currentLen = 0;

    bt_rece_parse();    
    
}

void bt_QueryLinkCommand(void)  // 每6秒循环
{
	static u8 cnt = 0;
    u8 x;
    
    cnt++;
    if(cnt == 0x01)
    {
        bt_SendCommand(CMD_CLASSIC_REMOTEN);
    }
    else if(cnt == 2)
    {
        bt_SendCommand(CMD_LE_REMOTEN);
    }
    else if(cnt == 3)
    {
        for(x = 0; x < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); x++)
        {
            if(stateCnt[x] == 0)
            {
                bt_Message.bt_LinkState[x].btConnect = BT_DISCONNECT;
				bt_Message.bt_LinkState[x].btMAC[0] 
                    = bt_Message.bt_LinkState[x].btMAC[1] 
                    = bt_Message.bt_LinkState[x].btMAC[2] 
                    = bt_Message.bt_LinkState[x].btMAC[3] 
                    = bt_Message.bt_LinkState[x].btMAC[4] 
                    = bt_Message.bt_LinkState[x].btMAC[5] = 0x00;
                
            }
            else
                bt_Message.bt_LinkState[x].btConnect = BT_CONNECT_OK;
            
            stateCnt[x] = 0;    
        }
    }
    if(cnt >= 3) cnt = 0;
		
}

void bt_poll(void)
{
    static u8 step = 0;
	static u32 tmr = 0;
    static u8 connectStatus = BT_DISCONNECT;
    u8 buf[128], x, idx;
    
    switch(step)
    {
        case 0:
            printf("\r\n[bt_Task].................OK\r\n");
            
            bt_PowerCtrIOInit();
            bt_PowerOn();
            
            tmr = tick_get();
            step = 1;
        break;

        case 1:
            if(tick_cmp(tmr, 2000) == c_ret_ok)
            {
                tmr = tick_get();
                step = 2;
            }
        break;

        case 2:            	
            if(BtuartTimeDelay >= 30 && (btBuf.iHead + BT_UART_RX_LEN - btBuf.iTail) % BT_UART_RX_LEN > 5)
            {
                BtuartTimeDelay = 0;
                bt_FormatAnalysis();
            }
            else 
            {
                if(BtInitFlag != 4)
                {
                    if(tick_cmp(tmr , 1000) == c_ret_ok)
                    {
                        tmr = tick_get();

                        if(BtInitFlag == 0)
                            bt_SendCommand(CMD_MOD_IF_CHECKSUM);
                        else if(BtInitFlag == 1)
                            bt_SendCommand(CMD_VERSION);                        
                        else if(BtInitFlag == 2)
                        {
                            if(btRenameDone == 1)
                                BtInitFlag = 4;
                            else
                                bt_SendCommand(CMD_READ_NAME); 
                        }
                        else if(BtInitFlag == 3)
                            bt_SendCommand(CMD_RENAME);                        
                    }
                }
                else if(tick_cmp(tmr , 2000) == c_ret_ok)
                {
                    tmr = tick_get();
                    bt_QueryLinkCommand();
                }
            }
            
            x = bt_GetLinkState();
            if( (x == BT_CONNECT_OK) && (connectStatus == BT_DISCONNECT))
                Led_SetParameter(LED_BT, LED_LIGHT, 0, 0);
            else if((x == BT_DISCONNECT) && (connectStatus == BT_CONNECT_OK))
                Led_SetParameter(LED_BT, LED_FLASH, 500, 1500);
            connectStatus = x;
            
            if(x == BT_CONNECT_OK)
            {                           
                for(x = 0; x < sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); x++)
                {
                    if(bt_Message.bt_LinkState[x].IdRxLen != 0)
                    {
                        idx = 0;
                        while(bt_Message.bt_LinkState[x].rxTail != bt_Message.bt_LinkState[x].rxHead)
                        {
                            buf[idx++] = bt_Message.bt_LinkState[x].btRxBuf[bt_Message.bt_LinkState[x].rxTail];
                            bt_Message.bt_LinkState[x].rxTail++;
                            if(bt_Message.bt_LinkState[x].rxTail >= sizeof(bt_Message.bt_LinkState[x].btRxBuf))
                            {
                                bt_Message.bt_LinkState[x].rxTail = 0;
                            }
                            if(idx >= sizeof(buf)) 
                            {
                                bt_Message.bt_LinkState[x].rxTail = bt_Message.bt_LinkState[x].rxHead;
                                break;
                            }
                        }                            
                        bt_Message.bt_LinkState[x].IdRxLen = 0;
                        
                        bt_sendDataSet(buf, idx, x);
                    }
                }
                
                bt_SendData(&bt_Message);                
            }
        break;

        default: step = 0; break;                     
	}
}
