/*******************************************************************************
* FILENAME DefineData.h
*******************************************************************************/
#ifndef _DEFINEDATA_H_
#define _DEFINEDATA_H_

#include "stm32_config.h"			//

extern u32 FlashReadDataAddr;
#pragma pack(1)
/**********************************************************************************/

/**********************************************************************************/
typedef union
{
	u32 DEBUG;
	struct DEBUG_STATUS
		{
		u32 GSM_LOG_EN			:1;  
		u32 xGSM_DEBUG_EN		:1;  
		u32 GPS_DEBUF_EN		:1;  
		u32 OBD_DEBUG_EN		:1; 
		u32 OBD_LOG_EN			:1;
		u32 BT_DEBUG_EN		:1;
		u32 SYS_DEBUG_EN		:1;  

		}debug_status;
}DEBUG_BIT;

//******************************************************************************
//主机参数设置
//******************************************************************************
typedef struct					
{
	u8   SerialNum[19];			//主机设备号
	u8   obdSerialNum[31];
	u8   EngineDisplacement;		//发动机排量
	u8   FuelType;				//燃油类型
	u8   LaguageType;			//语音类型
	u8   SleepMode;				//睡眠模式
	u8 	 gps_precision;			//	0:		1:
	
	u8   password[16];	
	u32	 DriverID;				// 当前认证司机
	u32  DriverIDAll[8];		// 
	
} UNIT_DATA;

//******************************************************************************
//短信参数
//******************************************************************************
typedef struct					
{
	u8    SecretKey[7];                	//SMS维护密码
	u8    SMSEnable;				//报警时是否需要发送短信使能位，= 0x00 关闭，默认关闭 = 0x01 打开
	u8    PhoneNum;				//电话号码数目
	u8    PhoneMode[3];			//拨号模式
	u8    PhoneNumber[3][22];        //电话号码，21个字节ASCII 字符，不足21位，以空字符“\0”补齐。
} SMS_PARAMETER;

//******************************************************************************
//网络参数设置
//******************************************************************************
typedef struct				
{
	u8	DailMode;                  	//拨号方式,=0x00,采用IP拨号方式 =0x01,采用域名拨号方式
	u8	Domain[64];				//域名
	u8 	Ip[20];						//IP地址
	u8 	Port[10];						//端口
	u8 	Apn[64];						
	u8 	User[64];
	u8 	Pwd[64];
	u8 	PinCode[10];
	u8  tcpudp;					//0:默认udp
}SYS_NETWORK_PARAMETER;
//PID类型
typedef struct				
{					
	u8 Pid[10];
}PID_TYPE;

//******************************************************************************

//******************************************************************************
typedef struct                              				  
{
	u8     Header[4];                        							//="@@@@";

	UNIT_DATA					UnitData;					//主机工作参数
	SYS_NETWORK_PARAMETER 	sysNetworkParameter;  		//网络参数
	SMS_PARAMETER			 	SmsParameter;				//设置用户手机号码
	PID_TYPE					PidType;
	
	u16     						CheckSum;                        	//SysData0的校验和

}SYS_PARAMETER;
extern SYS_PARAMETER SysParameter;

//******************************************************************************
//FLASH 存储参数
//******************************************************************************
typedef struct				
{					
	u16 SequenceID;
	u32 WaitConfirmTotal;				// 等待上传确认的日志总数
	u32 ConfirmDataAddr;				// 待确认数据地址 
	u32 WriteDataAddr;	           		// 写数据的地址
	u32 StartDataAddr;					// 有效记录日志开始地址 
	u32 ConfirmDataRtc;					// 最后一次确认时间	
	
	u32 ConfirmDataAddrBt;				// 蓝牙移动应用待确认数据地址 
}FLASH_PARA;


//******************************************************************************
//OBD运行参数
//******************************************************************************
typedef struct				
{					
	u32 total_fuel;						//总油耗
	u32 total_trip_mileage;				//总里程
	u32 total_engineHours;				//发动机运行总时间
    u16 carType;                         // 0:电压选择，其他:0001,ffff

}OBD_PARA;

//******************************************************************************
//ELD运行参数
//******************************************************************************
#define c_evt21para_vpwr    0xffff
#define c_evt21para_odom    0xfffe
#define c_evt21para_fuel    0xfffd
#define c_evt21para_hour    0xfffc
#define c_evt21para_code    0xfffb          // 故障状态
#define c_evt21para_vinc    0xfffa
#define c_evt21para_gxyz    0xfff9
#define c_evt21para_axyz    0xfff8
#define c_evt21para_trid    0xfff7
#define c_evt21para_trif    0xfff6
#define c_evt21para_trim    0xfff5

typedef struct				
{			
    u16 evt21freq;                      //1~3600 - 1,默认0
    u16 evt21freqAcc;                   //      
    u16 evt21para[32];                  // 0xffxx专用，不属于PID
    
}ELD_PARA;

//******************************************************************************
//******************************************************************************
typedef struct{

	u8    Header[4];                        			//="@@@@";

	OBD_PARA	OBD_Parameter;
	FLASH_PARA	FLASH_Parameter;
	ELD_PARA	ELD_parameter;
	
	u16	ReportId;
	//u32  SysRtcTime; 
	u8	VIN_CODE[20];
	//u8 	gsmFunDisable;
	u16  CheckSum;

}SYS_RUN_PARAMETER ;

extern SYS_RUN_PARAMETER SysRunParameter;

//******************************************************************************

//******************************************************************************
extern u32 STM_SysTickCnt;

#pragma pack()

extern DEBUG_BIT debug_bit;
                                                    
#endif

