/*
    ELD_event.c
*/

#include "stm32_config.h"

u8  evt_run = 0;                 // 运行标志

static void ELD_event_21_proc(void)
{
    static u32 tmr = 0;
    u8 tbuf[501];
    u32 x, y, i, para;
    
    if(     (ELD_data.acc_on == 0)
        ||  (SysRunParameter.ELD_parameter.evt21freq == 0) )
    {
        tmr = get_RTCCounter();
        return;
    }
    
    x = get_RTCCounter();
    if( (x - tmr) < SysRunParameter.ELD_parameter.evt21freq) return;
    tmr = x;
    
    tbuf[0] = 0;
    for(i = 0; i < sizeof(SysRunParameter.ELD_parameter.evt21para)/sizeof(SysRunParameter.ELD_parameter.evt21para[0]); i++)
    {
        para = SysRunParameter.ELD_parameter.evt21para[i];
        if(para == 0) break;
        if(para >= 0xff00)
        {
            switch(para)
            {
                case c_evt21para_vpwr:
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d.%d",Power12v/1000,(Power12v/100)%10);
                break;
                
                case c_evt21para_odom:
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d",SysRunParameter.OBD_Parameter.total_trip_mileage/1000);
                break;
                
                case c_evt21para_fuel:
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d",SysRunParameter.OBD_Parameter.total_fuel/10);
                break;
                
                case c_evt21para_hour:
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d",SysRunParameter.OBD_Parameter.total_engineHours/60);
                break;
                
                case c_evt21para_code:
                    for(y = x = 0; x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]); x++)
                    {
                        if(ELD_data.obd.storCode[x] != 0x00) y++;
                    }
                    if(y == 0)
                    {
                        for(y = x = 0; x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]); x++)
                        {
                            if(ELD_data.obd.pendCode[x] != 0x00) y++;
                        }                        
                    }
                    sprintf((char*)tbuf+strlen((char*)tbuf), "|%d", (y != 0) ? 1 : 0);
                break;
                    
                
                case c_evt21para_vinc:
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%s",SysRunParameter.VIN_CODE);
                break;
                
                case c_evt21para_gxyz:
                    switch(SysRunParameter.ELD_parameter.evt21freqAcc)
                    {
                        case 0:
                            sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                        break;
                        
                        case 1:
                            sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                            sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[0].gyro_x);
                            sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[0].gyro_y);
                            sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[0].gyro_z);
                        break;
                        
                        case 5:
                            for(x = 0; x < sizeof(ELD_data.mpu6050Data)/sizeof(ELD_data.mpu6050Data[0]); x+=2)
                            {
                                sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].gyro_x);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].gyro_y);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].gyro_z);
                            }                            
                        break;

                        case 10:
                            for(x = 0; x < sizeof(ELD_data.mpu6050Data)/sizeof(ELD_data.mpu6050Data[0]); x++)
                            {
                                sprintf((char*)tbuf+strlen((char*)tbuf),"|");                                
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].gyro_x);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].gyro_y);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].gyro_z);
                            }                                                        
                        break;        
                    }
                break;
                    
                case c_evt21para_axyz:                                    
                    switch(SysRunParameter.ELD_parameter.evt21freqAcc)
                    {
                        case 0:
                            sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                        break;
                        
                        case 1:
                            sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                            sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[0].accel_x);
                            sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[0].accel_y);
                            sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[0].accel_z);
                        break;
                        
                        case 5:
                            for(x = 0; x < sizeof(ELD_data.mpu6050Data)/sizeof(ELD_data.mpu6050Data[0]); x+=2)
                            {
                                sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].accel_x);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].accel_y);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].accel_z);
                            }                            
                        break;

                        case 10:
                            for(x = 0; x < sizeof(ELD_data.mpu6050Data)/sizeof(ELD_data.mpu6050Data[0]); x++)
                            {
                                sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].accel_x);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].accel_y);
                                sprintf((char*)tbuf+strlen((char*)tbuf),"%04X",(u16)ELD_data.mpu6050Data[x].accel_z);
                            }                            
                        break;
                    }
                break;
                
                case c_evt21para_trid:  // 米       
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d", ELD_data.obd.CurrentTripMileage );
                break;

                case c_evt21para_trif:  // 0.1L   
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d", ELD_data.obd.CurrentTripFuelConsumption);
                break;

                case c_evt21para_trim:  // 分钟   ---H   
                    sprintf((char*)tbuf+strlen((char*)tbuf),"|%d", ELD_data.obd.CurrentTripTime);    
                break;                
            }
        }
        else
        {
            for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
            {
                if(para == ELD_data.obd.pid[x].name)
                {
                    if(ELD_data.obd.pid[x].valid == 1)
                        sprintf((char*)tbuf+strlen((char*)tbuf),"|%d%s",ELD_data.obd.pid[x].val,ELD_data.obd.pid[x].unit);
                    else
                        sprintf((char*)tbuf+strlen((char*)tbuf),"|");
                    break;
                }
            }
            if(x >= sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]))   // 开始阶段或不支持
            {
                sprintf((char*)tbuf+strlen((char*)tbuf),"|");
            }                
        }
    }    
    ELD_packet_make_eld(2, 1, (tbuf[0]=='|') ? tbuf + 1 : tbuf); 
        
}

//   Engine power up发动机点火[3-1]      $$161B_DC2C26ADF321,5A5C,3,1,122417,162647,27.9,CS**
//   Engine shut down发动机熄火[3-2]     $$161B_DC2C26ADF321,5A5C,3,2,122417,162647,24.3,CS**
//   Device is power on设备上电[3-3]
void ELD_event_313233_proc(void)
{
    static u8 powerOn = 0;
    static u8 acc_on = 0;
    u8 tbuf[16];
    
    if(powerOn == 0)
    {
        powerOn = 1;

        sprintf((char*)tbuf,"%d.%d",Power12v/1000,(Power12v/100)%10);
        ELD_packet_make_eld(3,3,tbuf);  
        
        ELD_data.obd.accOnReq.b.vin = 1;        // 每次系统上电时读一次
    }
    
    if(ELD_data.acc_on != acc_on)
    {
        acc_on = ELD_data.acc_on;

        sprintf((char*)tbuf,"%d.%d",Power12v/1000,(Power12v/100)%10);
        if(acc_on == 1)
        {
            ELD_packet_make_eld(3,1,tbuf);
            
            ELD_data.obd.accOnReq.b.storCod = 
            ELD_data.obd.accOnReq.b.pendCod =
            ELD_data.obd.accOnReq.b.sup     = 1;    // 每次ACC ON时读一次

            ELD_data.obd.accOnWait.b.storCod = 
            ELD_data.obd.accOnWait.b.pendCod =
            ELD_data.obd.accOnWait.b.sup     = 0; 
            
			dbg(GSENSOR_DBG_EN,"\r\n**** ACC ON ****\r\n");
        }            
        else
        {    
            ELD_packet_make_eld(3,2,tbuf);
			memset(&ELD_data.obd,0,sizeof(ELD_data.obd));			
			dbg(GSENSOR_DBG_EN,"\r\n**** ACC OFF ****\r\n");
        }
    }            
}

//  Vehicle movement车辆运动[3-4]   车辆的速度大于8KM/H时产生此事件
//  Vehicle still车辆静止[3-5]      车辆的速度小于8KM/H，且持续3秒，产生此事件
void ELD_event_3435_proc(void)
{
	static enum
	{
		c_step_init,
		c_step_idle,
		c_step_run_chk,
		c_step_stop_chk,
	}step = c_step_init;
    static u32 tmr = 0;
    static u8 run = 0;
    static u8 stop= 1;
    u8 tbuf[16];
    
	switch(step)
	{
		case c_step_init:
			if(ELD_data.acc_on == 1) 
				step = c_step_idle;
		break;

		case c_step_idle:
            STM_GetSysTick(&tmr);
			step = (ELD_get_vehicle_run() == c_ret_ok) ? c_step_run_chk : c_step_stop_chk;
		break;

		case c_step_run_chk:
			if(ELD_get_vehicle_stop() == c_ret_ok)
            {
                STM_GetSysTick(&tmr);
				step = c_step_stop_chk;
            }
			else if((ELD_get_vehicle_run() == c_ret_ok)			//	连续3秒运行
				&&	(TRUE == STM_CheckSysTick((u32 *)&tmr,(u32)3*(1000/c_systick))) )
			{
                if(run == 0)
                {
                    run = 1;
                    sprintf((char*)tbuf,"%d",ELD_get_vehicle_speed());
                    ELD_packet_make_eld(3,4,tbuf);
                }
                stop = 0;
			}
		break;

		case c_step_stop_chk:
			if(ELD_get_vehicle_run() == c_ret_ok)
            {
                STM_GetSysTick(&tmr);
				step = c_step_run_chk;
            }
			else if((ELD_get_vehicle_stop() == c_ret_ok)
				&&	(TRUE == STM_CheckSysTick((u32 *)&tmr,(u32)3*(1000/c_systick))) )
			{
                if(stop == 0)
                {
                    stop = 1;
                    sprintf((char*)tbuf,"%d",ELD_get_vehicle_speed());
                    ELD_packet_make_eld(3,5,tbuf);
                }
                run = 0;
			}
		break;

		default:step = c_step_init;
		break;
	}	
}

//  Harsh acceleration急加速[3-6]     车辆加速且加速度超过0.4g以上时产生此事件   事件数据：加速度值，格式是浮点数字符串，单位是g
//  Harsh brake急减速[3-7]             车辆减速且加速度超过0.6g以上时产生此事件  事件数据：加速度值，格式是浮点数字符串，单位是g
//  Harsh cornering急转弯[3-8]         车辆转弯，航向角加速度值超过2弧度秒2时产生此事件   弧度/秒2（rad/s2）
//  Vehicle is dragged拖吊[3-9]      车辆被拖起来，俯仰角超过15度时产生此事件
//  Vehicle collision碰撞[3-10]       车辆发生碰撞，加速度值超过1g时产生此事件
void ELD_event_363738393a_proc(void)
{
}

//  Bluetooth connection蓝牙连接 [3-11]
//  Bluetooth is disconnected蓝牙断开[3-12]
void ELD_event_3b3c_proc(void)
{
    static struct st_bt_link
    {
        u8 btConnect;
        u8 btMac[6];        
    }bt_link[sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0])] = {0};
    u8 tbuf[32], x;
    
	for(x = 0; x<sizeof(bt_Message.bt_LinkState)/sizeof(bt_Message.bt_LinkState[0]); x++)
	{
		if(     (bt_Message.bt_LinkState[x].btConnect == BT_CONNECT_OK)
            &&  (bt_link[x].btConnect != BT_CONNECT_OK) )
        {
            bt_link[x].btConnect = BT_CONNECT_OK;
            memcpy(bt_link[x].btMac, bt_Message.bt_LinkState[x].btMAC, 6);
            
            sprintf((char*)tbuf, "%02X%02X%02X%02X%02X%02X|",
                    bt_link[x].btMac[0],bt_link[x].btMac[1],bt_link[x].btMac[2],
                    bt_link[x].btMac[3],bt_link[x].btMac[4],bt_link[x].btMac[5]);                        
            ELD_packet_make_eld(3, 11, tbuf);
        }
		else if((bt_Message.bt_LinkState[x].btConnect != BT_CONNECT_OK)
            &&  (bt_link[x].btConnect == BT_CONNECT_OK) )
        {
            bt_link[x].btConnect = BT_DISCONNECT;
            
            sprintf((char*)tbuf, "%02X%02X%02X%02X%02X%02X|",
                    bt_link[x].btMac[0],bt_link[x].btMac[1],bt_link[x].btMac[2],
                    bt_link[x].btMac[3],bt_link[x].btMac[4],bt_link[x].btMac[5]);
            ELD_packet_make_eld(3, 12, tbuf);            
        }           
	}        
}

void ELD_event_313_proc(void)
{
    u32 x;

    for(x = 0;x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]); x++)
    {
        if(ELD_data.obd.storCode[x] != 0x00) break;
    }
    ELD_data.response_bt[0] = 0;
    if(x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]))
    {
        for(x = 0;x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]); x++)
        {
            if(ELD_data.obd.storCode[x] != 0x00)
            {
                sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt),"|%04X",ELD_data.obd.storCode[x]);
            }
        }
    }
    ELD_packet_make_eld(3,13,(ELD_data.response_bt[0]=='|') ? ELD_data.response_bt + 1 : ELD_data.response_bt);    
}

void ELD_event_314_proc(void)
{
    u32 x;

    for(x = 0;x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]); x++)
    {
        if(ELD_data.obd.pendCode[x] != 0x00) break;
    }
    ELD_data.response_bt[0] = 0;    
    if(x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]))
    {
        for(x = 0;x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]); x++)
        {
            if(ELD_data.obd.pendCode[x] != 0x00)
            {
                sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt),"|%04X",ELD_data.obd.pendCode[x]);
            }
        }
    }
    ELD_packet_make_eld(3,14,(ELD_data.response_bt[0]=='|') ? ELD_data.response_bt + 1 : ELD_data.response_bt);    
}

void ELD_event_315_proc(void)
{
    u32 x;

    for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
    {
        if(ELD_data.obd.pid[x].name != 0) break;
    }
    ELD_data.response_bt[0] = 0;
    if(x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]))
    {        
        for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
        {
            if(ELD_data.obd.pid[x].name != 0)
            {
                sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt),"|%04X",ELD_data.obd.pid[x].name);
            }
        }
    }
    ELD_packet_make_eld(3,15,(ELD_data.response_bt[0]=='|') ? ELD_data.response_bt + 1 : ELD_data.response_bt);            
}

void ELD_event_proc(void)
{
    ELD_event_21_proc();
    ELD_event_313233_proc();
    ELD_event_3435_proc();
    //ELD_event_363738393a_proc();
    ELD_event_3b3c_proc();
    
}
