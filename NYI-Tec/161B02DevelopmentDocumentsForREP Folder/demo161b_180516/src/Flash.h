
/*******************************************************************************
* FILENAME 
*******************************************************************************/
#ifndef _FALSH_H_
#define _FALSH_H_

#define GPS_FLASH_SIZE				512


/********************************************************************************
********************************************************************************/
extern u32 ReadSoftParaAddr;

void NandFlashInit(void);

void WriteSysParameterToFlash(void);
void ReadFlashToSysParameter(void);

void WriteSysRunParameterToFlash(void);
void ReadFlashToSysRunParameter(void);

void WriteDataToFlash(u8 *src , u16 len);

#define c_gprs_confirm_ok	0
#define c_gprs_confirm_nk	1
#define c_gprs_confirm_all	2
#define c_bt_confirm_ok		3
#define c_bt_confirm_nk		4
#define c_bt_confirm_all	5
extern u8 ReadFlashToData(u8 *read_pack , u16 *len ,u8 typ);

extern void SetDefalutSysParameter(void);
extern void DefalutSet_SysRunParameter(u32 typ);

extern void setReadAddrRange(u32 addr);
extern void setWriteAddrRange(u32 addr);

#endif


