
#include "stm32_config.h"			//

#ifdef __CFG_GSENSOR_MPU6050__

/*************************************************************************************************************
圆点博士小四轴飞行器2014版配套源代码声明:
该源代码仅供参考,圆点博士不对源代码提供任何形式的担保,也不对因使用该源代码而出现的损失负责.
用户可以以学习的目的修改和使用该源代码.
但用户在修改该源代码时,不得移除该部分版权信息，必须保留原版声明.

更多信息，请访问官方网站www.etootle.com, 官方博客:http://weibo.com/xiaosizhou
**************************************************************************************************************/

void I2C_Configuration(void)	// PB8(SCL) / PB9(SDA)	/ PA0(ADC_INT)	
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	//
	GPIO_InitStructure.GPIO_Pin = I2C_SCL | I2C_SDA;					//圆点博士:配置使用的I2C口
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;   //圆点博士:设置I2C口最大允许输出速度
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;	  //圆点博士:设置I2C为开漏输出
	GPIO_Init(I2C_PORT, &GPIO_InitStructure); 
	//
	GPIO_InitStructure.GPIO_Pin = MPU6050_INT;					//圆点博士:配置使用的I2C口
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;   //圆点博士:设置I2C口最大允许输出速度
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	  //圆点博士:设置I2C为开漏输出
	GPIO_Init(MPU6050_INT_PORT, &GPIO_InitStructure); 
	//
	I2C_SCL_1; 
	I2C_SDA_1;
	I2C_DELAY;
}

void I2C_Delay(u32 dly)               
{
	while(--dly);	//dly=100: 8.75us; dly=100: 85.58 us (SYSCLK=72MHz)
}

u8 I2C_START(void)
{ 
	I2C_SDA_1; 
	I2C_NOP;
	// 
	I2C_SCL_1; 
	I2C_NOP;    
	//
	if(!I2C_SDA_STATE) return I2C_BUS_BUSY;
	//
	I2C_SDA_0;
	I2C_NOP;
	//
	I2C_SCL_0;  
	I2C_NOP; 
	//
	if(I2C_SDA_STATE) return I2C_BUS_ERROR;
	//
	return I2C_READY;
}

void I2C_STOP(void)
{
	I2C_SDA_0; 
	I2C_NOP;
	// 
	I2C_SCL_1; 
	I2C_NOP;    
	//
	I2C_SDA_1;
	I2C_NOP;
}

void I2C_SendACK(void)
{
	I2C_SDA_0;
	I2C_NOP;
	I2C_SCL_1;
	I2C_NOP;
	I2C_SCL_0; 
	I2C_NOP;  
}

void I2C_SendNACK(void)
{
	I2C_SDA_1;
	I2C_NOP;
	I2C_SCL_1;
	I2C_NOP;
	I2C_SCL_0; 
	I2C_NOP;  
}

u8 I2C_SendByte(u8 anbt_i2c_data)
{
 	u8 i;
 	
	I2C_SCL_0;
 	for(i=0;i<8;i++)
 	{  
  		if(anbt_i2c_data&0x80) I2C_SDA_1;
   		else I2C_SDA_0;
			//
  		anbt_i2c_data<<=1;
  		I2C_NOP;
			//
  		I2C_SCL_1;
  		I2C_NOP;
  		I2C_SCL_0;
  		I2C_NOP; 
 	}
	//
 	I2C_SDA_1; 
 	I2C_NOP;
 	I2C_SCL_1;
 	I2C_NOP;   
 	if(I2C_SDA_STATE)
 	{
  		I2C_SCL_0;
  		return I2C_NACK;
 	}
 	else
 	{
  		I2C_SCL_0;
  		return I2C_ACK;  
 	}    
}

u8 I2C_ReceiveByte(void)
{
	u8 i,anbt_i2c_data;
	//
 	I2C_SDA_1;
 	I2C_SCL_0; 
 	anbt_i2c_data=0;
	//
 	for(i=0;i<8;i++)
 	{
  		I2C_SCL_1;
  		I2C_NOP; 
  		anbt_i2c_data<<=1;
			//
  		if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
  
  		I2C_SCL_0;  
  		I2C_NOP;         
 	}
	I2C_SendNACK();
 	return anbt_i2c_data;
}

u8 I2C_ReceiveByte_WithACK(void)
{
	u8 i,anbt_i2c_data;
	//
 	I2C_SDA_1;
 	I2C_SCL_0; 
 	anbt_i2c_data=0;
	//
 	for(i=0;i<8;i++)
 	{
  		I2C_SCL_1;
  		I2C_NOP; 
  		anbt_i2c_data<<=1;
			//
  		if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
  
  		I2C_SCL_0;  
  		I2C_NOP;         
 	}
	I2C_SendACK();
 	return anbt_i2c_data;
}

void I2C_Receive6Bytes(u8 *anbt_i2c_data_buffer)
{
	u8 i,j;
	u8 anbt_i2c_data;

	for(j=0;j<5;j++)
	{
		I2C_SDA_1;
		I2C_SCL_0; 
		anbt_i2c_data=0;
		//
		for(i=0;i<8;i++)
		{
	  		I2C_SCL_1;
	  		I2C_NOP; 
	  		anbt_i2c_data<<=1;
				//
	  		if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
	  
	  		I2C_SCL_0;  
	  		I2C_NOP;         
		}
		anbt_i2c_data_buffer[j]=anbt_i2c_data;
		I2C_SendACK();
	}
	//
	I2C_SDA_1;
	I2C_SCL_0; 
	anbt_i2c_data=0;
	for(i=0;i<8;i++)
	{
	  	I2C_SCL_1;
	  	I2C_NOP; 
	  	anbt_i2c_data<<=1;
				//
	  	if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
	  
	  	I2C_SCL_0;  
	  	I2C_NOP;         
	}
	anbt_i2c_data_buffer[5]=anbt_i2c_data;
	I2C_SendNACK();
}

void I2C_Receive12Bytes(u8 *anbt_i2c_data_buffer)
{
	u8 i,j;
	u8 anbt_i2c_data;

	for(j=0;j<11;j++)
	{
		I2C_SDA_1;
		I2C_SCL_0; 
		anbt_i2c_data=0;
		//
		for(i=0;i<8;i++)
		{
	  		I2C_SCL_1;
	  		I2C_NOP; 
	  		anbt_i2c_data<<=1;
			//
  			if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
  
	  		I2C_SCL_0;  
	  		I2C_NOP;         
		}
		anbt_i2c_data_buffer[j]=anbt_i2c_data;
		I2C_SendACK();
	}
	//
	I2C_SDA_1;
	I2C_SCL_0; 
	anbt_i2c_data=0;
	for(i=0;i<8;i++)
	{
	  	I2C_SCL_1;
	  	I2C_NOP; 
	  	anbt_i2c_data<<=1;
				//
	  	if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
	  
	  	I2C_SCL_0;  
	  	I2C_NOP;         
	}
	anbt_i2c_data_buffer[11]=anbt_i2c_data;
	I2C_SendNACK();
}

void I2C_Receive14Bytes(u8 *anbt_i2c_data_buffer)
{
	u8 i,j;
	u8 anbt_i2c_data;

	for(j=0;j<13;j++)
	{
		I2C_SDA_1;
		I2C_SCL_0; 
		anbt_i2c_data=0;
		//
		for(i=0;i<8;i++)
		{
	  		I2C_SCL_1;
	  		I2C_NOP; 
	  		anbt_i2c_data<<=1;
				//
	  		if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
	  
	  		I2C_SCL_0;  
	  		I2C_NOP;         
		}
		anbt_i2c_data_buffer[j]=anbt_i2c_data;
		I2C_SendACK();
	}
	//
	I2C_SDA_1;
	I2C_SCL_0; 
	anbt_i2c_data=0;
	for(i=0;i<8;i++)
	{
	  	I2C_SCL_1;
	  	I2C_NOP; 
	  	anbt_i2c_data<<=1;
				//
	  	if(I2C_SDA_STATE)	anbt_i2c_data|=0x01; 
	  
		  I2C_SCL_0;  
		  I2C_NOP;         
	}
	anbt_i2c_data_buffer[13]=anbt_i2c_data;
	I2C_SendNACK();
}

void I2C_DMP_Delay_us(u32 dly)
{
	u8 i;
	while(dly--) for(i=0;i<10;i++);
}
//
void I2C_DMP_Delay_ms(u32 dly)
{
	while(dly--) I2C_DMP_Delay_us(1000);
}
//

u8 I2C_DMP_I2C_Write(u8 anbt_dev_addr, u8 anbt_reg_addr, u8 anbt_i2c_len, u8 *anbt_i2c_data_buf)
{		
	u8 i;
	I2C_START();
	I2C_SendByte(anbt_dev_addr << 1 | I2C_Direction_Transmitter);					//圆点博士:发送陀螺仪写地址
	I2C_SendByte(anbt_reg_addr);  //圆点博士:发送陀螺仪PWM地址
	for (i=0;i<anbt_i2c_len;i++) I2C_SendByte(anbt_i2c_data_buf[i]); //圆点博士:发送陀螺仪PWM值
	I2C_STOP();
	return 0x00;
}
u8 I2C_DMP_I2C_Read(u8 anbt_dev_addr, u8 anbt_reg_addr, u8 anbt_i2c_len, u8 *anbt_i2c_data_buf)
{
	
	I2C_START();
	I2C_SendByte(anbt_dev_addr << 1 | I2C_Direction_Transmitter);			//圆点博士:发送陀螺仪写地址
	I2C_SendByte(anbt_reg_addr);  //圆点博士:发送陀螺仪ID地址
	I2C_START();
	I2C_SendByte(anbt_dev_addr << 1 | I2C_Direction_Receiver);      //圆点博士:发送陀螺仪读地址
	//
	while (anbt_i2c_len) 
	{
		if (anbt_i2c_len==1)
		{
			*anbt_i2c_data_buf = I2C_ReceiveByte();  
		}
		else
		{
			*anbt_i2c_data_buf = I2C_ReceiveByte_WithACK();
		}
		anbt_i2c_data_buf++;
		anbt_i2c_len--;
	}
	I2C_STOP();
	return 0x00;
}

#endif

