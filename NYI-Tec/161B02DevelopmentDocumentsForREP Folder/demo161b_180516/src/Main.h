/******************************************************************************
* Function Name -->
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/

#ifndef _main_h_
#define _main_h_

#include "STM32_config.h"

extern u16 Power12v;
//==============================ADC================================
//常量定义

#define	MCUVCCVALUE			(2.4)
#define	ADC1VVALUE				(4096/MCUVCCVALUE)
#define	ADCCOEFFICIENT		(11)
#define	ADC1_DR_Address		((u32)0x4001244C)
#define	hardV90					0//蜂鸣器频率，默认是0 = V1.1之后的版本是2.7K(546000), 1 = 用于旧机器V1.1前的版本是4KHz(800100),


extern const u8 version[64];
extern const u8 readVersion[9];

extern u32 tick_get(void);

extern u32 tick_cmp(u32 tmr,u32 tmo);

extern void delay_us(u32 nus);

extern void delay_ms(u16 nms);

#endif

