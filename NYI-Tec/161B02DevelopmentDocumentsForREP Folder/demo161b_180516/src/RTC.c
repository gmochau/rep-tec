
/*******************************************************************************                            
* @ Version   -->
* @ Author    --> 
* @ Date      --> 
* @ Revise    -->
*
******************************************************************************/

#include "STM32_config.h"			//

#define LEAPS_THRU_END_OF(y) ((y)/4 - (y)/100 + (y)/400)
#define LEAP_YEAR(year) ((!(year % 4) && (year % 100)) || !(year % 400))

static const unsigned char rtc_days_in_month[] = 
{
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

rtc_time SysRtcTime;

/*******************************************************************************/
/*******************************************************************************/
u32 mktime (unsigned int year, unsigned int mon,
		    unsigned int day, unsigned int hour,
		    unsigned int min, unsigned int sec)
{
    if (0 >= (int) (mon -= 2)){    /**//* 1..12 -> 11,12,1..10 */
         mon += 12;      /**//* Puts Feb last since it has leap day */
         year -= 1;
    }

    return ((((unsigned long) (year/4 - year/100 + year/400 + 367*mon/12 + day)
    		+ year*365 - 719499	// 1970/1/1之前天数
          )*24 + hour
       )*60 + min
    )*60 + sec;
}

/*******************************************************************************/
/*******************************************************************************/
s16 rtc_month_days(u16 month, u16 year)
{
	return rtc_days_in_month[month] + (LEAP_YEAR(year) && month == 1);
}

/*******************************************************************************/
//Convert seconds since 01-01-1970 00:00:00 to Gregorian date.
/*******************************************************************************/
void rtc_time_to_tm(u32 time, rtc_time *tm)
{
	s16 days, month, year;

	s16 newdays;

	if(time<1)return;
	days = time / 86400;
	time -= days * 86400;

	/* day of the week, 1970-01-01 was a Thursday */
	tm->tm_wday = (days + 4) % 7;

	year = 1970 + days / 365;
	days -= (year - 1970) * 365
		+ LEAPS_THRU_END_OF(year - 1)
		- LEAPS_THRU_END_OF(1970 - 1);
	if (days < 0) 
	{
		year -= 1;
		days += 365 + LEAP_YEAR(year);
	}
	tm->tm_year = year - 1900;
	tm->tm_yday = days + 1;

	for (month = 0; month < 11; month++) 
	{
		newdays = days - rtc_month_days(month, year);
		if (newdays < 0)
			break;
		days = newdays;
	}
	tm->tm_mon = month + 1;
	tm->tm_mday = days + 1;

	tm->tm_hour = time / 3600;
	time -= tm->tm_hour * 3600;
	tm->tm_min = time / 60;
	tm->tm_sec = time - tm->tm_min * 60;
	tm->tm_year += 1900;
}

/*******************************************************************************/
// Does the rtc_time represent a valid date/time?
/*******************************************************************************/
s16 rtc_valid_tm(rtc_time *tm)
{
	if (tm->tm_year < 70
		|| ((unsigned)tm->tm_mon) >= 12
		|| tm->tm_mday < 1
		|| tm->tm_mday > rtc_month_days(tm->tm_mon, tm->tm_year + 1900)
		|| ((unsigned)tm->tm_hour) >= 24
		|| ((unsigned)tm->tm_min) >= 60
		|| ((unsigned)tm->tm_sec) >= 60)
		return -1;

	return 0;
}

/*******************************************************************************/
// Convert Gregorian date to seconds since 01-01-1970 00:00:00.
/*******************************************************************************/
s16 rtc_tm_to_time(rtc_time *tm, u32 *time)
{
	*time = mktime(tm->tm_year,tm->tm_mon,tm->tm_mday,
				   tm->tm_hour,tm->tm_min,tm->tm_sec);
	return 0;
}

/*******************************************************************************/
//得到 u32类型(UNIX)的RTC时间格式
/*******************************************************************************/
u32 get_RTCCounter(void)
{
	return RTC_GetCounter();
}


/*******************************************************************************/
//设置 用u32格式的时间格式设置RTC
/*******************************************************************************/
void set_RTCCounter(u32 time)
{

	RTC_WaitForLastTask();
	RTC_SetCounter(time);
	RTC_WaitForLastTask();
	
}

/*******************************************************************************/
//设置u32类型(UNIX)的RTC时间格式,并存到BackupRegister
/*******************************************************************************/
void set_RTCTime(u32 time)
{
	//RTC_Configuration();
	set_RTCCounter(time);
	BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void RTC_Configuration(void)
{
	 u32 delay;

	 RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	 PWR_BackupAccessCmd(ENABLE);

	 RCC_LSEConfig(RCC_LSE_ON);

	 delay = tick_get();
	 while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
	 {
			if(tick_cmp(delay,3000) == c_ret_ok)
			{
				 break;
			}
	 }
	 RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	 RCC_RTCCLKCmd(ENABLE);
	 RTC_WaitForSynchro();
	 RTC_WaitForLastTask();
	 RTC_ITConfig(RTC_IT_SEC, DISABLE);
	 RTC_WaitForLastTask();
	 RTC_ITConfig(RTC_IT_ALR, DISABLE);
	 RTC_WaitForLastTask();
	 RTC_ITConfig(RTC_IT_OW, DISABLE);
	 RTC_WaitForLastTask();

	 RTC_SetPrescaler(32767);
	 RTC_WaitForLastTask();
}

/*******************************************************************************/
//设置 用固定的一个初值y m d h m s的时间格式设置RTC
/*******************************************************************************/
void rtc_DefTimeAdjust(void)
{
	u32 time;
	rtc_time tima;

	SysRtcTime.tm_sec = 0;
	SysRtcTime.tm_min = 0;
	SysRtcTime.tm_hour = 12;

	SysRtcTime.tm_mday = 1;
	SysRtcTime.tm_mon = 1;
	SysRtcTime.tm_year = 2018;

	rtc_tm_to_time(&SysRtcTime, &time);
	delay_ms(100);
	rtc_time_to_tm(time,&tima);
	set_RTCCounter(time);
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void RTCInit(void)
{
	u32 time = 0;

	RTC_Configuration();
	if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5)
	{

		PWR_BackupAccessCmd(ENABLE);
		rtc_DefTimeAdjust();
		BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
		time = RTC_GetCounter();
	}
	else
	{
		if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
		{
		}
		else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET)
		{
		}
		RTC_WaitForSynchro();
		RTC_WaitForLastTask();
		time = RTC_GetCounter();
        
        printf("\r\n**** RTC pwron ok ****\r\n");
        
	}
	rtc_time_to_tm(time,&SysRtcTime);
	
}

void rtc_test(void)
{
    static u32 tmr = 0;
    rtc_time tm;
    u8 buf[64], *px;

    if(tick_cmp(tmr, 10000) == c_ret_ok)
    {
        tmr = tick_get();

        rtc_time_to_tm(get_RTCCounter(), &tm);
        sprintf((char*)buf,"rtc = %02d%02d%02d/%02d%02d%02d\r\n",
                    tm.tm_year % 100, 
                    tm.tm_mon, 
                    tm.tm_mday, 
                    tm.tm_hour,
                    tm.tm_min,
                    tm.tm_sec);            
        px = buf;
        while(*px)
        {
            while((USART1->SR & 0x40) == 0);		
                USART1->DR = *px;			    		
            px++;
        }
    }        
}


