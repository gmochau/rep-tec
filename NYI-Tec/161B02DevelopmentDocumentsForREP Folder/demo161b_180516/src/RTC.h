
#ifndef _rtc_h_
#define _rtc_h_

typedef struct 
{
	s16 tm_sec;
	s16 tm_min;
	s16 tm_hour;
	s16 tm_mday;
	s16 tm_mon;
	s16 tm_year;
	s16 tm_wday;
	s16 tm_yday;
	s16 tm_isdst;
}rtc_time;
extern rtc_time SysRtcTime;

extern void rtc_time_to_tm(u32 time, rtc_time *tm);

extern s16 rtc_tm_to_time(rtc_time *tm, u32 *time);
	
extern u32 get_RTCCounter(void);

extern void set_RTCCounter(u32 time);

extern void RTCInit(void);

extern u32 mktime (unsigned int year, unsigned int mon,
		    unsigned int day, unsigned int hour,
		    unsigned int min, unsigned int sec);


extern void rtc_test(void);

#endif


