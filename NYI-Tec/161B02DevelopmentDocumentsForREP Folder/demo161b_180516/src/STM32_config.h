/*
    STM32_config.h
*/
    
#ifndef		_STM32_config_h_
#define		_STM32_config_h_


#define IAP_PAGE_SIZE			2048								// GD32F105XX
#define IAP_BOOT_ADDR			0x08000000
#define IAP_BOOT_END            0x08002000
#define IAP_DATA_ADDR			0x08002000
#define IAP_APP_ADDR			0x08002800     
#define IAP_VER_ADDR			0x08003000
#define IAP_APP_END             0x08040000

#define FLASH_PAGE_SIZE		    0x00000800							// 2048
#define FLASH_BLOCK_SIZE        0x20000
#define FLASH_APP_ADDR     		ADDR_UPDATE   						// 256K*2	系统升级
#define FLASH_APP_ADDR_END		(FLASH_APP_ADDR + 0x40000)   						// 256K*2	系统升级
#define FLASH_TMP_ADDR     		(FLASH_APP_ADDR_END)   						// 256K*2	系统升级
#define FLASH_TMP_ADDR_END		(FLASH_TMP_ADDR + 0x40000)   						// 256K*2	系统升级

/******************************************************************************/

typedef enum {false=0,true=!false} bool;
typedef enum {FAIL=0,OK=!FAIL,PENDING} toolStaus;//接收或发送数据的状态
typedef enum {GOING=0,STOP=!GOING} taskStaus;//任务执行状态

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef union
{
	u32 r;
	struct st_16w
	{
		u16 l;
		u16 h;
	}w;
	struct st_08b
	{
		u8 l;
		u8 m;
		u8 h;
		u8 u;
	}b;
}un32;



#ifndef c_ret_ok
#define c_ret_ok				0
#endif
#ifndef c_ret_nk
#define c_ret_nk				1
#endif
#ifndef c_ret_next
#define c_ret_next				2
#endif
#ifndef c_ret_exit
#define c_ret_exit				3
#endif
#ifndef c_ret_nk_tmo
#define c_ret_nk_tmo			4
#endif
#ifndef c_ret_err
#define c_ret_err			    5
#endif
#ifndef c_ret_full
#define c_ret_full			    6
#endif


#define ALL_DBG_EN                      ((u32)1<<31)
#define TICK_DBG_EN                     ((u32)1<<30)

#define TASK_DBG_EN                     ((u32)1<<0)
#define OBD_DBG_EN                      ((u32)1<<1)
#define GPS_DBG_EN                      ((u32)1<<2)
#define GSM_DBG_EN                      ((u32)1<<3)
#define BT_DBG_EN                       ((u32)1<<4)
#define ELD_DBG_EN                      ((u32)1<<5)
#define UPDATE_DBG_EN                   ((u32)1<<6)
#define FLASH_DBG_EN                    ((u32)1<<7)
#define GSENSOR_DBG_EN                  ((u32)1<<8)
#define AD_DBG_EN                       ((u32)1<<9)
#define REPORT_DBG_EN                   ((u32)1<<10)

/******************************************************************************
                               外部函数头文件
                   应用到不同的外设头文件请在这里修改即可                        
******************************************************************************/

#include <string.h>	//内存操作相关函数库
#include <math.h>	//数学运算相关函数库
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "stm32f10x.h"
#include "core_cm3.h"
#include "misc.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_bkp.h"
#include "stm32f10x_can.h"
#include "stm32f10x_cec.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_crc.h"
#include "stm32f10x_dac.h"
#include "stm32f10x_dbgmcu.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_flash.h"
#include "stm32f10x_fsmc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_it.h"
#include "stm32f10x_iwdg.h"
#include "stm32f10x_pwr.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_sdio.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_tim.h"
#include "system_stm32f10x.h"
#include "sys.h"


#include "BT_Drive.h"
#include "dbg.h"
#include "led.h"
#include "Main.h"
#include "mc3230.h"
#include "RTC.h"
#include "usartDrive.h"
#include "w25xx.h"

#endif

