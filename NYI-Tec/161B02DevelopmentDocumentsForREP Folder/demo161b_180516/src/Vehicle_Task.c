
/*******************************************************************************
* FILENAME：Vehicle_Task.C
*******************************************************************************/
#include "stm32_config.h"			//

u16 Power12v = 0;

struct st_powerCtl powerCtl = {0};

u32 Vehicle_getPower12v(void)
{
    u8 x;
    u32 ad;
    
    ad = 0;
    for(x = 0; x < 8;x++)
    {
        BSP_adcStart();
        ad += BSP_ADcPower12v();
    }
    ad /= 8;
    return ad;
}
        
/******************************************************************************
* Function Name --> 开始任务
* Description   --> none
* Input         --> *pdata: 任务指针
* Output        --> none
* Reaturn       --> none 
******************************************************************************/

//每次上外部的电时需要初始化一次，当这些参数发生改变时要重新初始化一次,未知的就用默认值
//应用层需要存储这些参数，无需用到的参数应用层可以选择不存储。
//CarType: 车型，0xFFFF=乘用车，通常是汽车电瓶12V的车辆(12v)；0x0000=商业车，通常是汽车电瓶24V的车辆；其它值待定义。(24v)
//TotalMileage：汽车仪表盘上的总里程。单位KM，默认值0
//TotalFuelConsumption：汽车总油耗。单位L，默认值0
//TotalTravelTime：汽车行驶总时间。单位小时，默认值0
//FuelType： 发动机燃油类型。1=汽油，2=柴油，默认值1
//Displacement：发动机排量。单位L，默认值1.6L	
	//extern void obd_init(u16 CarType,u32 TotalMileage,u32 TotalFuelConsumption,u32 TotalTravelTime,u8 FuelType,u8 Displacement);    

void Vehicle_Task(void * pdata)
{
	static u32 delay = 0;
    static u8 obdInitDone = 0;
    static u16 Power12vLast = 0;    
    
	printf("\r\n[Vehicle_Task]..................OK.\r\n");	
	STM_GetSysTick(&delay);			
	while(1)
	{

		dbg(TASK_DBG_EN,"[vehicle_Task] \r\n");
		taskCntInc();

				
		if(TRUE == STM_CheckSysTick((u32 *)&delay , 1000/c_systick))
		{            
            BSP_adcStart();    
			Power12v = BSP_ADcPower12v();					
			dbg(AD_DBG_EN,  "POWER :[%d]V\r\n",
                            Power12v);

            //      蓝牙模块工作和电压的关系：
            //A.   外部供电(9~36V)或USB供电，工作
            //B.   无外部供电且无USB供电，判断锂电池供电（<15%）,断模块的电不工作
            if( (Power12v < c_ad_gsm_off) )
            {	// 无外部供电且              //  锂电池供电<15%      // 无USB供电  
                powerCtl.btFunDisable = 1;
            }
            else if( (Power12v > c_ad_gsm_on) )
            {
                powerCtl.btFunDisable = 0;
            }	
            
            if(obdInitDone == 0)
            {
                if( ((Power12v >= Power12vLast) ? (Power12v - Power12vLast) : (Power12vLast - Power12v)) < 40)        // 上电
                {
                    obdInitDone = 1;
                    
                    obd_init( (SysRunParameter.OBD_Parameter.carType == 0) ? ((Power12v > 18000) ? 0x0000 :0xffff) : SysRunParameter.OBD_Parameter.carType,
                             SysRunParameter.OBD_Parameter.total_trip_mileage,
                             SysRunParameter.OBD_Parameter.total_fuel,
                             SysRunParameter.OBD_Parameter.total_engineHours,
                             SysParameter.UnitData.FuelType,
                             SysParameter.UnitData.EngineDisplacement);

                    printf("obd_init:%04X\r\n", (SysRunParameter.OBD_Parameter.carType == 0) ? ((Power12v > 18000) ? 0x0000 :0xffff) : SysRunParameter.OBD_Parameter.carType);
                    
        
                    dbg(AD_DBG_EN, "odb_init\r\n");
                }
            }
            Power12vLast = Power12v;

			STM_GetSysTick(&delay);
		}
		
		DMP_MPU6050_SEND_DATA_FUN();	
				
	}
}

