/*******************************************************************************
* FILENAME Vehicle_Task.h
*******************************************************************************/
#ifndef _VEHICLE_TASK_H_
#define _VEHICLE_TASK_H_

typedef enum
{
	EVENT_DRIVER = 0x01,		// 	司机状态
	EVENT_PRECISION = 0x02,		//	位置精度
	EVENT_ACCURACY  = 0x03,		//	位置精度
	EVENT_CERTIFICATION = 0x04,	//	司机认证
	EVENT_AUTHENTICATED  = 0x05,//	司机身份认证
	EVENT_ENGINE  = 0x06, 		//	点/熄火,车辆运动/静止
	EVENT_MALFUNCTION = 0x07,	//	故障和诊断确认/清除
	EVENT_VEHICLE = 0x08,		//	系统事件	
	EVENT_ALARM = 50,			//	数据监控	
}EVENT_TYPE;

typedef enum
{
	CODE_DRIVER_Off_duty = 0x01,			//非法驾驶者
	CODE_DRIVER_Sleeper_Berth = 0x02,		//司机停车睡眠
	CODE_DRIVER_Driving  = 0x03,			//正常驾驶
	CODE_DRIVER_On_duty_not_driving = 0x04,	//无人驾驶
}DRIVER_CODE;

typedef enum
{
	CODE_PRECISION_conventional = 0x01,
	CODE_PRECISION_reduced = 0x02,
}PRECISION_CODE;

typedef enum
{
	CODE_ACCURACY_cleared = 0x00,			// 之后产生的日志经纬度恢复5个小数点
	CODE_ACCURACY_Authorized = 0x01,		// 合法的商业机动车辆使用者,定位精度降低
	CODE_ACCURACY_Moves = 0x02,				// 合法的商业机动车辆使用者,他在停车场移动,定位精度降低

	//CODE_ACCURACY_cleared = 0x03,
	CODE_ACCURACY_default  = 0x04,
	CODE_ACCURACY_Set_sys = 0x05,
	CODE_ACCURACY_Query_sys  = 0x06,
	CODE_ACCURACY_Answer  = 0x07,
	CODE_ACCURACY_Set_network  = 0x08,
	CODE_ACCURACY_Query_network  = 0x09,
	CODE_ACCURACY_Reply_network = 0x0A,
	CODE_ACCURACY_FTP  = 0x0B,
	CODE_ACCURACY_Resetthe   = 0x0C,	
}ACCURACYN_CODE;

typedef enum
{
	CODE_CERTIFICATION_certification = 0x01,	// 驾驶员每天第一次认证的日志
	CODE_CERTIFICATION_Nth_certification = 0x02,// 驾驶员每天第 N次认证的日志	2~9(大于9取9)
}CERTIFICATION_CODE;

typedef enum
{
	CODE_AUTHENTICATED_login = 0x01,
	CODE_AUTHENTICATED_logout= 0x02,	
}AUTHENTICATED_CODE;

typedef enum
{
	CODE_ENGINE_power_up_conventional  = 0x01,	//	传统定位精度下ACC_ON事件
	CODE_ENGINE_power_up_reduced= 0x02,			//
	CODE_ENGINE_shut_down_conventional =0x03,	//
	CODE_ENGINE_shut_down_reduced =0x04,		//
	CODE_VEHICLE_still  = 0x05,					//
	CODE_VEHICLE_movement=0x06,					//
	
}ENGINE_CODE;

typedef enum
{
	CODE_MALFUNCTION_Standard_Coding_Requirementsl  = 0x01,	//硬性故障
	CODE_MALFUNCTION_malfunction_cleared = 0x02,
	CODE_MALFUNCTION_diagnostic_logged =0x03,
	CODE_MALFUNCTION_diagnostic_event_cleared =0x05,		//诊断事件
}MALFUNCTION_CODE;

typedef enum
{
	CODE_VEHICLE_reset  = 0x00,					// 	设备重启
	CODE_VEHICLE_restore_default  = 0x01,		//	恢复默认参数
	CODE_VEHICLE_set_system_parameter = 0x02,	//	设置系统参数
	CODE_VEHICLE_query_system_parameter = 0x03,	//	查询系统参数
	CODE_VEHICLE_answer_system_parameter = 0x04,//	回复系统参数
	CODE_VEHICLE_set_network_parameter = 0x05,	//	设置网络参数
	CODE_VEHICLE_query_network_parameter = 0x06,//	查询网络参数
	CODE_VEHICLE_answer_network_parameter = 0x07,//	回复网络参数	
	CODE_VEHICLE_ftp_upgrade =0x08,
}VEHICLE_CODE;

typedef enum
{
	CODE_ALARM_acc_on = 0x00,
	CODE_ALARM_more_4_hours  = 0x01,
	CODE_ALARM_ACC_OFF  = 0x02,
	CODE_ALARM_Acceleration = 0x03,
	CODE_ALARM_Brake  = 0x04,
	CODE_ALARM_Cornering  = 0x05,
	
	CODE_ALARM_engine_idle  = 0x06,
	CODE_ALARM_engine_off  = 0x07,
	
	CODE_ALARM_speed_alarm   = 0x08,
	CODE_ALARM_Speed_stop   = 0x09,
	
	CODE_ALARM_Engine_speed_greater   = 0x0A,
	CODE_ALARM_Engine_speed_stop   = 0x0B,
	
	CODE_ALARM_coolant_temperature_alarm   = 0x0C,
	CODE_ALARM_coolant_temperature_stop  = 0x0D,

	CODE_ALARM_disconnect_power  = 0x0E,
	CODE_ALARM_connected_power  = 0x0F,

	CODE_ALARM_power_low_alarm  = 0x10,
	CODE_ALARM_power_low_stop  = 0x11,
	
}ALARM_CODE;


#define c_ad_gsm_on				8600	// 高于8.6伏，启动GSM
#define c_ad_gsm_off			8300	// 低于8.3伏，关闭GSM
/*=======================================================================================================
 ---------------------------------------  函数声明 -----------------------------------
 =======================================================================================================*/
struct st_powerCtl
{
    u8 gsmFunDisable;
    u8 gpsFunDisable;
    u8 btFunDisable;
};
extern struct st_powerCtl powerCtl;

extern u16 Power12v;

extern u32 Vehicle_getPower12v(void);

extern void Vehicle_Task(void * pdata);                                                      

#endif

