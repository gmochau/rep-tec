/*
    dbg.c
*/

#include "STM32_config.h"

union un_dbg_en dbg_en = {0};
u32 dbg_tmr;

u32 dbg(u32 en,u8* fmt,...)
{
    static u8 buf[512] = {0};
    u8* px;
    u32 ret = 0;
    va_list ap;
    rtc_time tm;

    if((dbg_en.r & 0x80000000) == 0) return ret;
    
    if(dbg_en.r & en)
    {
        ret = buf[0] = 0;
        if((dbg_en.r & TICK_DBG_EN) != 0) 
        {
            rtc_time_to_tm(get_RTCCounter(), &tm);
            ret += sprintf((char*)buf,"%02d%02d%02d/%02d%02d%02d.%04d:",
                        tm.tm_year % 100, 
                        tm.tm_mon, 
                        tm.tm_mday, 
                        tm.tm_hour,
                        tm.tm_min,
                        tm.tm_sec,
                        dbg_tmr % 1000);        
        }
        va_start(ap,fmt);
        ret += vsprintf((char*)buf + strlen((char*)buf),(char*)fmt,ap);           
        va_end(ap);        

        px = buf;
        while(*px)
        {
            while((USART1->SR & 0x40) == 0);		
                USART1->DR = *px;			    		
            px++;
        }
    }     
    return ret;
}

u8*  dbg_getCtl(u32 x)
{
    return (u8*)((dbg_en.r & ((u32)1<<x)) ? "ON" : "OFF");
}

void dbg_proc(u8 *buf)
{
    u8* px, c;
    
    if((px = (u8*)strstr((char*)buf,"AT+")) != NULL)
    {
        px += 3;
        if(*px == '?')
        {
            dbg_en.b.all = 1;
            dbg(ALL_DBG_EN,"\r\n*  AT+N=1 Open N type debugging information output                  *\r\n"
                        "*  AT+N=0 Turn off N type debugging information output              *\r\n"
                        "*  AT+?   Check the status and pause the message output             *\r\n"
                        "*  AT command without spaces in the middle, to enter a newline end  *\r\n");
            
            dbg(ALL_DBG_EN, "\r\n[AT+1  :Task run information ,current status:%s]\r\n"
                        "[AT+2  :OBD task information, current status:%s]\r\n"
                        "[AT+3  :gps task information, current status:%s]\r\n"
                        "[AT+4  :gsm task information, current status:%s]\r\n"
                        "[AT+5  :bt  task information, current status:%s]\r\n"
                        "[AT+6  :eld task information, current status:%s]\r\n"
                        "[AT+7  :Update   information, current status:%s]\r\n"
                        "[AT+8  :flash    information, current status:%s]\r\n"
                        "[AT+9  :gsensor  information, current status:%s]\r\n"
                        "[AT+10 :ad       information, current status:%s]\r\n"
                        "[AT+11 :report   information, current status:%s]\r\n"            
                        "[AT+31 :Additional Time Stamp Information,Current Status:%s]\r\n",
            
                dbg_getCtl(0),dbg_getCtl(1),dbg_getCtl(2),dbg_getCtl(3),dbg_getCtl(4),
                dbg_getCtl(5),dbg_getCtl(6),dbg_getCtl(7),dbg_getCtl(8),dbg_getCtl(9),
                dbg_getCtl(10),
                dbg_getCtl(30) );
            
            dbg_en.r &= ~((u32)1<<31);
        }
        else
        {
            c = atoi((char*)px);
            px++;
            if(c >= 10) px++;
            if(c > 0) c--;
            if(*px == '=')
            {
                px++;
                if(c >= 31)
                {
                }                
                else if(px[1] == 0x0)
                {
                    if(     px[0] == '0') {dbg_en.r &= ~((u32)1<<c);dbg_en.r |= ((u32)1<<31);dbg(ALL_DBG_EN,"AT OK\r\n");}
                    else if(px[0] == '1') {dbg_en.r |=  ((u32)1<<c);dbg_en.r |= ((u32)1<<31);dbg(ALL_DBG_EN,"AT OK\r\n");}
                }
            }
        }
    }
}
