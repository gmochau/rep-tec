/*
    dbg.h
*/

#ifndef __dbg_h__
#define __dbg_h__

#include "STM32_config.h"

union un_dbg_en
{
    u32 r;
    struct st_dbg_en
    {
        u8 task:1;
        u8 obd:1;
        u8 gps:1;
        u8 gsm:1;
        u8 bt:1;
        u8 eld:1;
        u8 update:1;
        u8 flash:1;
        
        u8 gsensor:1;
        u8 ad:1;
        u8 report:1;
        u8 nc0:5;
        
        u8 nc1:8;
        
        u8 nc2:6;
        u8 tick:1;
        u8 all:1;
    }b;
};

extern union un_dbg_en dbg_en;

extern u32 dbg_tmr;

extern u32 dbg(u32 en,u8* fmt,...);

extern void dbg_proc(u8 *buf);


#endif
