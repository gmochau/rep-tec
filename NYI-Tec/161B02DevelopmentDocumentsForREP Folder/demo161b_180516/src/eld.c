/*
	eld.c

request:{source_id:bluetooth123456,events:[{0018,6,1,1,1,0,10042017,003713,,0,5,JKVIN000000001,0.0,0.0,10,0,0,,,,2E,B2}
request:{source_id:bluetooth123456,events:[{0019,6,5,1,1,0,10042017,003714,,0,5,JKVIN000000001,0.0,0.0,10,0,0,,,,33,5A}
request:{source_id:bluetooth123456,events:[{001a,6,6,1,1,0,10042017,003719,,0,5,JKVIN000000001,0.0,0.0,20,0,0,,,,39,0A}
request:{source_id:bluetooth123456,events:[{001b,6,3,1,1,0,10042017,003720,,0,5,JKVIN000000001,0.0,0.0,20,0,0,,,,37,7A}

request:{source_id:ELD000000001,events:[{0060,8,2,1,1,0,10042017,065552,AT$SYSS=|123456|eld123456789abc|100|200|*,0,5,,0.0,0.0,10,0,0,,,,E1,CC}]}
request:{source_id:ELD000000001,events:[{005f,8,5,1,1,0,10042017,064818,AT$GPRS=|0|112.74.16.127|5001|3gnet|123456|123456|*,0,5,,0.0,0.0,10,0,0,,,,E4,E4}]}	  //UDP
request:{source_id:ELD000000001,events:[{005f,8,5,1,1,0,10042017,064818,AT$GPRS=|1|112.74.16.127|5001|3gnet|123456|123456|*,0,5,,0.0,0.0,10,0,0,,,,E5,EC}]}	  //TCP
*/

#include "stm32_config.h"

struct ftpPara ftpSendPara;

u32 FlashReadDataAddr;
struct st_eld_data ELD_data;

OS_FLAG_GRP *ELD_rece_status;   //事件标志组
OS_FLAG_GRP *ELD_send_status;   

OS_EVENT *ELD_rece_bt;
OS_EVENT *ELD_rece_obd;

OS_STK ELD_task_poll_STK[ELD_task_poll_Size];

//const u16 ELD_pid_idx_tab[ ELD_pid_end]={0x0104,0x0105,0x010c,0x010d,0x010f,0x011f,0x0123,0x012f,0x0146,0x0151,0x015c,0x015e,0x0174,0x0175};

u32 tick_get(void)
{
    return dbg_tmr;
}

u32 tick_cmp(u32 tmr,u32 tmo)
{
    return ((dbg_tmr - tmr) >= tmo) ? c_ret_ok : c_ret_nk;
}

static u32 ELD_report_last_addr(u32 addr)
{
	u32 ret;
	
	if(addr == GpsData_ADDR)
		ret = GpsData_ADDR_END - GPS_FLASH_SIZE;
	else 
		ret = addr - GPS_FLASH_SIZE;
	
	return ret;	
}

static u32 ELD_report_next_addr(u32 addr)
{
	u32 ret;
	
	if(addr == GpsData_ADDR_END)
		ret = GpsData_ADDR;
	else 
		ret = addr + GPS_FLASH_SIZE;
	
	return ret;	
}

//	strncpy()当n==0退出时不填充结束符
u8* ELD_strncpy(u8* buf,u8* str,u16 n)
{
	u8* px = buf;
	while(*str && n)
	{
		*px++ = *str++;
        n--;
    }
    *px = 0;
    return buf; 	
}
//	在字符串str中统计字符chr个数
static u8 ELD_str_chr_num(u8* str,u8 chr)
{
	u8 x = 0;
	
	while(*str)
	{		
		if(*str == chr) x++;
		str++;
	}	
	return x;
}
//	剪切字符串左边的空格
static u8* ELD_str_trim_space(u8* str)
{
	while(*str == ' ') str++;
	return str;
}
//	判断全十进制字符串
static u8 ELD_str_all_dec(u8 *str)
{
	str = ELD_str_trim_space(str);
	if(*str == 0) return c_ret_nk;
    while(*str)
    {
		if((*str < '0')||(*str > '9')) return c_ret_nk;
		str++;
    }
    return c_ret_ok;
}
//	判断全16进制字符串
static u8 ELD_str_all_hex(u8 *str)
{
	u8 c;
	
	str = ELD_str_trim_space(str);
	if(*str == 0) return c_ret_nk;
    while(*str)
    {
		c = *str;
		if(	    ((c >= '0') && (c <= '9')) 
			||	((c >= 'a') && (c <= 'f'))
			||	((c >= 'A') && (c <= 'F')) )
			str++;
		else 
			return c_ret_nk;
    }
    return c_ret_ok;
}

//	16进制整数字符串转化成16进制整数
u32 ELD_str_hex(u8* str)
{
	u8 c,x;
	u32 ret = 0;
	
	str = ELD_str_trim_space(str);
    while(*str)
    {
		c = *str++;
		if(	     (c >= '0') && (c <= '9') ) x = c - '0';
		else if( (c >= 'a') && (c <= 'f') ) x = c - 'a' + 10;
		else if( (c >= 'A') && (c <= 'F') ) x = c - 'A' + 10;
		else break;
		ret *= 16;
		ret += x;
    }			
	return ret;
}
//	10进制整数字符串转化成10进制整数
static u32 ELD_str_dec(u8* str)
{
	u8 c;
	u32 ret = 0;
	
	str = ELD_str_trim_space(str);
    while(*str)
    {
		c = *str++;
		if(	(c >= '0') && (c <= '9') )
		{
			ret = ret * 10 + c - '0';
		}
		else break;
    }			
	return ret;
}

//	返回字符串str中第n个chr的位置,错误返回NULL
static u8* ELD_strnchr(u8 *str,u8 chr,u8 n)
{
	while(*str)
	{
		if(*str == chr)
		{
			n--;			
			if(n == 0) return str;
        }
        str++;
	}
    return NULL;	
}

//	输出第h个字符chr后一字符到第h+1个字符chr前一字符之间的字符个数
u16 ELD_strlen_ht(u8 *str,u8 chr,u8 h)
{
	u8 *px,*py;
	
	if(h == 0) 
	{
		if(*str == chr)		// 第一个字符chr
			return 0;
		px = str;
		if(px == NULL) return 0;
	}	
	else 
	{
		px = ELD_strnchr(str,chr,h);
		if(px == NULL) return 0;
		px++;
	}	
	
	py = ELD_strnchr(str,chr,h+1);
    if(py == NULL) py = str + strlen((char*)str);

    return py - px;
}

//	复制第h个字符chr后一字符到第h+1个字符chr前一字符
u8 ELD_strcpy_ht(u8* buf,u8* str,u8 chr,u8 h,u16 maxLen)
{
	u8 *px,*py;
	u32 len;
    
	if(h == 0) 
	{
		if(*str == chr)
		{
			buf[0] = 0;
			return c_ret_ok;
		}
		px = str;
		if(px == NULL) return c_ret_nk;
	}	
	else 
	{	
		px = ELD_strnchr(str,chr,h);
		if(px == NULL) return c_ret_nk;
	    px++;
	}
	
	py = ELD_strnchr(str,chr,h+1);
    if(py == NULL) py = str + strlen((char*)str);

    len = py - px;
    if(len >= maxLen) len = maxLen - 1;
    ELD_strncpy(buf,px,len);
	return c_ret_ok;	
}


static u8 ELD_upload_confirm_bt(u32 id)
{ 	
	u8 x, y;	
	
	if(ELD_data.bt_upload_date_en == 1)		// 蓝牙9-3命令上传制定时间段日志 蓝牙9-1命令上传最近8天日志
    {
        for(x = 0; x < sizeof(ELD_data.bt_resp)/sizeof(ELD_data.bt_resp[0]); x++)
        {
             if(ELD_data.bt_resp[ x] .addr == 0x00) break;
        }
        
        for(y = 0; y < x; y++)
        {
            if(id == ELD_data.bt_resp[ y ].id)
                return c_ret_ok;
        }
        return c_ret_nk;
    }

    if(id == ELD_data.bt_resp[ 0 ].id)
    {
        x = 0x00;						
        if(ELD_data.bt_upload_addr_en == 1)
        {
            SPI_FLASH_BytesWrite(&x, ELD_data.bt_resp[0].addr + GPS_FLASH_SIZE - 1,  1, c_spiFlashWriteType_write);		
        }
        else
        {
            SPI_FLASH_BytesWrite(&x,  ELD_data.bt_resp[0].addr + GPS_FLASH_SIZE - 1, 1, c_spiFlashWriteType_write);
            SysRunParameter.FLASH_Parameter.ConfirmDataAddrBt = ELD_data.bt_resp[0].addr;
        }
        return c_ret_ok;
    }
    return c_ret_nk;
}    

#define c_packet_error_none         0
#define c_packet_error_json			1
#define c_packet_error_checksum		2

static void	ELD_packet_parse_error(u8 src,u8 err)
{
    if(ELD_data.btSendRespReq == 0)
    {
        if(err == c_packet_error_json) strcpy((char*)ELD_data.response_bt,"unsupported log\r\n");
        else strcpy((char*)ELD_data.response_bt,"checksum error\r\n");
	}
    ELD_data.btSendRespReq = 1;
}

static u8 ELD_sum_cmp(u8 *buf)
{
	u16 sum, len, x;
	//$$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**
    //以上所有参数字符的16进制值相加，包括两个参数之间的间隔符号'，'，但是不包括两个参数之间的空格或回车换行，也不包括日志起始符号'$$'和结束符号'**'。    
    // APP_12345678,01,8,9,122417,151747,,1234**
    len = strlen((char*)buf);
    while(len > 0)
    {
        if(buf[len - 1] == ',') break;
        len--;
    }
    for(x = sum = 0; x < len; x++)
        sum += buf[x];
	dbg(ELD_DBG_EN,"&&&&&&&&&&&&&&&&&  sum = %04X  &&&&&&&&&&&&&&&\r\n",sum);

    
    x = ELD_str_hex(buf + len);
    return (x == sum) ? c_ret_ok : c_ret_nk;
}

void ELD_sum_set(u8 *str)
{
	u16 sum, len, x;
	
	//$$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**
    //以上所有参数字符的16进制值相加，包括两个参数之间的间隔符号'，'，但是不包括两个参数之间的空格或回车换行，也不包括日志起始符号'$$'和结束符号'**'。    
	len = strlen((char*)str);
    for(sum = x = 0; x < len; x++)
        sum += str[x];
    sum += ',';
    
    sprintf((char*)str + strlen((char*)str),",%04X",sum);
    
	dbg(ELD_DBG_EN,"&&&&&&&&&&&&&&&&&  sum = %04X  &&&&&&&&&&&&&&&\r\n",sum);
}

u8 ELD_get_tm(u8 *str,u32 *tm)
{
	u8 xbuf[5];
	u32 year,month,day,hour,min,sec;

	if(ELD_str_all_dec(str) == c_ret_nk) return c_ret_nk;
	
	ELD_strncpy(xbuf,str,2);
	month = ELD_str_dec(xbuf);
	
	ELD_strncpy(xbuf,str+2,2);
	day = ELD_str_dec(xbuf);
	
	ELD_strncpy(xbuf,str+4,2);
	year = ELD_str_dec(xbuf) + 2000;
	
	ELD_strncpy(xbuf,str+6,2);
	hour = ELD_str_dec(xbuf);
	
	ELD_strncpy(xbuf,str+8,2);
	min = ELD_str_dec(xbuf);
	
	ELD_strncpy(xbuf,str+10,2);
	sec = ELD_str_dec(xbuf);
	
	*tm = mktime(year,month,day,hour,min,sec);
	return c_ret_ok;
}

u8 ELD_packet_make_eld(u8 EventType,u16 EventCode,void *EventData)
{
    dbg(REPORT_DBG_EN,"EVENT type:%d,code:%X,data:%s\r\n",EventType,EventCode,(u8*)EventData);
	
	protocol_HEX_Format.EventType = EventType;
	protocol_HEX_Format.EventCode = EventCode;
	ELD_strncpy(protocol_HEX_Format.EventData,EventData,sizeof(protocol_HEX_Format.EventData)-1);

	protocol_HEX_Format.SequenceID = 0xffffffff;	//由ELD分配行号
			
     //if((protocol_ASCII_Format.Date[0] == 0)&&(protocol_ASCII_Format.Time[0] == 0))
    protocol_ASCII_Format.Date[0] = 0;
    protocol_ASCII_Format.Time[0] = 0;
	app_PackageToFlash();
	
	return c_ret_ok;
}

u8 ELD_set_system_parameter(u8 *EventData, u8 src)
{
	//$$APP_00000000000001, ,8,4,122417,162647,<123456>DateTime=122417162647|CarType=0000,CS**
	//AT$<123456>DateTime=122417162647|CarType=FFFF*
    //AT$<123456>DebugFlag=0*
	u8 xbuf[128], tbuf[sizeof(ELD_data.response_bt)], *px, *py, splitReq, obdInitReq;
    rtc_time tm;
	u32 rtc, x, y;
    
    px = (u8*)strchr((char*)EventData, '<');
    py = (u8*)strchr((char*)EventData, '>');
    if( (px != NULL) && (py != NULL) ) 
    {
        ELD_strncpy(tbuf, px + 1, py - px - 1);
        if(strcmp((char*)tbuf, (char*)SysParameter.UnitData.password) != 0) return c_ret_err;
        py++;    
    }
    else
    {
        py = EventData;
    }
    
    splitReq = obdInitReq = 0;    
    tbuf[0] = 0;
    y = ELD_str_chr_num(py,'|');
    for(x = 0; x <= y; x++)
    {
        ELD_strcpy_ht(xbuf, py, '|', x, sizeof(xbuf));
        
        if((px = (u8*)strstr((char*)xbuf,"SourceID=")) != NULL)
        {            
            px += sizeof("SourceID=")-1;
            ELD_strncpy(SysParameter.UnitData.SerialNum, px, sizeof(SysParameter.UnitData.SerialNum)-1);

            sprintf((char*)tbuf + strlen((char*)tbuf), "|SourceID=%s",SysParameter.UnitData.SerialNum);
            splitReq = 1;                
        }
        else if((px = (u8*)strstr((char*)xbuf,"PassWord=")) != NULL)
        {
            px += sizeof("PassWord=")-1;
            ELD_strncpy(SysParameter.UnitData.password, px, sizeof(SysParameter.UnitData.password)-1);

            sprintf((char*)tbuf + strlen((char*)tbuf), "|PassWord=%s",SysParameter.UnitData.password);
            splitReq = 1;   
            
        }
        else if((px = (u8*)strstr((char*)xbuf,"DateTime=")) != NULL)
        {            
            px += sizeof("DateTime=")-1;
            if(ELD_get_tm(px, &rtc) == c_ret_ok)
            {
                set_RTCCounter(rtc);
            }            

            rtc = get_RTCCounter();	// 秒计数器
            rtc_time_to_tm(rtc, &tm);	// Format: MMDDYY	Format: HHMMSS
            sprintf((char*)tbuf + strlen((char*)tbuf), "|DateTime=%02d%02d%02d", tm.tm_mon,tm.tm_mday,tm.tm_year % 2000);
            sprintf((char*)tbuf + strlen((char*)tbuf), "%02d%02d%02d", tm.tm_hour,tm.tm_min,tm.tm_sec);        
            splitReq = 1;                
        }
        else if((px = (u8*)strstr((char*)xbuf,"CarType=")) != NULL)
        {            
            px += sizeof("CarType=") - 1;
            if(ELD_str_all_hex(px) == c_ret_ok)
            {
                SysRunParameter.OBD_Parameter.carType = ELD_str_hex(px);
                obdInitReq = 1;
            }

            sprintf((char*)tbuf + strlen((char*)tbuf), "|CarType=%04X", SysRunParameter.OBD_Parameter.carType);
            splitReq = 1;    
            
        }
        else if((px = (u8*)strstr((char*)xbuf,"TotalMileage=")) != NULL)
        {            
            px += sizeof("TotalMileage=") - 1;
            if(ELD_str_all_dec(px) == c_ret_ok)
            {
                SysRunParameter.OBD_Parameter.total_trip_mileage = ELD_str_dec(px);
                obdInitReq = 1;
            }

            sprintf((char*)tbuf + strlen((char*)tbuf), "|TotalMileage=%d", SysRunParameter.OBD_Parameter.total_trip_mileage);
            splitReq = 1;    
            
        }
        else if((px = (u8*)strstr((char*)xbuf,"TotalFuel=")) != NULL)
        {            
            px += sizeof("TotalFuel=") - 1;
            if(ELD_str_all_dec(px) == c_ret_ok)
            {
                obdInitReq = 1;
                SysRunParameter.OBD_Parameter.total_fuel = ELD_str_dec(px);
            }            
            sprintf((char*)tbuf + strlen((char*)tbuf), "|TotalFuel=%d", SysRunParameter.OBD_Parameter.total_fuel);
            splitReq = 1;    
            
        }
        else if((px = (u8*)strstr((char*)xbuf,"EngineHours=")) != NULL)
        {            
            px += sizeof("EngineHours=") - 1;
            if(ELD_str_all_dec(px) == c_ret_ok)
            {
                obdInitReq = 1;
                SysRunParameter.OBD_Parameter.total_engineHours = ELD_str_dec(px);
            }            
            sprintf((char*)tbuf + strlen((char*)tbuf), "|EngineHours=%d", SysRunParameter.OBD_Parameter.total_engineHours);
            splitReq = 1;                
        }
        else if((px = (u8*)strstr((char*)xbuf,"FirmwareVersion=")) != NULL)
        {            
            sprintf((char*)tbuf + strlen((char*)tbuf), "|FirmwareVersion=%s", readVersion);
            splitReq = 1;                
        }
        else if((px = (u8*)strstr((char*)xbuf,"DebugFlag=")) != NULL)
        {
            px += sizeof("DebugFlag=") - 1;
            if(px[1] == 0)
            {
                if(px[0] == '0') ELD_data.btDbgEn = 1;
                else if(px[0] == '1')
                {
                    ELD_data.btDbgEn = ELD_data.btDbgReq = 0;
                    ELD_data.bt_upload_addr_en = ELD_data.bt_upload_date_en = 0;
                }
            }            
            sprintf((char*)tbuf + strlen((char*)tbuf), "|DebugFlag=%d", ELD_data.btDbgEn ^ 1);
            splitReq = 1;                
            
        }        
    }    
    if(splitReq == 0) return c_ret_nk;

    if(obdInitReq == 1)
    {
        obd_init( (SysRunParameter.OBD_Parameter.carType == 0) ? ((Power12v > 18000) ? 0x0000 :0xffff) : SysRunParameter.OBD_Parameter.carType,
            SysRunParameter.OBD_Parameter.total_trip_mileage,
            SysRunParameter.OBD_Parameter.total_fuel,
            SysRunParameter.OBD_Parameter.total_engineHours,
            SysParameter.UnitData.FuelType,
            SysParameter.UnitData.EngineDisplacement);   
        
        printf("obd_init:%04X\r\n", (SysRunParameter.OBD_Parameter.carType == 0) ? ((Power12v > 18000) ? 0x0000 :0xffff) : SysRunParameter.OBD_Parameter.carType);
            
        dbg(AD_DBG_EN,"odb_init\r\n");        
    }
    
    if(src == ELD_PACKET_bt)
    {
        sprintf((char*)ELD_data.response_bt,"$$%s",tbuf+1);    
        ELD_sum_set(ELD_data.response_bt+2);
        sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt), "**\r\n");
        ELD_data.btSendRespReq = 1;            
    }
    else if(src == ELD_PACKET_uart)
    {
        printf("$OK\r\n");    
    }
    
	return c_ret_ok;
}

u8 ELD_query_system_parameter(u8 *EventData,u8 src)
{
	//$$APP_00000000000001, ,8,3,122417,162647,<123456>DateTime|CarType,CS**
	//$$DateTime=122417162647|CarType=FFFF,CS**
	u8 xbuf[128], tbuf[sizeof(ELD_data.response_bt)], *px, *py, splitReq;
    rtc_time tm;
	u32 rtc, x, y;
    
    px = (u8*)strchr((char*)EventData, '<');
    py = (u8*)strchr((char*)EventData, '>');
    if( (px != NULL) && (py != NULL) ) 
    {
        ELD_strncpy(tbuf, px + 1, py - px - 1);
        if(strcmp((char*)tbuf, (char*)SysParameter.UnitData.password) != 0) return c_ret_err;
        py++;    
    }
    else
    {
        py = EventData;
    }
    
    splitReq = tbuf[0] = 0;
    
    y = ELD_str_chr_num(py,'|');
    for(x = 0; x <= y; x++)
    {
        ELD_strcpy_ht(xbuf, py, '|', x, sizeof(xbuf));
    
        if(strncmp((char*)xbuf,"SourceID",sizeof("SourceID")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|SourceID=%s",SysParameter.UnitData.SerialNum);
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"PassWord",sizeof("PassWord")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|PassWord=%s",SysParameter.UnitData.password);
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"DateTime",sizeof("DateTime")) == 0)
        {
            rtc = get_RTCCounter();	// 秒计数器
            rtc_time_to_tm(rtc, &tm);	// Format: MMDDYY	Format: HHMMSS
            sprintf((char*)tbuf + strlen((char*)tbuf), "|DateTime=%02d%02d%02d", tm.tm_mon,tm.tm_mday,tm.tm_year % 2000);
            sprintf((char*)tbuf + strlen((char*)tbuf), "%02d%02d%02d", tm.tm_hour,tm.tm_min,tm.tm_sec);        
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"CarType",sizeof("CarType")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|CarType=%04X",SysRunParameter.OBD_Parameter.carType);
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"TotalMileage",sizeof("TotalMileage")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|TotalMileage=%d", SysRunParameter.OBD_Parameter.total_trip_mileage);
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"TotalFuel",sizeof("TotalFuel")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|TotalFuel=%d", SysRunParameter.OBD_Parameter.total_fuel);
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"EngineHours",sizeof("EngineHours")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|EngineHours=%d", SysRunParameter.OBD_Parameter.total_engineHours);
            splitReq = 1;    
        }
        else if(strncmp((char*)xbuf,"FirmwareVersion",sizeof("FirmwareVersion")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|FirmwareVersion=%s", readVersion);
            splitReq = 1;    
        }
    }    
    if(splitReq == 0) 
    {
        if(strncmp((char*)py,"=?",sizeof("=?")) == 0)
        {
            sprintf((char*)tbuf + strlen((char*)tbuf), "|SourceID=%s",SysParameter.UnitData.SerialNum);
            sprintf((char*)tbuf + strlen((char*)tbuf), "|PassWord=%s",SysParameter.UnitData.password);
            rtc = get_RTCCounter();	// 秒计数器
            rtc_time_to_tm(rtc, &tm);	// Format: MMDDYY	Format: HHMMSS
            sprintf((char*)tbuf + strlen((char*)tbuf), "|DateTime=%02d%02d%02d", tm.tm_mon,tm.tm_mday,tm.tm_year % 2000);
            sprintf((char*)tbuf + strlen((char*)tbuf), "%02d%02d%02d", tm.tm_hour,tm.tm_min,tm.tm_sec);        
            sprintf((char*)tbuf + strlen((char*)tbuf), "|CarType=%04X",SysRunParameter.OBD_Parameter.carType);
            sprintf((char*)tbuf + strlen((char*)tbuf), "|TotalMileage=%d", SysRunParameter.OBD_Parameter.total_trip_mileage);
            sprintf((char*)tbuf + strlen((char*)tbuf), "|TotalFuel=%d", SysRunParameter.OBD_Parameter.total_fuel);
            sprintf((char*)tbuf + strlen((char*)tbuf), "|EngineHours=%d", SysRunParameter.OBD_Parameter.total_engineHours);
            sprintf((char*)tbuf + strlen((char*)tbuf), "|FirmwareVersion=%s", readVersion);
            sprintf((char*)tbuf + strlen((char*)tbuf), "|DebugFlag=%d", ELD_data.btDbgEn ^ 1);            
        }
        else
            return c_ret_nk;
    }
    
    if(src == ELD_PACKET_bt)
    {
        sprintf((char*)ELD_data.response_bt,"$$%s",tbuf+1);
        ELD_sum_set(ELD_data.response_bt+2);
        sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt), "**\r\n");
        ELD_data.btSendRespReq = 1;            
    }
    else if(src == ELD_PACKET_uart)
    {
        printf("$SYSS=%s*\r\n",tbuf+1);    
    }
	return c_ret_ok;
}

u8 ELD_file_name_check(u8 *name) //request:{source_id: ELD000000001,events: [{0001,8,8,,,,,,AT$FOAT=|112.74.16.127|21|wwwftp|123wohegebaoSXD|/update/ELD_v1_0_4_enc.bin|*,,,,,,,,,,,,1,58,01}]}
{	//路径和文件分隔用‘/’，第一个字符是字母或数字或下划线，最后一个字符不能是‘.’或'/',文件名不能包含 \:*?"<>|,;10个字符
	u32 x;
	u8 c;
	
	for(x = 0; name[x] != 0; x++)
	{
		if(name[x] == '/')				// 检测文件或路径的第一个字符
		{
			c = name[x+1];
			if(		((c >= '0')&&(c <= '9'))
				||	((c >= 'a')&&(c <= 'z'))
				||	((c >= 'A')&&(c <= 'Z'))
				||	(c == '_'))
			{	
            }
			else 
				return c_ret_nk;
		}	
		else if(name[x+1] == '/')		// 检测文件或路径的最后一个字符
		{
			c = name[x];
			if(	(c == '.') || (c == '/') ) return c_ret_nk;
		}			
		else
		{
			c = name[x];
			if(		(c == '\\')
				||	(c == ':')
				||	(c == '*')
				||	(c == '?')
				||	(c == '"')
				||	(c == '<')
				||	(c == '>')
				||	(c == '|')
				||	(c == ',')
				||	(c == ';') )
				return c_ret_nk;
		}				
	}	
	return c_ret_ok;	
}

u8 ELD_ip_str_check(u8 *ip)
{	//192.168.100.101
	u8 tbuf[16],x;
	
	if(strlen((char*)ip) >= sizeof(tbuf)) return c_ret_nk;
	if(ELD_str_chr_num(ip,'.') != 3) return c_ret_nk;
	
	for(x = 0;x < 4;x++)
	{
		ELD_strcpy_ht(tbuf,ip,'.',x,sizeof(tbuf));
		if(ELD_str_all_dec(tbuf)==c_ret_nk) return c_ret_nk;
	}
	return c_ret_ok;	
}

u8 ELD_set_network_parameter(u8 *EventData)
{
	//AT$GPRS=|UDPTCP|HostAddress|Port|APN|UserName|UserPassword|* 
	u8 tbuf[61], *px, *py, x;

    px = (u8*)strchr((char*)EventData, '<');
    py = (u8*)strchr((char*)EventData, '>');
    if( (px != NULL) && (py != NULL) ) 
    {
        ELD_strncpy(tbuf, px + 1, py - px - 1);
        if(strcmp((char*)tbuf, (char*)SysParameter.UnitData.password) != 0) return c_ret_nk;
        py++;    
    }
    else
    {
        py = EventData;
    }
	
	if(ELD_str_chr_num(py,'|') != 7) return c_ret_nk;
	if(py[strlen((char*)py)-1] != '*') return c_ret_nk;
	if( (px = (u8*)strstr((char*)py,"AT$GPRS=")) == NULL) return c_ret_nk;
	px += sizeof("AT$GPRS=") - 1;
	
	ELD_strcpy_ht(tbuf,px,'|',1,sizeof(tbuf));
	if(tbuf[0] != 0) 
		SysParameter.sysNetworkParameter.tcpudp = ELD_str_dec(tbuf);	//0:udp,1:tcp
	
	ELD_strcpy_ht(tbuf,px,'|',2,sizeof(tbuf));
	if(tbuf[0] != 0) 
	{	
		x = 'D';
		if(ELD_str_chr_num(tbuf,'.')==3)
		{
			if(ELD_ip_str_check(tbuf) == c_ret_ok)
			{	
				SysParameter.sysNetworkParameter.DailMode = 0;
				ELD_strncpy(SysParameter.sysNetworkParameter.Ip,tbuf,sizeof(SysParameter.sysNetworkParameter.Ip)-1);				
				x = 'I';
			}
		}
		if(x == 'D')
		{
			SysParameter.sysNetworkParameter.DailMode = 1;
			ELD_strncpy(SysParameter.sysNetworkParameter.Domain,tbuf,sizeof(SysParameter.sysNetworkParameter.Domain)-1);
		}
	}
	
	ELD_strcpy_ht(tbuf,px,'|',3,sizeof(tbuf));
	if(tbuf[0] != 0) 
	{	
		if(ELD_str_all_dec(tbuf) == c_ret_ok)
		{	
			ELD_strncpy(SysParameter.sysNetworkParameter.Port,tbuf,sizeof(SysParameter.sysNetworkParameter.Port)-1);
		}
	}	
	
	ELD_strcpy_ht(tbuf,px,'|',4,sizeof(tbuf));
	if(tbuf[0] != 0) 
		ELD_strncpy(SysParameter.sysNetworkParameter.Apn,tbuf,sizeof(SysParameter.sysNetworkParameter.Apn)-1);

	ELD_strcpy_ht(tbuf,px,'|',5,sizeof(tbuf));
	if(tbuf[0] != 0) 
		ELD_strncpy(SysParameter.sysNetworkParameter.User,tbuf,sizeof(SysParameter.sysNetworkParameter.User)-1);

	ELD_strcpy_ht(tbuf,px,'|',6,sizeof(tbuf));
	if(tbuf[0] != 0) 
		ELD_strncpy(SysParameter.sysNetworkParameter.Pwd,tbuf,sizeof(SysParameter.sysNetworkParameter.Pwd)-1);
	
	
	return c_ret_ok;
}

u8 ELD_query_network_parameter(u8 *EventData,u8 src)
{
	//1001,8,3,1,1,3,060317,131647,AT$SYSS=?,,,,,,,,,,,,XX,XX
	//AT$GPRS=?
	//response:{results:[{5B70:1}]}
	u8 tbuf[128], *px, *py;

    px = (u8*)strchr((char*)EventData, '<');
    py = (u8*)strchr((char*)EventData, '>');
    if( (px != NULL) && (py != NULL) ) 
    {
        ELD_strncpy(tbuf, px + 1, py - px - 1);
        if(strcmp((char*)tbuf, (char*)SysParameter.UnitData.password) != 0) return c_ret_nk;
        py++;    
    }
    else
    {
        py = EventData;
    }
	
	if(strncmp((char*)py, "AT$GPRS=?", sizeof("AT$GPRS=?")) != 0) return c_ret_nk;
	
	switch(src)
	{
		case ELD_PACKET_gprs:
			ELD_data.gprs_upload_addr_en = 1;
		break;
		
			
		case ELD_PACKET_bt:
			ELD_data.bt_upload_addr_en = 1;
		break;
		
		case ELD_PACKET_uart:return c_ret_ok;
		case ELD_PACKET_sms: return c_ret_ok;		
		default:return c_ret_nk;
	}		

	
	//5B71,8,4,1,1,3,060317,131647,$SYSS=|123456|eld123456789abc|0|0|*,,,,,,,,,,,,XX,XX
	//AT$SYSS=|PassWord|DeviceID|TotalMileage|EngineHours*
	sprintf((char*)tbuf,"$GPRS=|%d|%s|%s|%s|%s|%s|*",
			SysParameter.sysNetworkParameter.tcpudp,
			SysParameter.sysNetworkParameter.DailMode ? SysParameter.sysNetworkParameter.Domain : SysParameter.sysNetworkParameter.Ip,
			SysParameter.sysNetworkParameter.Port,
			SysParameter.sysNetworkParameter.Apn,
			SysParameter.sysNetworkParameter.User,
			SysParameter.sysNetworkParameter.Pwd);
	
	ELD_packet_make_eld(8,7,tbuf);

	if(src == ELD_PACKET_gprs)
		ELD_data.gprs_upload_report_addr = ELD_report_last_addr(SysRunParameter.FLASH_Parameter.WriteDataAddr);
	else if(src == ELD_PACKET_bt)
		ELD_data.bt_upload_report_addr = ELD_report_last_addr(SysRunParameter.FLASH_Parameter.WriteDataAddr);
	
	return c_ret_nk;
}

static void ELD_event_reset(void)
{
	SYS_SoftReset();
	while(1) STM_RESET_DOG();	
}

u8 ELD_ftp_update(u8 *EventData,u8 src)
{
//	ftp账号：wwwftp
//	密码：123wohegebaoSXD
//	网站路径：C:\phpStudy\WWW\nyi-tech\update
//request:{source_id: ELD000000001,events: [{0001,8,8,,,,,,AT$FOAT=|112.74.16.127|21|wwwftp|123wohegebaoSXD|/update/ELD_v1_0_4_enc.bin|*,,,,,,,,,,,,1,58,01}]}
	//AT$FOAT=|ServerIPaddress|Port|Username|Password|FirmwareFileName|*.

	u32 x;
	u8 *px, *py, tbuf[sizeof(ftpSendPara.file)];

    px = (u8*)strchr((char*)EventData, '<');
    py = (u8*)strchr((char*)EventData, '>');
    if( (px != NULL) && (py != NULL) ) 
    {
        ELD_strncpy(tbuf, px + 1, py - px - 1);
        if(strcmp((char*)tbuf, (char*)SysParameter.UnitData.password) != 0) return c_ret_err;
        py++;    
    }
    else
    {
        py = EventData;
    }
	
	if( (px = (u8*)strstr((char*)py,"AT$FOAT=")) == NULL) return c_ret_err;
    if(src == ELD_PACKET_uart) return c_ret_ok;
    
	px += sizeof("AT$FOAT=") - 1;           
	if(		(ELD_str_chr_num(px,'|') != 6)
		||	(px[strlen((char*)px)-1] != '*')
		||	(ELD_strlen_ht(px,'|',1) >= sizeof(ftpSendPara.ip))
		||	(ELD_strlen_ht(px,'|',2) >= 6)
		||	(ELD_strlen_ht(px,'|',3) >= sizeof(ftpSendPara.name))
		||	(ELD_strlen_ht(px,'|',4) >= sizeof(ftpSendPara.password))
		||	(ELD_strlen_ht(px,'|',5) >= sizeof(ftpSendPara.file)) )
		return c_ret_err;
	
    strcpy((char*)ftpSendPara.ip,(char*)"112.74.16.127");
    ftpSendPara.port = 21;
    strcpy((char*)ftpSendPara.name,"wwwftp");
    strcpy((char*)ftpSendPara.password,"123wohegebaoSXD");
    
	ELD_strcpy_ht(tbuf,px,'|',1,sizeof(tbuf));
    if(tbuf[0] != 0x00)
    {   
        if(ELD_str_chr_num(tbuf,'.')==3)
        {
            if(ELD_ip_str_check(tbuf) == c_ret_ok)
                strcpy((char*)ftpSendPara.ip,(char*)tbuf);	
            else return c_ret_nk;
        }
        else
            strcpy((char*)ftpSendPara.ip,(char*)tbuf);
    }
        
	ELD_strcpy_ht(tbuf,px,'|',2,sizeof(tbuf));
    if(tbuf[0] != 0x00)
    {
        if(ELD_str_all_dec(tbuf) != c_ret_ok)
            return c_ret_nk;
        x = ELD_str_dec(tbuf);
        if((x >= 65536) || (x == 0)) return c_ret_nk;
        ftpSendPara.port = x;
    }
    
   	ELD_strcpy_ht(tbuf,px,'|',3,sizeof(tbuf));
    if(tbuf[0] != 0)
    {    
        strcpy((char*)ftpSendPara.name,(char*)tbuf);
	}
    
   	ELD_strcpy_ht(tbuf,px,'|',4,sizeof(tbuf));
    if(tbuf[0] != 0x00)
    {        
        strcpy((char*)ftpSendPara.password,(char*)tbuf);
    }
    
	ELD_strcpy_ht(tbuf, px, '|', 5,sizeof(tbuf));
	if(ELD_file_name_check(tbuf) == c_ret_nk) return c_ret_nk;
	strcpy((char*)ftpSendPara.file,(char*)tbuf);
	
	ftpSendPara.src = src;
	ELD_data.ftpSendSrc = src;
	return c_ret_ok;
}

u8 ELD_report_tm_ht(u32* s, u32* e)
{
	u16 len;
	u8 ret, rBuf[GPS_FLASH_SIZE], tbuf[32]; 	
	
	ret = 0xff;
	if(SysRunParameter.FLASH_Parameter.StartDataAddr != SysRunParameter.FLASH_Parameter.WriteDataAddr)
	{	
		spiFlash_readAddrSet(SysRunParameter.FLASH_Parameter.StartDataAddr);
		while(1)
		{	
			memset(rBuf, 0x00, sizeof(rBuf));
			ret = ReadFlashToData(rBuf, &len , c_bt_confirm_all);	//0xff,TRUE,FLASE
			if(TRUE == ret)
			{
				if(ELD_str_chr_num(rBuf,',') == ELD_SPLIT_NUM)
				{	
					ELD_strcpy_ht(tbuf,                    rBuf, ',', 4, 7);
					ELD_strcpy_ht(tbuf+strlen((char*)tbuf),rBuf, ',', 5, 7);
					if(ELD_get_tm(tbuf, s)==c_ret_ok)
					{
						break;
					}			
				}
			}
			else if(ret == 0xff) 	// 没有有效报告
			{
				break;
			}
		}
		if(ret == TRUE)
		{	
			spiFlash_readAddrSet(ELD_report_last_addr(SysRunParameter.FLASH_Parameter.WriteDataAddr));
			while(1)
			{	
				memset(rBuf, 0x00, sizeof(rBuf));
				ret = ReadFlashToData(rBuf, &len , c_bt_confirm_all);	//0xff,TRUE,FLASE
				if(TRUE == ret)
				{
					if(ELD_str_chr_num(rBuf,',') == ELD_SPLIT_NUM)
					{	
                        ELD_strcpy_ht(tbuf,                    rBuf, ',', 4, 7);
                        ELD_strcpy_ht(tbuf+strlen((char*)tbuf),rBuf, ',', 5, 7);
						if(ELD_get_tm(tbuf, e)==c_ret_ok)
						{
							break;
						}			
					}
				}
				else if(ret == 0xff) 	// 没有有效报告
				{
					break;
				}
			}
		}
	}
    return (ret == 0xff) ? c_ret_nk : c_ret_ok;    
}

//  Query logs for a period of time查询某一段时间的日志[4-2]
static u8 ELD_bt_query_42(u8 *str)
{
	u8 tbuf[32];
	u32 sr, si, er, ei; 
    
	if((strlen((char*)str) >= 30) || (ELD_str_chr_num(str,'|') != 1)) return c_ret_nk;
			
	ELD_strcpy_ht(tbuf,str,'|',0,sizeof(tbuf));
	if(ELD_str_all_dec(tbuf) == c_ret_ok)
	{
		if(ELD_get_tm(tbuf,&si)==c_ret_ok)
		{	
			ELD_strcpy_ht(tbuf,str,'|',1,sizeof(tbuf));
			if(ELD_get_tm(tbuf,&ei)==c_ret_ok)			
			{
                if(ELD_report_tm_ht(&sr, &er) == c_ret_ok)
                {
                    if(     ((si >= sr)&&(si <= er))
                        ||  ((ei >= sr)&&(ei <= er))
                        ||  ((si <= sr)&&(si <= er)&&(ei >= sr)&&(ei >= er)) )
                    {
                        ELD_data.bt_upload_tm_start = si;
                        ELD_data.bt_upload_tm_end   = ei;                        
                        ELD_data.bt_upload_date_en  = 1;
                        ELD_data.bt_upload_addr_en  = 0;
                        ELD_data.bt_upload_report_addr = SysRunParameter.FLASH_Parameter.StartDataAddr;
                        return c_ret_ok;
                    }
                }
			}
		}			
	}
	return c_ret_nk;	
}

// Query all log start time and end time查询所有日志起始时间和结束时间[4-3]
static u8	ELD_bt_query_43(void)
{
	rtc_time tm;
	u8 tbuf[32]; 	
    u32 s, e;
 	
    if(ELD_report_tm_ht(&s,&e)==c_ret_nk)
	{	
		rtc_time_to_tm(protocol_HEX_Format.RtcTime, &tm);	// Format: MMDDYY	Format: HHMMSS
		s = e = mktime(tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);		
	}

	rtc_time_to_tm(s, &tm);
	sprintf((char*)tbuf,"$$%02d%02d%02d", tm.tm_mon,
										tm.tm_mday,
										tm.tm_year % 100);
	sprintf((char*)tbuf+strlen((char*)tbuf),"%02d%02d%02d|", 
										tm.tm_hour,
										tm.tm_min,
										tm.tm_sec);
	
	rtc_time_to_tm(e, &tm);	
	sprintf((char*)tbuf+strlen((char*)tbuf),"%02d%02d%02d", 
										tm.tm_mon,
										tm.tm_mday, 
										tm.tm_year % 100);
	sprintf((char*)tbuf+strlen((char*)tbuf),"%02d%02d%02d", 
										tm.tm_hour,
										tm.tm_min,
										tm.tm_sec);	
    
	ELD_sum_set(tbuf+2);
    sprintf((char*)tbuf + strlen((char*)tbuf), "**\r\n");

    strcpy((char*)ELD_data.response_bt,(char*)tbuf);
    ELD_data.btSendRespReq = 1;            
	return c_ret_ok;    
}

u8 ELD_set_evt21_parameter(u8 *EventData)
{   //$$APP_00000000000001, ,2,2,122417,162647,1|5|010D|010C|0105|012F|VPWR|ODOM|FUEL|HOUR|STOR|PEND|VINC|GXYZ|AXYZ,CS**    
	u8  xbuf[128], tbuf[sizeof(ELD_data.response_bt)], *py;
	u32 x, y, idx, pid, para;
        
	py = EventData;    
    y = ELD_str_chr_num(py, '|');
    if(y >= sizeof(SysRunParameter.ELD_parameter.evt21para)/sizeof(SysRunParameter.ELD_parameter.evt21para[0]) + 2)
        return c_ret_nk;
    
    tbuf[0] = 0;
    while(1)
    {        
        ELD_strcpy_ht(xbuf, py, '|', 0, sizeof(xbuf));
        if(ELD_str_all_dec(xbuf) == c_ret_ok)
        {                
            SysRunParameter.ELD_parameter.evt21freq = ELD_str_dec(xbuf);            
        }
        sprintf((char*)tbuf + strlen((char*)tbuf), "|%d", SysRunParameter.ELD_parameter.evt21freq);
        if(y == 0) break;
        
        ELD_strcpy_ht(xbuf, py, '|', 1, sizeof(xbuf));
        if(ELD_str_all_dec(xbuf) == c_ret_ok)
        {                
            x = ELD_str_dec(xbuf);
            if((x == 0)||(x == 5)||(x == 10)||(x == 1))
            {
                SysRunParameter.ELD_parameter.evt21freqAcc = ELD_str_dec(xbuf);                
            }            
        }
        sprintf((char*)tbuf + strlen((char*)tbuf), "|%d", SysRunParameter.ELD_parameter.evt21freqAcc);
        if(y == 1) break;

        for(x = 0; x < sizeof(SysRunParameter.ELD_parameter.evt21para)/sizeof(SysRunParameter.ELD_parameter.evt21para[0]);x++)
        {
            SysRunParameter.ELD_parameter.evt21para[x] = 0;
        }    
        idx = 0;        
        for(x = 2; x <= y; x++)
        {
            ELD_strcpy_ht(xbuf, py, '|', x, sizeof(xbuf));
            
            if(strncmp((char*)xbuf,"VPWR",sizeof("VPWR")) == 0)
            {   
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_vpwr;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|VPWR");
            }
            else if(strncmp((char*)xbuf,"ODOM",sizeof("ODOM")) == 0)
            {
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_odom;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|ODOM");
            }
            else if(strncmp((char*)xbuf,"FUEL",sizeof("FUEL")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_fuel;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|FUEL");
            }
            else if(strncmp((char*)xbuf,"HOUR",sizeof("FUEL")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_hour;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|HOUR");
            }
            else if(strncmp((char*)xbuf,"VINC",sizeof("VINC")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_vinc;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|VINC");
            }
            else if(strncmp((char*)xbuf,"AXYZ",sizeof("AXYZ")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_axyz;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|AXYZ");
            }
            else if(strncmp((char*)xbuf,"GXYZ",sizeof("GXYZ")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_gxyz;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|GXYZ");
            }
            else if(strncmp((char*)xbuf,"CODE",sizeof("CODE")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_code;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|CODE");
            }     
            else if(strncmp((char*)xbuf,"TRIM",sizeof("TRIM")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_trim;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|TRIM");
            }        
            else if(strncmp((char*)xbuf,"TRID",sizeof("TRID")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_trid;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|TRID");
            }        
            else if(strncmp((char*)xbuf,"TRIF",sizeof("TRIF")) == 0)
            {            
                SysRunParameter.ELD_parameter.evt21para[idx++] = c_evt21para_trif;            
                sprintf((char*)tbuf + strlen((char*)tbuf), "|TRIF");
            }        
            else if(ELD_str_all_hex(xbuf) == c_ret_ok)
            {            
                pid = ELD_str_hex(xbuf);
                SysRunParameter.ELD_parameter.evt21para[idx++] = pid;
                sprintf((char*)tbuf + strlen((char*)tbuf), "|%04X", pid);
            }
        }
        break;    
    }

    for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
    {
        ELD_data.obd.pid[x].enable = 0;
    }    
    for(y = 0; y < sizeof(SysRunParameter.ELD_parameter.evt21para)/sizeof(SysRunParameter.ELD_parameter.evt21para[0]); y++)
    {
        para = SysRunParameter.ELD_parameter.evt21para[y];
        if(para == 0) break;
        if(para < 0xff00)
        {
            for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
            {
                if(para == ELD_data.obd.pid[x].name)
                {
                    ELD_data.obd.pid[x].enable = 1;
                    break;
                }
            }
        }
    }    
    
    sprintf((char*)ELD_data.response_bt,"$$%s",tbuf+1);
	ELD_sum_set(ELD_data.response_bt+2);
    sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt), "**\r\n");
    ELD_data.btSendRespReq = 1;            

	return c_ret_ok;        
}

u8 ELD_set_evt75_parameter(u8* EventData)
{
	u8  xbuf[128], *py;
	u32 x, y, z;
        
	py = EventData;    
    y = ELD_str_chr_num(py, '|');   // pid1|pid2|pid3|pid4|pid5
    if(y >= sizeof(ELD_data.obd.sendArg)/sizeof(ELD_data.obd.sendArg[0]))
        return c_ret_nk;
    
    for(x = 0; x < sizeof(ELD_data.obd.sendArg)/sizeof(ELD_data.obd.sendArg[0]); x++)
        ELD_data.obd.sendArg[x] = 0;
    
    for(z = x = 0; x <= y; x++)
    {
        ELD_strcpy_ht(xbuf, py, '|', x, sizeof(xbuf));
        if(ELD_str_all_hex(xbuf) == c_ret_ok)
        {
            ELD_data.obd.sendArg[x] = ELD_str_hex(xbuf);            
            z = 1;
        }
    }
    if(z != 0)
    {
        ELD_data.obd.req.b.pidx  = 1;
        ELD_data.obd.wait.b.pidx = 0;
        return c_ret_ok;
    }
    else 
        return c_ret_nk;
}

u8 ELD_packet_make_ext(u8 *buf,u8 src)
{
	u8 tbuf[201], sendResp;
	PROTOCOL_HEX_FORMAT result;
	u32 ux;
    
	result = protocol_HEX_Format;
    
    // $$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**
 	ELD_strcpy_ht(tbuf,buf, ',', 0, sizeof(tbuf));              
	ELD_strncpy(protocol_ASCII_Format.deviceID,tbuf,sizeof(protocol_ASCII_Format.deviceID)-1);
    
	ELD_strcpy_ht(tbuf, buf, ',', 1, sizeof(tbuf));
    if( (tbuf[0] == 0x00) || (ELD_str_all_hex(tbuf) == c_ret_nk) )
		result.SequenceID = 0xffffffff;				            // 空白，由ELD分配行号
	else
		result.SequenceID = ELD_str_hex(tbuf);			        // 不分配行号，直接存储 
	
	ELD_strcpy_ht(tbuf,buf, ',', 2, sizeof(tbuf));
	result.EventType = ELD_str_dec(tbuf)&0xff;

	ELD_strcpy_ht(tbuf,buf, ',', 3, sizeof(tbuf));
	result.EventCode = ELD_str_dec(tbuf)&0xff;

 	ELD_strcpy_ht(tbuf,buf, ',', 4, sizeof(tbuf));              // MMDDYY
	ELD_strncpy(protocol_ASCII_Format.Date,tbuf,sizeof(protocol_ASCII_Format.Date)-1);

 	ELD_strcpy_ht(tbuf,buf, ',', 5, sizeof(tbuf));              // HHMMSS
	ELD_strncpy(protocol_ASCII_Format.Time,tbuf,sizeof(protocol_ASCII_Format.Time)-1);
    

	ELD_strcpy_ht(tbuf, buf, ',', 6, sizeof(tbuf));
	ELD_strncpy(result.EventData,tbuf,sizeof(result.EventData)-1);
 
	protocol_HEX_Format = result;	
        
	app_PackageToFlash();

	sendResp = 0;

    if(result.EventType == 2)
    {
		switch(result.EventCode)
		{
            case 2:                 
                if(ELD_set_evt21_parameter(result.EventData)==c_ret_nk) sendResp = 1;                   
            break;      
            
        }        
    }
//	ELD和蓝牙移动应用程序之间的响应延时是15s，超时就认为响应失败。失败后重复发送两次，依然响应失败，那就中断本次的交互。
    if(result.EventType == 4)
	{
		switch(result.EventCode)
		{
			case 1:	// 蓝牙移动应用程序请求ELD获取最近1天的日志
                if(SysRunParameter.FLASH_Parameter.StartDataAddr != SysRunParameter.FLASH_Parameter.WriteDataAddr)
                {                    
                    ELD_data.bt_upload_tm_start = get_RTCCounter() - 1L * 24 * 3600;
                    ELD_data.bt_upload_tm_end   = get_RTCCounter();			
                    ELD_data.bt_upload_date_en = 1;
                    ELD_data.bt_upload_addr_en = 0;
                    ELD_data.bt_upload_report_addr = SysRunParameter.FLASH_Parameter.StartDataAddr;
                }
                else sendResp = 1;
			break;
						
			case 2:	
				if(ELD_bt_query_42(result.EventData) == c_ret_nk) sendResp = 1;
			break;	
			
			case 3:	                
				if(ELD_bt_query_43() == c_ret_nk) sendResp = 1;
			break;	

			default:sendResp = 0xff;break;	
		}
	}

	if(result.EventType == 7)
	{
		switch(result.EventCode)
		{
			case 1:	// 读支持的pid列表    $$APP_0000000000001,,7,1,122417,162647,,CS**
                if(ELD_data.acc_on == 0)
                {
                    strcpy((char*)ELD_data.response_bt,"$$FAIL=3**\r\n"); 
                    ELD_data.btSendRespReq = 1;                                
                }
                else
                {
                    ELD_data.obd.req.b.sup = 1;    
                    ELD_data.obd.wait.b.sup= 0;
                }
            break;
                
			case 2:	// 读故障码     $$APP_0000000000001,,7,2,122417,162647,0,CS**
                if(ELD_data.acc_on == 0)
                {
                    strcpy((char*)ELD_data.response_bt,"$$FAIL=3**\r\n"); 
                    ELD_data.btSendRespReq = 1;                                
                }
                else
                {
                    if(result.EventData[1] == 0)
                    {
                        if(result.EventData[0] == '0')        
                        {
                            ELD_data.obd.req.b.pendCod = ELD_data.obd.wait.b.pendCod = ELD_data.obd.wait.b.storCod = 0;
                            ELD_data.obd.req.b.storCod = 1;
                        }
                        else if(result.EventData[0] == '1')
                        {
                            ELD_data.obd.req.b.storCod = ELD_data.obd.wait.b.pendCod = ELD_data.obd.wait.b.storCod = 0;
                            ELD_data.obd.req.b.pendCod = 1;
                        }
                    }
                }
            break;
            
			case 3:	// 清故障码     $$APP_0000000000001,,7,3,122417,162647,,CS**
                if(ELD_data.acc_on == 0)
                {
                    strcpy((char*)ELD_data.response_bt,"$$FAIL=3**\r\n"); 
                    ELD_data.btSendRespReq = 1;                                
                }
                else
                {                
                    ELD_data.obd.req.b.cle  = 1;
                    ELD_data.obd.wait.b.cle = 0;
                }
            break;
            
			case 4:	// 读VIN码    $$APP_0000000000001,,7,4,122417,162647,,CS**
                if(ELD_data.acc_on == 0)
                {
                    strcpy((char*)ELD_data.response_bt,"$$FAIL=3**\r\n"); 
                    ELD_data.btSendRespReq = 1;                                
                }
                else
                {                
                    ELD_data.obd.req.b.vin  = 1;
                    ELD_data.obd.wait.b.vin = 0;
                }
			break;	
            
            case 5: // 读取某几个PID的值  $$APP_0000000000001,,7,5,122417,162647,|pid1|pid2|...pid5|,CS**
                if(ELD_data.acc_on == 0)
                {
                    strcpy((char*)ELD_data.response_bt,"$$FAIL=3**\r\n"); 
                    ELD_data.btSendRespReq = 1;                                
                }
                else
                {               
                    if(ELD_set_evt75_parameter(result.EventData)==c_ret_nk) sendResp = 1;          
                }                    
            break;    
                
        }
    }
        
	if(result.EventType == 8)
	{
		switch(result.EventCode)
		{
			case 1:	// 重启设备				
				ELD_event_reset();            
			break;	
			
			case 2:	// 恢复默认参数
                ux = protocol_HEX_Format.SequenceID;
                if(result.EventData[1] == 0)
                {
                    if(result.EventData[0] == '2')
                    {
                        DefalutSet_SysRunParameter(2);
                    }
                    else if(result.EventData[0] == '1')
                    {
                        SetDefalutSysParameter();	
                        DefalutSet_SysRunParameter(1);
                    }
                    else if(result.EventData[0] == '0')    
                    {
                        SetDefalutSysParameter();	
                        DefalutSet_SysRunParameter(0);
                    }                        
                }
			    upload_RundParaData();
                protocol_HEX_Format.SequenceID = ux;
                sendResp = 1;
			break;
						
			case 3:	// 查询系统参数
				if(ELD_query_system_parameter(result.EventData,src)==c_ret_nk) sendResp = 1;
			break;	
			
			case 4:	// 设置系统参数 
				if(ELD_set_system_parameter(result.EventData, src)==c_ret_nk) sendResp = 1;
			break;	
			
			case 5:	//  Query collision data获取碰撞数据 [8-5]
				//request_result = ELD_set_network_parameter(result.EventData);
			break;	
			
            case 9: // 通过蓝牙升级   $$ APP_12345678, ,8,9,122417,151747, ,XXXX **
                if(src == ELD_PACKET_bt)
                {
                    ftpSendPara.src = src;
                    ELD_data.ftpSendSrc = src;
                    memcpy(ftpSendPara.btMac,bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btMAC,6);        

                    sprintf((char*)ELD_data.response_bt, "busying");
                    ELD_data.btSendRespReq = 1;                    
                }
                else sendResp = 1;
            break;
			
			default:sendResp = 0xff;break;	
		}	
	}
    
	if(sendResp == 1)
	{	
    	// $$SequenceID**
        sprintf((char*)ELD_data.response_bt,"$$%04X**\r\n",protocol_HEX_Format.SequenceID);
        ELD_data.btSendRespReq = 1;    
	}	
	return c_ret_ok;
}

static u8 ELD_spaceTrimChar(u8 chr)
{
    if(     (chr == '|')
        ||  (chr == ',')
        ||  (chr == '.')
        //||  (chr == '{')
        //||  (chr == '}')
        //||  (chr == '[')
        //||  (chr == ']')
        //||  (chr == ':') 

        ||  (chr == '$')
        ||  (chr == '*')

        ||  (chr == '=')
    
    
        ||  (chr == '\r')
        ||  (chr == '\n') )
    {
        return c_ret_ok;
    }
    return c_ret_nk;
    
}

static u16 ELD_logStrTrim(u8* pTail,u8* pHead,u8* pEnd,u8* pStart,u8* buf)
{
    u8 *px, c, key = '|';
    u16 x;
    
    x = 0;
    while(pTail != pHead)
    {       
        c = *pTail;
        if(     (( c < ' ')&&(c != '\r')&&(c != '\n'))
            ||  ( c > '~') )
        {
        }
        else if(c == ' ')
        {
            if(ELD_spaceTrimChar(key) == c_ret_ok) // 连续空格前面是关键字
            {                                   
            }
            else                                    // 连续空格后面是关键字
            {
                px = pTail;
                while(px != pHead)
                {
                    c = *px;
                    if(c == ' ') px++;
                    else
                    {                        
                        if(ELD_spaceTrimChar(c) == c_ret_ok)
                        {
                            key = buf[x++] = c;
                            pTail = px;
                        }
                        else
                        {
                            while(pTail != px)
                            {
                                key = buf[x++] = *pTail;
                                pTail++;
                                if((pEnd != NULL)&&(pStart != NULL))
                                {
                                    if(pTail >= pEnd) pTail = pStart;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            
        }
        else
        {
            key = c;
            if((c == '\r')||(c == '\n'))
            {
            }
            else
                buf[x++] = c;            
        }
        pTail++;
        if((pEnd != NULL)&&(pStart != NULL))
        {
            if(pTail >= pEnd) pTail = pStart;
        }
    }        
    buf[x] = 0;
    
    return x;
}

static u16 ELD_packetGet(u8* buf,u8 src)
{
    u8 *pTail, *pStart, *pEnd, *pHead, c;
    u16 x, y, len;
    
    {
        pTail = &bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf[bt_Message.bt_LinkState[ELD_data.bt_currentTab].rxTail];
        pHead = &bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf[bt_Message.bt_LinkState[ELD_data.bt_currentTab].rxHead];
        pStart= &bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf[0];
        pEnd  = &bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf[sizeof(bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf)];
        len = (bt_Message.bt_LinkState[ELD_data.bt_currentTab].rxHead
            + sizeof(bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf)
            - bt_Message.bt_LinkState[ELD_data.bt_currentTab].rxTail) % sizeof(bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf);
    }
    
    x = ELD_logStrTrim(pTail,pHead,pEnd,pStart,buf);

    {
        if(dbg_en.b.bt == 1)
        {
            dbg(BT_DBG_EN,"**** BT RECE ");
            for(y = 0; y < x; )
            {
                if(x - y > 250) 
                {
                    c = buf[y + 250];
                    buf[y + 250] = 0;
                    dbg(BT_DBG_EN,"%s",&buf[y]);
                    buf[y + 250] = c;
                    y += 250;
                }
                else
                {
                    dbg(BT_DBG_EN,"%s ****\r\n",&buf[y]);
                    y += 250;
                }
            }        
        }
    }    
    
    return len;
}    

void ELD_bt_uart_parse(u8* buf)
{
    //SourceID,PassWord,DataTime,CarType,TotalMileage,TotalFuel,EngineHours,FirmwareVersion,DebugFlag
    //AT$DebugFlag=1*
    u8* px;
    u8* py;
    u8 xbuf[64];
    u32 rtc, typ, obdInitReq, respReq;
    rtc_time tm;
    
    px = (u8*)strchr((char*)buf, '<');
    py = (u8*)strchr((char*)buf, '>');
    if( (px != NULL) && (py != NULL) ) 
    {
        ELD_strncpy(xbuf, px + 1, py - px - 1);
        if(strcmp((char*)xbuf, (char*)SysParameter.UnitData.password) != 0) return;
        strcpy((char*)px, (char*)py+1);
    }

    if( (px = (u8*)strstr((char*)buf, "AT$")) == NULL) return;
    px += sizeof("AT$") - 1;
    if( (py = (u8*)strchr((char*)buf, '*')) == NULL) return;
    
    if(py - px >= sizeof(xbuf)) return;    
    ELD_strncpy(xbuf, px, py - px);
    if(ELD_str_chr_num(xbuf,'=') != 1) return;

    typ = (strstr((char*)xbuf, "=?") != NULL) ? 'G' : 'S';
    px = (u8*)strchr((char*)xbuf, '=');        
    respReq = obdInitReq = px[0] = *py = 0;
    px++;
        
    if(strncmp((char*)xbuf,"SourceID",sizeof("SourceID")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            ELD_strncpy(SysParameter.UnitData.SerialNum, px, sizeof(SysParameter.UnitData.SerialNum)-1);
        }
        sprintf((char*)xbuf, "$SourceID=%s*\r\n",SysParameter.UnitData.SerialNum);
    }
    else if(strncmp((char*)xbuf,"PassWord",sizeof("PassWord")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            ELD_strncpy(SysParameter.UnitData.password, px, sizeof(SysParameter.UnitData.password)-1);
        }
        sprintf((char*)xbuf, "$PassWord=%s*\r\n",SysParameter.UnitData.password);
    }
    else if(strncmp((char*)xbuf,"DateTime",sizeof("DateTime")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            if(ELD_get_tm(px, &rtc) == c_ret_ok)
            {
                set_RTCCounter(rtc);
            }            
        }
        rtc = get_RTCCounter();	// 秒计数器
        rtc_time_to_tm(rtc, &tm);	// Format: MMDDYY	Format: HHMMSS
        sprintf((char*)xbuf, "$DateTime=%02d%02d%02d", tm.tm_mon,tm.tm_mday,tm.tm_year % 2000);
        sprintf((char*)xbuf + strlen((char*)xbuf), "%02d%02d%02d*\r\n", tm.tm_hour,tm.tm_min,tm.tm_sec);            
    }
    else if(strncmp((char*)xbuf,"CarType",sizeof("CarType")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            if(ELD_str_all_hex(px) == c_ret_ok)
            {
                SysRunParameter.OBD_Parameter.carType = ELD_str_hex(px);
                obdInitReq = 1;
            }
        }
        sprintf((char*)xbuf, "$CarType=%04X*\r\n", SysRunParameter.OBD_Parameter.carType);        
    }
    else if(strncmp((char*)xbuf,"TotalMileage",sizeof("TotalMileage")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            if(ELD_str_all_dec(px) == c_ret_ok)
            {
                SysRunParameter.OBD_Parameter.total_trip_mileage = ELD_str_dec(px);
                obdInitReq = 1;
            }
        }
        sprintf((char*)xbuf, "$TotalMileage=%d*\r\n", SysRunParameter.OBD_Parameter.total_trip_mileage);        
    }
    else if(strncmp((char*)xbuf,"TotalFuel",sizeof("TotalFuel")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            if(ELD_str_all_dec(px) == c_ret_ok)
            {
                obdInitReq = 1;
                SysRunParameter.OBD_Parameter.total_fuel = ELD_str_dec(px);
            }            
        }
        sprintf((char*)xbuf, "$TotalFuel=%d*\r\n", SysRunParameter.OBD_Parameter.total_fuel);              
    }
    else if(strncmp((char*)xbuf,"EngineHours",sizeof("EngineHours")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            if(ELD_str_all_dec(px) == c_ret_ok)
            {
                obdInitReq = 1;
                SysRunParameter.OBD_Parameter.total_engineHours = ELD_str_dec(px);
            }            
        }
        sprintf((char*)xbuf, "$EngineHours=%d*\r\n", SysRunParameter.OBD_Parameter.total_engineHours);           
    }
    else if(strncmp((char*)xbuf,"FirmwareVersion",sizeof("FirmwareVersion")) == 0)
    {
        respReq = 1;
        sprintf((char*)xbuf, "$FirmwareVersion=%s*\r\n", readVersion);
    }
    else if(strncmp((char*)xbuf,"DebugFlag",sizeof("DebugFlag")) == 0)
    {
        respReq = 1;
        if(typ == 'S')
        {
            if(px[1] == 0)
            {
                if(px[0] == '1') 
                {
                    ELD_data.btDbgEn = ELD_data.btDbgReq = 0;
                    ELD_data.bt_upload_addr_en = ELD_data.bt_upload_date_en = 0;
                }                
                else if(px[0] == '0') ELD_data.btDbgEn = 1;
            }
        }
        sprintf((char*)xbuf, "$DebugFlag=%d*\r\n", ELD_data.btDbgEn^1);        
    }
    
    if(respReq == 1)
    {
        sprintf((char*)ELD_data.response_bt, "%s", xbuf);
        ELD_data.btSendRespReq = 1;    
    }
    
    if(obdInitReq == 1)
    {
        obd_init( (SysRunParameter.OBD_Parameter.carType == 0) ? ((Power12v > 18000) ? 0x0000 :0xffff) : SysRunParameter.OBD_Parameter.carType,
            SysRunParameter.OBD_Parameter.total_trip_mileage,
            SysRunParameter.OBD_Parameter.total_fuel,
            SysRunParameter.OBD_Parameter.total_engineHours,
            SysParameter.UnitData.FuelType,
            SysParameter.UnitData.EngineDisplacement);   

        printf("obd_init:%04X\r\n", (SysRunParameter.OBD_Parameter.carType == 0) ? ((Power12v > 18000) ? 0x0000 :0xffff) : SysRunParameter.OBD_Parameter.carType);
        
        dbg(AD_DBG_EN,"odb_init\r\n");                
    }
}

// 两种格式的包
static u8 ELD_packet_parse(u8 src)
{
	u8 buf[2048 + 1];
	u8 *px, *py, *pz, c, err;
    u16 x, len, ilen;

    len = ELD_packetGet(buf, src);
    ilen = strlen((char*)buf);
    
    if( (px = (u8*)strstr((char*)buf,"AT+")) != NULL)
    {
        dbg_proc(buf);
        buf[0] = 0;
    }
    else if( (px = (u8*)strstr((char*)buf,"AT$")) != NULL)
    {
        ELD_bt_uart_parse(buf);
        buf[0] = 0;
    }
    else
    {    
        err = c_packet_error_json;
        while(1)
        {
            py = px = buf;            
            while((px = (u8*)strstr((char*)px, "$$")) != NULL)
            {
                py = px;
                px++;
            }
            px = py;
            
            if((pz = (u8*)strstr((char*)px,"**")) == NULL)
            {
                if(err != c_packet_error_none)
                    ELD_packet_parse_error(src,err);
                 break;
            }

            pz += 2;        
            c = pz[0];
            pz[0] = 0;

            //$$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**
            if((py = (u8*)strstr((char*)px,"$$")) != NULL)
            { 
                py += 2;    
                x = ELD_str_chr_num(py,',');
                if(x == 0)
                {                
                    x = ELD_str_hex(py);					
                    if(ELD_upload_confirm_bt(x) == c_ret_ok)
                    {
                        ELD_data.bt_upload_answer = c_ret_ok;
                        dbg(ELD_DBG_EN,"\r\n**** answer:id=%04X ****\r\n",x);                    
                    }
                    err = c_packet_error_none;
                }	
                else if(x == 7)
                {               
                    if(ELD_sum_cmp(py) == c_ret_nk) 
                    {
                        err = c_packet_error_checksum;
                    }
                    else
                    {
                        ELD_packet_make_ext(py, src);
                        err = c_packet_error_none;
                    }
                }
            }
            
            pz[0] = c;
            strcpy((char*)buf,(char*)pz);        
        }
    }
    if(ilen != strlen((char*)buf))      // 有处理过
    {
        x = len - strlen((char*)buf);
        {
            bt_Message.bt_LinkState[ELD_data.bt_currentTab].rxTail += x;
            bt_Message.bt_LinkState[ELD_data.bt_currentTab].rxTail %= sizeof(bt_Message.bt_LinkState[ ELD_data.bt_currentTab].btRxBuf);
        }    
    }
	return c_ret_ok;
}

static void ELD_obd_fail_proc(u8* px)
{
    //u32 x, first;
    
    if(ELD_data.obd.wait.r != 0)
    {        
        sprintf((char*)ELD_data.response_bt,"$$%s",px);    
        #if 0    
        if(ELD_data.obd.wait.b.pendCod == 1)
        {
            for(x = 0;x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]); x++)
            {
                if(ELD_data.obd.pendCode[x] != 0x00) break;
            }
            if(x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]))
            {
                sprintf((char*)ELD_data.response_bt,"$$");
                for(first = x = 0;x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]); x++)
                {
                    if(ELD_data.obd.pendCode[x] != 0x00)
                    {
                        sprintf((char*)ELD_data.response_bt+strlen((char*)ELD_data.response_bt),"%s%04X",
                            (first == 0) ? "" : "I",
                            ELD_data.obd.pendCode[x]);
                        first = 1;
                    }
                }
            }
        }
        else if(ELD_data.obd.wait.b.storCod == 1)
        {
            for(x = 0;x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]); x++)
            {
                if(ELD_data.obd.storCode[x] != 0x00) break;
            }
            if(x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]))
            {
                sprintf((char*)ELD_data.response_bt,"$$");
                for(first = x = 0;x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]); x++)
                {
                    if(ELD_data.obd.storCode[x] != 0x00)
                    {
                        sprintf((char*)ELD_data.response_bt+strlen((char*)ELD_data.response_bt),"%c%04X",
                            (first == 0) ? "" : "I",                            
                            ELD_data.obd.storCode[x]);
                        first = 1;
                    }
                }
            }                
        }  
        #endif    
        //ELD_sum_set(ELD_data.response_bt + 2);
        sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt), "**\r\n");
        ELD_data.btSendRespReq = 1;            
        
        ELD_data.obd.wait.r = 0;
    }
    else if(ELD_data.obd.accOnWait.b.storCod == 1)
    {
        ELD_event_313_proc();
    }
    else if(ELD_data.obd.accOnWait.b.pendCod == 1)
    {
        ELD_event_314_proc();
    }
    else if(ELD_data.obd.accOnWait.b.sup == 1)
    {
        ELD_event_315_proc();
    }   
    ELD_data.obd.accOnWait.r = 0;
}

static void ELD_obd_msg_proc(void *msg,u16 len)
{
	u8 x, y, z, tbuf[512], xbuf[16], *px, *py, *pz;
    u8 obd_buf[512];
    u16 c, para;
    
    strcpy((char*)obd_buf,(char*)msg);
    obd_buf[sizeof(obd_buf)-1] = 0;
	
	py = obd_buf;
	if(py[strlen((char*)py)-1] != '*') return;
    
    py[strlen((char*)py)-1] = 0;
    
	if((px = (u8*)strstr((char*)py,"@AKEY=")) != NULL)			// 	读取重要参数的值
	{		
		px += sizeof("@AKEY=") - 1;	
		ELD_strcpy_ht(tbuf,px,'|',1,sizeof(tbuf));
		if(ELD_str_dec(tbuf) == 1)	// OBD与汽车通信成功 
		{
			ELD_data.obd.send_cnt = 0;
            ELD_data.obd.send_en = 1;
			ELD_data.obd.handshake_ok = 1;					
//      1             2              3            4                    5               6                  7 			             8
//@AKEY=|HandshakeMark|ObdProtocolNum|TotalMileage|TotalFuelConsumption|TotalTravelTime|CurrentTripMileage|CurrentTripFuelConsumption|CurrentTravelTime|*	
//@KEY= |握手标志     |协议编号      |总里程      |总油耗              |总行驶时间     |当前行程里程      |当前行程油耗              |当前行程行驶时间 |*			

            ELD_strcpy_ht(tbuf,px,'|', 2, sizeof(tbuf));
			ELD_data.obd.protocolTyp = ELD_str_hex(tbuf);	
                        
            ELD_strcpy_ht(tbuf,px,'|',3,sizeof(tbuf));
			SysRunParameter.OBD_Parameter.total_trip_mileage = ELD_str_dec(tbuf);	
			
			ELD_strcpy_ht(tbuf,px,'|',4,sizeof(tbuf));
			SysRunParameter.OBD_Parameter.total_fuel = ELD_str_dec(tbuf);	

			ELD_strcpy_ht(tbuf,px,'|',5,sizeof(tbuf));
			SysRunParameter.OBD_Parameter.total_engineHours = ELD_str_dec(tbuf);	

			ELD_strcpy_ht(tbuf,px,'|',6,sizeof(tbuf));
			ELD_data.obd.CurrentTripMileage  = ELD_str_dec(tbuf);	

			ELD_strcpy_ht(tbuf,px,'|',7,sizeof(tbuf));
			ELD_data.obd.CurrentTripFuelConsumption  = ELD_str_dec(tbuf);	

			ELD_strcpy_ht(tbuf,px,'|',8,sizeof(tbuf));
			ELD_data.obd.CurrentTripTime  = ELD_str_dec(tbuf);				

		}
        else
            ELD_data.obd.handshake_ok = 0;        
	}		
	else if((px = (u8*)strstr((char*)py,"@ASUP=")) != NULL)	// 	读取车辆支持的PID列表
	{	//@ASUP=|pid1|pid2...|pidN|*
		//"@RPID=|04|05|0c|0d|0f|*");	//	发动机负载 冷却液温度 发动机转速 车速 进气温度 
		//|5c|5e|74|75|*");	//	发动机机油温度 发动机燃油量 涡轮增压器转速 涡轮增压器温度
		//|5c|5e|74|75|*");	//	发动机机油温度 发动机燃油量 涡轮增压器转速 涡轮增压器温度	
        
		px += sizeof("@ASUP=") - 1;
		
		//	不支持的PID数据
		y = ELD_str_chr_num(px,'|');
		if(y >= sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]))
			y = sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]);
		for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
        {
            ELD_data.obd.pid[0].valid = ELD_data.obd.pid[0].name = 0;
        }
        
		for(x = 1;x < y; x++)
		{
			ELD_strcpy_ht(tbuf,px,'|',x,sizeof(tbuf));
			ELD_data.obd.pid[x-1].name = ELD_str_hex(tbuf);
            ELD_data.obd.pid[x-1].unit[0] = 0;
            ELD_data.obd.pid[x-1].val = 0;
		}	
        
        if(ELD_data.obd.wait.b.sup == 1) 
        {
            ELD_data.obd.wait.b.sup = 0;
            x = strlen((char*)px+1);
            while(x != 0)
            {
                if(px[x ] == '|') 
                {
                    px[x] = 0;
                    break;
                }
                else if(px[x] != ' ') break;
            }                
            strcpy((char*)ELD_data.response_bt,"$$");
            ELD_strncpy(ELD_data.response_bt + strlen((char*)ELD_data.response_bt),px + 1,sizeof(ELD_data.response_bt)-11);
            //sprintf((char*)ELD_data.response_bt,"$$%s",(char*)px + 1);            
            ELD_sum_set(ELD_data.response_bt+2);
            sprintf((char*)ELD_data.response_bt + strlen((char*)ELD_data.response_bt), "**\r\n");
            ELD_data.btSendRespReq = 1;                        
        }   
        else if(ELD_data.obd.accOnWait.b.sup == 1)
        {            
            ELD_data.obd.accOnWait.b.sup = 0;
            ELD_event_315_proc();
        }            

        for(y = 0; y < sizeof(SysRunParameter.ELD_parameter.evt21para)/sizeof(SysRunParameter.ELD_parameter.evt21para[0]); y++)
        {
            para = SysRunParameter.ELD_parameter.evt21para[y];
            if(para == 0) break;
            if(para < 0xff00)
            {
                for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
                {
                    if(para == ELD_data.obd.pid[x].name)
                    {
                        ELD_data.obd.pid[x].enable = 1;
                        break;
                    }
                }
            }
        } 
	}		
	else if((px = (u8*)strstr((char*)py,"@AVIN=")) != NULL)
	{	//@AVIN=|VINCode|*
		px += sizeof("@AVIN=") - 1;
		ELD_strcpy_ht(tbuf,px,'|',1,sizeof(tbuf));
        for(x = 0,y = strlen((char*)tbuf); x < y; x++)
        {
            if((tbuf[x] < ' ')||(tbuf[x] > '~')) tbuf[x] = ' ';
        }
		ELD_strncpy(SysRunParameter.VIN_CODE,tbuf,sizeof(SysRunParameter.VIN_CODE)-1);
		ELD_strncpy(protocol_HEX_Format.VINNumber,tbuf,sizeof(SysRunParameter.VIN_CODE)-1);		
        
        if(ELD_data.obd.wait.b.vin == 1)  
        {
            ELD_data.obd.wait.b.vin = 0;
            sprintf((char*)tbuf,"$$%s",(char*)protocol_HEX_Format.VINNumber);        
            ELD_sum_set(tbuf+2);
            sprintf((char*)tbuf + strlen((char*)tbuf), "**\r\n");
            strcpy((char*)ELD_data.response_bt,(char*)tbuf);
            ELD_data.btSendRespReq = 1;                        
        }  
        ELD_data.obd.accOnWait.b.vin = 0;    
	}		
	else if((px = (u8*)strstr((char*)py,"@APID=")) != NULL)
	{	//@APID=|04>pid1_value(unit)|*
		px += sizeof("@APID=") - 1;

		y = ELD_str_chr_num(px,'|');
		for(x = 1;x < y; x++)
		{
			ELD_strcpy_ht(tbuf,px,'|',x,sizeof(tbuf));			
			ELD_strcpy_ht(xbuf,tbuf,'>',0,sizeof(xbuf));
			c = ELD_str_hex(xbuf);
			for(z = 0;z < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); z++)
			{				
				if(ELD_data.obd.pid[z].name == c)
				{	
					ELD_strcpy_ht(xbuf,tbuf,'>',1,sizeof(xbuf));
                    if(xbuf[0] != 0)
                    {
                        ELD_data.obd.pid[z].val   = ELD_str_dec(xbuf);
                        ELD_data.obd.pid[z].valid = 1;
                    }
                    else
                        ELD_data.obd.pid[z].valid = 0;
                    
                    ELD_data.obd.pid[z].unit[0] = 0;
                    if( (pz = (u8*)strchr((char*)xbuf,'(')) != NULL)
                    {                    
                        ELD_strncpy(ELD_data.obd.pid[z].unit, pz, sizeof(ELD_data.obd.pid[z].unit)-1); 
                    }
                    break;    
				}	
			}
		}		
        if(ELD_data.obd.wait.b.pidx == 1)
        {
            ELD_data.obd.wait.b.pidx = 0;
            x = strlen((char*)px+1);
            while(x != 0)
            {
                if(px[x ] == '|') 
                {
                    px[x] = 0;
                    break;
                }
                else if(px[x] != ' ') break;
            }                
            sprintf((char*)tbuf,"$$%s",(char*)px + 1);
            ELD_sum_set(tbuf+2);
            sprintf((char*)tbuf + strlen((char*)tbuf), "**\r\n");
            strcpy((char*)ELD_data.response_bt,(char*)tbuf);
            ELD_data.btSendRespReq = 1;                        
        }
        ELD_data.obd.accOnWait.b.pid2 = ELD_data.obd.accOnWait.b.pid1 = 0;
	}		
	else if((px = (u8*)strstr((char*)py,"@ACOD=")) != NULL)
	{	//@ACOD=|Code1...|CodeN|*   无故障码时为@ACOD=|*
		px += sizeof("@ACOD=") - 1;

		y = ELD_str_chr_num(px,'|');
        if(y >= 1)
        {
            if((ELD_data.obd.wait.b.storCod == 1)||(ELD_data.obd.accOnWait.b.storCod == 1))
            {                               
                if(y > sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]))
                    y = sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]);
                
                for(x = 0;x < sizeof(ELD_data.obd.storCode)/sizeof(ELD_data.obd.storCode[0]); x++)
                {
                    ELD_data.obd.storCode[x] = 0x00;
                }
                for(x = 1;x < y; x++)
                {
                    ELD_strcpy_ht(tbuf,px,'|',x,sizeof(tbuf));			
                    ELD_data.obd.storCode[x - 1] = ELD_str_hex(tbuf);
                }
            }
            else if( (ELD_data.obd.wait.b.pendCod == 1)||(ELD_data.obd.accOnWait.b.pendCod == 1))
            {                
                if(y > sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]))
                    y = sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]);
                
                for(x = 0;x < sizeof(ELD_data.obd.pendCode)/sizeof(ELD_data.obd.pendCode[0]); x++)
                {
                    ELD_data.obd.pendCode[x] = 0x00;
                }
                for(x = 1;x < y; x++)
                {
                    ELD_strcpy_ht(tbuf,px,'|',x,sizeof(tbuf));			
                    ELD_data.obd.pendCode[x - 1] = ELD_str_hex(tbuf);
                }
            }
        }
        
        if((ELD_data.obd.wait.b.pendCod == 1)||(ELD_data.obd.wait.b.storCod == 1))
        {   // @ACOD=|Code1...|CodeN|*  //无故障码时为@ACOD=|*
            ELD_data.obd.wait.b.pendCod = ELD_data.obd.wait.b.storCod = 0;

            x = strlen((char*)px+1);
            while(x != 0)
            {
                if(px[x ] == '|') 
                {
                    px[x] = 0;
                    break;
                }
                else if(px[x] != ' ') break;
            }                
            sprintf((char*)tbuf,"$$%s",(char*)px + 1);            
            ELD_sum_set(tbuf+2);
            sprintf((char*)tbuf + strlen((char*)tbuf), "**\r\n");
            strcpy((char*)ELD_data.response_bt,(char*)tbuf);
            ELD_data.btSendRespReq = 1;            
        }    
        else if(ELD_data.obd.accOnWait.b.storCod == 1)
        {
            ELD_data.obd.accOnWait.b.storCod = 0;
            ELD_event_313_proc();
        }            
        else if(ELD_data.obd.accOnWait.b.pendCod == 1)
        {
            ELD_data.obd.accOnWait.b.pendCod = 0;
            ELD_event_314_proc();
        }
	}
	else if((px = (u8*)strstr((char*)py,"@ACLE=")) != NULL)
	{
		//px += sizeof("@ACLE=") - 1;
        
        if(ELD_data.obd.wait.b.cle == 1)  
        {
            ELD_data.obd.wait.b.cle = 0;
            sprintf((char*)tbuf,"$$OK");        
            //ELD_sum_set(tbuf+2);
            sprintf((char*)tbuf + strlen((char*)tbuf), "**\r\n");
            strcpy((char*)ELD_data.response_bt,(char*)tbuf);
            ELD_data.btSendRespReq = 1;            
        }                    
	}	
	else if((px = (u8*)strstr((char*)py,"@FAIL=")) != NULL)
	{   // @FAIL=X*
        ELD_obd_fail_proc(px+1);
	}	    
}
/*
//	AT$SYSS<password>=|||||* 							// Set system parameters设置系统参数
//		$OK 											//Set Response 设置返回

//	AT$SYSS<password>=? 								// Query system parameters查询系统参数
//		$SYSS=|123456|eld123456789abc|0|0|* 			//Set Response 查询返回

//	AT$GPRS<password>=|||||||* 							// Set GPRS parameters设置网络参数
//		$OK 											//Set Response 设置返回

//	AT$GPRS<password>=? 								// Query system parameters查询网络参数
//		$GPRS=|0|www.nyi‐tech.com|5011|3gnet|||* 		// Query Response 查询返回

//	AT$FOAT=|||||* 										// Upgrade via FTP server 通过FTP服务器升级
//		$OK
*/
static void ELD_uart_parse(void)
{
	u8 *px, tbuf[sizeof(USART1_RX_BUF)];
    u16 x = 0;
    
    while(USART1_iTail != USART1_iHead)
    {
         tbuf[x++] = USART1_RX_BUF[USART1_iTail++];
         if(USART1_iTail >= sizeof(USART1_RX_BUF)) USART1_iTail = 0;
    }            
    
	if((px = (u8*)strchr((char*)tbuf,'\r')) != NULL) *px = 0;
    if(tbuf[strlen((char*)tbuf) - 1] == '*') tbuf[strlen((char*)tbuf) - 1] = 0;

    /*if((px = (u8*)strstr((char*)tbuf,"$$")) != NULL)
    { 
        px += 2;    
        x = ELD_str_chr_num(px,',');
        if(x == 7)
        {               
            if(ELD_sum_cmp(px) == c_ret_ok) 
            {
                ELD_strcpy_ht(tbuf, tbuf, ',', 6, sizeof(tbuf));                
                if(ELD_set_system_parameter(tbuf,ELD_PACKET_uart) == c_ret_ok)
                {	
                }
                else if(ELD_query_system_parameter(tbuf,ELD_PACKET_uart) == c_ret_ok)
                {
                }
            }
        }
    }    
    else*/ if((px = (u8*)strstr((char*)tbuf,"AT$SUM=")) != NULL)
    {
        px += sizeof("AT$SUM=") - 1;
        if(*px == '$') px++;
        if(*px == '$') px++;   
        
        ELD_logStrTrim(px, px + strlen((char*)px), NULL, NULL, tbuf);        
        ELD_sum_set(tbuf);
		printf((char*)tbuf);
        printf("\r\n");
    }    
    else if((px = (u8*)strstr((char*)tbuf,"AT$")) != NULL)
    {
        
        if(ELD_set_system_parameter(tbuf,ELD_PACKET_uart) == c_ret_ok)
        {	
        }
        else if(ELD_query_system_parameter(tbuf,ELD_PACKET_uart) == c_ret_ok)
        {
        }
    }        
    else 
    {
         dbg_proc(tbuf);   
    }    
}

static u8 ELD_getReportId(u8* buf,u16* id)
{   // $$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**
    u8 tbuf[32];
    
    ELD_strcpy_ht(tbuf, buf, ',', 1, sizeof(tbuf));
    if(ELD_str_all_hex(tbuf) == c_ret_ok)
    {
        *id = ELD_str_hex(tbuf);
        return c_ret_ok;
    }
    return c_ret_nk;
}

//$$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**
static u16 ELD_read_report_by_bt(void)
{
	u16 len, n, id;
	u8 ret, rBuf[GPS_FLASH_SIZE]; 
	
    spiFlash_readAddrSet(SysRunParameter.FLASH_Parameter.ConfirmDataAddrBt);
	
	n = 0;
	memset((u8*)&ELD_data.bt_resp, 0, sizeof(ELD_data.bt_resp));
	while(1)	
	{
		memset(rBuf, 0x00, sizeof(rBuf));
		ret = ReadFlashToData(rBuf, &len , c_bt_confirm_nk);	//0xff,TRUE,FLASE
		if(TRUE == ret)
		{
            if(ELD_getReportId(rBuf, &id) == c_ret_ok)
            {
                n += sprintf((char*)ELD_data.ProtocolSendBuf + n,"$$");
                memcpy(&ELD_data.ProtocolSendBuf[n], rBuf, len);		// GPRS每次最多发送512字节					
                n += len;
                n += sprintf((char*)ELD_data.ProtocolSendBuf + n,"**\r\n");

                ELD_data.bt_resp[0].id = id;
                ELD_data.bt_resp[0].addr = ELD_report_last_addr(w25n.readAddr);                
                break;										// BT每次发送1包
            }
		}
        else if(ret == 0xff) break;
	}	
	return n;
}

static u16 ELD_read_report_by_bt_date(void)
{
	u32 tm;
	u16 len, n, id;
	u8 ret, rBuf[GPS_FLASH_SIZE], tbuf[13], idx; 
	
	spiFlash_readAddrSet(ELD_data.bt_upload_report_addr);	
	n = ret = idx = 0;
    memset((u8*)&ELD_data.bt_resp, 0, sizeof(ELD_data.bt_resp));
    
	while(ret != 0xff)	
	{
		memset(rBuf, 0x00, sizeof(rBuf));
		ret = ReadFlashToData(rBuf, &len, c_bt_confirm_all);	//0xff,TRUE,FLASE
		if(TRUE == ret)
		{// $$SourceID,SequenceID,EventType,EventCode,Date,Time, EventData,ChecksumValue**					
			if((n + len + 8) > sizeof(ELD_data.ProtocolSendBuf)) break;
            
            if(ELD_getReportId(rBuf, &id) == c_ret_ok)
            {
                ELD_strcpy_ht(tbuf,   rBuf, ',', 4, 7);
                ELD_strcpy_ht(tbuf+6, rBuf, ',', 5, 7);			
                if(ELD_get_tm(tbuf, &tm)==c_ret_ok)
                {	
                    if(		(tm >= ELD_data.bt_upload_tm_start)
                        &&	(tm <= ELD_data.bt_upload_tm_end) )
                    {
                        n += sprintf((char*)ELD_data.ProtocolSendBuf + n,"$$");
                        memcpy(&ELD_data.ProtocolSendBuf[n], rBuf, len);		// GPRS每次最多发送512字节					
                        n += len;
                        n += sprintf((char*)ELD_data.ProtocolSendBuf + n,"**\r\n");

                        ELD_data.bt_resp[idx].id = id;
                        ELD_data.bt_resp[idx].addr = ELD_report_last_addr(w25n.readAddr);
                        idx++;   
                        break;    
                    }
                }				
            }
		}
        else if(ret == 0xff)
            break;
	}
    if(n != 0)
    {
        ELD_data.bt_upload_report_addr = ELD_report_next_addr(ELD_data.bt_resp[idx - 1].addr);
    }
	return n;
}

static u16 ELD_read_report_by_bt_addr(void)
{
	u16 len, n, id;
	u8  ret, rBuf[GPS_FLASH_SIZE]; 

	spiFlash_readAddrSet(ELD_data.bt_upload_report_addr);	
	n = 0;
	memset((u8*)&ELD_data.bt_resp, 0, sizeof(ELD_data.bt_resp));
	while(1)	
	{
		memset(rBuf, 0x00, sizeof(rBuf));
		ret = ReadFlashToData(rBuf, &len , c_bt_confirm_nk);	//0xff,TRUE,FLASE
		if(TRUE == ret)
		{
            if(ELD_getReportId(rBuf, &id) == c_ret_ok)
            {
                n += sprintf((char*)ELD_data.ProtocolSendBuf + n,"$$");
                memcpy(&ELD_data.ProtocolSendBuf[n], rBuf, len);		// GPRS每次最多发送512字节					
                n += len;
                n += sprintf((char*)ELD_data.ProtocolSendBuf + n,"**\r\n");

                ELD_data.bt_resp[0].id = id;
                ELD_data.bt_resp[0].addr = ELD_report_last_addr(w25n.readAddr);                
                break;										// BT每次发送1包
            }
		}
        else if(ret == 0xff) break;
	}	
	return n;
}

void ELD_upload(void)
{
	static enum
	{
		c_step_app_idle,
		c_step_app_response_bt,
        c_step_app_response_bt_wait,
		c_step_app_report_bt,
		c_step_app_report_bt_wait,
		c_step_app_report_bt_wait_1,
		c_step_app_report_bt_addr,
		c_step_app_report_bt_addr_wait,
		c_step_app_report_bt_addr_wait_1,
		c_step_app_report_bt_date,
		c_step_app_report_bt_date_wait,
		c_step_app_report_bt_date_wait_1,
        c_step_app_response_bt_dbg,
        c_step_app_response_bt_dbg_wait,
	}step_app = c_step_app_idle,step_app_bk = c_step_app_idle;

	static enum
	{
		c_step_ftp_idle,
		c_step_ftp_run,
		c_step_ftp_done_wait
	}step_ftp = c_step_ftp_idle;    
	static u8 app_sendcnt = 0;
	static u16 app_len = 0;
	static u32 app_tmr = 0;
    static u32 accOnDly = 0;
    static u8  accOffSendEn = 0;
	u8 err;
	BT_MESSAGE *boxMsg;
	u8 *ptr;
	
	if( (boxMsg = (BT_MESSAGE *)OSMboxAccept(ELD_rece_bt)) != NULL)
	{   //1. 不主动向蓝牙发送日志的条件，由ACC OFF超过30秒 改为 未收到外部任一蓝牙终端的数据（包括确认帧，请求帧，无意义数据）超过30秒 
        accOnDly = tick_get();
        //accOffSendEn = 1;   //ACC OFF时也可以主动发送完所有日志，由APP启动
        
        if(strstr((char*)bt_Message.bt_LinkState[bt_Message.currentTab].btRxBuf,"161B_UPDATE")!=NULL)  //313631425F555044415445
        {
            ftpSendPara.src = ELD_PACKET_bt;
            ELD_data.ftpSendSrc = ELD_PACKET_bt;
            memcpy(ftpSendPara.btMac,bt_Message.bt_LinkState[bt_Message.currentTab].btMAC,6); 
            //strcpy((char*)ELD_data.response_bt,"161B_UPDATE");
            //ELD_data.btSendRespReq = 1;            
            OSFlagPost(ELD_rece_status,(OS_FLAGS)ELD_rece_done_bt,OS_FLAG_SET,&err);		
        }
        else
        {
            ELD_data.bt_currentTab = boxMsg->currentTab;
            ELD_packet_parse(ELD_PACKET_bt);
            OSFlagPost(ELD_rece_status,(OS_FLAGS)ELD_rece_done_bt,OS_FLAG_SET,&err);		
        }
	}
	
	if(uartTimeDelay > 40/c_systick)    // 连续无数据40毫秒
	{	        
        uartTimeDelay = 0;
		ELD_uart_parse();
		OSFlagPost(ELD_rece_status,(OS_FLAGS)ELD_rece_done_uart,OS_FLAG_SET,&err);
	}		

	if( (ptr = (u8*)OSMboxAccept(obd_mbox_send)) != NULL)
	{	
		ELD_obd_msg_proc(ptr,0);
        dbg(ELD_DBG_EN,"obd rece:%s\r\n",ptr);
		OSFlagPost(obd_flag, (OS_FLAGS)2, OS_FLAG_CLR, &err);
	}
	
	switch(step_ftp)
	{
		case c_step_ftp_idle:
			if((ELD_data.ftpSendSrc==ELD_PACKET_bt)  &&(ELD_data.btSendRespReq==0))
			{
				app_sendFtpSet(&ftpSendPara);
				step_ftp = c_step_ftp_run;
			}	
		break;

		case c_step_ftp_run:
            err = app_sendFtpStatus(); 
			if(err == OS_ERR_NONE)
			{	
                if(ftpSendPara.result == c_ret_ok)
                {
                    switch(ELD_data.ftpSendSrc)
                    {
                        case ELD_PACKET_bt:
                            strcpy((char*)ELD_data.response_bt,"$OK");
                            ELD_data.btSendRespReq = 1;
                        break;					
                    }				
                    step_ftp = c_step_ftp_done_wait;
                }
                else
                {
                    ELD_data.ftpSendSrc = ELD_PACKET_idle;
                    step_ftp = c_step_ftp_idle;
                }
			}
            else if(err == OS_ERR_IDLE)
            {
                ELD_data.ftpSendSrc = ELD_PACKET_idle;
                step_ftp = c_step_ftp_idle;
            }
		break;

		case c_step_ftp_done_wait:
			if((ELD_data.ftpSendSrc==ELD_PACKET_bt)  &&(ELD_data.btSendRespReq==0))
			{
				ELD_data.ftpSendSrc = ELD_PACKET_idle;
				step_ftp = c_step_ftp_idle;
				ELD_event_reset();
			}	
		break;	
		
		default:step_ftp = c_step_ftp_idle;
        break;			
	}		
	
    if( (step_ftp != c_step_ftp_run)||(ELD_data.ftpSendSrc != ELD_PACKET_bt))    	
    {
        switch(step_app) 
        {
            case c_step_app_idle:
                if(bt_GetLinkState() != BT_CONNECT_OK) break;	
                
                if(ELD_data.btSendRespReq == 1)
                {   				
                    step_app = c_step_app_response_bt;
                    goto eld_step_app_response_bt;
                }	
                if(ELD_data.btDbgEn == 1)
                {
                    if(ELD_data.btDbgReq == 1)
                    {                        
                        step_app_bk = step_app;
                        step_app = c_step_app_response_bt_dbg;
                        goto eld_step_app_response_bt_dbg;                            
                    }                                    
                }
                else 
                {	
                    app_len = 0;
                    if(ELD_data.bt_upload_addr_en == 1)			// 蓝牙3个查询命令
                    {
                        if((app_len = ELD_read_report_by_bt_addr())==0) 
                            ELD_data.bt_upload_addr_en = 0;
                        else
                        {
                            app_sendcnt = 0;
                            ELD_data.bt_upload_answer = c_ret_nk;
                            step_app = c_step_app_report_bt_addr;
                            goto eld_step_app_report_bt_addr;						
                        }												
                    }
                    else if(ELD_data.bt_upload_date_en == 1)	// 蓝牙9-1/9-3
                    {	
                        if((app_len = ELD_read_report_by_bt_date())==0) 
                            ELD_data.bt_upload_date_en = 0;
                        else
                        {
                            app_sendcnt = 0;
                            ELD_data.bt_upload_answer = c_ret_nk;
                            step_app = c_step_app_report_bt_date;
                            goto eld_step_app_report_bt_date;												
                        }						
                    }
                    else if(SysRunParameter.FLASH_Parameter.ConfirmDataAddrBt != SysRunParameter.FLASH_Parameter.WriteDataAddr)
                    {	
                        if(ELD_data.acc_on == 1)    // ACC OFF时：蓝牙低功耗工作（可接收事件数据并回复事件数据，但不能主动发送缓存标记=0的日志）
                        {
                            accOnDly = tick_get();
                            accOffSendEn = 1;
                            app_len = ELD_read_report_by_bt();
                            if(app_len != 0)
                            {	
                                app_sendcnt = 0;
                                ELD_data.bt_upload_answer = c_ret_nk;
                                step_app = c_step_app_report_bt;
                                goto eld_step_app_report_bt;						
                            }	
                        }
                        else if(accOffSendEn == 1)
                        {                            
                            if(tick_cmp(accOnDly, 30000) == c_ret_nk)
                            {
                                app_len = ELD_read_report_by_bt();
                                if(app_len != 0)
                                {	
                                    app_sendcnt = 0;
                                    ELD_data.bt_upload_answer = c_ret_nk;
                                    step_app = c_step_app_report_bt;
                                    goto eld_step_app_report_bt;						
                                }	
                            }
                            accOffSendEn = 0;    
                        }   
                    }                        
                }
            break;

            case c_step_app_response_bt_dbg:
    eld_step_app_response_bt_dbg:
                {
                    OSSchedLock();                    
                    app_sendBtSet(ELD_data.btDbgBuf,strlen((char*)ELD_data.btDbgBuf),0);
                    ELD_data.btDbgBuf[0] = 0;
                    ELD_data.btDbgReq = 0;
                    OSSchedUnlock();    
                }
                STM_GetSysTick(&app_tmr);	
                step_app = c_step_app_response_bt_dbg_wait;
            break;			

            case c_step_app_response_bt_dbg_wait:
                if(     (app_sendBtStatus() == OS_ERR_NONE)
                    ||  (STM_CheckSysTick(&app_tmr,3*(1000/c_systick)) == TRUE) )
                {
                    step_app = step_app_bk;
                }
            break;
                
            case c_step_app_response_bt:
    eld_step_app_response_bt:
                app_sendBtSet(ELD_data.response_bt,strlen((char*)ELD_data.response_bt),0);		
                ELD_data.btSendRespReq = 0;
                STM_GetSysTick(&app_tmr);	
                step_app = c_step_app_response_bt_wait;
            break;			

            case c_step_app_response_bt_wait:
                if(     (app_sendBtStatus() == OS_ERR_NONE)
                    ||  (STM_CheckSysTick(&app_tmr,3*(1000/c_systick)) == TRUE) )
                {
                    
                    ELD_data.bt_upload_date_en = 0;
                    
                    step_app = c_step_app_idle;                    
                }
            break;
                
            case c_step_app_report_bt_addr:
    eld_step_app_report_bt_addr:				
                app_sendBtSet(ELD_data.ProtocolSendBuf, app_len,0);			
                step_app = c_step_app_report_bt_addr_wait_1;
            break;

            case c_step_app_report_bt_addr_wait_1:
                if(app_sendBtStatus() != OS_ERR_NONE) break;                
                STM_GetSysTick(&app_tmr);	
                step_app = c_step_app_report_bt_addr_wait;
            break;			
            
            case c_step_app_report_bt_addr_wait:
                if(ELD_data.bt_upload_answer == c_ret_ok)
                {	
                    ELD_data.bt_upload_addr_en = 0;
                    step_app = c_step_app_idle;
                }
                else if(STM_CheckSysTick(&app_tmr,3L*(1000/c_systick)) == TRUE)	// 3秒超时
                {
                    if(ELD_data.btSendRespReq == 1)
                    {
                        ELD_data.bt_upload_addr_en = 0;
                        step_app = c_step_app_response_bt;
                        goto eld_step_app_response_bt;
                    }
                    
                    if(++app_sendcnt >= 3)									// 失败3次退出
                    {
                        ELD_data.bt_upload_addr_en = 0;
                        step_app = c_step_app_idle;
                    }						
                    else
                    {
                        step_app = c_step_app_report_bt_addr;
                    }
                }
            break;

            case c_step_app_report_bt_date:
    eld_step_app_report_bt_date:				
                app_sendBtSet(ELD_data.ProtocolSendBuf, app_len,0);			
                step_app = c_step_app_report_bt_date_wait_1;
            break;

            case c_step_app_report_bt_date_wait_1:
                if(app_sendBtStatus() != OS_ERR_NONE) break;                
                STM_GetSysTick(&app_tmr);	
                step_app = c_step_app_report_bt_date_wait;
            break;			
            
            case c_step_app_report_bt_date_wait:
                if(ELD_data.bt_upload_answer == c_ret_ok)
                {	
                    step_app = c_step_app_idle;
                }
                else if(STM_CheckSysTick(&app_tmr,3L*(1000/c_systick)) == TRUE)	// 3秒超时
                {
                    if(ELD_data.btSendRespReq == 1)
                    {
                        ELD_data.bt_upload_date_en = 0;
                        step_app = c_step_app_response_bt;
                        goto eld_step_app_response_bt;
                    }                    
                    if(++app_sendcnt >= 3)									// 失败3次退出
                    {
                        ELD_data.bt_upload_date_en = 0;
                        step_app = c_step_app_idle;
                    }						
                    else
                    {
                        step_app = c_step_app_report_bt_date;
                    }
                }
            break;

            case c_step_app_report_bt:
    eld_step_app_report_bt:				
                app_sendBtSet(ELD_data.ProtocolSendBuf, app_len,1);			
                step_app = c_step_app_report_bt_wait;
            break;
            
            case c_step_app_report_bt_wait:
                if(app_sendBtStatus() != OS_ERR_NONE) break;
                STM_GetSysTick(&app_tmr);	
                step_app = c_step_app_report_bt_wait_1;
            break;
            
            case c_step_app_report_bt_wait_1:
                if(ELD_data.bt_upload_answer == c_ret_ok)
                {	
                    step_app = c_step_app_idle;                    
                }
                else if(STM_CheckSysTick(&app_tmr,3L*(1000/c_systick)) == TRUE)	// 3秒超时
                {
                    if(ELD_data.btSendRespReq == 1)
                    {
                        step_app = c_step_app_response_bt;
                        goto eld_step_app_response_bt;
                    }                                
                    if(++app_sendcnt >= 3)									// 失败3次退出
                    {
                        step_app = c_step_app_idle;
                    }						
                    else
                    {
                        step_app = c_step_app_report_bt;
                    }
                }
            break;
                
            default:step_app = c_step_app_idle;		
            break;
        }		
    }
}

u32 ELD_get_vehicle_rpm(void)
{
    u32 x;
    
    for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
    {
        if(ELD_data.obd.pid[x].name == 0) break;
        else if(ELD_data.obd.pid[x].name == 0x010c) 
            return ELD_data.obd.pid[x].val;
    }
    return 0;
}

u32 ELD_get_vehicle_speed(void)
{
    u32 x;
    
    for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
    {
        if(ELD_data.obd.pid[x].name == 0) break;
        else if(ELD_data.obd.pid[x].name == 0x010d) 
            return ELD_data.obd.pid[x].val;
    }
    return 0;
}

u8 ELD_get_vehicle_run(void)
{
    if(		(ELD_get_vehicle_speed() > 8)	// 车速大于8KM/H
    	&& 	(ELD_data.acc_on == 1) )
		return c_ret_ok;
	else return c_ret_nk;
}

u8 ELD_get_vehicle_stop(void)
{
    if(		(ELD_get_vehicle_speed() <= 8)	// 车速小于8KM/H
    	||	(ELD_data.acc_on == 0) )						//	车辆静止
		return c_ret_ok;
	else return c_ret_nk;
}

static void ELD_mpu6050_poll(void)	
{	
	static enum
	{
		c_step_idle,
		c_step_acc_on_chk,
		c_step_acc_off_chk,
		c_step_acc_off_wait
	}step = c_step_idle;
	static u32 vibrate_tmr = 0;
	static u32 rpm_tmr = 0;
	static u32 sleep_tmr = 0;
	static u8 sleep_en = 0;
    
	ELD_data.vibrate = MPU6050_vibrate;
	if(ELD_data.obd.handshake_ok == 1)
		ELD_data.vibrate = 1;
	
	switch(step)
	{
		case c_step_idle:
		    if(ELD_data.vibrate == 1)
			{
				STM_GetSysTick(&vibrate_tmr);
				step = c_step_acc_on_chk;
			}
		break;
				
		case c_step_acc_on_chk:
			if(		(ELD_get_vehicle_rpm() != 0)
                ||  (ELD_data.obd.handshake_ok == 1)
				||	(	(ELD_data.vibrate == 1)            
					&&	(TRUE == STM_CheckSysTick((u32 *)&vibrate_tmr,(u32)40*(1000/c_systick))) ) )
			{
				ELD_data.acc_on = 1;
				step = c_step_acc_off_chk;
			}
			else if(ELD_data.vibrate == 0)
			{
				step = c_step_idle;
			}				
		break;

		case c_step_acc_off_chk:
			if(ELD_get_vehicle_rpm() == 0)	
			{
				STM_GetSysTick(&rpm_tmr);
				step = c_step_acc_off_wait;
			}				
			if(ELD_data.vibrate == 0)
			{
				STM_GetSysTick(&vibrate_tmr);
				step = c_step_acc_off_wait;
			}
		break;

		case c_step_acc_off_wait:
			if(		(ELD_get_vehicle_rpm() != 0)
				||	(ELD_data.vibrate == 1) )
			{
				step = c_step_acc_off_chk;
			}			
			else if(	(	(ELD_get_vehicle_rpm() == 0)
						&&	(TRUE == STM_CheckSysTick((u32 *)&rpm_tmr,(u32)5*(1000/c_systick))) )
					||	(	(ELD_data.vibrate == 0)
						&&	(TRUE == STM_CheckSysTick((u32 *)&vibrate_tmr,(u32)40*(1000/c_systick))) ) )
			{
				ELD_data.acc_on = 0;
				step = c_step_idle;
			}					
		break;
			
		default:step = c_step_idle;
		break;	
	}	

	if(sleep_en == 0)
	{	
		if(STM_CheckSysTick(&sleep_tmr,60*(1000/c_systick)) == TRUE)
			sleep_en = 1;
	}
    else
    {		
		if(ELD_data.acc_on == 1)
		{	
			ELD_data.sys_sleep = 0;
		}		
		else
			ELD_data.sys_sleep = 1;
	}	
}


static void ELD_sysParameter_poll(void)
{
	static SYS_PARAMETER SysParameter_tmp = {0};	
	static SYS_RUN_PARAMETER SysRunParameter_tmp = {0};

	if(memcmp(&SysParameter_tmp,&SysParameter,sizeof(SysParameter)) != 0)
	{	
		if(SysParameter_tmp.Header[0] == '@')
			WriteSysParameterToFlash();
		SysParameter_tmp = SysParameter;
	}	

	if(memcmp(&SysRunParameter_tmp,&SysRunParameter,sizeof(SysRunParameter)) != 0)
	{	
		if(SysRunParameter_tmp.Header[0] == '@')
			WriteSysRunParameterToFlash();
		SysRunParameter_tmp = SysRunParameter;
	}

}
		
static void ELD_obd_poll(void)
{
#define PRO_485_J1708                   0x0b    
	static enum
	{
		c_step_idle,
        c_step_sup_req,
        c_step_sup_wait,
        c_step_rpmSpeed_req,
        c_step_rpmSpeed_wait,
		c_step_send_req,
        c_step_send_req_wait,
        c_step_send_pid,
        c_step_send_pid_wait,
	}step = c_step_idle;    
	static u8 pidIdx = 0;
    static u8 tmoRun = 0;
    static u32 tmoTmr = 0;
	static u32 tmr = 0;
    static u32 tmo = 20000;
    static u32 tmr500ms = 0;
	u8 x, y, send_exe;
			
    if(tick_cmp(tmr500ms, 500) == c_ret_nk)
        return;
    tmr500ms = tick_get();
    
    OSFlagAccept(obd_flag, (OS_FLAGS)1, OS_FLAG_WAIT_CLR_ALL, &x);    
    if( (x != OS_ERR_NONE) && (tick_cmp(tmr, tmo) == c_ret_nk) )
        return;

    switch(step)
    {
        case c_step_idle:
			if(OSMboxPost(obd_mbox_recv,"@RKEY=|*") == OS_ERR_NONE)	// 	读取重要参数的值	//总里程|总油耗|总行驶时间|当前行程里程|当前行程油耗|当前行程行驶时间|*
            {
                OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                dbg(ELD_DBG_EN,"obd_send: @RKEY=|*\r\n");
                ELD_data.obd.send_cnt++;
                tmo = 2000;
                tmr = tick_get();
                ELD_data.obd.send_en = 0;
                step = c_step_sup_req;
            }
            else
            {
                dbg(ELD_DBG_EN,"OSMboxPost(obd_mbox_recv,)!=OS_ERR_NONE\r\n");
                ELD_data.obd.send_cnt++;
                tmo = 2000;
                tmr = tick_get();
            }
		break;

        case c_step_sup_req:    
            if(ELD_data.obd.send_en == 1)
            {
                if(ELD_data.obd.accOnReq.b.sup == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RSUP=|*") == OS_ERR_NONE)				// 	读取车辆支持的PID列表
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RSUP=|*\r\n");
                        ELD_data.obd.accOnReq.b.sup = 0;
                        ELD_data.obd.accOnWait.b.sup = 1;
                        tmo = (ELD_data.obd.protocolTyp == PRO_485_J1708) ? 10000 : 5000;
                        tmr = tick_get();
                        step = c_step_sup_wait;
                    }                                                            
                }                                    
                else if(OSMboxPost(obd_mbox_recv,"@RPID=|010c|010d|0105|012f|*")==OS_ERR_NONE)					//	发动机转速	//	车速
                {
                    OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                    dbg(ELD_DBG_EN,"obd_send: @RPID=|010c|010d|0105|012f|*\r\n");
                    ELD_data.obd.accOnWait.b.pid2 = 1;
                    tmo = 5000;
                    tmr = tick_get();
                    step = c_step_rpmSpeed_wait;
                }                                                           
            }
            else 
                step = c_step_idle;
        break;

        case c_step_sup_wait:    
            if(tick_cmp(tmr, tmo) == c_ret_ok) 
            {
                step = c_step_idle;
                break;
            }
            else
            {
                step = c_step_rpmSpeed_req;            
                if(OSMboxPost(obd_mbox_recv,"@RPID=|010c|010d|0105|012f|*")==OS_ERR_NONE)					//	发动机转速	//	车速
                {
                    OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                    dbg(ELD_DBG_EN,"obd_send: @RPID=|010c|010d|0105|012f|*\r\n");
                    ELD_data.obd.accOnWait.b.pid2 = 1;
                    tmo = 5000;
                    tmr = tick_get();
                    step = c_step_rpmSpeed_wait;
                }                                                           
            }
        break;
            
        case c_step_rpmSpeed_wait:
            if(tick_cmp(tmr, tmo) == c_ret_ok) 
            {
                step = c_step_idle;
                break;
            }
            else
                step = c_step_send_req;
            
        case c_step_send_req:
            if(ELD_data.obd.send_en == 1)
            {
                tmoRun = send_exe = 0;
                if(ELD_data.obd.accOnReq.b.storCod == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RCOD=|0|*") == OS_ERR_NONE)				// 读存储故障码		
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RCOD=|0|*\r\n");             
                        ELD_data.obd.accOnReq.b.storCod = 0;
                        ELD_data.obd.accOnWait.b.storCod= 1;
                        tmo = 10000;
                        send_exe = 1;
                    }                        
                }
                else if(ELD_data.obd.accOnReq.b.pendCod == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RCOD=|1|*") == OS_ERR_NONE)				// 读未决故障码		
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RCOD=|1|*\r\n");             
                        ELD_data.obd.accOnReq.b.pendCod = 0;
                        ELD_data.obd.accOnWait.b.pendCod= 1;
                        tmo = 10000;
                        send_exe = 1;
                    }                        
                }
                else if(ELD_data.obd.accOnReq.b.sup == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RSUP=|*") == OS_ERR_NONE)				// 	读取车辆支持的PID列表
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RSUP=|*\r\n");
                        ELD_data.obd.accOnReq.b.sup = 0;
                        ELD_data.obd.accOnWait.b.sup = 1;
                        tmo = (ELD_data.obd.protocolTyp == PRO_485_J1708) ? 10000 : 5000;
                        send_exe = 1;
                    }                                                            
                }                    
                else if(ELD_data.obd.req.b.storCod == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RCOD=|0|*") == OS_ERR_NONE)				// 读存储故障码		
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RCOD=|0|*\r\n");             
                        ELD_data.obd.req.b.storCod = 0;
                        ELD_data.obd.wait.b.storCod= 1;
                        tmo = 10000;
                        send_exe = 1;
                    }                        
                }
                else if(ELD_data.obd.req.b.pendCod == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RCOD=|1|*") == OS_ERR_NONE)				// 读未决故障码		
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RCOD=|1|*\r\n");             
                        ELD_data.obd.req.b.pendCod = 0;
                        ELD_data.obd.wait.b.pendCod= 1;
                        tmo = 10000;
                        send_exe = 1;
                    }                        
                }
                else if(ELD_data.obd.req.b.cle == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RCLE=|*") == OS_ERR_NONE)				//	清障码		
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RCLE=|*\r\n");        
                        ELD_data.obd.req.b.cle = 0;
                        ELD_data.obd.wait.b.cle = 1;
                        tmo = 10000;
                        send_exe = 1;
                    }
                }                        
                else if(ELD_data.obd.req.b.sup == 1)
                {
                    if(OSMboxPost(obd_mbox_recv,"@RSUP=|*") == OS_ERR_NONE)				// 	读取车辆支持的PID列表
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RSUP=|*\r\n");
                        ELD_data.obd.req.b.sup = 0;
                        ELD_data.obd.wait.b.sup = 1;
                        tmo = (ELD_data.obd.protocolTyp == PRO_485_J1708) ? 10000 : 5000;
                        send_exe = 1;
                    }                                                            
                }
                else if((ELD_data.obd.req.b.vin == 1)||(ELD_data.obd.accOnReq.b.vin == 1))
                {
                    if(OSMboxPost(obd_mbox_recv,"@RVIN=|*") == OS_ERR_NONE)				//	读VIN码		
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RVIN=|*\r\n");
                        if(ELD_data.obd.req.b.vin == 1)
                            ELD_data.obd.wait.b.vin = 1;
                        else
                            ELD_data.obd.accOnWait.b.vin = 1;
                        ELD_data.obd.req.b.vin = ELD_data.obd.accOnReq.b.vin = 0;    
                        tmo = 10000;    
                        send_exe = 1;
                    }                                                            
                }
                else if(ELD_data.obd.req.b.pidx == 1)
                {
                    sprintf((char*)ELD_data.send_obd,"@RPID=");
                    for(y = x = 0; x < sizeof(ELD_data.obd.sendArg)/sizeof(ELD_data.obd.sendArg[0]); x++)
                    {
                        if(ELD_data.obd.sendArg[x] != 0)
                        {
                            sprintf((char*)ELD_data.send_obd + strlen((char*)ELD_data.send_obd),"|%04x",ELD_data.obd.sendArg[x]);
                            y++;
                        }
                    }
                    if(y != 0)
                    {
                        sprintf((char*)ELD_data.send_obd + strlen((char*)ELD_data.send_obd),"|*");                            
                        if(OSMboxPost(obd_mbox_recv,ELD_data.send_obd) == OS_ERR_NONE)
                        {
                            OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                            dbg(ELD_DBG_EN,"obd_send: %s\r\n",ELD_data.send_obd);
                            ELD_data.obd.req.b.pidx = 0;
                            ELD_data.obd.wait.b.pidx = 1;
                            tmo = 5000;
                            send_exe = 1;
                        }
                    }
                    else
                    {
                        ELD_data.obd.req.b.pidx = 0;
                        if(OSMboxPost(obd_mbox_recv,"@RPID=|010c|010d|0105|012f|*")==OS_ERR_NONE)					//	发动机转速	//	车速
                        {
                            OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                            dbg(ELD_DBG_EN,"obd_send: @RPID=|010c|010d|0105|012f|*\r\n");
                            ELD_data.obd.accOnWait.b.pid2 = 1;
                            tmo = 5000;
                            send_exe = 1;
                        }                                                           
                    }                                    
                }
                else
                {
                    if(OSMboxPost(obd_mbox_recv,"@RPID=|010c|010d|0105|012f|*")==OS_ERR_NONE)					//	发动机转速	//	车速
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: @RPID=|010c|010d|0105|012f|*\r\n");
                        ELD_data.obd.accOnWait.b.pid2 = 1;
                        tmo = 5000;
                        send_exe = 1;
                    }                                                           
                }
                if(send_exe == 1)
                {
                    tmr = tick_get();
                    step = c_step_send_req_wait;                                    
                }
            }
            else if((tmoRun == 0)&&(ELD_data.obd.req.r != 0))
            {
                tmoRun = 1;
                tmoTmr = tick_get();
            }
            if( (tmoRun == 1)&&(tick_cmp(tmoTmr,20000)==c_ret_ok) )
            {
                if(     ELD_data.obd.req.b.storCod == 1)  ELD_data.obd.wait.b.storCod = 1;
                else if(ELD_data.obd.req.b.pendCod == 1)  ELD_data.obd.wait.b.pendCod = 1;
                else if(ELD_data.obd.req.b.cle == 1)      ELD_data.obd.wait.b.cle = 1;  
                else if(ELD_data.obd.req.b.vin == 1)      ELD_data.obd.wait.b.vin = 1;
                else if(ELD_data.obd.req.b.sup == 1)      ELD_data.obd.wait.b.sup = 1;
                else if(ELD_data.obd.req.b.pidx == 1)     ELD_data.obd.wait.b.pidx = 1;
                ELD_data.obd.req.r = 0;    
                ELD_obd_fail_proc("FAIL=2");
                tmoRun = 0;
            }
            if(tick_cmp(tmr, tmo) == c_ret_ok)
            {
                step = c_step_idle;
            }
        break;
        
        case c_step_send_req_wait:
            if( (ELD_data.obd.wait.r == 0)&&(ELD_data.obd.accOnWait.r == 0) )
            {
                step = c_step_send_pid;
            }                            
            else if(tick_cmp(tmr, tmo) == c_ret_ok)
            {
                ELD_obd_fail_proc("FAIL=2");                
                step = c_step_idle;                
            }            
        break;   

        case c_step_send_pid:
            for(x = 0; x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
            {    
                if(pidIdx >= sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0])) pidIdx = 0;	
                if(     (ELD_data.obd.pid[pidIdx].name != 0x0000)
                    &&  (ELD_data.obd.pid[pidIdx].enable == 1)
                    &&  (ELD_data.obd.pid[pidIdx].name != 0x010c)
                    &&  (ELD_data.obd.pid[pidIdx].name != 0x0105)
                    &&  (ELD_data.obd.pid[pidIdx].name != 0x012f)
                    &&  (ELD_data.obd.pid[pidIdx].name != 0x010d) )
                {
                    sprintf((char*)ELD_data.send_obd,"@RPID=|%04x|*",ELD_data.obd.pid[pidIdx].name);
                    if(OSMboxPost(obd_mbox_recv,ELD_data.send_obd) == OS_ERR_NONE)
                    {
                        OSFlagPost(obd_flag, (OS_FLAGS)1, OS_FLAG_SET, &x);
                        dbg(ELD_DBG_EN,"obd_send: %s\r\n",ELD_data.send_obd);
                        ELD_data.obd.accOnWait.b.pid1 = 1;
                        tmo = 5000;
                        tmr = tick_get();
                        step = c_step_send_pid_wait;
                        pidIdx++;
                    }
                    break;    
                }
                else
                {
                    pidIdx++;
                }
            }
            if(x >= sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]))
                step = c_step_idle;
        break;    
        
        case c_step_send_pid_wait:
            if(     (ELD_data.obd.accOnWait.b.pid1 == 0)
                ||  (tick_cmp(tmr, tmo) == c_ret_ok) )
            {
                step = c_step_idle;
            }                            
        break;
            
        default:step = c_step_idle;            
        break;    
    }
        
	if(ELD_data.obd.send_cnt >= 3)				// 连续3次握手失败,所有OBD数据无效
	{
		ELD_data.obd.send_cnt = 0;
		for(x = 0;x < sizeof(ELD_data.obd.pid)/sizeof(ELD_data.obd.pid[0]); x++)
		{
            ELD_data.obd.pid[x].val = 0;
		}
	}
}

static void ELD_init(void)
{	
	u16 x;
	
	memset(&ELD_data,0,sizeof(ELD_data));
    
	for(x = 0; x < ELD_task_poll_Size; x++)	
		ELD_task_poll_STK[x] = 0x55555555;	
	
	FlashReadDataAddr = ELD_data.bt_upload_report_addr = SysRunParameter.FLASH_Parameter.ConfirmDataAddr;	
	
	ELD_data.bt_upload_answer = ELD_data.gprs_upload_answer = c_ret_nk;	
}


void ELD_task_poll_proc(void)
{
    //	数据收集
    ELD_obd_poll();				
    ELD_mpu6050_poll();		
	
    if((ftpSendPara.step != c_sendStep_set)&&(ftpSendPara.step != c_sendStep_run))
    {
        ELD_sysParameter_poll();	    
        //	数据处理
        ELD_event_proc();			
    }
    //	数据上传
    ELD_upload();				
}


void ELD_start(void)
{
	INT8U errFlag;
	
	printf("\r\nELD start task................OK.\r\n");
	
	ELD_init();
	
	ELD_rece_status =	OSFlagCreate (0, &errFlag);
	ELD_send_status =	OSFlagCreate (0, &errFlag);
	
	ELD_rece_bt			=	OSMboxCreate((void *)0);
	ELD_rece_obd		=	OSMboxCreate((void *)0);
	
	OSTaskCreate(ELD_task_poll,		(void *)0, (OS_STK *)&ELD_task_poll_STK[	ELD_task_poll_Size-1], 		ELD_task_poll_PRIO);

}

