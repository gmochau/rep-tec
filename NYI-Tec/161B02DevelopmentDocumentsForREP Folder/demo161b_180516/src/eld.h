/*
	eld.h

AT$<123456>DateTime=122417162647|CarType=FFFF*
AT$<123456>DebugFlag=0*
AT$<123456>DateTime*
AT$SYSS<123456>=?*

$$,,8,3,,,<123456>DateTime,065b**
$$,,4,1,,,,0199**
$$,,8,3,,,<123456>FirmwareVersion,0971**
$$,,4,3,,,,019b**
    $$100317120002|100617015609,055D**
$$,,4,2,,,100317120002|100617015609,06cb**

$$,,2,2,,,10|5|TRIM|GXYZ|AXYZ,07f8**


$$ APP_12345678 , ,  4  ,1 ,122417 , 151747 , , 06E7 **
$$ APP_12345678 , ,  4  ,2 ,122417 , 151747 ,100117151747|123017151747 , 0c2e **
$$ APP_12345678 , ,  4  ,3 ,122417 , 151747 , , 06E9**

重启设备
$$ APP_12345678 , ,  8  ,1 ,122417 , 151747 , , 06EB **

恢复默认参数
$$ APP_12345678 , ,  8  ,2 ,122417 , 151747 , , 06EC **

恢复默认参数，清除日志
$$ APP_12345678 , ,  8  ,2 ,122417 , 151747 ,0 , 71C **
$$ APP_12345678 , ,  8  ,2 ,122417 , 151747 ,1 , 71D **
$$ APP_12345678 , ,  8  ,2 ,122417 , 151747 ,2 , 71E **
$$,,8,2,,,1,01CF**

查询参数
$$ APP_12345678 , ,  8  ,3 ,122417 , 151747 ,<123456>SourceID|PassWord|DateTime|CarType|TotalMileage|TotalFuel|EngineHours ,2429 **
$$ APP_12345678 , ,  8  ,3 ,122417 , 151747 ,<123456>DateTime|CarType, 0edd **
$$ APP_12345678 , ,  8  ,3 ,122417 , 151747 ,<123456>EngineHours ,0d03 **
$$ APP_12345678 , ,  8  ,3 ,122417 , 151747 ,<123456>FirmwareVersion ,0EBF**

设置参数
$$ APP_12345678 , ,  8  ,4 ,122417 , 151747 ,<123456>DateTime=122917120000|CarType=0000, 1271 **
$$ APP_12345678 , ,  8  ,4 ,122417 , 151747 ,<123456>DateTime=122917120000, 0e40 **
$$APP_12345678,,8,4,,,<123456>DebugFlag=0,0a01**
$$APP_12345678,,8,4,,,<123456>DebugFlag=1,0a02**

蓝牙升级
$$ APP_12345678 , ,  8  ,9 ,122417 , 151747 , , 06F3 ** 
$$,,8,9,,,,01A5**

$$APP_0000000000001,,7,1,122417,162647,,07b8**
$$APP_0000000000001,,7,2,122417,162647,0,07E9**
$$APP_0000000000001,,7,2,122417,162647,1,07EA**
$$APP_0000000000001,,7,3,122417,162647,,07BA**
$$APP_0000000000001,,7,4,122417,162647,,07BB**

$$APP_0000000000001,,7,5,122417,162647,010b|010d|010c|010e,0d02**

$$APP_0000000000001,,2,2,122417,162647,1|5|010b|010d|010c|010e|VPWR|ODOM|CODE|HOUR|VINC|GXYZ|AXYZ,1A61**
$$APP_0000000000001,,2,2,122417,162647,5|10|010b|010d|010c|010e|VPWR|ODOM|CODE|HOUR|VINC|GXYZ|AXYZ,1A91**

*/

#ifndef __eld_h__
#define __eld_h__

#include "stm32_config.h"

#define	ELD_PACKET_LEN			512		//包最大长度

#define ELD_SPLIT_SequeID		0
#define ELD_SPLIT_EventType		1
#define ELD_SPLIT_EventCode		2
#define ELD_SPLIT_Date			3
#define ELD_SPLIT_Time			4
#define ELD_SPLIT_EventData		5
#define ELD_SPLIT_EventSum		6
#define ELD_SPLIT_NUM			7		//分隔符数量

#define ELD_PACKET_idle			0
#define ELD_PACKET_gprs			1
#define ELD_PACKET_sms			2
#define ELD_PACKET_bt			3
#define ELD_PACKET_uart			4
#define ELD_PACKET_eld			5

#define ELD_rece_done_gprs       (1<<0)	//1:eld已经处理完成接收的消息数据
#define ELD_rece_done_sms        (1<<1)
#define ELD_rece_done_bt         (1<<2)
#define ELD_rece_done_obd        (1<<3)
#define ELD_rece_done_uart       (1<<4)
#define ELD_rece_done_ftp		 (1<<5)

#define ELD_send_done_gprs       (1<<7)	//1:eld请求发送
#define ELD_send_done_sms        (1<<8)
#define ELD_send_done_bt         (1<<9)
#define ELD_send_done_obd        (1<<10)
#define ELD_send_done_uart       (1<<11)
#define ELD_send_done_ftp        (1<<12)

extern OS_FLAG_GRP *ELD_send_status;
extern OS_FLAG_GRP *ELD_rece_status;

extern OS_EVENT *ELD_rece_bt;
extern OS_EVENT *ELD_rece_obd;

extern OS_EVENT *ELD_send_obd;

extern OS_STK ELD_task_poll_STK[ELD_task_poll_Size];

#define c_driverStatus_offDuty			1	// 司机职责状况改为“不在职”
#define c_driverStatus_sleep			2	// 司机职责状况改为“睡眠者泊位”
#define c_driverStatus_driving			3	// 司机职责状况改为“驾驶”
#define c_driverStatus_onDutyNotDriving	4	// 司机职责状况改为“无人驾驶”

struct st_eld_data
{
	u8  vibrate;						// 1: 车辆处于振动状态
	u8  acc_on;							// 1: 车辆处于acc_on点火状态
		
	u8 btSendRespReq;
	u8 gprsSendRespReq;
	u8 smsSendRespReq;
	//u8 uartSendRespReq;
	u8 ftpSendSrc;
	
	struct st_eld_data_obd
	{
        u8  send_en;
		u8  send_cnt;					// 连续10次握手不成功,所有obd数据无效	
		u8  handshake_ok;				// 1: 握手成功               
        union un_acc_on_req
        {
            u8 r;
            struct st_acc_on_req
            {
                u8 storCod  :1;
                u8 pendCod  :1;
                u8 sup      :1;
                u8 vin      :1;
            }b;
        }accOnReq;
        union un_acc_on_wait
        {
            u8 r;
            struct st_acc_on_wait
            {
                u8 storCod  :1;
                u8 pendCod  :1;
                u8 sup      :1;
                u8 vin      :1;
                u8 pid2     :1;
                u8 pid1     :1;
            }b;
        }accOnWait;        
        union un_req
        {
            u8 r;
            struct st_req
            {
                u8 storCod  :1;
                u8 pendCod  :1;
                u8 cle      :1;
                u8 vin      :1;
                u8 sup      :1;
                u8 pidx     :1;
            }b;
        }req;
        union un_wait
        {
            u8 r;
            struct st_wait
            {
                u8 storCod  :1;
                u8 pendCod  :1;
                u8 cle      :1;
                u8 vin      :1;
                u8 sup      :1;
                u8 pidx     :1;
            }b;
        }wait;        
        u16 sendArg[5];
        
		struct st_obd_pid
		{
            u8  enable;
            u8  valid;
            u8  unit[13];
            u16 name;                   // 
			u32 val;					// 数据值	
		}pid[48];	
		u16 storCode[16];		        // 车辆存储故障码
        u16 pendCode[16];               // 车辆未决故障码
        
        u16 protocolTyp;                // 确定不同超时时间
        
		u32 CurrentTripMileage;			// 当前行驶里程
		u32 CurrentTripFuelConsumption;	// 当前油耗
		u32 CurrentTripTime;			// 当前行驶时间		        
	}obd;

	struct st_last_valid_loction
	{
		double		lat;
		u8			n_s;
	 	double		lng;
		u8			e_w;
	}last_loction;
	
    struct st_gprs_resp
    {
        u8 confirm;
        u8 confirm_done;
        u16 id;
        u32 addr;
    }gprs_resp[15],bt_resp[15];
        
    u8 report71R_req;
    
	u8 sys_sleep;						//  1:进入系统睡眠状态，GPS/GSM/OBD指示灯全灭；0:进入标准工作模式
	u8 gps_pwron;						//  1:模块上电,0:模块掉电
	u8 gsm_pwron;
	u8 bt_pwron;
	
	u8 bt_upload_date_en;				//	仅上传请求时间段未上传日志
	u8 bt_upload_addr_en;				//  蓝牙[9-4]查询存储器中的日志起始时间和日志结束时间 
	
	u8 bt_upload_answer;
	u8 bt_currentTab;					//	对应当前通信蓝牙设备
	
	u8 gprs_upload_answer;				//	1:对应包号日志收到gprs服务器应答
	
	u8 driverStatus;					//  offDuty,sleep,driving,onDutyNotDriving
	u8 driverValid;						//  1:合法驾驶者，0：非法驾驶者 ELD接收的事件日志中的司机ID，如果不为空那就是合法驾驶者，为空就是非法驾驶者
	
	u8 response_bt[512];		
	u8 response_gsm[512];
	u8 response_obd[128];
	u8 send_obd[128];
	u8 ProtocolSendBuf[800];
	
    u16 bt_upload_report_id;			// 应答解析用
	u32 bt_upload_report_addr;			// bt_upload_date_en/bt_upload_addr_en上传指针
	
	u32 gprs_upload_report_addr;		// gprs_upload_addr_en上传指针
	u8  gprs_upload_addr_en;
	u8  gprs_send_cnt;					// 用于网络重连
	
	u32 gprs_confirm_addr_end;	
	
	u32 bt_upload_tm_start;
	u32 bt_upload_tm_end;
	u32 confirmDataAddrTm;				// 第一条待确认日志产生的时间
	
	u32 tcp_keepalive_tmr;
    
    struct st_mpu6050_data
    {
        short gyro_x;
        short gyro_y;
        short gyro_z;
        short accel_x;
        short accel_y;
        short accel_z;
    }mpu6050Data[10];
    u32 mpu6050DataTmr;
    u8  mpu6050DataIdx;     

    u8 btDbgEn;                         // 总开关
    u8 btDbgReq;                        // 发送请求
    u8 btDbgBuf[1024];                  // 发送数据ASCII码
};
extern struct st_eld_data ELD_data;


enum enSendStep
{
	c_sendStep_idle,
	c_sendStep_set,
	c_sendStep_run,
	c_sendStep_done
};

struct ftpPara
{
	u8 result;		
    u8 src;	
	u8 ip[101];
	u8 name[31];
	u8 password[41];
	u8 file[101];
    u8 btMac[6];
	enum enSendStep step;
	u16 port;
	u32 len;
};
extern struct ftpPara ftpSendPara;

extern void app_sendFtpSet(struct ftpPara *pFtpPara);

extern INT8U app_sendFtpStatus(void);

extern void ELD_start(void);

//	ELD 自身生成日志事件
extern u8 ELD_packet_make_eld(u8 EventType,u16 EventCode,void *EventData);
//	在事件字符串后填充事件校验和事件数据校验字段
extern void ELD_sum_set(u8 *str);
//	立即应答数据包和缓存数据包上传
extern u32 ELD_str_hex(u8* str);
//	strncpy()当n==0退出时不填充结束符
extern u8* ELD_strncpy(u8* xbuf,u8* str,u16 n);
//	复制第h个字符chr后一字符到第h+1个字符chr前一字符
extern u8 ELD_strcpy_ht(u8* buf,u8* str,u8 chr,u8 h,u16 maxLen);

extern u32 ELD_get_vehicle_rpm(void);

extern u32 ELD_get_vehicle_speed(void);

extern u8 ELD_get_vehicle_run(void);

extern u8 ELD_get_vehicle_stop(void);

extern u32 tick_get(void);

extern u32 tick_cmp(u32 tmr,u32 tmo);

extern void ELD_test(void);

#endif
