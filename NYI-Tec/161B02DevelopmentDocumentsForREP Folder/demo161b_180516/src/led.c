
/******************************************************************************
*
*                               
* @ Version   -->
* @ Author    -->
* @ Date      -->
* @ Revise    -->
*
******************************************************************************/

#include "STM32_config.h"

#define MCU_BUZZ_GPIOX			GPIOC
#define MCU_BUZZ_PIN 			GPIO_Pin_8
#define MCU_BUZZ_ON			    GPIO_WriteBit(MCU_BUZZ_GPIOX, MCU_BUZZ_PIN , Bit_SET)
#define MCU_BUZZ_OFF			GPIO_WriteBit(MCU_BUZZ_GPIOX, MCU_BUZZ_PIN , Bit_RESET)


typedef struct				
{
	u8 BuzzOnEn;
	u16 BuzzTimeOn;
	u16 BuzzTimeOff;
	u8 BuzzOnCnt;
	u8 BuzzOutFlag;
} BUZZ_CONFIG;
BUZZ_CONFIG BuzzConfig = {0};

typedef struct				
{
	u8 LED_OBD_Enable;
	u16 LED_OBD_ON_TIME;
	u16 LED_OBD_OFF_TIME;

	u8 LED_BT_Enable;
	u16 LED_BT_ON_TIME;
	u16 LED_BT_OFF_TIME;
	
} LED_CONFIG;
LED_CONFIG LedConfig = {0};

/******************************************************************************
* Function Name --> LED接口初始化
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 

******************************************************************************/
void LED_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//GPIOB时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	//GPIOB时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	//GPIOB时钟
	
    // PA8
	GPIO_InitStructure.GPIO_Pin = LED_BT_RED;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LED_BT_GOIOX, &GPIO_InitStructure);
	BT_LED_OFF;

    // PB7
	GPIO_InitStructure.GPIO_Pin = LED_OBD_RED;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LED_OBD_GOIOX, &GPIO_InitStructure);
	OBD_LED_OFF;
	
}

void Led_OutControl(void)
{
	static u16 Bt_cnt = 0;
	static u16 Obd_cnt = 0;
	
	static u8 LED_BtFlag=0;
	static u8 LED_ObdFlag = 0;
	

	if(LedConfig.LED_BT_Enable)
	{
		switch(LED_BtFlag)
		{
			case 0:
				BT_LED_ON;
				if(++Bt_cnt>=LedConfig.LED_BT_ON_TIME)
				{
					Bt_cnt = 0;
					LED_BtFlag = 1;
				}
				break;
			case 1:
				BT_LED_OFF;
				if(++Bt_cnt>= LedConfig.LED_BT_OFF_TIME)
				{
					Bt_cnt = 0;
					LED_BtFlag = 0;
				}
				break;
			default:
				break;
		}
	}	

	if(LedConfig.LED_OBD_Enable)
	{
		switch(LED_ObdFlag)
		{
			case 0:
				OBD_LED_ON;
				if(++Obd_cnt>= LedConfig.LED_OBD_ON_TIME)
				{
					Obd_cnt = 0;
					LED_ObdFlag = 1;
				}
				break;
			case 1:
				OBD_LED_OFF;
				if(++Obd_cnt>= LedConfig.LED_OBD_OFF_TIME)
				{
					Obd_cnt = 0;
					LED_ObdFlag = 0;
				}
				break;
			default:
				break;
		}
	}
	
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void Led_SetParameter(u8 LedType,u8 LedMode, u16 LedOnTime , u16 LedOffTime)
{
	
	if(LedType == LED_BT)
	{
		switch(LedMode)
		{
			case LED_EXTINGUISH:
				LedConfig.LED_BT_Enable = 0;
				BT_LED_OFF;
				break;
				
			case LED_LIGHT:
				LedConfig.LED_BT_Enable = 0;
				BT_LED_ON;
				break;
				
			case LED_FLASH:
				LedConfig.LED_BT_ON_TIME = LedOnTime;
				LedConfig.LED_BT_OFF_TIME = LedOffTime;
				LedConfig.LED_BT_Enable = 1;
				break;
				
			default:
				break;
		}
	}
	
	if(LedType == LED_OBD)
	{
		switch(LedMode)
		{
			case LED_EXTINGUISH:
				LedConfig.LED_OBD_Enable = 0;
				OBD_LED_OFF;	
				break;
				
			case LED_LIGHT:
				LedConfig.LED_OBD_Enable = 0;
				OBD_LED_ON;
				break;
				
			case LED_FLASH:
				LedConfig.LED_OBD_ON_TIME = LedOnTime;
				LedConfig.LED_OBD_OFF_TIME = LedOffTime;
				LedConfig.LED_OBD_Enable = 1;
				break;
				
			default:
				break;
		}
	}
}



