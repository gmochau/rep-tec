
/******************************************************************************
* Function Name --> LED接口初始化
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/

#ifndef _led_h_
#define _led_h_


#define LED_OBD      			0      //OBD指示灯
#define LED_GPS     			1     //GPS指示灯
#define	LED_GSM			        2	//GSM指示灯
#define LED_BT                  3

#define LED_EXTINGUISH   	    0x30    //灯灭//
#define LED_LIGHT        		0x31    //灯亮//
#define LED_FLASH        		0x32    //灯闪//


#define LED_BT_GOIOX 			GPIOA
#define LED_BT_RED				GPIO_Pin_8		//
#define BT_LED_ON				GPIO_WriteBit(LED_BT_GOIOX, LED_BT_RED, Bit_SET)
#define BT_LED_OFF				GPIO_WriteBit(LED_BT_GOIOX, LED_BT_RED, Bit_RESET)

#define LED_OBD_GOIOX 			GPIOB
#define LED_OBD_RED				GPIO_Pin_7		//
#define OBD_LED_ON				GPIO_WriteBit(LED_OBD_GOIOX, LED_OBD_RED, Bit_SET)
#define OBD_LED_OFF				GPIO_WriteBit(LED_OBD_GOIOX, LED_OBD_RED, Bit_RESET)

/******************************************************************************/
extern void LED_Init(void);	//初始化LED接口

extern void Led_OutControl(void);

extern void Led_SetParameter(u8 LedType,u8 LedMode, u16 LedOnTime , u16 LedOffTime);

#endif





