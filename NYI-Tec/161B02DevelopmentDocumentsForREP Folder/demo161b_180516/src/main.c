
/*******************************************************************************                            
* @ Version   -->
* @ Author    --> 
* @ Date      --> 
* @ Revise    -->
*
******************************************************************************/

#include "STM32_config.h"			//

u16 Power12v = 0;

u16 BSP_ADcPower12v(void)
{
    //ext12v---150k---adin13---10k---gnd
	return (ADC_GetConversionValue(ADC1) * (3.3*16*1000/4096)) + 500;
}

u16 BSP_ADCConfig()
{
	ADC_InitTypeDef ADC_InitStructure;
       u8 i;

	GPIO_InitTypeDef GPIO_InitStructure;
	// 将 PC3(ADC12_IN13) 设置为模拟输入脚
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;//GPIO_Mode_AIN;
	GPIO_Init(GPIOC , &GPIO_InitStructure);

    RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC1,DISABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC , &GPIO_InitStructure);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    
	ADC_Cmd(ADC1, DISABLE);for(i=0;i<100;i++);//20us
    
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;//ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;//ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;//4;//3;
	ADC_Init(ADC1, &ADC_InitStructure);

	/* 设置 ADC1 使用11,12,13转换通道，转换顺序1-3，采样时间为 71.5 周期 */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_13,      1, ADC_SampleTime_71Cycles5);//PC3 12V

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);for(i=0;i<100;i++);//20us

	return 0;
}

void BSP_adcStart(void)
{
    u32 cnt;    
    BSP_ADCConfig();
    
	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);

	/* Check the end of ADC1 reset calibration register */
    while(ADC_GetResetCalibrationStatus(ADC1))
    {
    }

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	/* Check the end of ADC1 calibration */
    while(ADC_GetCalibrationStatus(ADC1))
    {
    }
    for(cnt = 0; cnt < 100; cnt++); //20us

	/* Start ADC1 Software Conversion */
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

    for(cnt = 0; cnt < 100; cnt++)
    {    
        if(ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC) != RESET) break;
    }
    ADC_ClearFlag(ADC1, ADC_FLAG_EOC);    
}

/******************************************************************************
* Function Name --> 
* Description   --> none
* Input         --> *pdata: 
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void BSP_ADCInit(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	// 将 PC3(ADC12_IN13) 设置为模拟输入脚

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC , &GPIO_InitStructure);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	BSP_ADCConfig();
	
}

void delay_init(void)
{
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);  
	
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;	
	SysTick->LOAD = 9 * 1000 - 1;	
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;     

}

void delay_us(u32 nus)
{		
	uint32_t temp=0;
	uint32_t VAL_Prev=0;  
	uint32_t VAL_Now=0;   
	uint32_t VAL_cnt=0;   
	uint32_t Reload=SysTick->LOAD;  

	temp = nus * SystemCoreClock / 8000000;  
	VAL_Prev = SysTick->VAL;  

	while(1)
	{
		VAL_Now = SysTick->VAL;  
		if(VAL_Now != VAL_Prev)
		{
			if(VAL_Now < VAL_Prev)  VAL_cnt += VAL_Prev-VAL_Now;  
			else                      VAL_cnt += Reload - VAL_Now + VAL_Prev;

			VAL_Prev = VAL_Now;  
			if(VAL_cnt >= temp)  break;  
		}
	}
}

void delay_ms(u16 nms)
{
	delay_us((u32)(nms*1000));  
}

u32 tick_get(void)
{
    return dbg_tmr;
}

u32 tick_cmp(u32 tmr,u32 tmo)
{
    return ((dbg_tmr - tmr) >= tmo) ? c_ret_ok : c_ret_nk;
}

void SysTick_Handler(void)
{
    dbg_tmr++;
    Led_OutControl();
    uartTimeDelayInc();
    BtuartTimeDelayInc();
} 

const u8 version[64] __attribute__((at(IAP_VER_ADDR)))  = 
                        "161B H1.0V1.1.0"__DATE__" "__TIME__" Nyitech Co.,Ltd.";     
const u8 readVersion[9] = "V1.1.0";

int main(void)
{				
    SCB->VTOR = IAP_APP_ADDR;
    __ASM("CPSIE I");
        
	delay_init();  										
	MY_NVIC_PriorityGroup_Config(NVIC_PriorityGroup_2);		
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_I2C1, ENABLE);           

	BSP_USART_Init();
	
	printf("\r\n************************************************************\r\n");
	printf("*    %s    \r\n",version);
	printf("************************************************************\r\n\r\n");
	printf("system init .............OK!. \r\nenter system............OK.\r\n");
	
	NandFlashInit();

    BSP_ADCInit();
    BSP_adcStart();    
	Power12v = BSP_ADcPower12v();
            
	RTCInit();

	LED_Init();
    Led_SetParameter(LED_OBD, LED_FLASH, 500, 500);
    
    while(1)
    {        
        bt_poll();
        rtc_test();
        w25xx_test();

    }
}
