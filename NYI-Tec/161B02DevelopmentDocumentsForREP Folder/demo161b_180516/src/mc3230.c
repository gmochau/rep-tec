/*
    mc3230_acc.c
    三轴加速度传感器
    
    1~128次采样/秒
    基于中断事件驱动和轮询读取采样值
    +-1.5g
    6/7/8位精度 
    tap/shake/drop 拍击、摇动、跌落事件侦测
    
    85 LSB/g        精度较低
    8位有符号数
    I2C快速模式400Khz
    字节间间隔最低1.3US 开始和结束波形之间间隔
    
    SAMPR   采样率设置寄存器
    20~50次采样每秒---侦测摇动事件
    64~128次采样每秒---侦测拍击事件
    XOUT/YOUT/ZOUT  补码
    SHAKED/TAPD/DROPD
    BAFR/POLA
    
    上电处于STANDBY模式     SNIFF/WAKE
    仅仅在待机模式能写所有寄存器内容，其它模式仅能写MODE寄存器；
    OPCON[1：0] = 01    强制切换到唤醒状态
    SAMPR.WAKER[2：0] = 000 128次/秒
    
    TAPD/SHAKED/DROPD直到读TILT寄存器后清零，POLA和BAFR每次采样更新
    
    Portrait/Landscape纵向/横向
    
    corresponding trip angle and amount of hysteresis对应的行程角和滞洄量
    
    TAP侦测能用于侦测使用者按钮操作
    
    写寄存器
    MASTER    START   ID(0X4C) R/W    ADDR    DATA    STOP
    SLAVE                          ACK     ACK     ACK
    
    读寄存器
    MASTER      START   ID(0X4C) /W     ADDR        RESTART     ID(0X4C) R              NAK     STOP
    SLAVE                           ACK         ACK                         ACK  DATA
    
    1. 时钟线锁死
    通信对方具有时钟拉长功能，时钟线被对方拉低
    ====>释放时钟线和数据线，一直轮询时钟线和数据线，都为高时再发送停止位后退出
    2. 数据线锁死
    从机时钟被干扰，主机还没有发送第9个时钟时，从机已经认为收到第9个时钟而输出ACK，把
数据线拉低，导致主机不能输出数据高，造成主机仲裁失败；
    ====>释放数据线，连续发送9个时钟，再发送停止位后退出            
*/    

#include "STM32_config.h"

#ifdef __CFG_GSENSOR_MC32XX__
//  加速度寄存器
#define reg_xout        0x00    //r
#define reg_yout        0x01    //r
#define reg_zout        0x02    //r
//  倾斜状态寄存器      
#define reg_tilt        0x03    //r     shaked  dropd   tapd    pola2   pola1   pola0   bafr1   bafr0
//  操作状态            
#define reg_opstat      0x04    //r     otpa    0       otpe    0       0       0       opstat1 opstat0
//  睡眠计数器          
#define reg_sc          0x05
//                      
#define reg_inten       0x06    //      shintx  shinty  shintz  gint    asint   tint    plint   fbint
//                      
#define reg_mode        0x07    //      iah     ipp     scps    ase     awe     0       opcon1  opcon0
//                      
#define reg_sampr       0x08    //      filt2   filt1   filt0   sniffr1 sniff0  waker2  waker1  waker0
//  敲击允许            
#define reg_tapen       0x09    //      zda     yda     xda     resv    resv    resv    resv    resv
//  敲击脉冲            
#define reg_tapp        0x0a    //      resv    resv    resv    resv    tapp3   tapp2   tapp1   tapp0
//  跌落事件            
#define reg_drop        0x0b    //      mode    dint    resv    resv    resv    db2     db1     db0
//  摇晃防抖动          
#define reg_shdb        0x0c    //      resv    resv    db5     db4     db3     db2     db1     db0
//                      
#define reg_chipid      0x18    //r 0x01
//                      
#define reg_xoffl       0x21    //      xoff7   xoff6   xoff5   xoff4   xoff3   xoff2   xoff1   xoff0
#define reg_xoffh       0x22    //      xgain8  resv    xoff13  xoff12  xoff11  xoff10  xoff9   xoff8
#define reg_yoffl       0x23    //      yoff7   xoff6   xoff5   xoff4   xoff3   xoff2   xoff1   xoff0
#define reg_yoffh       0x24    //      ygain8  resv    xoff13  xoff12  xoff11  xoff10  xoff9   xoff8
#define reg_zoffl       0x25    //      zoff7   xoff6   xoff5   xoff4   xoff3   xoff2   xoff1   xoff0
#define reg_zoffh       0x26    //      zgain8  resv    xoff13  xoff12  xoff11  xoff10  xoff9   xoff8
#define reg_xgain       0x27    //      xgain7  xgain6
#define reg_ygain       0x28    //      xgain7  xgain6
#define reg_zgain       0x29    //      xgain7  xgain6
#define reg_shake_th    0x2b
#define reg_ud_z_th     0x2c
#define reg_ud_x_th     0x2d
#define reg_rl_z_th     0x2e
#define reg_rl_y_th     0x2f
#define reg_fb_z_th     0x30
#define reg_drop_th     0x31
#define reg_tap_th      0x32
//  产品代码
#define reg_pcode       0x3b    //r 0x19

//up    xout=-1g, yout= 0g,  zout= 0g
//down  xout=+1g, yout= 0g,  zout= 0g
//left  xout= 0g, yout=+1g,  zout= 0g
//right xout= 0g, yout=-1g,  zout= 0g
//front xout= 0g, yout= 0g,  zout=+1g
//back  xout= 0g, yout= 0g,  zout=-1g

//  进入待机模式    [0x07] = 0x03
//  设置采样率      [0x08] = 0x00
//  进入唤醒模式    [0x07] = 0x01

#define MPU6050_INT			    GPIO_Pin_0
#define MPU6050_INT_PORT	    GPIOA
#define I2C_SDA 			    GPIO_Pin_9	 
#define I2C_SCL 			    GPIO_Pin_8	
#define I2C_PORT   		        GPIOB

#define p_mc323x_sda_h()        GPIO_SetBits(I2C_PORT, I2C_SDA)
#define p_mc323x_sda_l()        GPIO_ResetBits(I2C_PORT, I2C_SDA)
#define p_mc323x_sck_h()        GPIO_SetBits(I2C_PORT, I2C_SCL)
#define p_mc323x_sck_l()        GPIO_ResetBits(I2C_PORT, I2C_SCL)
#define pin_mc323x_sda_h()      (GPIO_ReadInputDataBit(I2C_PORT, I2C_SDA) != 0)
#define pin_mc323x_sda_l()      (GPIO_ReadInputDataBit(I2C_PORT, I2C_SDA) == 0)
#define pin_mc323x_sck_h()      (GPIO_ReadInputDataBit(I2C_PORT, I2C_SCL) != 0)
#define pin_mc323x_sck_l()      (GPIO_ReadInputDataBit(I2C_PORT, I2C_SCL) == 0)


void I2C_Configuration(void)	// PB8(SCL) / PB9(SDA)	/ PA0(ADC_INT)	
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = I2C_SCL | I2C_SDA;				
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;   
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;	    //开漏输出
	GPIO_Init(I2C_PORT, &GPIO_InitStructure); 
	
	GPIO_InitStructure.GPIO_Pin = MPU6050_INT;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;   
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
	GPIO_Init(MPU6050_INT_PORT, &GPIO_InitStructure); 
	
    p_mc323x_sck_h();
    p_mc323x_sda_h();
    delay_us(10);
}

// 设置时间和保持时间最小0.6秒
static void mc323x_delay(void)      // 400khz--1us
{
    delay_us(1);
}

static u8   mc323x_start(void)
{
    p_mc323x_sda_h();
    p_mc323x_sck_h();    
    mc323x_delay();        
    if( pin_mc323x_sck_l() || pin_mc323x_sda_l() )
        return c_ret_nk;   
             
    p_mc323x_sda_l();
    mc323x_delay();    
    if( pin_mc323x_sck_l() || pin_mc323x_sda_h() )
        return c_ret_nk;

    p_mc323x_sck_l();        
    mc323x_delay();        
    if( pin_mc323x_sck_h() || pin_mc323x_sda_h() )
        return c_ret_nk;
    
    return c_ret_ok;            
}

static u8   mc323x_restart(void)
{
    p_mc323x_sck_l();        
    mc323x_delay();        
    if( pin_mc323x_sck_h() )
        return c_ret_nk;

    p_mc323x_sda_h();
    p_mc323x_sck_h();    
    mc323x_delay();        
    if( pin_mc323x_sck_l() || pin_mc323x_sda_l() )
        return c_ret_nk;   
             
    p_mc323x_sda_l();
    mc323x_delay();    
    if( pin_mc323x_sck_l() || pin_mc323x_sda_h() )
        return c_ret_nk;

    p_mc323x_sck_l();        
    mc323x_delay();        
    if( pin_mc323x_sck_h() || pin_mc323x_sda_h() )
        return c_ret_nk;
    
    return c_ret_ok;            
}

static u8   mc323x_high(void)
{
    p_mc323x_sck_l();
    mc323x_delay();
    if( pin_mc323x_sck_h() )
        return c_ret_nk;
        
    p_mc323x_sda_h();
    mc323x_delay();
    if( pin_mc323x_sck_h() )
        return c_ret_nk;
        
    p_mc323x_sck_h();
    mc323x_delay();
    if( pin_mc323x_sck_l() )
        return c_ret_nk;
    
    return c_ret_ok;        
}

static u8   mc323x_low(void)
{
    p_mc323x_sck_l();
    mc323x_delay();
    if( pin_mc323x_sck_h() )
        return c_ret_nk;
        
    p_mc323x_sda_l();
    mc323x_delay();
    if( pin_mc323x_sck_h() || pin_mc323x_sda_h() )
        return c_ret_nk;
        
    p_mc323x_sck_h();
    mc323x_delay();
    if( pin_mc323x_sck_l() || pin_mc323x_sda_h() )
        return c_ret_nk;
    
    return c_ret_ok;        
}

static u8   mc323x_ack(u8* ack)
{
    p_mc323x_sck_l();    
    p_mc323x_sda_h();    
    mc323x_delay();        
    if( pin_mc323x_sck_h() )
        return c_ret_nk;   
    
    mc323x_delay();        

    p_mc323x_sck_h();    
    mc323x_delay();        
    if( pin_mc323x_sck_l() )
        return c_ret_nk;

    *ack = pin_mc323x_sda_h() ? 1 : 0;   
    
    return c_ret_ok;            
}     

static u8   mc323x_stop(void)
{
    p_mc323x_sck_l();    
    mc323x_delay();            
    if( pin_mc323x_sck_h() )
        return c_ret_nk;   

    p_mc323x_sda_l();
    mc323x_delay();
    if( pin_mc323x_sck_h() || pin_mc323x_sda_h() )
        return c_ret_nk;

    p_mc323x_sck_h();    
    mc323x_delay();            
    if( pin_mc323x_sck_l() || pin_mc323x_sda_h() )
        return c_ret_nk;   
             
    p_mc323x_sda_h();
    mc323x_delay();    
    if( pin_mc323x_sck_l() || pin_mc323x_sda_l())
        return c_ret_nk;

    return c_ret_ok;            
}

static u8   mc323x_dat(u8 *dat)
{
    u8 x, c;
    
    c = *dat;
    for(x = 0; x < 8; x++)
    {
        if(c & 0x80)
        {    
            if( mc323x_high() == c_ret_nk ) return c_ret_nk;
        }
        else if( mc323x_low() == c_ret_nk ) return c_ret_nk;
        c <<= 1;
        c |= pin_mc323x_sda_h() ? 1 : 0;
    }
    
    *dat = c;
    return c_ret_ok;
}

//  总线仲裁，等待SDA释放造成死锁---从机等待SCL变低释放sda，主机等待sda变高发送下一波形
//  时钟同步，从机拉低SCL
static u8   mc323x_err_proc(void)
{
    u32 tmr;
    u8 x;
    
    tmr = tick_get();
    while(1)
    {
        x = 0;
        p_mc323x_sck_l();
        p_mc323x_sda_h();
        mc323x_delay();
        if( pin_mc323x_sck_l() || pin_mc323x_sda_h() )
            x = 1;
            
        p_mc323x_sck_h();
        mc323x_delay();
        if( (x == 1) && pin_mc323x_sda_h() && pin_mc323x_sck_h() ) 
            break;            
        else if( tick_cmp(tmr, 100) == c_ret_ok ) 
            break;
    }    
    
    if( mc323x_stop() == c_ret_ok )
        return c_ret_ok;
    else
        return c_ret_nk;    
}

static u8 mc323x_write(u8 addr,u8* dat, u16 len)
{
    u8 ack, c;
    u16 x;
    
    while(1)
    {
        if( mc323x_start() == c_ret_nk ) break;

        c = (0x4c << 1) + 0;
        if( mc323x_dat( &c ) == c_ret_nk ) break;
        if( mc323x_ack( &ack ) == c_ret_nk ) break;
        if( ack != 0 ) break;
                
        c = addr;        
        if( mc323x_dat( &c ) == c_ret_nk ) break;
        if( mc323x_ack( &ack ) == c_ret_nk ) break;
        if( ack != 0 ) break;
    
        for( x = 0; x < len; x++ )
        {    
            c = dat[ x ];
            if( mc323x_dat( &c ) == c_ret_nk ) break;
            if( mc323x_ack( &ack ) == c_ret_nk ) break;
            if( ack != 0 ) break;
        }
        if(x < len) break;
        
        if( mc323x_stop() == c_ret_nk ) break;        
        return c_ret_ok;
    }   
    mc323x_err_proc();       
    return c_ret_nk;
}

static u8 mc323x_read(u8 addr, u8* dat, u16 len)
{
    u8 ack, c;
    u16 x;
    
    if(len == 0) return c_ret_ok;
    
    while(1)
    {
        if( mc323x_start() == c_ret_nk ) break;

        c = (0x4c << 1) + 0;    
        if( mc323x_dat( &c   ) == c_ret_nk ) break;
        if( mc323x_ack( &ack ) == c_ret_nk ) break;
        if( ack != 0 ) break;
        
        c = addr;
        if( mc323x_dat( &c ) == c_ret_nk ) break;
        if( mc323x_ack( &ack  ) == c_ret_nk ) break;
        if( ack != 0 ) break;

        if( mc323x_restart() == c_ret_nk ) break;
        c = (0x4c << 1) + 1;
        if( mc323x_dat( &c ) == c_ret_nk ) break;
        if( mc323x_ack( &ack  ) == c_ret_nk ) break;
        if( ack != 0 ) break;
        
        for(x = 0; x < len - 1; x++)
        {                    
            c = 0xff;
            if( mc323x_dat( &c ) == c_ret_nk ) break;
            if( mc323x_low() == c_ret_nk ) break;
            dat[ x ] = c;
        }
        if(x < len - 1) break;
        
        c = 0xff;
        if( mc323x_dat( &c ) == c_ret_nk ) break;
        if( mc323x_high() == c_ret_nk ) break;
        dat[ x ] = c;
        
        if( mc323x_stop() == c_ret_nk ) break;        
        return c_ret_ok;
    }
    
    mc323x_err_proc();           
    return c_ret_nk;
}

static u8 mc323x_set_reg(u8 reg, u8* dat, u8 len)
{    
    u8 x , y;
    
    for(y = 0; y < len; y++)
    {        
        if( mc323x_write(reg + y, &dat[ y ], 1) == c_ret_nk ) break;
        if( mc323x_read( reg + y, &x, 1) == c_ret_nk ) break;
        if(x != dat[ y ]) break;     
    }    
    if( y < len)
        return c_ret_nk;
    else 
        return c_ret_ok;
}

//  进入待机模式    [0x07] = 0x03
//  设置采样率      [0x08] = 0x00--128,01--64,10--32,11--16
//  进入唤醒模式    [0x07] = 0x01

u8 mc323x_init(void)
{
    u8 dat;
    
    p_mc323x_sck_h();
    p_mc323x_sda_h();
    mc323x_delay();
    if( pin_mc323x_sda_l() || pin_mc323x_sck_l() )
        mc323x_err_proc();
                
    while(1)
    {
        dat = 0x03;
        if( mc323x_set_reg(7, &dat, 1) == c_ret_nk ) break;
        dat = 0x00; // 00
        if( mc323x_set_reg(8, &dat, 1) == c_ret_nk ) break;
        dat = 0x01;
        if( mc323x_set_reg(7, &dat, 1) == c_ret_nk ) break;
        return c_ret_ok;
    }
    return c_ret_nk;
}

//  加速度寄存器
#define reg_xout        0x00    //r
#define reg_yout        0x01    //r
#define reg_zout        0x02    //r

u8 mc323x_read_data(s8* acc_x,s8* acc_y,s8* acc_z)
{
    u8 buf[3];
    
    if( mc323x_read( 0, buf, 3) == c_ret_nk) return c_ret_nk;
    *acc_x = (s8)buf[0];
    *acc_y = (s8)buf[1];
    *acc_z = (s8)buf[2];
    return c_ret_ok;
}

u8 MPU6050_vibrate = 0;

float avedev_xbuf[100] = {0};
float avedev_ybuf[100] = {0};
float avedev_zbuf[100] = {0};
float avedev_vbuf[100] = {0};
float avedev_vsum[  3] = {0}; 

u32   avedev_cnt = 0;
u32   avedev_vcnt= 0;

float avedev(float* avedev_buf, u32 len)
{
    float sum,sum1;
    u32 x;
    
    sum = 0;
    for(x = 0; x < len; x++)
        sum += avedev_buf[x];
    sum /= x;
    
    sum1 = 0;
    for(x = 0; x < len; x++)
        sum1 += (avedev_buf[x] >= sum) ? avedev_buf[x] - sum : sum - avedev_buf[x];
    sum1 /= x;
    
    return sum1;
}

u8 DMP_MPU6050_Init(void)
{
    mc323x_init();
    return c_ret_ok;
}

void DMP_MPU6050_SEND_DATA_FUN(void)	
{
    static u8 vibrate_cnt = 0;
    static u8 still_cnt = 0;
    float fx,fy,fz,fv;
    int ix,iy,iz,iv,ix0,iy0,iz0,iv0;
    s8 buf[3];
    s16 sbuf[3];
    
    if( mc323x_read_data(&buf[0], &buf[1], &buf[2]) == c_ret_nk) return;
    
    sbuf[0] = buf[0];
    sbuf[1] = buf[1];
    sbuf[2] = buf[2];
    if((dbg_tmr - ELD_data.mpu6050DataTmr) > 100)
    {
        ELD_data.mpu6050DataTmr = dbg_tmr;

        //ELD_data.mpu6050Data[ELD_data.mpu6050DataIdx].gyro_x = gyro[0];
        //ELD_data.mpu6050Data[ELD_data.mpu6050DataIdx].gyro_y = gyro[1];
        //ELD_data.mpu6050Data[ELD_data.mpu6050DataIdx].gyro_z = gyro[2];
        
        ELD_data.mpu6050Data[ELD_data.mpu6050DataIdx].accel_x = sbuf[0];
        ELD_data.mpu6050Data[ELD_data.mpu6050DataIdx].accel_y = sbuf[1];
        ELD_data.mpu6050Data[ELD_data.mpu6050DataIdx].accel_z = sbuf[2];
        
        ELD_data.mpu6050DataIdx++;
        if(ELD_data.mpu6050DataIdx >= sizeof(ELD_data.mpu6050Data)/sizeof(ELD_data.mpu6050Data[0]))
            ELD_data.mpu6050DataIdx = 0;
    }
    
        
    fz = sqrt( sbuf[0] * sbuf[0] + sbuf[1] * sbuf[1]) / sbuf[2];
    fz = atan(fz) * 1800 / 3.14;
    
    fx = sbuf[0] / sqrt( sbuf[1] * sbuf[1] + sbuf[2] * sbuf[2]);
    fx = atan(fx) * 1800 / 3.14;
    
    fy = sbuf[1] / sqrt( sbuf[0] * sbuf[0] + sbuf[2] * sbuf[2]);
    fy = atan(fy) * 1800 / 3.14;
    
    ix = (int)(fx * 100.0);
    iy = (int)(fy * 100.0);
    iz = (int)(fz * 100.0);
    ix0 = ix % 100;if(ix0 < 0) ix0 = -ix0;
    iy0 = iy % 100;if(iy0 < 0) iy0 = -iy0;
    iz0 = iz % 100;if(iz0 < 0) iz0 = -iz0;
    //dbg(GSENSOR_DBG_EN,"x:%d.%02d,y:%d.%02d,z:%d.%02d,   %dmv\r\n",ix/100,ix0,iy/100,iy0,iz/100,iz0,Power12v);
    
    avedev_xbuf[avedev_cnt] = fx;
    avedev_ybuf[avedev_cnt] = fy;
    avedev_zbuf[avedev_cnt] = fz;
    avedev_vbuf[avedev_cnt] = Power12v;
    avedev_cnt++;
    if(avedev_cnt >= sizeof(avedev_xbuf)/sizeof(avedev_xbuf[0]))
    {
        avedev_cnt = 0;
        
        fx = avedev(avedev_xbuf,sizeof(avedev_xbuf)/sizeof(avedev_xbuf[0]));
        fy = avedev(avedev_ybuf,sizeof(avedev_ybuf)/sizeof(avedev_ybuf[0]));
        fz = avedev(avedev_zbuf,sizeof(avedev_zbuf)/sizeof(avedev_zbuf[0]));
        
        fv = avedev(avedev_vbuf,sizeof(avedev_vbuf)/sizeof(avedev_vbuf[0]));
        avedev_vsum[0] = avedev_vsum[1];
        avedev_vsum[1] = avedev_vsum[2];
        avedev_vsum[2] = fv;            
        fv += avedev_vsum[0] + avedev_vsum[1];
        
        ix = (int)(fx * 100.0);
        iy = (int)(fy * 100.0);
        iz = (int)(fz * 100.0);
        iv = (int)(fv * 100.0);
        
        ix0 = ix % 100;if(ix0 < 0) ix0 = -ix0;
        iy0 = iy % 100;if(iy0 < 0) iy0 = -iy0;
        iz0 = iz % 100;if(iz0 < 0) iz0 = -iz0;
        iv0 = iv % 100;if(iv0 < 0) iv0 = -iv0;
        dbg(GSENSOR_DBG_EN,"x:%d.%02d,y:%d.%02d,z:%d.%02d,v:%d.%02d\r\n",ix/100,ix0,iy/100,iy0,iz/100,iz0,iv0/100,iv0);
        
        //if((fx > 2)||(fy > 2)||(fz > 2))  
        if((fx > 3)||(fy > 3)||(fz > 3))  
        {
            vibrate_cnt++;
            still_cnt = 0;
        }
        //else if((fx < 1.8)&&(fy < 1.8)&&(fz < 1.8))
        else if((fx < 2.8)&&(fy < 2.8)&&(fz < 2.8))        
        {
            still_cnt++;
            vibrate_cnt = 0;
        }

        
        if(vibrate_cnt >= 5)
        {
            vibrate_cnt = 0;
            still_cnt = 0;
            if(MPU6050_vibrate == 0)
                dbg(GSENSOR_DBG_EN,"\r\n**** MPU6050_vibrate = 1 ****\r\n");
            MPU6050_vibrate = 1;
            obd_vibration = 1;                
        }
        if(still_cnt >= 5)
        {
            vibrate_cnt = 0;
            still_cnt = 0;
            if(MPU6050_vibrate == 1)
                dbg(GSENSOR_DBG_EN,"\r\n**** MPU6050_vibrate = 0 ****\r\n");
            MPU6050_vibrate = 0;
            obd_vibration = 0;
        }
    }
    
}
#endif
