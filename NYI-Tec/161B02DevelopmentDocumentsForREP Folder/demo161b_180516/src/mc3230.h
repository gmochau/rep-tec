/*
    mc3230.h
*/
    
#ifndef __mc3230_h__
#define __mc3230_h__

#include "STM32_config.h"

#ifdef __CFG_GSENSOR_MC32XX__

extern u8 MPU6050_vibrate;

extern void I2C_Configuration(void);

extern u8 DMP_MPU6050_Init(void);	

extern void DMP_MPU6050_SEND_DATA_FUN(void);

#endif

#endif

