/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"

//#include "delay.h"
//#include "led.h"
//#include "sys.h"
//#include "lcd_tft.h"

/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
    #if 0
	while (1)
	{
        //printf("\r\n**** HardFault_Handler ****\r\n");
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'H';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'d';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'F';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'u';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'l';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'t';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			

        SCB->AIRCR = 0x05fa0000 | 0x00000004;
        while(1) ;	
/*		
		delay_ms(200);
		LED1 = !LED1;	//Ӳ������
*/
	}
    #else
    u8 x = 0;
    
    while(1)
    {
        if(x != 0)
            break;
    }        
    #endif

}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{

    while(1)
    {
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'M';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'e';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'m';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'M';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'g';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'e';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			

        SCB->AIRCR = 0x05fa0000 | 0x00000004;
        while(1) ;	
    }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{

	while (1)
	{
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'B';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'u';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'s';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'F';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'l';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'t';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			

        SCB->AIRCR = 0x05fa0000 | 0x00000004;
        while(1) ;	
	}
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{

	while (1)
	{
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'U';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'s';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'g';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'e';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'F';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'u';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'l';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'t';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			

        SCB->AIRCR = 0x05fa0000 | 0x00000004;
        while(1) ;	
	}
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{

	while (1)
	{
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'S';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'V';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'C';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'_';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'H';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'a';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'d';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'l';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'e';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			

        SCB->AIRCR = 0x05fa0000 | 0x00000004;
        while(1) ;	
	}

}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{

	while (1)
	{
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'D';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'e';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'b';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'u';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'g';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'M';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'o';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'n';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\r';			
        while((USART1->SR & 0x40) == 0); USART1->DR = (uint8_t)'\n';			

        SCB->AIRCR = 0x05fa0000 | 0x00000004;
        while(1) ;	
	}
	
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
//void PendSV_Handler(void)
//{
//}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
//void SysTick_Handler(void)
//{
//}

/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
