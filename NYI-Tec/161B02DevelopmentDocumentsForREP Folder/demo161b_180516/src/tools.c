
#include "includes.h"
#include "stm32_config.h"			//

/*
 ******************************************************************************
 * 功能描述：转换1个数字字符为数字是否为'0~9, a~f, A~F',并判断该字符是否为数字
 *           字符
 * 输入参数：isrc数据
 * 输出参数：无
 * 返回参数：转换后的数据
 *           -1: 出错
 * 外部调用：无
 * 注意事项：无
 ******************************************************************************
 */
s16 AsciiToHex( u8 isrc)
{
    s8 itemp = 0;

    itemp = isrc;
	if (('0' <= itemp)&&('9'>=itemp))
    {
        itemp -= '0';
    }
    else if (('a' <= itemp) && ('f' >= itemp))
    {
        itemp -= 'a';
        itemp += 0x0a;
    }
    else if (('A' <= itemp) && ('F' >= itemp))
    {
        itemp -= 'A';
        itemp += 0x0a;
    }
    else
    {
        itemp = (s8)-1;
    }
    return itemp;
}


/*
 ******************************************************************************
 * 功能描述：转换1个数字字符为数字是否为'0~9, a~f, A~F',并判断该字符是否为数字
 *           字符
 * 输入参数：isrc数据
 * 输出参数：无
 * 返回参数：转换后的数据
 *           -1: 出错
 * 外部调用：无
 * 注意事项：无
 ******************************************************************************
 */
s8 HexToAscii(u8 hex)
{
    const s8 Byte2Hex[] = "0123456789ABCDEF";

	return Byte2Hex[hex];
}


/*
 ******************************************************************************
 * 功能描述：将一个字节的数据转换成两个字符表示，如0x6a转换成"6a"存放在16位
             数据中
 * 输入参数：isrc数据
 * 输出参数：无
 * 返回参数：转换后的数据
 * 外部调用：无
 * 注意事项：无
 ******************************************************************************
 */
u16 ByteToAscii( const u8 isrc )
{
	u16 iResult = 0x3030;
	u8 ASCIITAB[] = "0123456789ABCDEF";
	
	
	iResult = (u16)ASCIITAB[ isrc >> 4  ] << 8;
	iResult |= ASCIITAB[ isrc & 0x0f];

	return iResult;
}

/*
 ******************************************************************************
 * 功能描述：将一个字节的数据转换成两个字符表示，如0x6a转换成"6a".
 * 输入参数：bByte
 * 输出参数：pStr
 * 返回参数：0: 成功  -1: 失败
 ******************************************************************************
 */
s16 ByteToStr(u8 * pStr, u8 bByte)
{
    const s8 Byte2Hex[] = "0123456789ABCDEF";
	
	if (NULL == pStr)
	{
		return (s16)-1;
	}
	
	*(pStr++) = Byte2Hex[ bByte >> 4  ];
	*(pStr++) = Byte2Hex[ bByte & 0xF ];
	
	return 0;
}


/*
 ******************************************************************************
 * 功能描述：ASCII 到 HEX的转换函数
 * 输入参数：O_data: 转换数据的入口指针，
 *			N_data: 转换后新数据的入口指针
 *			len : 需要转换的长度
 * 输出参数：无
 * 返回参数：-1: 转换失败
 *			其它：转换后数据长度
 * 注意：O_data[]数组中的数据在转换过程中会被修改。
 ******************************************************************************
 */
s16 Ascii_2_Hex(s8  *O_data, s8  *N_data, s16 len)
{
	int i,j,tmp_len;
	s8  tmpData;
	s8  *O_buf = O_data;
	s8  *N_buf = N_data;
	
	for(i = 0; i < len; i++)
	{
		if ((O_buf[i] >= '0') && (O_buf[i] <= '9'))
		{
			tmpData = O_buf[i] - '0';
		}
		else if ((O_buf[i] >= 'A') && (O_buf[i] <= 'F')) //A....F
		{
			tmpData = O_buf[i] - 0x37;
		}
		else if((O_buf[i] >= 'a') && (O_buf[i] <= 'f')) //a....f
		{
			tmpData = O_buf[i] - 0x57;
		}
		else
		{
			return -1;
		}
		O_buf[i] = tmpData;
	}
	
	for(tmp_len = 0,j = 0; j < i; j+=2)
	{
		N_buf[tmp_len++] = (O_buf[j]<<4) | O_buf[j+1];
	}
	
	return tmp_len;
}

/*
 ******************************************************************************
 * 功能描述：HEX 到 ASCII的转换函数
 * 输入参数：data: 转换数据的入口指针
 *		   buffer: 转换后数据入口指针
 *			 len : 需要转换的长度
 * 返回参数：转换后数据长度
 ******************************************************************************
 */
s16 Hex_2_Ascii(s8  *data, u8  *buffer, s16 len)
{
	const s8 ascTable[17] = {"0123456789ABCDEF"};
	u8  *tmp_p = buffer;
	int i, pos;
	
	pos = 0;
	for(i = 0; i < len; i++)
	{
		tmp_p[pos++] = ascTable[(data[i] >> 4)&0x0f];
		tmp_p[pos++] = ascTable[data[i] & 0x0f];
	}
	tmp_p[pos] = '\0';
	
	return pos;
}
/*
 ******************************************************************************
 * 功能描述：将2个字符串合并成一个字节的数据，并将结果存放在目的地址中
 * 输入参数：psrc 字符串指针，如果输入的字符串非16进制的数据话就返回-1，否则为0
 * 输出参数：转换后的数据
 * 返回参数：-1: 错误
              0: 正确
 * 外部调用：无
 * 注意事项：无
 ******************************************************************************
 */
s16 AsciiToByte(const u8 * psrc, u8 * pdst )
{
    s8 itemph = 0;
    s8 itempl = 0;
 
    itemph = AsciiToHex(*psrc);

    if ((s8)-1 == itemph )
    {
        return -1;
    }
    itempl = AsciiToHex(*(psrc+1));
    if ((s8)-1 == itempl )
    {
        return -1;
    }

    itemph <<=4;
    itemph |= itempl;

    *pdst = itemph;
	return 0;
}


/**************************************************************************************************
** 函数名: EndDataProcess
** 输　入: float ProcVal
** 输　出: u32  
** 功能描述: 在分位上做四舍五入,将float类型转换成u32类型，分位若大于0.5则加1
**************************************************************************************************/

u32  EndDataProcess(float ProcVal)
{  
  unsigned long  TempData;
  
  TempData = ProcVal;
  if((ProcVal-(float)TempData) >=0.5)
     return ((u32)ProcVal+1);
  else
     return (u32)ProcVal;
}


/*
 ******************************************************************************
 * 功能描述：计算校验和
 * 输入参数：psrc 字符串指针
 *			  len 需要计算的长度
 * 输出参数：无
 * 返回参数：-1: 错误
              0: 正确
 * 外部调用：无
 * 注意事项：无
 ******************************************************************************
 */
u8 GetXorSum(u8 *psrc, s16 len)
{
    s16 i = 0;
    u8 checksum = 0;
    
    for (i=0;i<len;i++)
    {
		STM_RESET_DOG();
		checksum = checksum ^ psrc[i];
    }

    return checksum;
}
/*
 ******************************************************************************
 * 功能描述：将数字字符串转化为BCD码
 * 输入参数；src,dstlen,keyc
 * 输出参数；dst
 * 返回参数：无
 * 注意事项: 必须是0~9的数字字符，字符串长度小于255
 ******************************************************************************
 */
void StringToBcd(s8 * dst,s8 * src,u8 dstlen, u8 keyc)
{
    u8 i;
    
    i = 0;
    while (dstlen != 0)
    {
        if (((*src) >= '0') && (*src <= '9'))
        {
            dst[i] = *src -0x30;
            src ++;
        }
        else if (((*src) >= 'a') && (*src <= 'f'))
        {
            dst[i] = *src - 'a' + 0x0a;
            src ++;
        }
        else if (((*src) >= 'A') && (*src <= 'E'))
        {
            dst[i] = *src - 'A' + 0x0a;
            src ++;
        }
        else
        {
            dst[i] = keyc;
        }
        
        dst[i] <<= 4;
        
	if (((*src) >= '0') && (*src <= '9'))
        {
            dst[i] |= *src -0x30;
            src ++;
        }
        else if (((*src) >= 'a') && (*src <= 'f'))
        {
            dst[i] |= *src - 'a' + 0x0a;
            src ++;
        }
        else if (((*src) >= 'A') && (*src <= 'E'))
        {
            dst[i] |= *src - 'A' + 0x0a;
            src ++;
        }
        else
        {
            dst[i] |= keyc;
        }
        
        i++;
        dstlen --;
    }
}

/*
 ******************************************************************************
 * 功能描述：将Fbuf中'F'的值填零
 * 输入参数；len
 * 输出参数；Fbuf
 * 返回参数：无
 ******************************************************************************
 */
u8 DelAsciiF(u8 *Fbuf, u8 len)
{
	u8	i;
	
	for(i=len; i>0; i--)
	{
		if(Fbuf[i-1] == 'F')
		{
			Fbuf[i-1]=0;
		}
	}

	return	(len-i+1);
}

/*
 ******************************************************************************
 * 功能描述：将Fbuf中为零的值填'F'
 * 输入参数；len
 * 输出参数；Fbuf
 * 返回参数：无
 ******************************************************************************
 */
u8 AddAsciiF(u8 *Fbuf, u8 len)
{
	u8	i,j;
	
	for(i=0; i<len; i++)
	{
		if(Fbuf[i] == 0)
			break;
	}
	for(j=i; j<len; j++)
	{
		Fbuf[j] = 'F';
	}

	return	i;
}

/*
 ******************************************************************************
 * 功能描述：数组1与数组2逐一比较，当两个数组里的所有元素完全相同时，就认为相等，
 *           比较时遇‘\0'也不结束
 * 输入参数：pdat-数组1的指针    pLen-数组1和数组2的实际长度
 *           sbuf-数组2的指针    
 * 输出参数：无
 * 返回参数：1:相等
 *           0:不相等
 * 外部调用：无
 * 注意事项：两个数组长度需相等
 ******************************************************************************
 */
u8 strTest(u8 *pdat,u16 pLen,u8 *sbuf)
{
	u16 i=0,j=0;
	u8 ret = 0;

	if(pLen == 0)
	{
		return 0;
	}

	for(i=0;i<pLen;i++)
	{
		if(pdat[i] == sbuf[i])
		{
			j++;
		}
		else
		{
			break;
		}
	}
	if(j==pLen)
	{
		ret = 1;
	}
	return ret;
}

