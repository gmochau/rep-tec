#ifndef _COMMON_TOOLS_H
#define _COMMON_TOOLS_H

#define FCS_START	0xffff	/* Starting bit string for FCS calculation */
#define FCS_FINAL	0xf0b8	/* FCS when summed over frame and sender FCS */

u32  EndDataProcess(float ProcVal);
void Delay(u32 ms);
void TraceHexStrN(u8 * pStr, u8 ulen);
s16 AsciiToByte(const u8 * psrc, u8 * pdst );
u16 ByteToAscii( const u8 isrc );
s16 ByteToStr(u8 * pStr, u8 bByte);
s16 AsciiToHex( u8 isrc);
s8 HexToAscii(u8 hex);
s16 Ascii_2_Hex(s8  *O_data, s8  *N_data, s16 len);
s16 Hex_2_Ascii(s8  *data, u8  *buffer, s16 len);
u8 GetXorSum(u8 *psrc, s16 len);
void StringToBcd(s8 * dst,s8 * src,u8 dstlen, u8 keyc);
u8 DelAsciiF(u8 *Fbuf, u8 len);
u8 AddAsciiF(u8 *Fbuf, u8 len);
s8* cm_strstr(s8 *s1, s8 *s2);
s32 MyPrintf(s8 * format, ...);
u8 strTest(u8 *pdat,u16 pLen,u8 *sbuf);
#endif 

