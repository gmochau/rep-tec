/*
    trace.h
*/

#ifndef __dd_proc__
#define __dd_proc__

//#define __TRACE__

#ifdef __TRACE__
extern void trace_proc(const char* fun);
#define TRACE_BEGIN                trace_proc(__FUNCTION__);{
#define TRACE_END                   }    

#else
#define TRACE_BEGIN                    
#define TRACE_END                   
#endif

#endif

