/*
	update.c
*/

#include "includes.h"  				
#include "stm32_config.h"

#define c_xmodem_soh    0x01        //数据头
#define c_xmodem_stx    0x02        //1k_xmodem协议数据头
#define c_xmodem_eot    0x04        //发送结束
#define c_xmodem_ack    0x06        //认可响应
#define c_xmodem_nak    0x15        //不认可响应
#define c_xmodem_can    0x18        //撤销传送
#define c_xmodem_eof    0x1a        //最后一包数据填充

enum
{
	c_xmodemRet_null,				// 无应答
	c_xmodemRet_done,				// 传输完成 
	c_xmodemRet_next,				// 传输下一包
	c_xmodemRet_last,				// 收到上一包	
	c_xmodemRet_tmo,				// 超时
    c_xmodemRet_handshake,          // 握手超时    
	c_xmodemRet_err					// 包解析错误
};	

u8 firstPacket;
u8 checksumRcc;
u16 receHead;
u16 receTail;
u8  receBuf[132 + 1];

void update_st32(u32 x,u8 *buf)
{
	un32 ux;
	
	ux.r = x;
	buf[0] = ux.b.l;
	buf[1] = ux.b.m;
	buf[2] = ux.b.h;
	buf[3] = ux.b.u;
}

u32 update_ld32(u8 *buf)
{
	un32 ux = {0};
	
	ux.b.l = buf[0];
	ux.b.m = buf[1];
	ux.b.h = buf[2];
	ux.b.u = buf[3];
	
	return ux.r;
}

void update_getFileLenSumFlag(u32 *fileLen,u32 *fileSum)
{
    un32 ux = {0};
    
    spiFlash_readAddrSet(FLASH_TMP_ADDR);
    spiFlash_read(NULL,0,c_spiFlashReadType_readCheck);

    if(fileLen != NULL)
    {
        ux.b.l = w25n.readBuf[ 3];
        ux.b.m = w25n.readBuf[ 2];
        ux.b.h = w25n.readBuf[ 1];
        ux.b.u = w25n.readBuf[ 0];
        *fileLen = ux.r + 8;
    }

    if(fileSum != NULL)
    {
        ux.b.l = w25n.readBuf[ 7];
        ux.b.m = w25n.readBuf[ 6];
        ux.b.h = w25n.readBuf[ 5];
        ux.b.u = w25n.readBuf[ 4];
        *fileSum = ux.r;
    }
    
    spiFlash_read(NULL,0,c_spiFlashReadType_readCheck); // 版本
}

u32 update_getCheckSum(u32 startAddr,u32 fileLen)
{
	u32 readLen, len, sum, x;
	
    spiFlash_readAddrSet(startAddr);
	for(sum = len = 0; len < fileLen; )
	{		
        spiFlash_read(NULL,0,c_spiFlashReadType_readCheck);
		readLen = ((fileLen - len) >= sizeof(w25n.readBuf))  ? sizeof(w25n.readBuf) : (fileLen - len);		
        if(len == 0)
        {            
            for(x = 8; x < readLen; x ++)
                sum += w25n.readBuf[x];		
        }
        else
        {
            for(x = 0; x < readLen; x ++)
                sum += w25n.readBuf[x];		
        }
		len      += readLen;		
	}
	return sum;
}

void readFlash(u8 *src ,u32 addr, u16 len)
{ 
	u16 x;
	u32 dat;
	
	for(x = 0;x < len; x += 4)
	{
		dat = *(u32 *)addr;
		update_st32(dat, &src[x]);
		addr += 4;
	}	
}

void writeFlash(u8 *src ,u32 addr,  u16 len)
{
	u16 x;
	
	FLASH_Unlock();
		
	for(x = 0; x < len; x += 4)
	{
        if(addr % IAP_PAGE_SIZE == 0)  
            FLASH_ErasePage(addr);
        
		FLASH_ProgramWord(addr, update_ld32(&src[x]));
		addr += 4;
	}		
	
	FLASH_Lock();
}

// 各模块自己负责互斥发送
void update_sendCmd(u8 cmd)	
{
	receTail = 0;
	switch(ftpSendPara.src)
	{
		case ELD_PACKET_bt:
			receHead = btBuf.iHead;				
            bt_sendXmodemCmd(cmd);
		break;
		
		case ELD_PACKET_uart:
			receHead = USART1_iHead;	
			while((USART1->SR & 0x40) == 0);		
				USART1->DR = cmd;			    		
		break;		
	}
}

u8  update_receNewData(u16 *len)
{
	u8 ret = c_ret_nk;
    static enum
    {
        c_step_head,
        c_step_cmdh,
        c_step_cmdl,
        c_step_resp,
        c_step_lenh,
        c_step_lenl,
        c_step_addr,
        c_step_handleWriteType,
        c_step_dat,
        c_step_chksum,
    }step = c_step_head;
    static u8 buf[4] = {0};
    static u16 cmd = 0xffff;
    static u16 plen = 0xffff;
    static u16 idx = 0;
    static u8 sum = 0;
    u8 dat;
	
	switch(ftpSendPara.src)
	{
		case ELD_PACKET_bt:		//uart5
			while(receHead != btBuf.iHead)
			{	
                ret = c_ret_ok;
                
                dat = btBuf.UartbtRxBuf[receHead];
                receHead++;
                if(receHead >= sizeof(btBuf.UartbtRxBuf)) receHead = 0; 
                
                buf[0] = buf[1];
                buf[1] = buf[2];
                buf[2] = buf[3];
                buf[3] = dat;
                        
                switch(step)
                {
                    case c_step_head:
                        if((buf[0] == 'i')&&(buf[1] == 't')&&(buf[2] == 'a')&&(buf[3] == 'z')) 
                            step = c_step_cmdh;
                    break;

                    case c_step_cmdh:cmd = dat << 8;step = c_step_cmdl;break;

                    case c_step_cmdl:cmd += dat;step = c_step_resp;break;
                    
                    case c_step_resp:
                        if(dat == 1)
                        {
                            step = c_step_lenh;
                        }
                        else
                        {
                            buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                            step = c_step_head;
                        }
                    break;

                    case c_step_lenh:plen = dat << 8;step = c_step_lenl;break;

                    case c_step_lenl:
                        plen += dat;
                        if(     (plen > (sizeof(receBuf) + 9))
                            ||  (plen <= 9) ) 
                        {
                            buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                            step = c_step_head;
                        }  
                        else 
                        {
                            sum = idx = 0;
                            step = c_step_addr;
                        }
                    break;

                    case c_step_addr:
                        sum += dat;
                        if(ftpSendPara.btMac[idx] != dat)
                        {
                            buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                            step = c_step_head;                            
                        }
                        idx++;
                        if(idx >= 6)
                            step = c_step_handleWriteType;
                    break;
                        
                    case c_step_handleWriteType:        
                        sum += dat;
                        idx++;
                        if(idx >= 9)
                            step = c_step_dat;
                    break;
                        
                    case c_step_dat:
                        sum += dat;
                        if(receTail >= sizeof(receBuf)) break;
                        receBuf[receTail++] = dat;
                        idx++;
                        if(idx == plen)
                            step = c_step_chksum;
                    break;

                    case c_step_chksum:
                        sum = 0xff - sum + 1;
                        if(sum != dat) 
                        {
                            receTail -= plen;
                            buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                            step = c_step_head;
                        }                    
                        else 
                        {
                            buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                            step = c_step_head;
                        }
                    break;
                        
                    default:
                        buf[0] = buf[1] = buf[2] = buf[3] = 0xff;
                        step = c_step_head;
                    break;            
                }        
            }                                
		break;
						
	}			

	*len = receTail;
	return ret;			
}

//	soh sn ~sn d0...d127 sum
u8  update_xmodemComm(u8 sn,u8 cmd)
{
    u32 tmr, tmr0;
	u16 len, sum, x, i;

    //static u32 tmr1 = 0;
    //printf("%d, %d,  %d\r\n",sn,cmd,dbg_tmr - tmr1);
    //tmr1 = dbg_tmr;
    
	update_sendCmd(cmd);

    x = 0;
    tmr0 = tmr = tick_get();
    while(1)
    {
        OSTimeDly(10 / c_systick);
        if(update_receNewData(&len) == c_ret_ok) 
        {
            tmr0 = tick_get();
            x = 0;
        }
        if(firstPacket == 1)
        {
            if((len != 0) && (x >= 110)) break;
        }
        else if(		((len != 0) && (x >= 110))			    // 1字节完成包
                        ||	((checksumRcc == 1) && (len >= 132))
                        ||  ((checksumRcc == 0) && (len >= 133)) )
            break;
        
        if(tick_cmp(tmr,10000) == c_ret_ok)                // 包超时间隔为10秒
        {
            if(receBuf[0] == c_xmodem_eot)     			               
                return c_xmodemRet_done;
            if(firstPacket == 1)
                return c_xmodemRet_handshake;
            else
                return c_xmodemRet_tmo;
        }
        if(tick_cmp(tmr0,1100) == c_ret_ok)
            x = 110;
    }
	
	if(receBuf[0] == c_xmodem_eot)     			                // 全部传输完成
		return c_xmodemRet_done;

	if(len >= 132)
	{	
		if(		(receBuf[0] == c_xmodem_soh) 				    // 信息包头
			&&	(receBuf[1] == (0xff - receBuf[2])))		    // 信息包序号
		{
            if(firstPacket == 1)
            {                
                if(len >= 133)
                {
                    for(sum = 0, x = 3; x < 131; x++) 
                    {
                        sum = sum ^ (receBuf[x] << 8);
                        for( i = 0; i < 8; i++)
                        {
                            if(sum & 0x8000) sum = (sum << 1) ^ 0x1021;
                            else sum <<= 1;
                        }                        
                    }
                    if(sum == (receBuf[131] << 8) + receBuf[132])
                    {
                        firstPacket = 0;
                        checksumRcc = 0;
                        if(receBuf[1] == sn)
                            return c_xmodemRet_next;				    // 包接收正常    
                        else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                            return c_xmodemRet_last;			
                        else
                            return c_xmodemRet_err;					    // 包序号错误退出				
                    }
                }

                for(sum = 0, x = 3; x < 131; x++) 
                    sum += receBuf[x];
                if((sum&0xff) == receBuf[x])            
                {	
                    firstPacket = 0;
                    checksumRcc = 1;
                    if(receBuf[1] == sn)
                        return c_xmodemRet_next;				    // 包接收正常    
                    else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                        return c_xmodemRet_last;			
                    else
                        return c_xmodemRet_err;					    // 包序号错误退出				
                }                
            }
            else if(checksumRcc == 1)
            {    
                for(sum = 0, x = 3; x < 131; x++) 
                    sum += receBuf[x];
                if((sum&0xff) == receBuf[x])            
                {	
                    if(receBuf[1] == sn)
                        return c_xmodemRet_next;				    // 包接收正常    
                    else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                        return c_xmodemRet_last;			
                    else
                        return c_xmodemRet_err;					    // 包序号错误退出				
                }
            }
            else
            {
                for(sum = 0, x = 3; x < 131; x++) 
                {
                    sum = sum ^ (receBuf[x] << 8);
                    for( i = 0; i < 8; i++)
                    {
                        if(sum & 0x8000) sum = (sum << 1) ^ 0x1021;
                        else sum <<= 1;
                    }                        
                }
                if(sum == (receBuf[131] << 8) + receBuf[132])
                {
                    if(receBuf[1] == sn)
                        return c_xmodemRet_next;				    // 包接收正常    
                    else if(receBuf[1] + 1 == sn)				    // 重复发送的上一包 
                        return c_xmodemRet_last;			
                    else
                        return c_xmodemRet_err;					    // 包序号错误退出				
                }
            }
		}
	}	
	return c_xmodemRet_last;

}

u8 update_ftpXmodem(void)
{
    u32 sum, fileSum, len, fileLen;
  	u8 ret, sn, errCnt, x, y, lastCnt;
    
    firstPacket = 1;
    spiFlash_writeAddrSet(FLASH_TMP_ADDR);	
	len = errCnt = lastCnt = 0;
    sn = 1;														// 第一个信息包的序号为 1
    //ret = update_xmodemComm(sn, c_xmodem_nak);     		        // 启动传输
    ret = update_xmodemComm(sn, 'C');     		        // 启动传输
    while(1)
    {               
		switch(ret)
		{		
			case c_xmodemRet_next:
                spiFlash_write(&receBuf[3],128,c_spiFlashWriteType_writeCheckBuffer);
				if(w25n.writeAddr >= FLASH_TMP_ADDR + FLASH_BLOCK_SIZE * 2)
				{
					update_sendCmd(c_xmodem_can);
					return c_ret_nk;
				}					

				errCnt = lastCnt = 0;
				sn++;    
				len++;
				ret = update_xmodemComm(sn,c_xmodem_ack);       // 请求下一包    
			break;
			
			case c_xmodemRet_last:
                OSTimeDly(50 / c_systick);
				errCnt = 0;
                lastCnt++;
                if(lastCnt < 5)
                    ret = update_xmodemComm(sn,c_xmodem_nak);   // 请求重传
                else    
				{
					update_sendCmd(c_xmodem_can);
					return c_ret_nk;
				}					
			break;

        case c_xmodemRet_err:
        case c_xmodemRet_tmo:
        case c_xmodemRet_handshake:
            errCnt++;

            if(errCnt >= 10)
            {
                update_sendCmd(c_xmodem_can);
                return c_ret_nk_tmo;
            }
            if(ret == c_xmodemRet_handshake)
            {
                //ret = update_xmodemComm(sn, c_xmodem_nak);
                ret = update_xmodemComm(sn, 'C');
            }
            else
                ret = update_xmodemComm(sn, c_xmodem_nak);      // 请求重传
            break;
				
			case c_xmodemRet_done:   
				update_sendCmd(c_xmodem_ack);				
                for(y = x = 0; x < 128; x++)
				{
					if(receBuf[130 - x] != c_xmodem_eof) break;
					y++;
				}
                len = len * 128 - y;
                spiFlash_write(NULL, 0, c_spiFlashWriteType_writeCheck);
                
                update_getFileLenSumFlag(&fileLen, &fileSum);
                if(fileLen != len) 
                {
                    dbg(GSM_DBG_EN,"****len error ****\r\n");
                    return c_ret_nk;
                }
                for(x = 0; x < sizeof(version); x++)	
                { 
                    if(version[x] != w25n.readBuf[8 + x]) 
                    {
                        dbg(GSM_DBG_EN,"****version error ****\r\n");
                        return c_ret_nk;	
                    }
                    if(version[x] == '.') break;
                }				
                sum = update_getCheckSum(FLASH_TMP_ADDR, fileLen);
                if(fileSum != sum)
                {                    
                    dbg(GSM_DBG_EN,"****TMP sum = %x ****\r\n",sum);
                    return c_ret_nk;
                }               
                                
                spiFlash_readAddrSet(FLASH_TMP_ADDR);
                spiFlash_writeAddrSet(FLASH_APP_ADDR); 
                for(len = 0; len < fileLen; )
                {
                    spiFlash_read(NULL,0,c_spiFlashReadType_readCheck);
                    spiFlash_write(w25n.readBuf, sizeof(w25n.readBuf), c_spiFlashWriteType_writeCheckBuffer);
                    len       += sizeof(w25n.readBuf);				
                }

                sum = update_getCheckSum(FLASH_APP_ADDR, fileLen);
                if(fileSum != sum)
                {                    
                    dbg(GSM_DBG_EN,"****APP sum = %x ****\r\n",sum);
                    return c_ret_nk;
                }               

                readFlash(w25n.readBuf, IAP_DATA_ADDR, sizeof(w25n.readBuf));
                update_st32(0x88888888,&w25n.readBuf[0]);
                writeFlash(w25n.readBuf,IAP_DATA_ADDR, sizeof(w25n.readBuf));    // 存入升级标志
                
			return c_ret_ok;
			
			default:return c_ret_nk;
		}	
    }     
}


void update_ftpProc(void)
{
    bt_wakeup_dly();
    delay_us(500);
    
    //FlashBusyWait();
    //OSSchedLock();
    //FlashBusyClr();
    
    ftpSendPara.result = update_ftpXmodem();

    //OSSchedUnlock();    
}
