/*
	update.h
*/

#ifndef __update_h__
#define __update_h__

#include "stm32_config.h"

#define IAP_PAGE_SIZE		2048								// GD32F105XX
#define IAP_BOOT0_ADDR		0x08000000
#define IAP_DATA_ADDR		0x08002000
#define IAP_APP_ADDR		0x08002800
#define IAP_VER_ADDR		0x08003000
#define IAP_APP_END		    0x08040000

extern void update_ftpProc(void);


#endif
