/******************************************************************************
* @ File name --> 
* @ Author    -->
* @ Version   -->
* @ Date      -->
* @ Brief     -->
******************************************************************************/

#include "STM32_config.h"			//

uint8_t USART1_RX_BUF[USART1_REC_LEN];	//
uint16_t USART1_iTail = 0;
uint16_t USART1_iHead = 0;

uint8_t USART2_RX_BUF[USART2_REC_LEN];	//
uint16_t USART2_iTail = 0;
uint16_t USART2_iHead = 0;

uint8_t USART3_RX_BUF[USART3_REC_LEN];	//
uint16_t USART3_iTail = 0;
uint16_t USART3_iHead = 0;

/******************************************************************************
           加入以下代码，支持printf函数，而不需要选择use MicroLIB
******************************************************************************/

#if 1

#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 

}; 
/* FILE is typedef’ d in stdio.h. */ 
FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
void _sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{      
    while((USART1->SR & 0x40) == 0);		//循环发送，直到发送完毕   
        USART1->DR = (uint8_t)ch;				//发送数据      
	return ch;
}
#endif

/******************************************************************************
* Function Name --> 初始化IO 串口1
* Description   --> none
* Input         --> bound：波特率	
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void USART1_Init(uint32_t bound)    // MCU_RX/MCU_TX
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	MY_NVIC_Init(3, 3, USART1_IRQn, NVIC_PriorityGroup_0);	//中断分组0，最高优先级
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);	//使能USART1，GPIOA时钟

	USART_DeInit(USART1);  //复位串口1
	
	//USART1_TX   PA.9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure); //初始化PA9
   
	//USART1_RX	  PA.10
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);  //初始化PA10
  
	//USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;	//设置波特率，一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;	//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;	//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;	//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	
	USART_Init(USART1, &USART_InitStructure); //初始化串口
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启中断
	USART_Cmd(USART1, ENABLE);                    //使能串口 

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}


/******************************************************************************
* Function Name --> 串口1接收中断服务程序
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
u32 uartTimeDelay;

void uartTimeDelayInc(void)
{
    if(USART1_iHead != USART1_iTail)
        uartTimeDelay++;
}

void USART1_IRQHandler(void)
{
    uartTimeDelay = 0;
    
	/* 判断ORE位是否为SET状态 */
	//if(USART_GetFlagStatus(USART1, USART_FLAG_ORE) != RESET)
	//{
		/* 进行空读操作，目的是清除ORE位 */
	//	USART_ReceiveData(USART1);	
	//}
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		/* 将接收到的数据写入缓冲 */
		USART1_RX_BUF[USART1_iHead]  = USART_ReceiveData(USART1);
		
		if((USART1_iHead += 1) >= USART1_REC_LEN) 
		{
			USART1_iHead -= USART1_REC_LEN;
		}
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	}	

}


/******************************************************************************
* Function Name --> 初始化IO 串口3
* Description   --> none
* Input         --> bound：波特率	
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void USART2_Init(uint32_t bound)    // 485_TX/485_RX
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	MY_NVIC_Init(3, 3, USART2_IRQn, NVIC_PriorityGroup_0);	//?/中断分组0，最高优先级

	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);

    	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);


	//USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;	//设置波特率，一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;	//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;	//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;	//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	
	USART_Init(USART2, &USART_InitStructure); //初始化串口
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);//开启中断
	USART_Cmd(USART2, ENABLE);                    //使能串口 

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

/******************************************************************************
* Function Name --> 串口2接收中断服务程序
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void USART2_IRQHandler(void)
{
}

/******************************************************************************
* Function Name --> 初始化IO 串口3
* Description   --> none
* Input         --> bound：波特率	
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void USART3_Init(uint32_t bound)    // K_RXD/K_TXD
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	MY_NVIC_Init(3, 3, USART3_IRQn, NVIC_PriorityGroup_0);	//?/中断分组0，最高优先级

	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);			  
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	//USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;	//设置波特率，一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;	//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;	//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;	//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	
	USART_Init(USART3, &USART_InitStructure); //初始化串口
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);//开启中断
	USART_Cmd(USART3, ENABLE);                    //使能串口 

	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	

}

/******************************************************************************
* Function Name --> 串口2接收中断服务程序
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
#if 0
void USART3_IRQHandler(void)
{

	/* 判断ORE位是否为SET状态 */
	if(USART_GetFlagStatus(USART3, USART_FLAG_ORE) != RESET)
	{
		/* 进行空读操作，目的是清除ORE位 */
		USART_ReceiveData(USART3);	
	}
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
		/* 将接收到的数据写入缓冲 */
		USART3_RX_BUF[USART3_iHead]  = USART_ReceiveData(USART3);	
		if((USART3_iHead += 1) >= USART3_REC_LEN) 
		{
			USART3_iHead -= USART3_REC_LEN;				
		}
		USART_ClearITPendingBit(USART3, USART_IT_RXNE);
	}	

}
#endif


/******************************************************************************
* Function Name --> 初始化IO 串口5
* Description   --> none
* Input         --> bound：波特率	
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void USART5_Init(uint32_t bound)    // BT_RXD(PC12)/BT_TXD(PD2)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	MY_NVIC_Init(3, 3, UART5_IRQn, NVIC_PriorityGroup_0);	//?/中断分组0，最高优先级

	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);			  
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	//USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;	//设置波特率，一般设置为9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;	//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;	//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;	//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
	
	USART_Init(UART5, &USART_InitStructure); //初始化串口
	USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);//开启中断
	USART_Cmd(UART5, ENABLE);                    //使能串口 

	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

/******************************************************************************
* Function Name --> 串口2接收中断服务程序
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void BtuartTimeDelayInc(void)
{
    if(btBuf.iTail != btBuf.iHead)
        BtuartTimeDelay++;
}

void UART5_IRQHandler(void)
{
	u8 data;
	
	/* 判断ORE位是否为SET状态 */
	//if(USART_GetFlagStatus(UART5, USART_FLAG_ORE) != RESET)
	//{
		/* 进行空读操作，目的是清除ORE位 */
	//	USART_ReceiveData(UART5);	
	//}
	if(USART_GetITStatus(UART5, USART_IT_RXNE) != RESET)
	{
		/* 将接收到的数据写入缓冲 */
        data = USART_ReceiveData(UART5);
        {
            btBuf.UartbtRxBuf[btBuf.iHead]  = data;	
            
            //if(btBuf.UartbtRxBuf[btBuf.iHead] == 0x0A && data == 0x0D)
            //{
            //	OSMboxPost(bt_UART_Mbox, (void *)0);
            //}
            //data = btBuf.UartbtRxBuf[btBuf.iHead];
            
            //if(TRUE == debug_bit.debug_status.BT_DEBUG_EN)
            //{
            //    USART1->DR = btBuf.UartbtRxBuf[btBuf.iHead];
            //}
            
            BtuartTimeDelay = 0;
            if((btBuf.iHead += 1) >= BT_UART_RX_LEN) 
            {
                btBuf.iHead-= BT_UART_RX_LEN;				
            }
        }
		USART_ClearITPendingBit(UART5, USART_IT_RXNE);
	}	

}

/******************************************************************************
* Function Name --> USART_Init
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void UartSendByte(u8 UartNum, u8 *pStr, u16 nLen)
{
	u16 bLen;
	u16 i;
	u8* pbuf;
	USART_TypeDef *USARTx = {0};

	bLen=nLen;
	pbuf=pStr;

	switch( UartNum)
	{
		case 1:
			USARTx =USART1;
		    	break;
		case 2:
			USARTx = USART2;
		    	break;
		case 3:
			USARTx = USART3;
		    	break;
		case 4:
			USARTx = UART4;
		    	break;
		case 5:
			USARTx = UART5;
		    	break;
		default:
		    	break;
	}
    
    if(UartNum == BT_UART)
    {
        bt_wakeup_dly();
    }
    
	for(i=0;i<bLen;i++)
	{
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
		//STM_RESET_DOG();
		USART_SendData(USARTx, *pbuf++);
	}

    if(UartNum == BT_UART)
    {
        bt_sleep_set();
    }
}

/******************************************************************************
* Function Name --> USART_Init
* Description   --> none
* Input         --> none
* Output        --> none
* Reaturn       --> none 
******************************************************************************/
void BSP_USART_Init(void)
{
	USART1_Init(115200);
	USART2_Init(115200);	
	USART3_Init(115200);
	USART5_Init(115200);
}



	

