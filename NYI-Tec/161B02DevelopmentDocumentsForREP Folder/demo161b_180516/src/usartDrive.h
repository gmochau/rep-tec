/******************************************************************************
* @ File name --> 
* @ Author    --> 
* @ Version   --> 
* @ Date      --> 
* @ Brief     --> 
*******************************************************************************
*
*                                  File Update
* @ Version   -->
* @ Author    -->
* @ Date      --> 
* @ Revise    --> 
*
******************************************************************************/

#ifndef _usart_h_
#define _usart_h_

#include "STM32_config.h"

/******************************************************************************
                               外部函数头文件                        
******************************************************************************/

/******************************************************************************
                               定义相关应用项
******************************************************************************/

#define USART1_REC_LEN					1024	//定义最大接收字节数
#define USART2_REC_LEN					1024
#define USART3_REC_LEN					1024

/******************************************************************************
                              外部调用功能函数
******************************************************************************/
extern uint8_t  USART1_RX_BUF[USART1_REC_LEN];	//接收缓冲，最大USART_REC_LEN个字节，末字节为换行符 
extern uint16_t USART1_iTail;
extern uint16_t USART1_iHead;
extern u16 USART1_atTail;

extern uint8_t  USART2_RX_BUF[USART2_REC_LEN];	//接收缓冲，最大USART_REC_LEN个字节，末字节为换行符 
extern uint16_t USART2_iTail;
extern uint16_t USART2_iHead;

extern uint8_t USART3_RX_BUF[USART3_REC_LEN];	//接收缓冲,最大USART_REC_LEN个字节
extern uint16_t USART3_iTail;
extern uint16_t USART3_iHead;

void UartSendByte(u8 UartNum, u8 *pStr, u16 nLen);
void USART1_Init(uint32_t bound);
void BSP_USART_Init(void);

extern u32 uartTimeDelay;

extern void uartTimeDelayInc(void);

extern void BtuartTimeDelayInc(void);

/*===========================================================================*/


#endif  /* enf usart.h */





