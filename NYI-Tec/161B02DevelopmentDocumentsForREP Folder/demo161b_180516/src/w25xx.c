/*
    w25xx.c
*/

#include "STM32_config.h"			

struct st_w25n w25n;

void SPI_FLASH_Init(void)
{  
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;

    /* Configure SPI2 pins: SCK, MISO and MOSI -*/
    GPIO_InitStructure.GPIO_Pin = FLASH_SPI_SCK  | FLASH_SPI_SDI;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin =  FLASH_SPI_SDO ;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

    /* Configure PA.4 as Output push-pull, used as Flash Chip select */
    GPIO_InitStructure.GPIO_Pin = SPI_CS;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(FLASH_GPIO_SPI, &GPIO_InitStructure);

    SPI_FLASH_CS_HIGH();
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

    // SPI2 时钟源72M/2=36M(不能超过54M),SPI硬件支持上至18M
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master; //设置为主SPI
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;//SPI_BaudRatePrescaler_128;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(sFLASH_SPI, &SPI_InitStructure);
    SPI_Cmd(sFLASH_SPI, ENABLE);

    //GPIO_PinRemapConfig(GPIO_Remap_SPI1,ENABLE);
}

u8 SPI_FLASH_SendByte(u8 byte)  //  通过SPI硬件发送/接收一个字节
{
    u8 cnt;

    for(cnt = 0; cnt < 20; cnt++)
    {
        if(SPI_I2S_GetFlagStatus(sFLASH_SPI, SPI_I2S_FLAG_TXE) != RESET) break;
    }
    SPI_I2S_SendData(sFLASH_SPI, byte);
    for(cnt = 0; cnt < 20; cnt++)
    {
        if(SPI_I2S_GetFlagStatus(sFLASH_SPI, SPI_I2S_FLAG_RXNE) != RESET) break;
    }
    return SPI_I2S_ReceiveData(sFLASH_SPI);         
}


u8 SPI_FLASH_Get_Byte(void)
{ 
    return (SPI_FLASH_SendByte(0xff));
}


static u8 w25xx_readStatus(void)
{
    u8 byte = 0;

    SPI_FLASH_CS_LOW();				
    SPI_FLASH_SendByte(0x05);		
    byte = SPI_FLASH_Get_Byte();	
    SPI_FLASH_CS_HIGH();			

    return byte;
}

static void w25xx_waitTmo(u8* str) 
{ 
    //printf("flash_tmo:%s\r\n",str);
}

static void w25xx_waitBusyClr(void)
{ 
    u8 status;
    u32 tmr;

    tmr = tick_get();
    while( tick_cmp(tmr, 400) == c_ret_nk )            
    {        
        STM_RESET_DOG();
        status = w25xx_readStatus();
        if((status & c_statusBit_busy) == 0)
        {
            return;
        }
    }
    w25xx_waitTmo("busyClr");
}

static void w25xx_waitWelSet(void)
{ 
    u8 status;
    u32 tmr;
    
    tmr = tick_get();
    while( tick_cmp(tmr, 50) == c_ret_nk )            
    {
        STM_RESET_DOG();
        status = w25xx_readStatus();
        if(     ((status & c_statusBit_wel ) != 0)
            &&  ((status & c_statusBit_busy) == 0) )
        {
            return;
        }
    }
    w25xx_waitTmo("welSet");
}

static void w25xx_waitWelClr(void)
{ 
    u8 status;
    u32 tmr;
    
    tmr = tick_get();
    while( tick_cmp(tmr, 400) == c_ret_nk )            
    {
        STM_RESET_DOG();
        status = w25xx_readStatus();
        if(     ((status & c_statusBit_wel ) == 0)
            &&  ((status & c_statusBit_busy) == 0) )
        {
            return;
        }
    }
    w25xx_waitTmo("welClr");
}

static void w25xx_writeEnable(void)
{
    SPI_FLASH_CS_LOW(); 
    SPI_FLASH_SendByte(0x06);
    SPI_FLASH_CS_HIGH();
    w25xx_waitWelSet();
}

static void w25xx_writeDisable(void)
{ 
    SPI_FLASH_CS_LOW();				
    SPI_FLASH_SendByte(0x04);		
    SPI_FLASH_CS_HIGH();		
    w25xx_waitWelClr();
}

void w25xx_erase(u32 addr)
{    
    un32 ux;

    w25xx_waitBusyClr();
    w25xx_writeEnable();              
    
    ux.r = addr;
    SPI_FLASH_CS_LOW();                    
    SPI_FLASH_SendByte(0x20);                   //  块擦除时间太长300ms~2sec，使用段擦除60ms~400ms   
    SPI_FLASH_SendByte(ux.b.h);
    SPI_FLASH_SendByte(ux.b.m);
    SPI_FLASH_SendByte(ux.b.l);
    SPI_FLASH_CS_HIGH();        

    w25xx_waitWelClr();
    w25xx_writeDisable();    
}

void w25xx_read(u32 addr,u8* buf,u16 len)
{ 
    un32 ux;
    u16 x;

    ux.r = addr;
           
    w25xx_waitBusyClr();    
    SPI_FLASH_CS_LOW();         	
    SPI_FLASH_SendByte(0x03); 		
    SPI_FLASH_SendByte(ux.b.h);
    SPI_FLASH_SendByte(ux.b.m);
    SPI_FLASH_SendByte(ux.b.l);
    for(x = 0; x < len; )                    //  可以连续读完整个芯片
    {
        buf[x] = SPI_FLASH_Get_Byte();
        x++;
    }
    SPI_FLASH_CS_HIGH();   
}

void w25xx_write(u32 addr, u8* buf, u16 len)
{   
    un32 ux;
    u16 x;
    u8 step = 0;
    
    ux.r = addr;    
    x = 0;
    while(1)
    {
        switch(step)
        {
            case 0:
                if( (ux.r & (4096 - 1)) == 0 )
                {
                    if(ux.r >= w25n.writeEndAddr)
                        ux.r = w25n.writeStartAddr;
                    w25xx_erase(ux.r);                    
                }
                step = 1;
            
            case 1:
                w25xx_waitBusyClr();
                w25xx_writeEnable();              
                
                SPI_FLASH_CS_LOW();                    
                SPI_FLASH_SendByte(0x02);            
                SPI_FLASH_SendByte(ux.b.h);
                SPI_FLASH_SendByte(ux.b.m);
                SPI_FLASH_SendByte(ux.b.l);
                for( ; x < len; )
                {
                    SPI_FLASH_SendByte(buf[x]); 
                    x++;
                    ux.r++;
                    if( (ux.r & (256 - 1)) == 0 )
                    {
                       if( (ux.r & (4096 - 1)) == 0 ) step = 0;
                       break;
                    }
                }
                SPI_FLASH_CS_HIGH();        
                w25xx_waitWelClr();
                w25xx_writeDisable();
            break;
            
            default:step = 0;
            break;    
        }
        if(x >= len) break;
    }                                                         
}

u32 SPI_FLASH_CheckID(void)
{
    u8 x, y;
    
    w25xx_waitBusyClr();
    w25xx_writeEnable();                  
    x = w25xx_readStatus();     //0x02
    
    w25xx_writeDisable();    
    y = w25xx_readStatus();     //0x00
    
    if( ((x & c_statusBit_wel) != 0) && ((y & c_statusBit_wel) == 0) )
        return c_ret_ok;
    else
        return c_ret_nk;
}

void NandFlashInit(void)
{
	SPI_FLASH_Init();
	SPI_FLASH_CheckID();
}

void w25xx_test(void)
{
    static u32 tmr = 0;
    static u32 addr = 0;
    static u8 len = 0;
    static u8 step = 0;
    u8 buf[13];
    
    switch(step)
    {
        case 0:
            if(tick_cmp(tmr,10000) == c_ret_ok)
            {
                tmr = tick_get();
                len = sprintf((char*)buf, "%d\r\n",addr);
                w25xx_write(addr, buf, len); 
                step = 1;        
            }
        break;

        case 1:
            if(tick_cmp(tmr,10000) == c_ret_ok)
            {
                tmr = tick_get();
                memset(buf, 0, sizeof(buf));
                w25xx_read(addr, buf, len); 
                printf("w25xx = %s", buf);
                addr += len;
                if(addr >= GpsData_ADDR_END)
                    addr = 0;
                step = 0;        
            }
        break;

        default: step = 0; break;
    }            
}

