/*
    w25xx.h    
*/
    
#ifndef __w25xx_h__
#define __w25xx_h__

#include "STM32_config.h"

#define FALSH_PAGESIZE 				0x1000			// 4K
#define FALSH_ERASE_PAGESIZE 		0x1000			// 4K
//-----------------------
//扇区 0/1/2 软件运行产生的参数
#define SOFTPARA_ADDROFFSET     	( ( (sizeof(SysRunParameter) - 1) / 128) * 128 + 128 ) //256        								
#define SOFTWAREPARA_ADDR   		0x000000    		     				
#define SOFTWAREPARA_ADDR_END		0x003000       //0x000000-0x000fff,0x001000-0x001fff,0x002000-0x002fff

//扇区 3-4 远程升级
#define ADDR_UPDATE       			0x003000       // 128K*2	系统升级
#define ADDR_UPDATE_END				0x043000	

//扇区 5-6 远程升级
#define ADDR_UPDATE_BACK       		0x043000	    // 128K*2	系统升级
#define ADDR_UPDATE_BACK_END	    0x083000		

//扇区 7/8 系统设置参数
#define SAVE_MAINPARA1   			0x083000								
#define SAVE_MAINPARA1_END			0x084000		// 0x083000-0x083fff

#define SAVE_MAINPARA2  		 	0x084000   	
#define SAVE_MAINPARA2_END			0x085000		// 0x084000-0x084fff

//扇区 9 DEBUG数据地址
#define LOG_ADDR  					0x085000	    // 
#define LOG_ADDR_END				0x086000		// 0x085000 - 085fff

//扇区10 -存储GPS数据
#define GpsData_ADDR      			0x086000   	    // 0x086000 - 0x200000 FALSH_ADD_END GPS数据存储开始的位置
#define GpsData_ADDR_END			0x200000	

struct st_w25n
{
    u8 readBuf[FLASH_PAGE_SIZE];
    u8 writeBuf[FLASH_PAGE_SIZE];
    u32 readAddr;
    u32 readAddrBk;
    u32 readStartAddr;
    u32 readEndAddr;
    u32 writeAddr;
    u32 writeStartAddr;
    u32 writeEndAddr;
};
extern struct st_w25n w25n;

//============================FLASH======================================

#define c_statusBit_busy        (1<<0)
#define c_statusBit_wel         (1<<1)

/******************************************************************************/
//FLASH 通讯SPI口
#define 	sFLASH_SPI			SPI2
#define	FLASH_GPIO_SPI		GPIOB
#define	FLASH_SPI_SCK		GPIO_Pin_13
#define	FLASH_SPI_SDO		GPIO_Pin_14
#define	FLASH_SPI_SDI		GPIO_Pin_15

//#define	GPIO_SPI			GPIOA
//#define	SPI_SCK				GPIO_Pin_5
//#define	SPI_SDO			GPIO_Pin_6
//#define	SPI_SDI				GPIO_Pin_7	    
#define	SPI_CS				GPIO_Pin_12

#define SPI_FLASH_CS_LOW()     GPIO_ResetBits(FLASH_GPIO_SPI, SPI_CS)
#define SPI_FLASH_CS_HIGH()    GPIO_SetBits(FLASH_GPIO_SPI, SPI_CS)

/******************************************************************************/

extern void w25xx_erase(u32 addr);

extern void w25xx_read(u32 addr,u8* buf,u16 len);

extern void w25xx_write(u32 addr, u8* buf, u16 len);

extern void NandFlashInit(void);

extern void w25xx_test(void);

#endif

